1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       144        9904       74402       80138      164588
      internal walks       594        2348        1368        1747        6057
valid internal walks       144        2144         912         906        4106
 lcore1,lcore2=             222777007             222813715
 lencor,maxblo             222822400                   766

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 222759102
 minimum size of srtscr:    131068 WP (     4 records)
 maximum size of srtscr:    262136 WP (     6 records)
========================================
 current settings:
 minbl3         351
 minbl4         351
 locmaxbl3     3220
 locmaxbuf     1610
 maxbl3       32768
 maxbl3       32768
 maxbl4       32768
 maxbuf       16390
========================================

 sorted 4-external integrals:     2 records of integral w-combinations 
                                  2 records of integral x-combinations of length= 16390
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     2 records of integral w-combinations 
                                  2 records of integral x-combinations of length= 16390
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        46
                             0ext.    :        72
                             2ext.    :       576
                             4ext.    :      1332


 Orig. off-diag. integrals:  4ext.    :     32655
                             3ext.    :     26688
                             2ext.    :      9948
                             1ext.    :      1872
                             0ext.    :       156
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       118


 Sorted integrals            3ext.  w :     25008 x :     23328
                             4ext.  w :     28525 x :     24943


 compressed index vector length=                  2002
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 0,
   gset =0,
  nROOT= 1,
  RTOLCI=  0.000100,  0.000100,
  VOUT=10,
  DAVCOR=10,
  CSFPRN=10,
  NITER=  16,
  IDEN=   1,
   IVMODE=   3,
   maxseg=10,
  NVCIMX = 8
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  16      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =  10
 iortls =    0      nvbkmx =   16      ibktv  =  -1      ibkthv =  -1
 nvcimx =    8      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    1      nvcimn =    1      maxseg =  10      nrfitr =  30
 ncorel =    0      nvrfmx =   16      nvrfmn =   1      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          222822399 DP per process
 Allocating              222822399  for integral sort step ...

********** Integral sort section *************


 workspace allocation information: lencor= 222822399

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
 /&end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      adler.itc.univie. 13:59:55.886 18-Apr-11
 cidrt_title                                                                     
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      adler.itc.univie. 13:59:56.816 18-Apr-11

 input energy(*) values:
 energy( 1)=  1.975806451613E+01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   1.975806451613E+01

 nsym = 8 nmot=  44

 symmetry  =    1    2    3    4    5    6    7    8
 slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
 nmpsy(*)  =   10    2    5    5    2   10    5    5

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   16390 maxbl3=   32768 maxbl4=   32768 intmxo=     766

 drt information:
 cidrt_title                                                                     
 nmotd =  46 nfctd =   2 nfvtc =   0 nmot  =  44
 nlevel =  44 niot  =   8 lowinl=  37
 orbital-to-level map(*)
   -1  37  38   1   2   3   4   5   6   7   8   9  10  39  11  12  13  14  40  15
   16  17  18  19  20  -1  41  42  21  22  23  24  25  26  27  28  43  29  30  31
   32  44  33  34  35  36
 compressed map(*)
   37  38   1   2   3   4   5   6   7   8   9  10  39  11  12  13  14  40  15  16
   17  18  19  20  41  42  21  22  23  24  25  26  27  28  43  29  30  31  32  44
   33  34  35  36
 levsym(*)
    1   1   1   1   1   1   1   1   2   2   3   3   3   3   4   4   4   4   5   5
    6   6   6   6   6   6   6   6   7   7   7   7   8   8   8   8   1   1   3   4
    6   6   7   8
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -9.914439617689E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1980
 number with all external indices:      1332
 number with half external - half internal indices:       576
 number with all internal indices:        72

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      32655 strt=          1
    3-external integrals: num=      26688 strt=      32656
    2-external integrals: num=       9948 strt=      59344
    1-external integrals: num=       1872 strt=      69292
    0-external integrals: num=        156 strt=      71164

 total number of off-diagonal integrals:       71319



 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 222701247
 !timer: setup required                  cpu_time=     0.003 walltime=     0.012
 pro2e        1     991    1981    2971    3961    3997    4033    5023   48711   92399
   125166  133358  138818  160657

 pro2e:     58437 integrals read in    11 records.
 pro1e        1     991    1981    2971    3961    3997    4033    5023   48711   92399
   125166  133358  138818  160657
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     5  records of                  32767 
 WP =               1310680 Bytes
 !timer: first half-sort required        cpu_time=     0.010 walltime=     0.012
 putdg        1     991    1981    2971    3737   36504   58349    5023   48711   92399
   125166  133358  138818  160657

 putf:       5 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     2 records of integral w-combinations and
             2 records of integral x-combinations of length= 16390 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    56 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      53468
 number of original 4-external integrals    32655


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     2 nrecx=     2 lbufp= 16390

 putd34:     2 records of integral w-combinations and
             2 records of integral x-combinations of length= 16390 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    44 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      48336
 number of original 3-external integrals    26688


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     2 nrecx=     2 lbufp= 16390

 putf:      18 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       cpu_time=     0.008 walltime=     0.008
 !timer: cisrt complete                  cpu_time=     0.021 walltime=     0.031
 deleting integral sort shared memory
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:    1332 2ext:     576 0ext:      72
 fil4w,fil4x  :   32655 fil3w,fil3x :   26688
 ofdgint  2ext:    9948 1ext:    1872 0ext:     156so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     351 minbl3     351 maxbl2     354nbas:   8   2   4   4   2   8   4   4 maxbuf 16390
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 222822399

 core energy values from the integral file:
 energy( 1)=  1.975806451613E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -9.914439617689E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -7.938633166076E+01
 nmot  =    46 niot  =     8 nfct  =     2 nfvt  =     0
 nrow  =    79 nsym  =     8 ssym  =     3 lenbuf=  1600
 nwalk,xbar:       6057      594     2348     1368     1747
 nvalwt,nvalw:     4106      144     2144      912      906
 ncsft:          164588
 total number of valid internal walks:    4106
 nvalz,nvaly,nvalx,nvalw =      144    2144     912     906

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld   1532
 nd4ext,nd2ext,nd0ext  1332   576    72
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    32655    26688     9948     1872      156        0        0        0
 minbl4,minbl3,maxbl2   351   351   354
 maxbuf 16390
 number of external orbitals per symmetry block:   8   2   4   4   2   8   4   4
 nmsym   8 number of internal orbitals   8
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     8                  2224
 block size     0
 pthz,pthy,pthx,pthw:   594  2348  1368  1747 total internal walks:    6057
 maxlp3,n2lp,n1lp,n0lp  2224     0     0     0
 orbsym(*)= 1 1 3 4 6 6 7 8

 setref:       59 references kept,
                0 references were marked as invalid, out of
               59 total.
 nmb.of records onel     1
 nmb.of records 2-ext    13
 nmb.of records 1-ext     3
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            33728
    threx             46125
    twoex              2384
    onex                927
    allin               766
    diagon             2123
               =======
   maximum            46125
 
  __ static summary __ 
   reflst               144
   hrfspc               144
               -------
   static->             144
 
  __ core required  __ 
   totstc               144
   max n-ex           46125
               -------
   totnec->           46269
 
  __ core available __ 
   totspc         222822399
   totnec -           46269
               -------
   totvec->       222776130

 number of external paths / symmetry
 vertex x      82      64      80      80      64     100      80      80
 vertex w     118      64      80      80      64     100      80      80
segment: free space=   222776130
 reducing frespc by                 15039 to              222761091 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         589|       144|         0|       144|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        2348|      9904|       144|      2144|       144|         2|
 -------------------------------------------------------------------------------
  X 3        1367|     74402|     10048|       912|      2288|         3|
 -------------------------------------------------------------------------------
  W 4        1743|     80138|     84450|       906|      3200|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        1192DP  conft+indsym=        8576DP  drtbuffer=        5271 DP

dimension of the ci-matrix ->>>    164588

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1    1367     589      74402        144     912     144
     2  4   1    25      two-ext wz   2X  4 1    1743     589      80138        144     906     144
     3  4   3    26      two-ext wx*  WX  4 3    1743    1367      80138      74402     906     912
     4  4   3    27      two-ext wx+  WX  4 3    1743    1367      80138      74402     906     912
     5  2   1    11      one-ext yz   1X  2 1    2348     589       9904        144    2144     144
     6  3   2    15      1ex3ex yx    3X  3 2    1367    2348      74402       9904     912    2144
     7  4   2    16      1ex3ex yw    3X  4 2    1743    2348      80138       9904     906    2144
     8  1   1     1      allint zz    OX  1 1     589     589        144        144     144     144
     9  2   2     5      0ex2ex yy    OX  2 2    2348    2348       9904       9904    2144    2144
    10  3   3     6      0ex2ex xx*   OX  3 3    1367    1367      74402      74402     912     912
    11  3   3    18      0ex2ex xx+   OX  3 3    1367    1367      74402      74402     912     912
    12  4   4     7      0ex2ex ww*   OX  4 4    1743    1743      80138      80138     906     906
    13  4   4    19      0ex2ex ww+   OX  4 4    1743    1743      80138      80138     906     906
    14  2   2    42      four-ext y   4X  2 2    2348    2348       9904       9904    2144    2144
    15  3   3    43      four-ext x   4X  3 3    1367    1367      74402      74402     912     912
    16  4   4    44      four-ext w   4X  4 4    1743    1743      80138      80138     906     906
    17  1   1    75      dg-024ext z  OX  1 1     589     589        144        144     144     144
    18  2   2    76      dg-024ext y  OX  2 2    2348    2348       9904       9904    2144    2144
    19  3   3    77      dg-024ext x  OX  3 3    1367    1367      74402      74402     912     912
    20  4   4    78      dg-024ext w  OX  4 4    1743    1743      80138      80138     906     906
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                164588

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        3772 2x:           0 4x:           0
All internal counts: zz :        3926 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:        3591
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:        2718    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension      59
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1        -108.6882585469
       2        -108.3330294933

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   144

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   144)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            164588
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    2.279061
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:           0 xx:           0 ww:           0
One-external counts: yz :       44283 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:        2896 wz:        2834 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:           0    task #     4:           0
task #     5:       40223    task #     6:           0    task #     7:           0    task #     8:        3591
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1
 sovl   1     1.00000000
 Final subspace hamiltonian 

                ht   1
   ht   1   -29.30192689
Spectrum of overlapmatrix:    1.000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -108.6882585469  2.8422E-14  3.4908E-01  1.0765E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0050
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0046
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0000
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0000
    5   11    0     0.05     0.05     0.00     0.05         0.    0.0402
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0000
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0000
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0036
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0000
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0000
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0000
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0000
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0000
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0000
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0000
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0000
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0378
   19   77    0     0.01     0.00     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.04     0.00     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1000s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1600s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -108.6882585469  2.8422E-14  3.4908E-01  1.0765E+00  1.0000E-04
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            164588
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             1
 number of iterations:                                   16
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.408665
ci vector #   2dasum_wr=    7.103131
ci vector #   3dasum_wr=    6.338675
ci vector #   4dasum_wr=    8.964179


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2
 sovl   1     1.00000000
 sovl   2     0.00000000     0.15583529
 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1   -29.30192689
   ht   2    -0.34907573    -4.18452152
Spectrum of overlapmatrix:    0.155835    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -3.098452E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1  -0.951522       0.307581    
 civs   2  -0.779159       -2.41038    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1  -0.951522       0.307581    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.95152202     0.30758064
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -108.9741011146  2.8584E-01  2.6275E-02  2.9798E-01  1.0000E-04
 mr-sdci #  1  2   -105.9526963170  2.6566E+01  0.0000E+00  1.8185E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0050
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0046
    3   26    0     0.16     0.01     0.00     0.16         0.    0.0506
    4   27    0     0.15     0.01     0.00     0.15         0.    0.0253
    5   11    0     0.06     0.04     0.01     0.05         0.    0.0402
    6   15    0     0.27     0.13     0.00     0.27         0.    0.2214
    7   16    0     0.27     0.12     0.00     0.27         0.    0.2096
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0036
    9    5    0     0.12     0.06     0.00     0.12         0.    0.1296
   10    6    0     0.14     0.02     0.00     0.14         0.    0.0369
   11   18    0     0.12     0.02     0.00     0.12         0.    0.0369
   12    7    0     0.10     0.01     0.00     0.10         0.    0.0223
   13   19    0     0.08     0.01     0.00     0.08         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.03     0.00     0.00     0.03         0.    0.0112
   16   44    0     0.04     0.00     0.00     0.04         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0378
   19   77    0     0.02     0.00     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.04     0.01     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.060000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.6000s 
time spent in multnx:                   1.5800s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.4900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.6600s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.313019
ci vector #   2dasum_wr=    2.086650
ci vector #   3dasum_wr=    2.991895
ci vector #   4dasum_wr=    3.645301


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3
 sovl   1     1.00000000
 sovl   2     0.00000000     0.15583529
 sovl   3    -0.02471508     0.00725087     0.01238954
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1   -29.30192689
   ht   2    -0.34907573    -4.18452152
   ht   3     0.72627214    -0.17471868    -0.32713668
Spectrum of overlapmatrix:    0.011408    0.156199    1.000618

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -3.098452E-14  -2.471508E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1  -0.930009      -0.418156       0.116692    
 civs   2  -0.821581        2.29503       0.814912    
 civs   3   0.699700       -4.43349        8.20104    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1  -0.947302      -0.308582      -8.599740E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.94730232    -0.30858184    -0.08599740
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -108.9925376363  1.8437E-02  4.9698E-03  1.1684E-01  1.0000E-04
 mr-sdci #  2  2   -106.0622800577  1.0958E-01  0.0000E+00  2.0317E+00  1.0000E-04
 mr-sdci #  2  3   -105.5780671619  2.6192E+01  0.0000E+00  1.9008E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0050
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0046
    3   26    0     0.15     0.01     0.00     0.15         0.    0.0506
    4   27    0     0.16     0.02     0.00     0.16         0.    0.0253
    5   11    0     0.05     0.04     0.00     0.05         0.    0.0402
    6   15    0     0.27     0.12     0.00     0.27         0.    0.2214
    7   16    0     0.28     0.14     0.00     0.28         0.    0.2096
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0036
    9    5    0     0.12     0.08     0.00     0.12         0.    0.1296
   10    6    0     0.15     0.02     0.00     0.14         0.    0.0369
   11   18    0     0.12     0.02     0.00     0.12         0.    0.0369
   12    7    0     0.10     0.01     0.00     0.10         0.    0.0223
   13   19    0     0.08     0.03     0.00     0.08         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.04     0.00     0.00     0.04         0.    0.0112
   16   44    0     0.03     0.00     0.00     0.03         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0378
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.05     0.00     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.060000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.6100s 
time spent in multnx:                   1.6000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.5700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.6800s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.192618
ci vector #   2dasum_wr=    0.830808
ci vector #   3dasum_wr=    1.305775
ci vector #   4dasum_wr=    1.410563


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     0.15583529
 sovl   3    -0.02471508     0.00725087     0.01238954
 sovl   4     0.01059565    -0.00142336    -0.00205458     0.00292220
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1   -29.30192689
   ht   2    -0.34907573    -4.18452152
   ht   3     0.72627214    -0.17471868    -0.32713668
   ht   4    -0.31090612     0.03899297     0.05373950    -0.07700073
Spectrum of overlapmatrix:    0.002465    0.011737    0.156214    1.000732

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -3.098452E-14  -2.471508E-02   1.059565E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.932037       0.300454       3.227590E-02  -0.334616    
 civs   2   0.828444       -1.84738       -1.34505       0.836495    
 civs   3  -0.812969        5.70653       -7.32504       -3.08192    
 civs   4  -0.607776        10.7786       -6.17045        15.4801    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.945690       0.273624       0.147935      -9.442385E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94568998     0.27362353     0.14793499    -0.09442385
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -108.9955596070  3.0220E-03  9.3074E-04  5.3791E-02  1.0000E-04
 mr-sdci #  3  2   -106.3052973968  2.4302E-01  0.0000E+00  2.0527E+00  1.0000E-04
 mr-sdci #  3  3   -105.5989616431  2.0894E-02  0.0000E+00  1.6725E+00  1.0000E-04
 mr-sdci #  3  4   -105.4571668294  2.6071E+01  0.0000E+00  2.2919E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0050
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0046
    3   26    0     0.16     0.02     0.00     0.16         0.    0.0506
    4   27    0     0.17     0.01     0.00     0.17         0.    0.0253
    5   11    0     0.04     0.04     0.00     0.04         0.    0.0402
    6   15    0     0.28     0.13     0.00     0.28         0.    0.2214
    7   16    0     0.42     0.18     0.00     0.42         0.    0.2096
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0036
    9    5    0     0.19     0.10     0.00     0.19         0.    0.1296
   10    6    0     0.20     0.04     0.00     0.20         0.    0.0369
   11   18    0     0.18     0.05     0.00     0.18         0.    0.0369
   12    7    0     0.15     0.04     0.00     0.15         0.    0.0223
   13   19    0     0.12     0.03     0.00     0.12         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.05     0.00     0.00     0.05         0.    0.0112
   16   44    0     0.04     0.00     0.00     0.04         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.03     0.03     0.00     0.03         0.    0.0378
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0147
   20   78    0     0.02     0.06     0.00     0.02         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.090000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     2.0900s 
time spent in multnx:                   2.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            2.1900s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.085365
ci vector #   2dasum_wr=    0.394663
ci vector #   3dasum_wr=    0.574223
ci vector #   4dasum_wr=    0.625520


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     0.15583529
 sovl   3    -0.02471508     0.00725087     0.01238954
 sovl   4     0.01059565    -0.00142336    -0.00205458     0.00292220
 sovl   5     0.00219721     0.00168041    -0.00042138     0.00028504     0.00045559
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.30192689
   ht   2    -0.34907573    -4.18452152
   ht   3     0.72627214    -0.17471868    -0.32713668
   ht   4    -0.31090612     0.03899297     0.05373950    -0.07700073
   ht   5    -0.06386604    -0.05110560     0.01246050    -0.00689953    -0.01206382
Spectrum of overlapmatrix:    0.000394    0.002482    0.011758    0.156232    1.000737

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -3.098452E-14  -2.471508E-02   1.059565E-02   2.197208E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.929150       0.272145       7.953106E-02  -0.332778       0.136547    
 civs   2   0.829975      -0.959888       -1.71480       0.830420       -1.33317    
 civs   3  -0.841426        4.36787       -5.99939       -2.96441        5.79381    
 civs   4  -0.760839        11.5217       -4.38220        15.5954        3.96484    
 civs   5   0.819609       -30.0817        5.17215       0.274520        39.7562    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.943685       0.220177       0.192739      -9.366544E-02   0.122715    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.94368525     0.22017669     0.19273852    -0.09366544     0.12271521
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -108.9963225727  7.6297E-04  1.7125E-04  2.3270E-02  1.0000E-04
 mr-sdci #  4  2   -107.2185189576  9.1322E-01  0.0000E+00  1.6206E+00  1.0000E-04
 mr-sdci #  4  3   -105.6233247857  2.4363E-02  0.0000E+00  1.6902E+00  1.0000E-04
 mr-sdci #  4  4   -105.4572261417  5.9312E-05  0.0000E+00  2.2928E+00  1.0000E-04
 mr-sdci #  4  5   -104.6447642475  2.5258E+01  0.0000E+00  2.1913E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0050
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0046
    3   26    0     0.24     0.03     0.00     0.24         0.    0.0506
    4   27    0     0.24     0.03     0.00     0.24         0.    0.0253
    5   11    0     0.07     0.06     0.00     0.07         0.    0.0402
    6   15    0     0.44     0.18     0.00     0.44         0.    0.2214
    7   16    0     0.40     0.17     0.00     0.40         0.    0.2096
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0036
    9    5    0     0.19     0.10     0.00     0.19         0.    0.1296
   10    6    0     0.20     0.03     0.00     0.20         0.    0.0369
   11   18    0     0.19     0.03     0.00     0.19         0.    0.0369
   12    7    0     0.15     0.03     0.00     0.15         0.    0.0223
   13   19    0     0.12     0.04     0.00     0.12         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.05     0.00     0.00     0.05         0.    0.0112
   16   44    0     0.05     0.00     0.00     0.05         0.    0.0112
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0027
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0378
   19   77    0     0.02     0.00     0.00     0.02         0.    0.0147
   20   78    0     0.01     0.06     0.01     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.090000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     2.4200s 
time spent in multnx:                   2.4200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.8000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            2.5300s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.036294
ci vector #   2dasum_wr=    0.185843
ci vector #   3dasum_wr=    0.252847
ci vector #   4dasum_wr=    0.300462


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     0.15583529
 sovl   3    -0.02471508     0.00725087     0.01238954
 sovl   4     0.01059565    -0.00142336    -0.00205458     0.00292220
 sovl   5     0.00219721     0.00168041    -0.00042138     0.00028504     0.00045559
 sovl   6     0.00126582     0.00053315    -0.00024478     0.00000056     0.00002070     0.00008193
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.30192689
   ht   2    -0.34907573    -4.18452152
   ht   3     0.72627214    -0.17471868    -0.32713668
   ht   4    -0.31090612     0.03899297     0.05373950    -0.07700073
   ht   5    -0.06386604    -0.05110560     0.01246050    -0.00689953    -0.01206382
   ht   6    -0.03692663    -0.01641026     0.00724258    -0.00001205    -0.00081989    -0.00220058
Spectrum of overlapmatrix:    0.000073    0.000395    0.002483    0.011763    0.156233    1.000738

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -3.098452E-14  -2.471508E-02   1.059565E-02   2.197208E-03   1.265817E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.926696      -0.308661      -3.843712E-02   0.283372      -0.215556       3.114967E-02
 civs   2  -0.830115       0.502565       -1.48931       -1.39961        1.36593      -0.519292    
 civs   3   0.847246       -2.88505       -3.19004       -2.54440       -8.92819       0.124795    
 civs   4   0.792377       -7.90820        8.26748       -16.6449       -2.26782        1.59951    
 civs   5  -0.989008        27.3370       -1.79895        4.69970       -15.9409        38.5869    
 civs   6  -0.921426        55.7650        41.0130       -19.4203       -63.6316       -67.3976    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.942580      -0.190496       0.175967       0.155637      -0.134496       4.448369E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.94257963    -0.19049574     0.17596670     0.15563704    -0.13449554     0.04448369
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -108.9964803744  1.5780E-04  4.1659E-05  1.1482E-02  1.0000E-04
 mr-sdci #  5  2   -107.8761044988  6.5759E-01  0.0000E+00  1.1011E+00  1.0000E-04
 mr-sdci #  5  3   -105.7587385196  1.3541E-01  0.0000E+00  2.0724E+00  1.0000E-04
 mr-sdci #  5  4   -105.5415601915  8.4334E-02  0.0000E+00  1.8441E+00  1.0000E-04
 mr-sdci #  5  5   -104.8738643358  2.2910E-01  0.0000E+00  2.1075E+00  1.0000E-04
 mr-sdci #  5  6   -104.4238986748  2.5038E+01  0.0000E+00  2.4607E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0050
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0046
    3   26    0     0.23     0.02     0.00     0.23         0.    0.0506
    4   27    0     0.23     0.02     0.00     0.23         0.    0.0253
    5   11    0     0.07     0.06     0.00     0.07         0.    0.0402
    6   15    0     0.41     0.18     0.00     0.41         0.    0.2214
    7   16    0     0.41     0.19     0.00     0.41         0.    0.2096
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0036
    9    5    0     0.19     0.11     0.00     0.19         0.    0.1296
   10    6    0     0.22     0.04     0.01     0.21         0.    0.0369
   11   18    0     0.20     0.05     0.00     0.20         0.    0.0369
   12    7    0     0.15     0.03     0.00     0.15         0.    0.0223
   13   19    0     0.12     0.03     0.00     0.12         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.05     0.00     0.00     0.05         0.    0.0112
   16   44    0     0.05     0.00     0.00     0.05         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.03     0.03     0.00     0.03         0.    0.0378
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0147
   20   78    0     0.02     0.06     0.00     0.02         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009999
time for cinew                         0.090000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     2.4100s 
time spent in multnx:                   2.4000s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.8400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            2.5100s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.021521
ci vector #   2dasum_wr=    0.094425
ci vector #   3dasum_wr=    0.128331
ci vector #   4dasum_wr=    0.148328


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     0.15583529
 sovl   3    -0.02471508     0.00725087     0.01238954
 sovl   4     0.01059565    -0.00142336    -0.00205458     0.00292220
 sovl   5     0.00219721     0.00168041    -0.00042138     0.00028504     0.00045559
 sovl   6     0.00126582     0.00053315    -0.00024478     0.00000056     0.00002070     0.00008193
 sovl   7    -0.00055653     0.00004654     0.00009487    -0.00000599    -0.00001470    -0.00000687     0.00001935
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.30192689
   ht   2    -0.34907573    -4.18452152
   ht   3     0.72627214    -0.17471868    -0.32713668
   ht   4    -0.31090612     0.03899297     0.05373950    -0.07700073
   ht   5    -0.06386604    -0.05110560     0.01246050    -0.00689953    -0.01206382
   ht   6    -0.03692663    -0.01641026     0.00724258    -0.00001205    -0.00081989    -0.00220058
   ht   7     0.01632162    -0.00120285    -0.00280958     0.00017626     0.00043480     0.00024837    -0.00051313
Spectrum of overlapmatrix:    0.000018    0.000073    0.000395    0.002483    0.011763    0.156233    1.000739

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000      -3.098452E-14  -2.471508E-02   1.059565E-02   2.197208E-03   1.265817E-03  -5.565259E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.925614      -0.312622       5.725196E-02  -0.280329      -0.172338      -0.142164      -6.167876E-02
 civs   2  -0.830062       0.283955       -1.37577        1.43767      -9.589312E-03    1.46060       0.642254    
 civs   3   0.848545       -2.09284        1.20900        2.72753       -8.58019       -4.27228       -1.11129    
 civs   4   0.799435       -5.82363        9.81541        16.4179        2.60450       -3.32176      -0.418599    
 civs   5   -1.02712        22.1106       -16.0007       -4.81264        3.96206       -41.1915        9.07246    
 civs   6   -1.12888        59.9221        31.2593        18.5942       -26.5756       -1.61866       -91.2774    
 civs   7   0.852962       -76.9326       -126.547       -1.85302        84.7551       -57.2311       -154.048    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.942275      -0.155355       0.206210      -0.159788      -4.784518E-03  -0.132475      -4.852290E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.94227531    -0.15535515     0.20621043    -0.15978793    -0.00478452    -0.13247540    -0.04852290
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -108.9965159082  3.5534E-05  1.3538E-05  6.5667E-03  1.0000E-04
 mr-sdci #  6  2   -108.1682812619  2.9218E-01  0.0000E+00  8.6628E-01  1.0000E-04
 mr-sdci #  6  3   -106.3917761298  6.3304E-01  0.0000E+00  2.0054E+00  1.0000E-04
 mr-sdci #  6  4   -105.5417605790  2.0039E-04  0.0000E+00  1.8235E+00  1.0000E-04
 mr-sdci #  6  5   -105.2046766340  3.3081E-01  0.0000E+00  2.1363E+00  1.0000E-04
 mr-sdci #  6  6   -104.6147890031  1.9089E-01  0.0000E+00  2.2058E+00  1.0000E-04
 mr-sdci #  6  7   -103.7661782127  2.4380E+01  0.0000E+00  2.5016E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0050
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0046
    3   26    0     0.24     0.03     0.00     0.24         0.    0.0506
    4   27    0     0.24     0.02     0.01     0.23         0.    0.0253
    5   11    0     0.07     0.06     0.00     0.07         0.    0.0402
    6   15    0     0.40     0.19     0.00     0.40         0.    0.2214
    7   16    0     0.41     0.18     0.01     0.40         0.    0.2096
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0036
    9    5    0     0.14     0.09     0.00     0.14         0.    0.1296
   10    6    0     0.20     0.04     0.00     0.20         0.    0.0369
   11   18    0     0.18     0.04     0.00     0.18         0.    0.0369
   12    7    0     0.15     0.03     0.00     0.15         0.    0.0223
   13   19    0     0.12     0.03     0.00     0.12         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.05     0.00     0.00     0.05         0.    0.0112
   16   44    0     0.06     0.00     0.00     0.06         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0378
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0147
   20   78    0     0.02     0.04     0.00     0.02         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.089999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     2.3400s 
time spent in multnx:                   2.3200s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.8000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            2.4500s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.011651
ci vector #   2dasum_wr=    0.056101
ci vector #   3dasum_wr=    0.067382
ci vector #   4dasum_wr=    0.078139


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     0.15583529
 sovl   3    -0.02471508     0.00725087     0.01238954
 sovl   4     0.01059565    -0.00142336    -0.00205458     0.00292220
 sovl   5     0.00219721     0.00168041    -0.00042138     0.00028504     0.00045559
 sovl   6     0.00126582     0.00053315    -0.00024478     0.00000056     0.00002070     0.00008193
 sovl   7    -0.00055653     0.00004654     0.00009487    -0.00000599    -0.00001470    -0.00000687     0.00001935
 sovl   8    -0.00005812     0.00009626     0.00001050     0.00000234    -0.00000262    -0.00000357     0.00000152     0.00000601
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -29.30192689
   ht   2    -0.34907573    -4.18452152
   ht   3     0.72627214    -0.17471868    -0.32713668
   ht   4    -0.31090612     0.03899297     0.05373950    -0.07700073
   ht   5    -0.06386604    -0.05110560     0.01246050    -0.00689953    -0.01206382
   ht   6    -0.03692663    -0.01641026     0.00724258    -0.00001205    -0.00081989    -0.00220058
   ht   7     0.01632162    -0.00120285    -0.00280958     0.00017626     0.00043480     0.00024837    -0.00051313
   ht   8     0.00173262    -0.00286427    -0.00031177    -0.00006905     0.00007760     0.00010583    -0.00006097    -0.00015901
Spectrum of overlapmatrix:    0.000006    0.000018    0.000073    0.000395    0.002483    0.011763    0.156233    1.000739

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -3.098452E-14  -2.471508E-02   1.059565E-02   2.197208E-03   1.265817E-03  -5.565259E-04  -5.811728E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.925208      -0.267877      -0.190440       0.278812       7.040732E-02  -0.186389       9.183181E-02  -1.351959E-02
 civs   2   0.830010       0.131195       0.953424      -0.395803       -1.49800        1.71859      -0.667702       7.690593E-03
 civs   3  -0.849024       -1.40552       -2.63801        1.07061       -6.90567       -5.98674        3.46407       0.986119    
 civs   4  -0.802076       -4.11702       -7.90463       -17.1404       -5.88870       -2.84969       -1.30809      -0.582055    
 civs   5    1.04144        16.6485        21.5977        5.41846        3.45902       -36.7927       -20.4761        3.60406    
 civs   6    1.20690        53.1410        11.5785       -31.0217       -4.06358       -25.6380        88.7492       -43.0732    
 civs   7   -1.17383       -101.920        93.8805        4.84689        29.0854       -37.9944       -18.2502       -186.907    
 civs   8  -0.987410       -122.954        208.679       -98.5791        92.2481       -61.8213        203.290        234.018    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.942220      -0.109048      -0.211260       4.640839E-02   0.159595      -0.157177       5.804907E-02  -2.445863E-04

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.94222029    -0.10904847    -0.21125966     0.04640839     0.15959537    -0.15717678     0.05804907    -0.00024459

 trial vector basis is being transformed.  new dimension:   1
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -108.9965292760  1.3368E-05  4.2699E-06  3.7649E-03  1.0000E-04
 mr-sdci #  7  2   -108.3699416137  2.0166E-01  0.0000E+00  7.3837E-01  1.0000E-04
 mr-sdci #  7  3   -107.2006663040  8.0889E-01  0.0000E+00  1.4489E+00  1.0000E-04
 mr-sdci #  7  4   -105.6143505344  7.2590E-02  0.0000E+00  2.0976E+00  1.0000E-04
 mr-sdci #  7  5   -105.4586764672  2.5400E-01  0.0000E+00  1.6941E+00  1.0000E-04
 mr-sdci #  7  6   -104.6375270095  2.2738E-02  0.0000E+00  2.1308E+00  1.0000E-04
 mr-sdci #  7  7   -104.3712580799  6.0508E-01  0.0000E+00  2.3744E+00  1.0000E-04
 mr-sdci #  7  8   -103.2126417463  2.3826E+01  0.0000E+00  2.4112E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0050
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0046
    3   26    0     0.25     0.02     0.00     0.25         0.    0.0506
    4   27    0     0.23     0.02     0.00     0.23         0.    0.0253
    5   11    0     0.08     0.05     0.00     0.08         0.    0.0402
    6   15    0     0.39     0.18     0.01     0.39         0.    0.2214
    7   16    0     0.29     0.15     0.00     0.29         0.    0.2096
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0036
    9    5    0     0.12     0.06     0.01     0.12         0.    0.1296
   10    6    0     0.14     0.03     0.00     0.14         0.    0.0369
   11   18    0     0.12     0.03     0.00     0.12         0.    0.0369
   12    7    0     0.11     0.03     0.00     0.11         0.    0.0223
   13   19    0     0.08     0.02     0.00     0.08         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.04     0.00     0.00     0.04         0.    0.0112
   16   44    0     0.03     0.00     0.00     0.03         0.    0.0112
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0027
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0378
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.04     0.00     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.070001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.9500s 
time spent in multnx:                   1.9500s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.6700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            2.0400s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.005372
ci vector #   2dasum_wr=    0.031863
ci vector #   3dasum_wr=    0.042910
ci vector #   4dasum_wr=    0.049098


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2
 sovl   1     1.00000000
 sovl   2     0.00016043     0.00000192
 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1   -29.61019762
   ht   2    -0.00475470    -0.00005151
Spectrum of overlapmatrix:    0.000002    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1   0.942220       1.762405E-04

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1  -0.999873       0.117573    
 civs   2  -0.787486       -726.090    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1  -0.942239      -1.718698E-02

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.94223948    -0.01718698
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -108.9965326384  3.3625E-06  1.5023E-06  2.0235E-03  1.0000E-04
 mr-sdci #  8  2   -106.1379327528 -2.2320E+00  0.0000E+00  2.6678E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0050
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0046
    3   26    0     0.16     0.01     0.00     0.16         0.    0.0506
    4   27    0     0.15     0.01     0.00     0.15         0.    0.0253
    5   11    0     0.05     0.05     0.00     0.05         0.    0.0402
    6   15    0     0.27     0.11     0.00     0.27         0.    0.2214
    7   16    0     0.28     0.13     0.00     0.28         0.    0.2096
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0036
    9    5    0     0.14     0.07     0.00     0.13         0.    0.1296
   10    6    0     0.14     0.02     0.00     0.14         0.    0.0369
   11   18    0     0.12     0.02     0.00     0.12         0.    0.0369
   12    7    0     0.10     0.01     0.00     0.10         0.    0.0223
   13   19    0     0.08     0.03     0.00     0.08         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.05     0.00     0.01     0.04         0.    0.0112
   16   44    0     0.03     0.00     0.00     0.03         0.    0.0112
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0027
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0378
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.03     0.00     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.050001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.6200s 
time spent in multnx:                   1.6000s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.5200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.6700s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.004098
ci vector #   2dasum_wr=    0.017930
ci vector #   3dasum_wr=    0.018314
ci vector #   4dasum_wr=    0.022642


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3
 sovl   1     1.00000000
 sovl   2     0.00016043     0.00000192
 sovl   3     0.00001371    -0.00000013     0.00000077
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1   -29.61019762
   ht   2    -0.00475470    -0.00005151
   ht   3    -0.00040598     0.00000590    -0.00002073
Spectrum of overlapmatrix:    0.000001    0.000002    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1   0.942220       1.762405E-04   2.941207E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1  -0.999823      -6.625138E-02  -0.101870    
 civs   2   -1.18402        468.580        560.818    
 civs   3    1.12700       -792.249        831.818    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1  -0.942230      -3.142376E-03   2.732054E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.94222951    -0.00314238     0.02732054
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -108.9965343316  1.6931E-06  6.6385E-07  1.4661E-03  1.0000E-04
 mr-sdci #  9  2   -107.9644424681  1.8265E+00  0.0000E+00  1.5486E+00  1.0000E-04
 mr-sdci #  9  3   -104.1244209393 -3.0762E+00  0.0000E+00  2.3971E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0050
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0046
    3   26    0     0.16     0.02     0.00     0.16         0.    0.0506
    4   27    0     0.16     0.02     0.00     0.16         0.    0.0253
    5   11    0     0.04     0.03     0.00     0.04         0.    0.0402
    6   15    0     0.28     0.13     0.00     0.27         0.    0.2214
    7   16    0     0.27     0.13     0.00     0.27         0.    0.2096
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0036
    9    5    0     0.13     0.08     0.00     0.13         0.    0.1296
   10    6    0     0.14     0.02     0.00     0.14         0.    0.0369
   11   18    0     0.12     0.02     0.00     0.12         0.    0.0369
   12    7    0     0.10     0.02     0.00     0.10         0.    0.0223
   13   19    0     0.09     0.02     0.01     0.08         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.04     0.00     0.00     0.04         0.    0.0112
   16   44    0     0.03     0.00     0.00     0.03         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0378
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.04     0.00     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.049999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.6200s 
time spent in multnx:                   1.6000s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.5700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.6800s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.002555
ci vector #   2dasum_wr=    0.011961
ci vector #   3dasum_wr=    0.017673
ci vector #   4dasum_wr=    0.019650


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00016043     0.00000192
 sovl   3     0.00001371    -0.00000013     0.00000077
 sovl   4    -0.00007685    -0.00000023     0.00000010     0.00000031
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1   -29.61019762
   ht   2    -0.00475470    -0.00005151
   ht   3    -0.00040598     0.00000590    -0.00002073
   ht   4     0.00227547     0.00000691    -0.00000357    -0.00000833
Spectrum of overlapmatrix:    0.000000    0.000001    0.000002    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1   0.942220       1.762405E-04   2.941207E-05  -6.744813E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.999731      -9.789985E-02  -4.230469E-03   0.137908    
 civs   2   -1.33185        345.399       -633.081       -238.800    
 civs   3    1.54716       -709.229       -118.770       -921.645    
 civs   4   0.950850       -646.362       -1406.31        1129.09    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.942220      -8.633922E-03  -2.420105E-02  -1.540902E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.94222020    -0.00863392    -0.02420105    -0.01540902
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -108.9965349628  6.3123E-07  1.9534E-07  8.2665E-04  1.0000E-04
 mr-sdci # 10  2   -108.4158075149  4.5137E-01  0.0000E+00  7.9426E-01  1.0000E-04
 mr-sdci # 10  3   -104.8094506271  6.8503E-01  0.0000E+00  2.3024E+00  1.0000E-04
 mr-sdci # 10  4   -103.6972488542 -1.9171E+00  0.0000E+00  2.4384E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0050
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0046
    3   26    0     0.16     0.01     0.00     0.16         0.    0.0506
    4   27    0     0.15     0.01     0.00     0.15         0.    0.0253
    5   11    0     0.05     0.05     0.00     0.05         0.    0.0402
    6   15    0     0.28     0.11     0.00     0.28         0.    0.2214
    7   16    0     0.28     0.14     0.00     0.28         0.    0.2096
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0036
    9    5    0     0.12     0.06     0.00     0.12         0.    0.1296
   10    6    0     0.16     0.03     0.00     0.16         0.    0.0369
   11   18    0     0.12     0.02     0.00     0.12         0.    0.0369
   12    7    0     0.10     0.01     0.00     0.10         0.    0.0223
   13   19    0     0.08     0.03     0.00     0.08         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.04     0.00     0.00     0.04         0.    0.0112
   16   44    0     0.03     0.00     0.00     0.03         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.02     0.01     0.00     0.02         0.    0.0378
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.04     0.00     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.059999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.6300s 
time spent in multnx:                   1.6300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.5300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.7000s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001136
ci vector #   2dasum_wr=    0.006716
ci vector #   3dasum_wr=    0.009377
ci vector #   4dasum_wr=    0.011165


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00016043     0.00000192
 sovl   3     0.00001371    -0.00000013     0.00000077
 sovl   4    -0.00007685    -0.00000023     0.00000010     0.00000031
 sovl   5    -0.00001885    -0.00000008     0.00000004     0.00000001     0.00000008
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.61019762
   ht   2    -0.00475470    -0.00005151
   ht   3    -0.00040598     0.00000590    -0.00002073
   ht   4     0.00227547     0.00000691    -0.00000357    -0.00000833
   ht   5     0.00055806     0.00000235    -0.00000127    -0.00000062    -0.00000215
Spectrum of overlapmatrix:    0.000000    0.000000    0.000001    0.000002    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.942220       1.762405E-04   2.941207E-05  -6.744813E-05   3.450237E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.999687      -0.110347      -5.898717E-02   5.956367E-02  -0.109453    
 civs   2   -1.37366        295.084       -432.706       -541.041        166.585    
 civs   3    1.66599       -634.373        458.867       -546.525        698.274    
 civs   4    1.21978       -716.996       -888.788       -817.414       -1303.14    
 civs   5   0.913953       -733.338       -2886.48        1463.01        1438.74    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.942198      -2.479342E-02  -6.835497E-02   4.875082E-03   3.962573E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.94219788    -0.02479342    -0.06835497     0.00487508     0.03962573
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -108.9965351413  1.7853E-07  5.5126E-08  3.8926E-04  1.0000E-04
 mr-sdci # 11  2   -108.5437649546  1.2796E-01  0.0000E+00  4.8263E-01  1.0000E-04
 mr-sdci # 11  3   -105.8489956266  1.0395E+00  0.0000E+00  2.3846E+00  1.0000E-04
 mr-sdci # 11  4   -104.4901362343  7.9289E-01  0.0000E+00  2.3162E+00  1.0000E-04
 mr-sdci # 11  5   -103.3874929446 -2.0712E+00  0.0000E+00  2.4348E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0050
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0046
    3   26    0     0.16     0.01     0.00     0.16         0.    0.0506
    4   27    0     0.16     0.01     0.00     0.16         0.    0.0253
    5   11    0     0.04     0.03     0.00     0.04         0.    0.0402
    6   15    0     0.28     0.14     0.00     0.28         0.    0.2214
    7   16    0     0.27     0.12     0.00     0.27         0.    0.2096
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0036
    9    5    0     0.12     0.06     0.00     0.12         0.    0.1296
   10    6    0     0.14     0.03     0.00     0.14         0.    0.0369
   11   18    0     0.12     0.04     0.00     0.12         0.    0.0369
   12    7    0     0.11     0.02     0.00     0.11         0.    0.0223
   13   19    0     0.10     0.03     0.00     0.10         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.03     0.00     0.00     0.03         0.    0.0112
   16   44    0     0.04     0.00     0.01     0.03         0.    0.0112
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0027
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0378
   19   77    0     0.01     0.00     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.04     0.00     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.060001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.6300s 
time spent in multnx:                   1.6200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.5700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.7000s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000636
ci vector #   2dasum_wr=    0.003382
ci vector #   3dasum_wr=    0.004304
ci vector #   4dasum_wr=    0.004913


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00016043     0.00000192
 sovl   3     0.00001371    -0.00000013     0.00000077
 sovl   4    -0.00007685    -0.00000023     0.00000010     0.00000031
 sovl   5    -0.00001885    -0.00000008     0.00000004     0.00000001     0.00000008
 sovl   6    -0.00001926    -0.00000001     0.00000001     0.00000001     0.00000000     0.00000003
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.61019762
   ht   2    -0.00475470    -0.00005151
   ht   3    -0.00040598     0.00000590    -0.00002073
   ht   4     0.00227547     0.00000691    -0.00000357    -0.00000833
   ht   5     0.00055806     0.00000235    -0.00000127    -0.00000062    -0.00000215
   ht   6     0.00057040     0.00000032    -0.00000029    -0.00000036    -0.00000015    -0.00000082
Spectrum of overlapmatrix:    0.000000    0.000000    0.000000    0.000001    0.000002    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.942220       1.762405E-04   2.941207E-05  -6.744813E-05   3.450237E-06  -3.990515E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.999661      -0.126483       9.004118E-02   2.223113E-02  -5.158132E-02  -0.115060    
 civs   2   -1.38393        270.127        289.952       -520.386        372.552        167.908    
 civs   3    1.69518       -590.978       -440.349        82.9184        674.811        629.119    
 civs   4    1.28584       -712.930        186.929       -1296.93        174.620       -1203.81    
 civs   5    1.13848       -917.395        1967.94       -1155.58       -1983.40        1765.98    
 civs   6   0.795597       -848.315        4138.40        3155.44        2167.00       -1045.70    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.942181      -4.064323E-02   0.100656       2.568623E-03   9.637030E-03   3.114477E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.94218107    -0.04064323     0.10065593     0.00256862     0.00963703     0.03114477
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -108.9965351852  4.3858E-08  1.7734E-08  2.2432E-04  1.0000E-04
 mr-sdci # 12  2   -108.5889330607  4.5168E-02  0.0000E+00  4.0158E-01  1.0000E-04
 mr-sdci # 12  3   -107.1023095140  1.2533E+00  0.0000E+00  1.7558E+00  1.0000E-04
 mr-sdci # 12  4   -104.8906264077  4.0049E-01  0.0000E+00  2.1657E+00  1.0000E-04
 mr-sdci # 12  5   -104.3369696564  9.4948E-01  0.0000E+00  2.4445E+00  1.0000E-04
 mr-sdci # 12  6   -103.3204840522 -1.3170E+00  0.0000E+00  2.4633E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0050
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0046
    3   26    0     0.16     0.02     0.00     0.16         0.    0.0506
    4   27    0     0.16     0.02     0.00     0.16         0.    0.0253
    5   11    0     0.05     0.04     0.00     0.05         0.    0.0402
    6   15    0     0.27     0.13     0.00     0.27         0.    0.2214
    7   16    0     0.27     0.12     0.00     0.27         0.    0.2096
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0036
    9    5    0     0.12     0.06     0.00     0.12         0.    0.1296
   10    6    0     0.14     0.03     0.01     0.14         0.    0.0369
   11   18    0     0.12     0.04     0.00     0.12         0.    0.0369
   12    7    0     0.11     0.03     0.00     0.11         0.    0.0223
   13   19    0     0.08     0.01     0.00     0.08         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.03     0.00     0.00     0.03         0.    0.0112
   16   44    0     0.04     0.00     0.00     0.04         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0378
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0147
   20   78    0     0.01     0.05     0.00     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.059999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.6100s 
time spent in multnx:                   1.6100s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.5900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.6800s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000380
ci vector #   2dasum_wr=    0.001710
ci vector #   3dasum_wr=    0.002651
ci vector #   4dasum_wr=    0.002874


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00016043     0.00000192
 sovl   3     0.00001371    -0.00000013     0.00000077
 sovl   4    -0.00007685    -0.00000023     0.00000010     0.00000031
 sovl   5    -0.00001885    -0.00000008     0.00000004     0.00000001     0.00000008
 sovl   6    -0.00001926    -0.00000001     0.00000001     0.00000001     0.00000000     0.00000003
 sovl   7     0.00003109     0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.61019762
   ht   2    -0.00475470    -0.00005151
   ht   3    -0.00040598     0.00000590    -0.00002073
   ht   4     0.00227547     0.00000691    -0.00000357    -0.00000833
   ht   5     0.00055806     0.00000235    -0.00000127    -0.00000062    -0.00000215
   ht   6     0.00057040     0.00000032    -0.00000029    -0.00000036    -0.00000015    -0.00000082
   ht   7    -0.00092049    -0.00000072    -0.00000004     0.00000005    -0.00000011     0.00000009    -0.00000029

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   8.879135E-09   3.026769E-08   7.654938E-08   2.643185E-07   7.642260E-07   1.949782E-06    1.00000    
 
   x:   1  -2.786994E-05   2.102696E-05  -1.231236E-05   5.851293E-05   2.515544E-05   1.676241E-04    1.00000    
   x:   2  -1.433724E-02   3.298476E-04  -3.726259E-02   0.117717      -0.149901      -0.980856       1.604326E-04
   x:   3   6.879787E-04  -7.093895E-03   5.602511E-02  -0.164632      -0.976470       0.127332       1.371092E-05
   x:   4  -1.589220E-02  -3.357215E-02  -3.584125E-03   0.978489      -0.146663       0.140204      -7.684755E-05
   x:   5  -7.530206E-02  -2.497931E-02  -0.994539      -1.920366E-02  -4.797099E-02   4.390125E-02  -1.884706E-05
   x:   6   0.159481       0.985843      -3.655816E-02   3.338033E-02  -1.405226E-02   5.542896E-03  -1.926349E-05
   x:   7   0.984092      -0.162209      -7.081683E-02   1.075279E-02  -5.263142E-03  -9.653927E-03   3.108687E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.942220       1.762405E-04   2.941207E-05  -6.744813E-05   3.450237E-06  -3.990515E-06   3.814124E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.999675      -0.105511       7.275366E-02  -0.296378      -0.167019      -1.291302E-02  -2.406832E-02
 civs   2   -1.38681        259.127       -264.987        136.058       -444.107       -441.640        261.525    
 civs   3    1.70339       -572.913        392.154        303.179        384.294       -569.972        595.610    
 civs   4    1.30441       -708.711       -58.9242        268.803       -1384.31       -457.806       -1011.18    
 civs   5    1.20157       -977.353       -1505.92       -575.256       -1707.44        1742.96        2019.56    
 civs   6    1.01916       -1119.04       -3923.72       -2537.58        2626.00       -1920.98       -1150.19    
 civs   7   0.694939       -819.007       -4610.05        7436.08        4679.27        1693.55       -2832.29    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.942170      -5.293976E-02  -0.128014       2.727473E-02   3.113686E-02   2.385894E-03   1.266469E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.94217021    -0.05293976    -0.12801405     0.02727473     0.03113686     0.00238589     0.01266469
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -108.9965351975  1.2324E-08  3.6634E-09  1.0776E-04  1.0000E-04
 mr-sdci # 13  2   -108.6034405528  1.4507E-02  0.0000E+00  3.5989E-01  1.0000E-04
 mr-sdci # 13  3   -107.5838448945  4.8154E-01  0.0000E+00  1.1772E+00  1.0000E-04
 mr-sdci # 13  4   -105.3985023416  5.0788E-01  0.0000E+00  2.2734E+00  1.0000E-04
 mr-sdci # 13  5   -104.6866622994  3.4969E-01  0.0000E+00  2.1342E+00  1.0000E-04
 mr-sdci # 13  6   -104.3119378087  9.9145E-01  0.0000E+00  2.4618E+00  1.0000E-04
 mr-sdci # 13  7   -103.1620785690 -1.2092E+00  0.0000E+00  2.3615E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0050
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0046
    3   26    0     0.16     0.01     0.00     0.16         0.    0.0506
    4   27    0     0.16     0.02     0.00     0.16         0.    0.0253
    5   11    0     0.05     0.04     0.00     0.05         0.    0.0402
    6   15    0     0.27     0.12     0.00     0.27         0.    0.2214
    7   16    0     0.28     0.14     0.00     0.28         0.    0.2096
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0036
    9    5    0     0.21     0.12     0.00     0.21         0.    0.1296
   10    6    0     0.21     0.04     0.00     0.21         0.    0.0369
   11   18    0     0.18     0.04     0.00     0.18         0.    0.0369
   12    7    0     0.15     0.03     0.00     0.15         0.    0.0223
   13   19    0     0.13     0.03     0.00     0.13         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.05     0.00     0.00     0.05         0.    0.0112
   16   44    0     0.05     0.00     0.00     0.05         0.    0.0112
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0027
   18   76    0     0.03     0.03     0.00     0.03         0.    0.0378
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0147
   20   78    0     0.02     0.06     0.00     0.02         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.019999
time for cinew                         0.080002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.9800s 
time spent in multnx:                   1.9800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            2.0800s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000136
ci vector #   2dasum_wr=    0.000834
ci vector #   3dasum_wr=    0.001313
ci vector #   4dasum_wr=    0.001492


====================================================================================================
Diagonal     counts:  0x:       92200 2x:       24385 4x:        3962
All internal counts: zz :        3926 yy:       83348 xx:       48788 ww:       44294
One-external counts: yz :       44283 yx:      270491 yw:      256982
Two-external counts: yy :       95415 ww:       35820 xx:       53174 xz:        2896 wz:        2834 wx:       69662
Three-ext.   counts: yx :       39498 yw:       39314

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:       14338
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00016043     0.00000192
 sovl   3     0.00001371    -0.00000013     0.00000077
 sovl   4    -0.00007685    -0.00000023     0.00000010     0.00000031
 sovl   5    -0.00001885    -0.00000008     0.00000004     0.00000001     0.00000008
 sovl   6    -0.00001926    -0.00000001     0.00000001     0.00000001     0.00000000     0.00000003
 sovl   7     0.00003109     0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
 sovl   8    -0.00000692     0.00000000     0.00000000     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -29.61019762
   ht   2    -0.00475470    -0.00005151
   ht   3    -0.00040598     0.00000590    -0.00002073
   ht   4     0.00227547     0.00000691    -0.00000357    -0.00000833
   ht   5     0.00055806     0.00000235    -0.00000127    -0.00000062    -0.00000215
   ht   6     0.00057040     0.00000032    -0.00000029    -0.00000036    -0.00000015    -0.00000082
   ht   7    -0.00092049    -0.00000072    -0.00000004     0.00000005    -0.00000011     0.00000009    -0.00000029
   ht   8     0.00020501     0.00000013    -0.00000002    -0.00000017    -0.00000006    -0.00000004     0.00000004    -0.00000005

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.469028E-09   9.052262E-09   3.030919E-08   7.658622E-08   2.643911E-07   7.642280E-07   1.949791E-06    1.00000    
 
   x:   1   2.854661E-07  -2.818498E-05   2.126029E-05   1.244651E-05   5.861937E-05   2.514427E-05  -1.676393E-04   -1.00000    
   x:   2  -3.027675E-03  -1.401848E-02   2.979196E-04   3.724212E-02   0.117743      -0.149908       0.980852      -1.604326E-04
   x:   3   2.467392E-03   2.737318E-04  -6.969479E-03  -5.597539E-02  -0.164663      -0.976465      -0.127333      -1.371092E-05
   x:   4  -1.781348E-02  -1.334836E-02  -3.423680E-02   3.053910E-03   0.978341      -0.146676      -0.140209       7.684755E-05
   x:   5  -3.202637E-02  -7.096142E-02  -2.616219E-02   0.994315      -1.905500E-02  -4.797438E-02  -4.390315E-02   1.884706E-05
   x:   6  -1.409823E-02   0.165514       0.984729       3.698613E-02   3.344477E-02  -1.405479E-02  -5.544204E-03   1.926349E-05
   x:   7   0.153083       0.971817      -0.164177       7.034382E-02   1.067673E-02  -5.260856E-03   9.655162E-03  -3.108687E-05
   x:   8   0.987425      -0.150886       3.806463E-02   2.218152E-02   1.662632E-02  -1.606805E-03  -2.203708E-03   6.923588E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.942220       1.762405E-04   2.941207E-05  -6.744813E-05   3.450237E-06  -3.990515E-06   3.814124E-05  -8.524623E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.999673      -0.105886       5.797651E-02  -0.187428      -0.257773      -0.120544      -4.600823E-02  -2.900377E-02
 civs   2   -1.38749        248.079       -267.940       -58.6173        165.893       -323.850       -553.966        212.781    
 civs   3    1.70530       -551.911        424.735        186.045        256.161        463.014       -465.193        623.821    
 civs   4    1.30874       -695.605        23.4426       -336.104        391.684       -1105.13       -785.868       -1158.98    
 civs   5    1.21629       -994.530       -1209.22        344.272       -611.180       -2697.19        1006.10        1720.80    
 civs   6    1.07134       -1241.13       -3482.91        1964.97       -3075.69        2201.46       -1403.24       -1170.39    
 civs   7   0.857121       -1203.46       -5064.00        2310.94        6995.15        5012.65        3257.42       -2033.42    
 civs   8   0.785120       -1758.23       -7144.48       -20369.0        4430.49        11949.0        4038.31        4305.05    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.942169      -5.475357E-02  -0.104201       9.633904E-02   6.674220E-03  -1.125987E-02  -2.769923E-03   3.043120E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94216923    -0.05475357    -0.10420096     0.09633904     0.00667422    -0.01125987    -0.00276992     0.00304312

 trial vector basis is being transformed.  new dimension:   1
 NCSF=                   144                  9904                 74402
                 80138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -108.9965352004  2.8762E-09  3.4730E-10  3.3522E-05  1.0000E-04
 mr-sdci # 14  2   -108.6160121726  1.2572E-02  0.0000E+00  3.0700E-01  1.0000E-04
 mr-sdci # 14  3   -107.7752929107  1.9145E-01  0.0000E+00  8.3058E-01  1.0000E-04
 mr-sdci # 14  4   -105.5803904248  1.8189E-01  0.0000E+00  1.9464E+00  1.0000E-04
 mr-sdci # 14  5   -105.3893352706  7.0267E-01  0.0000E+00  2.2254E+00  1.0000E-04
 mr-sdci # 14  6   -104.3728185462  6.0881E-02  0.0000E+00  2.2916E+00  1.0000E-04
 mr-sdci # 14  7   -104.3058767917  1.1438E+00  0.0000E+00  2.3877E+00  1.0000E-04
 mr-sdci # 14  8   -103.1049105924 -1.0773E-01  0.0000E+00  2.3446E+00  1.0000E-04
 
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0050
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0046
    3   26    0     0.24     0.02     0.00     0.24         0.    0.0506
    4   27    0     0.23     0.02     0.00     0.23         0.    0.0253
    5   11    0     0.07     0.06     0.00     0.07         0.    0.0402
    6   15    0     0.42     0.18     0.01     0.41         0.    0.2214
    7   16    0     0.41     0.20     0.00     0.41         0.    0.2096
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0036
    9    5    0     0.19     0.10     0.00     0.19         0.    0.1296
   10    6    0     0.20     0.03     0.00     0.20         0.    0.0369
   11   18    0     0.18     0.03     0.00     0.17         0.    0.0369
   12    7    0     0.16     0.03     0.00     0.16         0.    0.0223
   13   19    0     0.12     0.03     0.00     0.12         0.    0.0223
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0112
   15   43    0     0.05     0.00     0.00     0.05         0.    0.0112
   16   44    0     0.05     0.00     0.00     0.05         0.    0.0112
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0027
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0378
   19   77    0     0.02     0.01     0.00     0.02         0.    0.0147
   20   78    0     0.01     0.04     0.01     0.01         0.    0.0020
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.090000
time for eigenvalue solver             0.010000
time for vector access                 0.000000
================================================================
time spent in mult:                     2.4000s 
time spent in multnx:                   2.3800s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.8000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            2.5200s 

 mr-sdci  convergence criteria satisfied after 14 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -108.9965352004  2.8762E-09  3.4730E-10  3.3522E-05  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -108.996535200391

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   2 expansion vectors are transformed.
    1 of the   1 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.94217)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -108.9965352004

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8

                                          orbital     2    3   14   19   27   28   37   42

                                         symmetry   ag   ag  b2g  b3g  b1u  b1u  b2u  b3u 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.055516                        +-   +-   +-   +-   +          -      
 z*  1  1       5 -0.163990                        +-   +-   +-        +    +     -    - 
 z*  1  1       6 -0.016397                        +-   +-   +-             +-        +- 
 z*  1  1       9  0.116884                        +-   +-   +     -   +         +-    - 
 z*  1  1      10  0.010412                        +-   +-   +     -        +    +-    - 
 z*  1  1      13 -0.033580                        +-   +-   +    +     -        +-    - 
 z*  1  1      19  0.030202                        +-   +-             +-   +-        +- 
 z*  1  1      21 -0.107199                        +-   +    +-    -   +-        +-      
 z*  1  1      25 -0.057653                        +-   +    +-   +    +-    -         - 
 z*  1  1      26  0.010435                        +-   +    +-   +     -    -   +-      
 z*  1  1      31  0.018406                        +-   +     -   +-   +-   +          - 
 z*  1  1      32  0.170255                        +-   +     -   +-   +-        +     - 
 z*  1  1      37  0.053929                        +-   +     -   +-        +     -   +- 
 z*  1  1      38  0.011026                        +-   +     -   +-             +-   +- 
 z*  1  1      39  0.010541                        +-   +     -        +-   +-   +     - 
 z*  1  1      40 -0.887941                        +-   +     -        +-   +     -   +- 
 z*  1  1      41 -0.048767                        +-   +     -        +    +-    -   +- 
 z*  1  1      42  0.015164                        +-   +     -        +     -   +-   +- 
 z*  1  1      44 -0.016817                        +-   +    +    +-    -    -   +     - 
 z*  1  1      45  0.062823                        +-   +    +          -   +-    -   +- 
 z*  1  1      52  0.022355                        +-        +-        +-   +-        +- 
 z*  1  1      54 -0.015703                        +-        +     -   +-   +    +-    - 
 z*  1  1      56  0.021639                        +-        +    +    +-    -   +-    - 
 z   1  1      64 -0.025981                        +    +-   +-   +    +-    -         - 
 z   1  1      67  0.011226                        +    +-   +-   +     -         -   +- 
 z   1  1      71  0.011535                        +    +-    -   +-   +-        +     - 
 z   1  1      79 -0.069242                        +    +-    -        +-   +     -   +- 
 z   1  1      84  0.010111                        +    +-   +          -   +-    -   +- 
 z   1  1      95  0.027718                        +     -   +    +    +-    -   +-    - 
 z   1  1     103 -0.022594                        +    +    +-        +-   +-    -    - 
 z   1  1     105  0.012584                        +    +     -    -   +-   +    +-    - 
 z   1  1     107  0.033207                        +    +     -   +    +-    -   +-    - 
 z   1  1     117 -0.011099                        +          -   +-   +-   +     -   +- 
 z   1  1     138  0.010309                             +     -   +-   +-   +     -   +- 
 z   1  1     141  0.014434                             +     -   +-        +-   +-   +- 
 y   1  1     171 -0.011025              1(b3g )   +-   +-   +-    -   +               - 
 y   1  1     273  0.011088              1(b2u )   +-   +-   +-        +          -    - 
 y   1  1     303  0.011051              1(b1u )   +-   +-   +-                  +-    - 
 y   1  1     485  0.010307              1(b1g )   +-   +-    -        +-        +     - 
 y   1  1     523  0.016262              1(b2g )   +-   +-    -        +         +-    - 
 y   1  1     524 -0.013717              2(b2g )   +-   +-    -        +         +-    - 
 y   1  1     527 -0.012923              1(b3g )   +-   +-    -        +          -   +- 
 y   1  1     528  0.012933              2(b3g )   +-   +-    -        +          -   +- 
 y   1  1     558  0.026550              2( ag )   +-   +-    -                  +-   +- 
 y   1  1     641 -0.011033              1(b3u )   +-   +-   +     -   +          -    - 
 y   1  1    1031  0.015729              3(b1u )   +-   +-             +-        +-    - 
 y   1  1    1037 -0.015671              1( au )   +-   +-             +-         -   +- 
 y   1  1    1177  0.018126              1(b1u )   +-    -   +-   +    +          -    - 
 y   1  1    1235 -0.024463              1(b2g )   +-    -   +-        +-        +-      
 y   1  1    1239  0.018697              1(b3g )   +-    -   +-        +-        +     - 
 y   1  1    1274 -0.019134              2( ag )   +-    -   +-        +         +-    - 
 y   1  1    1275  0.016062              3( ag )   +-    -   +-        +         +-    - 
 y   1  1    1281 -0.018290              1(b1g )   +-    -   +-        +          -   +- 
 y   1  1    1321 -0.016502              1(b3u )   +-    -   +    +-   +-              - 
 y   1  1    1351  0.014331              1(b1u )   +-    -   +    +-    -             +- 
 y   1  1    1427 -0.021076              1(b3g )   +-    -   +     -   +-             +- 
 y   1  1    1457  0.011709              1(b1g )   +-    -   +     -   +         +-    - 
 y   1  1    1460  0.013731              2( ag )   +-    -   +     -   +          -   +- 
 y   1  1    1461  0.015819              3( ag )   +-    -   +     -   +          -   +- 
 y   1  1    1536 -0.012594              2( ag )   +-    -   +    +     -         -   +- 
 y   1  1    1589  0.070440              1(b3u )   +-    -   +         +-        +-    - 
 y   1  1    1590 -0.026464              2(b3u )   +-    -   +         +-        +-    - 
 y   1  1    1591  0.019176              3(b3u )   +-    -   +         +-        +-    - 
 y   1  1    1593 -0.044301              1(b2u )   +-    -   +         +-         -   +- 
 y   1  1    1595 -0.010728              3(b2u )   +-    -   +         +-         -   +- 
 y   1  1    1623  0.029407              1(b1u )   +-    -   +          -        +-   +- 
 y   1  1    1625 -0.017899              3(b1u )   +-    -   +          -        +-   +- 
 y   1  1    1633  0.013508              1(b3u )   +-    -   +         +     -   +-    - 
 y   1  1    1637 -0.012797              1(b2u )   +-    -   +         +     -    -   +- 
 y   1  1    1777 -0.022463              1(b3u )   +-    -        +    +-         -   +- 
 y   1  1    1778  0.025529              2(b3u )   +-    -        +    +-         -   +- 
 y   1  1    1801 -0.017408              1( au )   +-    -        +     -        +-   +- 
 y   1  1    1851 -0.038282              1(b2g )   +-    -             +-        +-   +- 
 y   1  1    1853 -0.020499              3(b2g )   +-    -             +-        +-   +- 
 y   1  1    1865 -0.015419              1(b2g )   +-    -             +     -   +-   +- 
 y   1  1    2019 -0.014795              1(b3g )   +-   +    +-        +-         -    - 
 y   1  1    2050  0.013823              2( ag )   +-   +    +-         -        +-    - 
 y   1  1    2057 -0.011500              1(b1g )   +-   +    +-         -         -   +- 
 y   1  1    2119 -0.012459              1(b1u )   +-   +     -   +-    -             +- 
 y   1  1    2191  0.022112              1(b2g )   +-   +     -    -   +-        +     - 
 y   1  1    2195 -0.035893              1(b3g )   +-   +     -    -   +-             +- 
 y   1  1    2225  0.020544              1(b1g )   +-   +     -    -   +         +-    - 
 y   1  1    2227  0.011538              1( ag )   +-   +     -    -   +          -   +- 
 y   1  1    2228  0.019123              2( ag )   +-   +     -    -   +          -   +- 
 y   1  1    2229  0.020579              3( ag )   +-   +     -    -   +          -   +- 
 y   1  1    2230 -0.015957              4( ag )   +-   +     -    -   +          -   +- 
 y   1  1    2257 -0.011665              1(b3g )   +-   +     -    -             +-   +- 
 y   1  1    2271  0.016802              1(b2g )   +-   +     -   +    +-         -    - 
 y   1  1    2301  0.015465              1(b1g )   +-   +     -   +     -        +-    - 
 y   1  1    2304 -0.017940              2( ag )   +-   +     -   +     -         -   +- 
 y   1  1    2305  0.017325              3( ag )   +-   +     -   +     -         -   +- 
 y   1  1    2357  0.057384              1(b3u )   +-   +     -        +-        +-    - 
 y   1  1    2358 -0.015636              2(b3u )   +-   +     -        +-        +-    - 
 y   1  1    2359  0.014252              3(b3u )   +-   +     -        +-        +-    - 
 y   1  1    2361 -0.074749              1(b2u )   +-   +     -        +-         -   +- 
 y   1  1    2362  0.018122              2(b2u )   +-   +     -        +-         -   +- 
 y   1  1    2363 -0.015225              3(b2u )   +-   +     -        +-         -   +- 
 y   1  1    2391 -0.073101              1(b1u )   +-   +     -         -        +-   +- 
 y   1  1    2392 -0.015231              2(b1u )   +-   +     -         -        +-   +- 
 y   1  1    2394 -0.012691              4(b1u )   +-   +     -         -        +-   +- 
 y   1  1    2401  0.019803              1(b3u )   +-   +     -        +     -   +-    - 
 y   1  1    2405 -0.019315              1(b2u )   +-   +     -        +     -    -   +- 
 y   1  1    2621  0.011127              1(b3u )   +-   +          -   +-         -   +- 
 y   1  1    2703  0.010452              3( ag )   +-   +              +-    -   +-    - 
 y   1  1    2709 -0.010441              1(b1g )   +-   +              +-    -    -   +- 
 y   1  1    2997 -0.018067              1(b1u )   +-        +-        +-        +-    - 
 y   1  1    2999  0.010199              3(b1u )   +-        +-        +-        +-    - 
 y   1  1    3005 -0.012798              1( au )   +-        +-        +-         -   +- 
 y   1  1    3193  0.038913              1(b1u )   +-         -   +    +-         -   +- 
 y   1  1    3224 -0.016871              2(b2u )   +-         -   +     -        +-   +- 
 y   1  1    3281  0.024188              1( ag )   +-         -        +-        +-   +- 
 y   1  1    3282 -0.025892              2( ag )   +-         -        +-        +-   +- 
 y   1  1    3283 -0.015013              3( ag )   +-         -        +-        +-   +- 
 y   1  1    3284 -0.042300              4( ag )   +-         -        +-        +-   +- 
 y   1  1    3285 -0.016534              5( ag )   +-         -        +-        +-   +- 
 y   1  1    3286 -0.011061              6( ag )   +-         -        +-        +-   +- 
 y   1  1    3401  0.012457              3(b1u )   +-        +     -   +-         -   +- 
 y   1  1    3430  0.018098              2(b2u )   +-        +     -    -        +-   +- 
 y   1  1    3615 -0.014528              1(b1g )   +-              -   +-        +-   +- 
 y   1  1    4427 -0.016375              1(b2g )    -   +-             +-        +-   +- 
 y   1  1    4721 -0.013688              1(b1u )    -   +    +-        +-        +-    - 
 y   1  1    4722  0.011159              2(b1u )    -   +    +-        +-        +-    - 
 y   1  1    4917  0.021715              1(b1u )    -   +     -   +    +-         -   +- 
 y   1  1    4918 -0.014266              2(b1u )    -   +     -   +    +-         -   +- 
 y   1  1    5005  0.028514              1( ag )    -   +     -        +-        +-   +- 
 y   1  1    5008 -0.015889              4( ag )    -   +     -        +-        +-   +- 
 y   1  1    5009 -0.010516              5( ag )    -   +     -        +-        +-   +- 
 y   1  1    6337  0.011980              1(b3u )   +    +-    -        +-        +-    - 
 y   1  1    6338  0.019164              2(b3u )   +    +-    -        +-        +-    - 
 y   1  1    6341 -0.014672              1(b2u )   +    +-    -        +-         -   +- 
 y   1  1    6342 -0.018596              2(b2u )   +    +-    -        +-         -   +- 
 y   1  1    6981  0.010137              1( au )   +     -   +-        +-         -   +- 
 y   1  1    7169  0.011804              1(b1u )   +     -    -   +    +-         -   +- 
 y   1  1    7257 -0.014892              1( ag )   +     -    -        +-        +-   +- 
 y   1  1    7259  0.010088              3( ag )   +     -    -        +-        +-   +- 
 y   1  1    7260  0.017215              4( ag )   +     -    -        +-        +-   +- 
 y   1  1    7377 -0.010102              3(b1u )   +     -   +     -   +-         -   +- 
 y   1  1    7461 -0.012064              1(b3g )   +     -   +         +-    -    -   +- 
 y   1  1    7641 -0.013496              1(b3u )   +     -             +-    -   +-   +- 
 y   1  1    7853 -0.014009              1( au )   +    +     -    -   +-        +-    - 
 y   1  1    7855 -0.015734              1(b1u )   +    +     -    -   +-         -   +- 
 y   1  1    7857 -0.014035              3(b1u )   +    +     -    -   +-         -   +- 
 y   1  1    7937  0.013181              1(b2g )   +    +     -        +-    -   +-    - 
 y   1  1    7941 -0.013512              1(b3g )   +    +     -        +-    -    -   +- 
 y   1  1    8393  0.011253              1(b1u )   +          -        +-    -   +-   +- 
 y   1  1    9825  0.016024              1(b1u )        +     -        +-    -   +-   +- 
 x   1  1   25971 -0.014565    3( ag )   1(b1g )   +-    -   +         +-         -    - 
 x   1  1   31227 -0.011756    3( ag )   1( au )   +-    -             +-         -   +- 
 x   1  1   31245  0.011732    1(b1g )   3(b1u )   +-    -             +-         -   +- 
 x   1  1   31257 -0.012636    1(b2g )   1(b2u )   +-    -             +-         -   +- 
 x   1  1   35623 -0.020297    3( ag )   1(b1g )   +-   +     -        +-         -    - 
 x   1  1   35669  0.011242    1(b2u )   1(b3u )   +-   +     -        +-         -    - 
 x   1  1   45421  0.010569    1(b2g )   1(b1u )   +-         -        +-        +-    - 
 x   1  1   45461  0.011021    1( ag )   1(b3u )   +-         -        +-        +-    - 
 x   1  1   45501 -0.011044    1(b3g )   1(b1u )   +-         -        +-         -   +- 
 x   1  1   45533 -0.011836    1( ag )   1(b2u )   +-         -        +-         -   +- 
 w   1  1   98028  0.013228    3( ag )   3( ag )   +-   +     -        +-        +-      
 w   1  1   98059  0.012866    1(b1g )   1(b1g )   +-   +     -        +-        +-      
 w   1  1   98062  0.013262    1(b2g )   1(b2g )   +-   +     -        +-        +-      
 w   1  1   98131  0.016612    1(b3u )   1(b3u )   +-   +     -        +-        +-      
 w   1  1   98133  0.013788    2(b3u )   2(b3u )   +-   +     -        +-        +-      
 w   1  1   98141  0.011255    1( ag )   1(b1g )   +-   +     -        +-        +     - 
 w   1  1   98157 -0.010540    1(b2g )   1(b3g )   +-   +     -        +-        +     - 
 w   1  1   98189 -0.012549    1(b2u )   1(b3u )   +-   +     -        +-        +     - 
 w   1  1   98194 -0.010998    2(b2u )   2(b3u )   +-   +     -        +-        +     - 
 w   1  1   98210  0.011321    3( ag )   3( ag )   +-   +     -        +-             +- 
 w   1  1   98241  0.011690    1(b1g )   1(b1g )   +-   +     -        +-             +- 
 w   1  1   98254  0.010503    1(b3g )   1(b3g )   +-   +     -        +-             +- 
 w   1  1   98303  0.012165    1(b2u )   1(b2u )   +-   +     -        +-             +- 
 w   1  1   98305  0.011645    2(b2u )   2(b2u )   +-   +     -        +-             +- 
 w   1  1   98848  0.011459    2( ag )   1(b2g )   +-   +     -        +         +-    - 
 w   1  1   98896 -0.011238    2(b1u )   1(b3u )   +-   +     -        +         +-    - 
 w   1  1   98936 -0.013567    2( ag )   1(b3g )   +-   +     -        +          -   +- 
 w   1  1   98968  0.011644    2(b1u )   1(b2u )   +-   +     -        +          -   +- 
 w   1  1   99469  0.013968    2( ag )   2( ag )   +-   +     -                  +-   +- 
 w   1  1   99529  0.017352    1(b1u )   1(b1u )   +-   +     -                  +-   +- 
 w   1  1  107865 -0.013631    1(b1g )   1( au )   +-   +              +-        +-    - 
 w   1  1  107887 -0.013640    3( ag )   3(b1u )   +-   +              +-        +-    - 
 w   1  1  107949 -0.025559    1(b2g )   1(b3u )   +-   +              +-        +-    - 
 w   1  1  107997  0.011325    1(b2g )   1(b2u )   +-   +              +-         -   +- 
 w   1  1  108013  0.010572    1(b3g )   1(b3u )   +-   +              +-         -   +- 
 w   1  1  108482 -0.011825    2( ag )   1(b3u )   +-   +               -        +-   +- 
 w   1  1  117301  0.010454    1(b2g )   1(b1u )   +-        +         +-        +-    - 
 w   1  1  117381 -0.011069    1(b3g )   1(b1u )   +-        +         +-         -   +- 
 w   1  1  117882  0.023893    2( ag )   1(b1u )   +-        +          -        +-   +- 
 w   1  1  121899  0.012460    1( ag )   1(b2g )   +-                  +-        +-   +- 
 w   1  1  121900  0.015044    2( ag )   1(b2g )   +-                  +-        +-   +- 
 w   1  1  121941  0.010233    1( au )   2(b2u )   +-                  +-        +-   +- 
 w   1  1  121947 -0.020186    1(b1u )   1(b3u )   +-                  +-        +-   +- 
 w   1  1  121955  0.011655    1(b1u )   2(b3u )   +-                  +-        +-   +- 
 w   1  1  121957  0.010208    3(b1u )   2(b3u )   +-                  +-        +-   +- 
 w   1  1  145370  0.010491    2( ag )   1(b3u )   +    +     -        +-        +-    - 
 w   1  1  145442 -0.010260    2( ag )   1(b2u )   +    +     -        +-         -   +- 
 w   1  1  162291  0.012518    1(b1u )   1(b1u )        +     -        +-        +-   +- 

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01             189
     0.01> rq > 0.001           3942
    0.001> rq > 0.0001         27886
   0.0001> rq > 0.00001        66785
  0.00001> rq > 0.000001       51802
 0.000001> rq                  13979
           all                164588
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        3772 2x:           0 4x:           0
All internal counts: zz :        3926 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        5043    task #     2:        4581    task #     3:       50579    task #     4:       25289
task #     5:       40223    task #     6:      221405    task #     7:      209587    task #     8:        3591
task #     9:      129614    task #    10:       36883    task #    11:       36883    task #    12:       22343
task #    13:       22343    task #    14:       11171    task #    15:       11173    task #    16:       11173
task #    17:        2718    task #    18:       37804    task #    19:       14749    task #    20:        2025
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.055516360235      6.053479669054
     2     2     -0.005886499041      0.642314008865
     3     3     -0.002603400649      0.280919135898
     4     4      0.001815748148     -0.196939571146
     5     5     -0.163990122387     17.840897811108
     6     6     -0.016397397162      1.782192953870
     7     7     -0.006144407255      0.667200045904
     8     8      0.001743610829     -0.189310462963
     9     9      0.116883515255    -12.715498944148
    10    10      0.010412193215     -1.131504433984
    11    11      0.003282819797     -0.356207930577
    12    12     -0.000793136395      0.086135838702
    13    13     -0.033580081993      3.651327113111
    14    14     -0.006445879193      0.702462123841
    15    15      0.006090019317     -0.663072382832
    16    16     -0.000655025461      0.071318437010
    17    17      0.001350090228     -0.146087870057
    18    18     -0.001588719449      0.173030943460
    19    19      0.030201894355     -3.285038680594
    20    20     -0.002602537837      0.283235945300
    21    21     -0.107198620038     11.659069007324
    22    22     -0.004072792537      0.441085174641
    23    23      0.009224831959     -1.007676965989
    24    24      0.002345853938     -0.255586012862
    25    25     -0.057653211487      6.273228390472
    26    26      0.010435050685     -1.135620022571
    27    27     -0.007569875131      0.823650920014
    28    28      0.009376520791     -1.019607139160
    29    29      0.002195858081     -0.239036495898
    30    30     -0.002167057100      0.234662163548
    31    31      0.018405930657     -1.999636891286
    32    32      0.170254700983    -18.514770757099
    33    33      0.001548402825     -0.167836625038
    34    34      0.007274293800     -0.787992695709
    35    35     -0.001106608506      0.120200609715
    36    36     -0.004432980089      0.482598582700
    37    37      0.053928628942     -5.862915820285
    38    38      0.011025830136     -1.194766105654
    39    39      0.010540584731     -1.141874828039
    40    40     -0.887941035141     96.497751241805
    41    41     -0.048766956965      5.288925439958
    42    42      0.015164101634     -1.651288821055
    43    43     -0.002060518540      0.224092985462
    44    44     -0.016816944259      1.830034794201
    45    45      0.062823476785     -6.828563006635
    46    46     -0.000353561186      0.038519667158
    47    47      0.001371009755     -0.148843256301
    48    48      0.009825151028     -1.069548139594
    49    49     -0.002028079160      0.220589473743
    50    50     -0.002004971560      0.219675959197
    51    51     -0.001913392865      0.209103835977
    52    52      0.022355424168     -2.430668590772
    53    53     -0.003692357603      0.400803296162
    54    54     -0.015703446415      1.707144288148
    55    55      0.002701161173     -0.293243349381
    56    56      0.021638710791     -2.350035599634
    57    57     -0.000863909016      0.093480686596
    58    58      0.000117705183     -0.013175623621
    59    59      0.000115490617     -0.012603014339

 number of reference csfs (nref) is    59.  root number (iroot) is  1.
 c0**2 =   0.89059754  c**2 (all zwalks) =   0.90001503

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =   -108.686199278648   "relaxed" cnot**2         =   0.890597540371
 eci       =   -108.996535200391   deltae = eci - eref       =  -0.310335921742
 eci+dv1   =   -109.030486713541   dv1 = (1-cnot**2)*deltae  =  -0.033951513150
 eci+dv2   =   -109.034657372989   dv2 = dv1 / cnot**2       =  -0.038122172598
 eci+dv3   =   -109.039996192385   dv3 = dv1 / (2*cnot**2-1) =  -0.043460991994
 eci+pople =   -109.029367504080   ( 10e- scaled deltae )    =  -0.343168225432
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
                       Size (real*8) of d2temp for two-external contributions       8932
 
                       Size (real*8) of d2temp for all-internal contributions        156
                       Size (real*8) of d2temp for one-external contributions       1872
                       Size (real*8) of d2temp for two-external contributions       8932
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1    9      336
                1    1    2    1    3       72
                1    1    3    1    5      240
                1    1    4    1    5      336
                1    1    5    1    3       72
                1    1    6    1    9      960
                1    1    7    1    5      720
                1    1    8    1    5      816
                1    2    1    1    9      336
                1    2    2    1    3       72
                1    2    3    1    5      240
                1    2    4    1    5      336
                1    2    5    1    3       72
                1    2    6    1    9      960
                1    2    7    1    5      720
                1    2    8    1    5      816
                3    3    3    1    5      516
                3    3    4    1    5      312
                3    3    5    1    3       36
                3    3    6    1    9      624
                3    3    7    1    5      504
                3    3    8    1    5     1128
                4    4    3    1    5      192
                4    4    4    1    5      636
                4    4    5    1    3       36
                4    4    6    1    9      624
                4    4    7    1    5      936
                4    4    8    1    5      696
                6    5    5    1    3      192
                6    5    6    1    9     1824
                6    5    7    1    5      720
                6    5    8    1    5      816
                6    6    5    1    3      192
                6    6    6    1    9     1824
                6    6    7    1    5      720
                6    6    8    1    5      816
                7    7    5    1    3      240
                7    7    6    1    9      960
                7    7    7    1    5     1224
                7    7    8    1    5      696
                8    8    5    1    3      240
                8    8    6    1    9      960
                8    8    7    1    5      576
                8    8    8    1    5     1344
                       Size (real*8) of d2temp for three-external contributions      26688
                       Size (real*8) of d2temp for four-external contributions      33009
 enough memory for temporary d2 elements on vdisk ... 
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=   13023
 files%d4ext =     unit=  24  vdsk=   1  filestart=   45791
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                   767      1533      3831         1         1
d2rec                     1         3        12         1         2
recsize                 766       766       766     32768     32768
d2bufferlen=          32768
maxbl3=               32768
maxbl4=               32768
  allocated                 111327  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 94% root-following 0
 MR-CISD energy:  -108.99653520   -29.61020354
 residuum:     0.00003352
 deltae:     0.00000000
 apxde:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94216923    -0.05475357    -0.10420096     0.09633904     0.00667422    -0.01125987    -0.00276992     0.00304312

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94216923     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    1843  DYX=   11025  DYW=   10909
   D0Z=     124  D0Y=    2309  D0X=     681  D0W=     757
  DDZI=     968 DDYI=   13660 DDXI=    5437 DDWI=    5288
  DDZE=       0 DDYE=    2144 DDXE=     912 DDWE=     906
================================================================================
adding (  1) 0.00to den1(    1)=   1.9688918
adding (  2) 0.00to den1(    3)=   1.0437188
adding (  3) 0.00to den1(   59)=   1.0411023
adding (  4) 0.00to den1(   74)=   0.1334283
adding (  5) 0.00to den1(   92)=   1.9010611
adding (  6) 0.00to den1(   94)=   0.0224418
adding (  7) 0.00to den1(  147)=   1.8498251
adding (  8) 0.00to den1(  162)=   1.9084426
Trace of MO density:    10.000000
   10  correlated and     4  frozen core electrons

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     1.97657861     1.04101411     0.00826237     0.00621069     0.00537976     0.00144994     0.00032959
              MO     9       MO    10       MO
  occ(*)=     0.00024208     0.00018925

Natural orbital populations,block 2
              MO     1       MO     2       MO
  occ(*)=     0.00584573     0.00024741

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO
  occ(*)=     1.04376428     0.00801720     0.00170430     0.00054220     0.00013548

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO
  occ(*)=     0.13820048     0.00240880     0.00101040     0.00021684     0.00008313

Natural orbital populations,block 5
              MO     1       MO     2       MO
  occ(*)=     0.00357562     0.00016579

Natural orbital populations,block 6
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     1.90606136     0.03313984     0.00616334     0.00389106     0.00210267     0.00046592     0.00021678
              MO     9       MO    10       MO
  occ(*)=     0.00014196     0.00008683

Natural orbital populations,block 7
              MO     1       MO     2       MO     3       MO     4       MO     5       MO
  occ(*)=     1.85729919     0.00561467     0.00485100     0.00038716     0.00013486

Natural orbital populations,block 8
              MO     1       MO     2       MO     3       MO     4       MO     5       MO
  occ(*)=     1.91642731     0.01145187     0.00497923     0.00059120     0.00025573


 total number of electrons =   14.0000000000

 test slabel:                     8
  ag b1g b2g b3g  au b1u b2u b3u 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        ag  partial gross atomic populations
   ao class       1ag        2ag        3ag        4ag        5ag        6ag 
     4_ s       2.000047   1.532250   0.281015  -0.000008   0.005386   0.000000
     4_ p      -0.000001   0.410357   0.751880   0.008015   0.000481   0.000000
     4_ d      -0.000046   0.033972   0.008119   0.000255   0.000344   0.005380
 
   ao class       7ag        8ag        9ag       10ag       11ag 
     4_ s       0.000140   0.000001   0.000005   0.000000   0.000104
     4_ p       0.000070   0.000321   0.000001   0.000000   0.000000
     4_ d       0.001240   0.000008   0.000236   0.000189   0.000003

                        1g  partial gross atomic populations
   ao class       11g        21g 
     4_ d       0.005846   0.000247

                        2g  partial gross atomic populations
   ao class       12g        22g        32g        42g        52g 
     4_ p       1.057203   0.008198  -0.000076   0.000550  -0.000001
     4_ d      -0.013439  -0.000181   0.001781  -0.000008   0.000136

                        3g  partial gross atomic populations
   ao class       13g        23g        33g        43g        53g 
     4_ p       0.139980   0.002463  -0.000045   0.000220   0.000000
     4_ d      -0.001779  -0.000054   0.001056  -0.000003   0.000084

                        au  partial gross atomic populations
   ao class       1au        2au 
     4_ d       0.003576   0.000166

                        1u  partial gross atomic populations
   ao class       11u        21u        31u        41u        51u        61u 
     4_ s       1.998929   1.736114  -0.018171  -0.002106   0.007652   0.000000
     4_ p       0.000601   0.157358   0.055659   0.007195  -0.003286   0.000000
     4_ d       0.000470   0.012590  -0.004349   0.001074  -0.000475   0.002103
 
   ao class       71u        81u        91u       101u       111u 
     4_ s       0.000102  -0.000029   0.000000   0.000005   0.000050
     4_ p      -0.000172   0.000259   0.000000  -0.000001   0.000006
     4_ d       0.000535  -0.000013   0.000142   0.000083   0.000002

                        2u  partial gross atomic populations
   ao class       12u        22u        32u        42u        52u 
     4_ p       1.830003   0.005492   0.000180   0.000388   0.000000
     4_ d       0.027296   0.000123   0.004671   0.000000   0.000135

                        3u  partial gross atomic populations
   ao class       13u        23u        33u        43u        53u 
     4_ p       1.888262   0.011201   0.000185   0.000592   0.000005
     4_ d       0.028165   0.000251   0.004794  -0.000001   0.000389


                        gross atomic populations
     ao            4_
      s         7.541486
      p         6.333542
      d         0.125111
    total      14.000139
 

 Total number of electrons:   14.00013917

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0      28.3      28.3       0.0       0.0      28.4
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0      28.3      28.3       0.0       0.0      28.4
 DA ...
