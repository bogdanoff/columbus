

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       222822400 of real*8 words ( 1700.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=9,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,30
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  2,2,40,2,3,40,2,4,40,2,5,40,2,6,40,2,7,40,2,8,40,2,9,40,4,2,40,4,3,40
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
   NAVST(2) = 2,
   WAVST(2,1)=1 ,
   WAVST(2,2)=1 ,
   NAVST(3) = 1,
   WAVST(3,1)=1 ,
   NAVST(4) = 1,
   WAVST(4,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /home3/plasserf/programs/Columbus/COL_tests/prop/DMABN/ru
 n3/

 Integral file header information:
 Hermit Integral Program : SIFS version  adler.itc.univie. 13:27:56.070 23-Feb-12

 Core type energy values:
 energy( 1)=  5.461735143427E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =  546.173514343


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions: 204

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    82   35   62   25


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=     9

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  4 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             0.200
  2   ground state          2             0.200 0.200
  3   ground state          1             0.200
  4   ground state          1             0.200

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        2
    2        3
    3        2
    4        2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       2( 84)    40
       2       3( 85)    40
       2       4( 86)    40
       2       5( 87)    40
       2       6( 88)    40
       2       7( 89)    40
       2       8( 90)    40
       2       9( 91)    40
       4       2(181)    40
       4       3(182)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0  0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:   78
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9408

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   78
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9996

 Informations for the DRT no.  3

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:   77
 Spin multiplicity:            2
 Number of active orbitals:   10
 Number of active electrons:   9
 Total number of CSFs:     14112

 Informations for the DRT no.  4

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   78
 Spin multiplicity:            3
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:     14916
 

 faar:   0 active-active rotations allowed out of:  29 possible.


 Number of active-double rotations:        10
 Number of active-active rotations:         0
 Number of double-virtual rotations:     1888
 Number of active-virtual rotations:      252

 Size of orbital-Hessian matrix B:                  2368463
 Size of the orbital-state Hessian matrix C:      125620200
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:      127988663


 Source of the initial MO coeficients:

 Input MO coefficient file: /home3/plasserf/programs/Columbus/COL_tests/prop/DMABN/run3/
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    82, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 20863, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222611142

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222489942
 address segment size,           sizesg = 222465072
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:   11673279 transformed 1/r12    array elements were written in    2138 records.

 !timer: 2-e transformation              cpu_time=    19.930 walltime=    22.235

 Size of orbital-Hessian matrix B:                  2368463
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con        2368463


 mosort: allocated sort2 space, avc2is=   222472992 available sort2 space, avcisx=   222473244
 !timer: mosrt1                          cpu_time=     1.278 walltime=     1.278
 !timer: mosort                          cpu_time=     1.511 walltime=     1.511

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    41
 ciiter=  21 noldhv= 14 noldv= 14
 !timer: hmc(*) diagonalization          cpu_time=     4.980 walltime=     4.983

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4787905168    -1001.6523048595        0.0000004306        0.0000010000
    2      -455.3828445963    -1001.5563589390        0.0050166820        0.0100000000
 !timer: hmcvec                          cpu_time=     4.986 walltime=     4.990

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    11
 ciiter=  42 noldhv= 19 noldv= 19
 !timer: hmc(*) diagonalization          cpu_time=    12.536 walltime=    12.548

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6502708566    -1001.8237851993        0.0000002740        0.0000010000
    2      -455.4319994736    -1001.6055138163        0.0000007045        0.0000010000
    3      -455.3815768038    -1001.5550911465        0.0052258594        0.0100000000
 !timer: hmcvec                          cpu_time=    12.544 walltime=    12.556

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    21
 !timer: hmcxv                           cpu_time=     0.507 walltime=     0.509
 !timer: hmcxv                           cpu_time=     0.508 walltime=     0.513
 !timer: hmcxv                           cpu_time=     0.508 walltime=     0.509
 !timer: hmcxv                           cpu_time=     0.509 walltime=     0.509
 !timer: hmcxv                           cpu_time=     0.509 walltime=     0.509
 !timer: hmcxv                           cpu_time=     0.508 walltime=     0.508
 !timer: hmcxv                           cpu_time=     0.508 walltime=     0.509
 !timer: hmcxv                           cpu_time=     0.513 walltime=     0.513
 !timer: hmcxv                           cpu_time=     0.509 walltime=     0.509
 !timer: hmcxv                           cpu_time=     0.510 walltime=     0.517
 !timer: hmcxv                           cpu_time=     0.509 walltime=     0.509
 !timer: hmcxv                           cpu_time=     0.509 walltime=     0.510
 ciiter=  20 noldhv= 15 noldv= 15
 !timer: hmc(*) diagonalization          cpu_time=     9.493 walltime=     9.512

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3918159422    -1001.5653302849        0.0000009798        0.0000010000
    2      -455.2931585195    -1001.4666728622        0.0045481917        0.0100000000
 !timer: hmcvec                          cpu_time=     9.502 walltime=     9.521

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    24
 !timer: hmcxv                           cpu_time=     0.611 walltime=     0.611
 !timer: hmcxv                           cpu_time=     0.610 walltime=     0.610
 !timer: hmcxv                           cpu_time=     0.611 walltime=     0.611
 !timer: hmcxv                           cpu_time=     0.611 walltime=     0.611
 !timer: hmcxv                           cpu_time=     0.610 walltime=     0.611
 !timer: hmcxv                           cpu_time=     0.610 walltime=     0.610
 !timer: hmcxv                           cpu_time=     0.610 walltime=     0.610
 !timer: hmcxv                           cpu_time=     0.592 walltime=     0.592
 !timer: hmcxv                           cpu_time=     0.610 walltime=     0.610
 !timer: hmcxv                           cpu_time=     0.610 walltime=     0.611
 ciiter=  21 noldhv= 13 noldv= 13
 !timer: hmc(*) diagonalization          cpu_time=    11.477 walltime=    11.483

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212476058    -1001.6947619485        0.0000007930        0.0000010000
    2      -455.4777613607    -1001.6512757034        0.0053283002        0.0100000000
 !timer: hmcvec                          cpu_time=    11.489 walltime=    11.494
 !timer: rdft                            cpu_time=     0.583 walltime=     0.583
 !timer: rdft                            cpu_time=     0.650 walltime=     0.653
 !timer: rdft                            cpu_time=     0.769 walltime=     0.768
 !timer: hbcon                           cpu_time=     2.497 walltime=     2.505
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.380029872411362E-004
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999584 pnorm= 0.0000E+00 rznorm= 7.0042E-07 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -31.333498  -31.171901  -22.650768  -22.644501  -22.594459  -22.572577  -22.559085  -22.552371   -2.672631   -2.443526
    -2.368687   -2.150008   -1.930179   -1.715941   -1.560251   -1.498245   -1.332640   -1.228556   -1.179187   -1.110460

 qvv(*) eigenvalues. symmetry block  1
     0.260038    0.325924    0.351922    0.464952    0.518310    0.648569    0.820036    0.846372    0.892622    1.080484
     1.174836    1.254738    1.300225    1.317475    1.379696    1.434613    1.584720    1.647309    1.731883    1.770186
     1.828178    1.860504    1.989895    2.091980    2.127046    2.304724    2.415010    2.488505    2.642055    2.780753
     2.940628    2.997486    3.138760    3.283724    3.453027    3.536280    3.618106    3.767189    3.809194    3.837518
     3.894093    3.991421    4.074390    4.126546    4.229064    4.335233    4.483961    4.496557    4.670566    4.835415
     4.941875    5.286222    5.402084    5.479349    5.808780    5.814287    5.921779    6.025268    6.211880    6.401051
     6.547673    7.746022

 fdd(*) eigenvalues. symmetry block  2
    -1.312084
 i,qaaresolved                     1  -1.13433733043543     
 i,qaaresolved                     2  -1.01577673976985     
 i,qaaresolved                     3 -0.858308161279341     
 i,qaaresolved                     4 -0.515272987158777     
 i,qaaresolved                     5  6.258209165918842E-002
 i,qaaresolved                     6  0.551776302632060     
 i,qaaresolved                     7  0.811683020887093     
 i,qaaresolved                     8   1.67144830640273     

 qvv(*) eigenvalues. symmetry block  2
     0.643590    1.193746    1.247625    1.371924    1.540982    1.597592    1.667911    1.815548    2.048071    2.235708
     2.411415    2.562564    2.974089    3.226944    3.358041    3.412219    3.623125    3.711412    3.794187    4.035847
     4.205914    4.649076    4.725009    5.136346    5.785813    5.874979

 fdd(*) eigenvalues. symmetry block  3
   -22.650817  -22.559144  -22.552434   -2.132951   -2.064773   -1.744721   -1.426884   -1.280877   -1.264618   -1.171872
    -1.065883   -0.930746

 qvv(*) eigenvalues. symmetry block  3
     0.332528    0.373818    0.418889    0.492297    0.547480    0.711582    0.842300    0.931354    1.143093    1.265965
     1.359229    1.424926    1.611222    1.653132    1.684446    1.700679    1.761060    1.834577    1.942845    2.183943
     2.238499    2.371535    2.565341    2.606934    2.805178    2.888957    3.141036    3.219273    3.481358    3.608058
     3.736509    3.783057    3.916522    4.012159    4.196361    4.259072    4.423059    4.444127    4.535403    4.675298
     4.912885    5.028688    5.263739    5.410292    5.500120    5.569540    5.698554    6.339731    6.499882    6.799677

 fdd(*) eigenvalues. symmetry block  4
    -1.236000
 i,qaaresolved                     1 -0.728301594722825     
 i,qaaresolved                     2  0.166005323705969     

 qvv(*) eigenvalues. symmetry block  4
     0.444767    1.273576    1.390677    1.536564    1.689492    2.084398    2.222177    2.396796    2.468506    2.762694
     3.243155    3.318405    3.517884    3.748220    3.825085    4.034127    4.354866    4.442142    4.462731    4.910752
     5.427894    5.664661

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    62.710 walltime=    65.062

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -455.4948248790 demc= 4.5549E+02 wnorm= 1.1040E-03 knorm= 2.8838E-03 apxde= 2.1273E-07    *not conv.*     

               starting mcscf iteration...   2
 !timer:                                 cpu_time=    62.731 walltime=    65.084

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    82, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 20863, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222611142

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222489942
 address segment size,           sizesg = 222465072
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:   11673279 transformed 1/r12    array elements were written in    2138 records.

 !timer: 2-e transformation              cpu_time=    10.886 walltime=    10.893

 Size of orbital-Hessian matrix B:                  2368463
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con        2368463


 mosort: allocated sort2 space, avc2is=   222472992 available sort2 space, avcisx=   222473244
 !timer: mosrt1                          cpu_time=     1.262 walltime=     1.262
 !timer: mosort                          cpu_time=     1.479 walltime=     1.503

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   9 noldhv= 10 noldv= 10
 !timer: hmc(*) diagonalization          cpu_time=     1.281 walltime=     1.282

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4788091256    -1001.6523234683        0.0000009394        0.0000010000
    2      -455.3828362191    -1001.5563505618        0.0050354785        0.0100000000
 !timer: hmcvec                          cpu_time=     1.286 walltime=     1.287

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  5 noldv=  5
 !timer: hmc(*) diagonalization          cpu_time=     3.632 walltime=     3.635

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6502935577    -1001.8238079004        0.0000005777        0.0000010000
    2      -455.4319611669    -1001.6054755096        0.0000009993        0.0000010000
    3      -455.3815951453    -1001.5551094880        0.0051872636        0.0100000000
 !timer: hmcvec                          cpu_time=     3.638 walltime=     3.641

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.509 walltime=     0.509
 ciiter=   9 noldhv= 10 noldv= 10
 !timer: hmc(*) diagonalization          cpu_time=     3.744 walltime=     3.749

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917920554    -1001.5653063981        0.0000006861        0.0000010000
    2      -455.2931335846    -1001.4666479273        0.0043951607        0.0100000000
 !timer: hmcvec                          cpu_time=     3.754 walltime=     3.758

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.611 walltime=     0.611
 ciiter=  11 noldhv= 12 noldv= 12
 !timer: hmc(*) diagonalization          cpu_time=     5.462 walltime=     5.466

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212699995    -1001.6947843422        0.0000006466        0.0000010000
    2      -455.4777999489    -1001.6513142916        0.0059821437        0.0100000000
 !timer: hmcvec                          cpu_time=     5.473 walltime=     5.477
 !timer: rdft                            cpu_time=     0.582 walltime=     0.588
 !timer: rdft                            cpu_time=     0.651 walltime=     0.650
 !timer: rdft                            cpu_time=     0.768 walltime=     0.769
 !timer: hbcon                           cpu_time=     2.504 walltime=     2.510
 
  tol(10)=  0.000000000000000E+000  eshsci=  6.392310058333716E-005
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999935 pnorm= 0.0000E+00 rznorm= 2.9987E-07 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -31.333667  -31.171905  -22.650763  -22.644457  -22.594464  -22.572590  -22.559057  -22.552366   -2.672652   -2.443533
    -2.368684   -2.150004   -1.930177   -1.715937   -1.560252   -1.498246   -1.332639   -1.228551   -1.179186   -1.110465

 qvv(*) eigenvalues. symmetry block  1
     0.260045    0.325927    0.351926    0.464951    0.518310    0.648564    0.820037    0.846375    0.892623    1.080482
     1.174833    1.254732    1.300223    1.317479    1.379691    1.434611    1.584729    1.647316    1.731878    1.770192
     1.828176    1.860513    1.989893    2.091976    2.127051    2.304726    2.415009    2.488503    2.642050    2.780750
     2.940625    2.997490    3.138760    3.283725    3.453033    3.536279    3.618108    3.767189    3.809194    3.837514
     3.894092    3.991423    4.074392    4.126548    4.229064    4.335235    4.483959    4.496560    4.670567    4.835412
     4.941875    5.286224    5.402087    5.479351    5.808782    5.814290    5.921774    6.025264    6.211868    6.401038
     6.547666    7.746024

 fdd(*) eigenvalues. symmetry block  2
    -1.312003
 i,qaaresolved                     1  -1.13436826923966     
 i,qaaresolved                     2  -1.01582913850994     
 i,qaaresolved                     3 -0.858365968035168     
 i,qaaresolved                     4 -0.515302827966141     
 i,qaaresolved                     5  6.260729365955374E-002
 i,qaaresolved                     6  0.551761893982478     
 i,qaaresolved                     7  0.811695830716795     
 i,qaaresolved                     8   1.67478597658678     

 qvv(*) eigenvalues. symmetry block  2
     0.641920    1.192762    1.247620    1.371782    1.541029    1.597561    1.667918    1.815436    2.048143    2.235714
     2.411224    2.562510    2.974007    3.226953    3.358029    3.412110    3.623134    3.711423    3.794185    4.035845
     4.205859    4.649059    4.724963    5.136359    5.785800    5.874972

 fdd(*) eigenvalues. symmetry block  3
   -22.650812  -22.559117  -22.552429   -2.132945   -2.064769   -1.744718   -1.426882   -1.280879   -1.264614   -1.171861
    -1.065882   -0.930754

 qvv(*) eigenvalues. symmetry block  3
     0.332534    0.373812    0.418890    0.492305    0.547479    0.711589    0.842302    0.931354    1.143089    1.265969
     1.359229    1.424927    1.611225    1.653134    1.684454    1.700681    1.761056    1.834576    1.942854    2.183939
     2.238499    2.371535    2.565340    2.606932    2.805184    2.888963    3.141040    3.219273    3.481358    3.608053
     3.736511    3.783064    3.916531    4.012162    4.196361    4.259079    4.423056    4.444130    4.535398    4.675305
     4.912893    5.028685    5.263737    5.410303    5.500122    5.569549    5.698552    6.339731    6.499877    6.799665

 fdd(*) eigenvalues. symmetry block  4
    -1.235989
 i,qaaresolved                     1 -0.728277956771438     
 i,qaaresolved                     2  0.166007246762068     

 qvv(*) eigenvalues. symmetry block  4
     0.444783    1.273588    1.390678    1.536567    1.689503    2.084403    2.222180    2.396796    2.468508    2.762688
     3.243171    3.318406    3.517886    3.748232    3.825084    4.034127    4.354863    4.442140    4.462733    4.910751
     5.427903    5.664655

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    29.279 walltime=    29.328

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=   -455.4948251810 demc= 3.0207E-07 wnorm= 5.1138E-04 knorm= 1.1384E-03 apxde= 3.8029E-08    *not conv.*     

               starting mcscf iteration...   3
 !timer:                                 cpu_time=    92.010 walltime=    94.411

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    82, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 20863, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222611142

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222489942
 address segment size,           sizesg = 222465072
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:   11673280 transformed 1/r12    array elements were written in    2138 records.

 !timer: 2-e transformation              cpu_time=    10.873 walltime=    10.878

 Size of orbital-Hessian matrix B:                  2368463
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con        2368463


 mosort: allocated sort2 space, avc2is=   222472992 available sort2 space, avcisx=   222473244
 !timer: mosrt1                          cpu_time=     1.260 walltime=     1.260
 !timer: mosort                          cpu_time=     1.488 walltime=     1.488

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  10 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     2.171 walltime=     2.178

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4788168990    -1001.6523312417        0.0000006729        0.0000010000
    2      -455.3828306500    -1001.5563449927        0.0051849940        0.0100000000
 !timer: hmcvec                          cpu_time=     2.178 walltime=     2.185

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  15 noldhv=  7 noldv=  7
 !timer: hmc(*) diagonalization          cpu_time=     4.431 walltime=     4.433

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503038575    -1001.8238182002        0.0000008097        0.0000010000
    2      -455.4319442761    -1001.6054586188        0.0000006798        0.0000010000
    3      -455.3816009179    -1001.5551152606        0.0052297915        0.0100000000
 !timer: hmcvec                          cpu_time=     4.440 walltime=     4.442

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.509 walltime=     0.509
 ciiter=  10 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     4.150 walltime=     4.153

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917813351    -1001.5652956778        0.0000005300        0.0000010000
    2      -455.2931209262    -1001.4666352689        0.0044954988        0.0100000000
 !timer: hmcvec                          cpu_time=     4.160 walltime=     4.162

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.611 walltime=     0.611
 ciiter=  11 noldhv= 12 noldv= 12
 !timer: hmc(*) diagonalization          cpu_time=     5.462 walltime=     5.469

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212798084    -1001.6947941511        0.0000009574        0.0000010000
    2      -455.4778103561    -1001.6513246988        0.0060426907        0.0100000000
 !timer: hmcvec                          cpu_time=     5.474 walltime=     5.481
 !timer: rdft                            cpu_time=     0.582 walltime=     0.583
 !timer: rdft                            cpu_time=     0.651 walltime=     0.651
 !timer: rdft                            cpu_time=     0.768 walltime=     0.768
 !timer: hbcon                           cpu_time=     2.484 walltime=     2.489
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.889713965475131E-005
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99999990 pnorm= 0.0000E+00 rznorm= 4.4142E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -31.333751  -31.171909  -22.650763  -22.644436  -22.594465  -22.572596  -22.559043  -22.552361   -2.672664   -2.443536
    -2.368682   -2.150002   -1.930177   -1.715936   -1.560253   -1.498247   -1.332639   -1.228549   -1.179186   -1.110468

 qvv(*) eigenvalues. symmetry block  1
     0.260048    0.325928    0.351928    0.464951    0.518309    0.648562    0.820038    0.846376    0.892623    1.080481
     1.174832    1.254729    1.300223    1.317480    1.379689    1.434610    1.584733    1.647319    1.731874    1.770195
     1.828174    1.860517    1.989891    2.091975    2.127053    2.304727    2.415008    2.488503    2.642047    2.780749
     2.940623    2.997491    3.138760    3.283725    3.453034    3.536279    3.618109    3.767189    3.809193    3.837512
     3.894091    3.991423    4.074392    4.126548    4.229064    4.335236    4.483957    4.496562    4.670567    4.835411
     4.941874    5.286223    5.402088    5.479351    5.808782    5.814291    5.921771    6.025261    6.211861    6.401031
     6.547662    7.746025

 fdd(*) eigenvalues. symmetry block  2
    -1.311969
 i,qaaresolved                     1  -1.13438137278074     
 i,qaaresolved                     2  -1.01585290908144     
 i,qaaresolved                     3 -0.858392890414034     
 i,qaaresolved                     4 -0.515317670306533     
 i,qaaresolved                     5  6.261689510290916E-002
 i,qaaresolved                     6  0.551754562279906     
 i,qaaresolved                     7  0.811701160716312     
 i,qaaresolved                     8   1.67610917007780     

 qvv(*) eigenvalues. symmetry block  2
     0.641262    1.192378    1.247618    1.371723    1.541048    1.597547    1.667921    1.815386    2.048171    2.235716
     2.411149    2.562489    2.973969    3.226957    3.358024    3.412067    3.623138    3.711427    3.794183    4.035844
     4.205838    4.649052    4.724944    5.136363    5.785794    5.874968

 fdd(*) eigenvalues. symmetry block  3
   -22.650812  -22.559102  -22.552425   -2.132942   -2.064768   -1.744717   -1.426881   -1.280880   -1.264612   -1.171857
    -1.065882   -0.930758

 qvv(*) eigenvalues. symmetry block  3
     0.332537    0.373809    0.418890    0.492309    0.547478    0.711592    0.842303    0.931354    1.143087    1.265970
     1.359229    1.424926    1.611226    1.653134    1.684457    1.700681    1.761054    1.834575    1.942859    2.183937
     2.238499    2.371535    2.565339    2.606931    2.805186    2.888965    3.141042    3.219272    3.481357    3.608051
     3.736513    3.783067    3.916534    4.012163    4.196360    4.259083    4.423054    4.444132    4.535395    4.675308
     4.912896    5.028683    5.263736    5.410308    5.500123    5.569552    5.698552    6.339732    6.499874    6.799658

 fdd(*) eigenvalues. symmetry block  4
    -1.235984
 i,qaaresolved                     1 -0.728266443519847     
 i,qaaresolved                     2  0.166010757628378     

 qvv(*) eigenvalues. symmetry block  4
     0.444790    1.273594    1.390679    1.536567    1.689507    2.084405    2.222181    2.396797    2.468509    2.762685
     3.243178    3.318406    3.517886    3.748237    3.825083    4.034127    4.354861    4.442140    4.462733    4.910751
     5.427907    5.664651

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    31.275 walltime=    31.302

 not all mcscf convergence criteria are satisfied.
 iter=    3 emc=   -455.4948252352 demc= 5.4173E-08 wnorm= 2.3118E-04 knorm= 4.5709E-04 apxde= 6.8959E-09    *not conv.*     

               starting mcscf iteration...   4
 !timer:                                 cpu_time=   123.285 walltime=   125.714

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    82, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 20863, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222611142

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222489942
 address segment size,           sizesg = 222465072
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:   11673276 transformed 1/r12    array elements were written in    2138 records.

 !timer: 2-e transformation              cpu_time=     7.546 walltime=     7.551

 Size of orbital-Hessian matrix B:                  2368463
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con        2368463


 mosort: allocated sort2 space, avc2is=   222472992 available sort2 space, avcisx=   222473244
 !timer: mosrt1                          cpu_time=     0.853 walltime=     0.853
 !timer: mosort                          cpu_time=     1.029 walltime=     1.029

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  10 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     1.419 walltime=     1.419

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4788201591    -1001.6523345018        0.0000007756        0.0000010000
    2      -455.3828274727    -1001.5563418154        0.0052128334        0.0100000000
 !timer: hmcvec                          cpu_time=     1.424 walltime=     1.424

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  15 noldhv=  7 noldv=  7
 !timer: hmc(*) diagonalization          cpu_time=     4.478 walltime=     4.486

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503084569    -1001.8238227996        0.0000009146        0.0000010000
    2      -455.4319369142    -1001.6054512569        0.0000007681        0.0000010000
    3      -455.3816033259    -1001.5551176686        0.0052624850        0.0100000000
 !timer: hmcvec                          cpu_time=     4.484 walltime=     4.492

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.507 walltime=     0.507
 ciiter=  10 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     4.144 walltime=     4.145

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917766170    -1001.5652909597        0.0000006080        0.0000010000
    2      -455.2931151181    -1001.4666294608        0.0045376241        0.0100000000
 !timer: hmcvec                          cpu_time=     4.153 walltime=     4.154

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.611 walltime=     0.611
 ciiter=  12 noldhv= 13 noldv= 13
 !timer: hmc(*) diagonalization          cpu_time=     5.947 walltime=     5.949

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212840781    -1001.6947984208        0.0000005549        0.0000010000
    2      -455.4778256707    -1001.6513400134        0.0051067467        0.0100000000
 !timer: hmcvec                          cpu_time=     5.958 walltime=     5.961
 !timer: rdft                            cpu_time=     0.589 walltime=     0.589
 !timer: rdft                            cpu_time=     0.651 walltime=     0.651
 !timer: rdft                            cpu_time=     0.768 walltime=     0.773
 !timer: hbcon                           cpu_time=     2.512 walltime=     2.516
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.287662890805441E-005
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999998 pnorm= 0.0000E+00 rznorm= 7.1217E-07 rpnorm= 0.0000E+00 noldr=  5 nnewr=  5 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -31.333789  -31.171911  -22.650763  -22.644427  -22.594464  -22.572599  -22.559035  -22.552359   -2.672670   -2.443538
    -2.368681   -2.150002   -1.930177   -1.715935   -1.560253   -1.498248   -1.332639   -1.228549   -1.179186   -1.110469

 qvv(*) eigenvalues. symmetry block  1
     0.260050    0.325928    0.351929    0.464950    0.518309    0.648561    0.820038    0.846377    0.892623    1.080480
     1.174831    1.254727    1.300223    1.317481    1.379687    1.434610    1.584735    1.647320    1.731873    1.770196
     1.828173    1.860519    1.989890    2.091974    2.127054    2.304728    2.415007    2.488502    2.642046    2.780748
     2.940622    2.997491    3.138760    3.283725    3.453035    3.536279    3.618109    3.767189    3.809193    3.837511
     3.894091    3.991423    4.074392    4.126547    4.229064    4.335236    4.483957    4.496562    4.670567    4.835411
     4.941874    5.286223    5.402088    5.479351    5.808782    5.814291    5.921770    6.025259    6.211858    6.401028
     6.547661    7.746026

 fdd(*) eigenvalues. symmetry block  2
    -1.311955
 i,qaaresolved                     1  -1.13438691904715     
 i,qaaresolved                     2  -1.01586345320653     
 i,qaaresolved                     3 -0.858405125435098     
 i,qaaresolved                     4 -0.515324636640999     
 i,qaaresolved                     5  6.262063707349032E-002
 i,qaaresolved                     6  0.551751222372222     
 i,qaaresolved                     7  0.811703388743073     
 i,qaaresolved                     8   1.67664331545807     

 qvv(*) eigenvalues. symmetry block  2
     0.640998    1.192224    1.247617    1.371698    1.541055    1.597541    1.667923    1.815364    2.048183    2.235717
     2.411120    2.562480    2.973952    3.226959    3.358022    3.412049    3.623140    3.711428    3.794182    4.035844
     4.205829    4.649049    4.724936    5.136365    5.785791    5.874966

 fdd(*) eigenvalues. symmetry block  3
   -22.650812  -22.559095  -22.552422   -2.132941   -2.064768   -1.744716   -1.426881   -1.280881   -1.264612   -1.171855
    -1.065882   -0.930760

 qvv(*) eigenvalues. symmetry block  3
     0.332538    0.373807    0.418890    0.492311    0.547478    0.711593    0.842303    0.931354    1.143085    1.265971
     1.359229    1.424926    1.611226    1.653135    1.684459    1.700681    1.761053    1.834574    1.942860    2.183936
     2.238499    2.371534    2.565338    2.606931    2.805187    2.888966    3.141042    3.219272    3.481357    3.608050
     3.736513    3.783068    3.916536    4.012163    4.196360    4.259085    4.423054    4.444132    4.535393    4.675309
     4.912897    5.028681    5.263736    5.410310    5.500124    5.569554    5.698551    6.339732    6.499872    6.799654

 fdd(*) eigenvalues. symmetry block  4
    -1.235982
 i,qaaresolved                     1 -0.728261090749641     
 i,qaaresolved                     2  0.166013181646842     

 qvv(*) eigenvalues. symmetry block  4
     0.444793    1.273596    1.390679    1.536567    1.689509    2.084406    2.222181    2.396797    2.468509    2.762683
     3.243181    3.318406    3.517886    3.748239    3.825083    4.034127    4.354860    4.442139    4.462733    4.910751
     5.427908    5.664648

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    27.321 walltime=    27.344

 not all mcscf convergence criteria are satisfied.
 iter=    4 emc=   -455.4948252451 demc= 9.8429E-09 wnorm= 1.0301E-04 knorm= 1.8641E-04 apxde= 1.2631E-09    *not conv.*     

               starting mcscf iteration...   5
 !timer:                                 cpu_time=   150.606 walltime=   153.058

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    82, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 20863, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222611142

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222489942
 address segment size,           sizesg = 222465072
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:   11673281 transformed 1/r12    array elements were written in    2138 records.

 !timer: 2-e transformation              cpu_time=    10.884 walltime=    10.890

 Size of orbital-Hessian matrix B:                  2368463
 Size of the orbital-state Hessian matrix C:      125620200
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:      127988663


 mosort: allocated sort2 space, avc2is=   222472992 available sort2 space, avcisx=   222473244
 !timer: mosrt1                          cpu_time=     1.260 walltime=     1.259
 !timer: mosort                          cpu_time=     1.487 walltime=     1.487

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  10 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     2.086 walltime=     2.091

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4788215423    -1001.6523358850        0.0000008223        0.0000010000
    2      -455.3828259476    -1001.5563402903        0.0052255325        0.0100000000
 !timer: hmcvec                          cpu_time=     2.094 walltime=     2.098

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  15 noldhv=  8 noldv=  8
 !timer: hmc(*) diagonalization          cpu_time=     4.522 walltime=     4.525

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503104981    -1001.8238248408        0.0000003877        0.0000010000
    2      -455.4319337255    -1001.6054480682        0.0000007439        0.0000010000
    3      -455.3816049391    -1001.5551192818        0.0051532765        0.0100000000
 !timer: hmcvec                          cpu_time=     4.531 walltime=     4.534

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.507 walltime=     0.508
 ciiter=  10 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     4.142 walltime=     4.143

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917745255    -1001.5652888682        0.0000006433        0.0000010000
    2      -455.2931125488    -1001.4666268915        0.0045572290        0.0100000000
 !timer: hmcvec                          cpu_time=     4.151 walltime=     4.153

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.610 walltime=     0.610
 ciiter=  12 noldhv= 13 noldv= 13
 !timer: hmc(*) diagonalization          cpu_time=     5.944 walltime=     5.948

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212859430    -1001.6948002857        0.0000005821        0.0000010000
    2      -455.4778276318    -1001.6513419745        0.0051064571        0.0100000000
 !timer: hmcvec                          cpu_time=     5.957 walltime=     5.960
 !timer: rdft                            cpu_time=     7.033 walltime=     7.044
 !timer: rdft                            cpu_time=    16.502 walltime=    16.512
 !timer: rdft                            cpu_time=    11.882 walltime=    11.893
 !timer: rdft                            cpu_time=    12.424 walltime=    12.470
 !timer: hbcon                           cpu_time=    52.832 walltime=    53.078
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.680123139107693E-006
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999999 pnorm= 2.2202E-03 rznorm= 6.4342E-07 rpnorm= 8.7347E-07 noldr=  6 nnewr=  6 nolds=  5 nnews=  5
 
 !timer: Direct Mxs time contribution    cpu_time=     7.822 walltime=     7.831
 !timer: solvek total                    cpu_time=    19.450 walltime=    19.465

 fdd(*) eigenvalues. symmetry block  1
   -31.333807  -31.171912  -22.650764  -22.644422  -22.594464  -22.572600  -22.559032  -22.552358   -2.672673   -2.443539
    -2.368680   -2.150001   -1.930177   -1.715935   -1.560253   -1.498249   -1.332639   -1.228548   -1.179187   -1.110470

 qvv(*) eigenvalues. symmetry block  1
     0.260051    0.325928    0.351929    0.464950    0.518309    0.648560    0.820038    0.846377    0.892622    1.080479
     1.174830    1.254726    1.300223    1.317481    1.379687    1.434610    1.584736    1.647320    1.731872    1.770197
     1.828173    1.860520    1.989889    2.091973    2.127055    2.304728    2.415007    2.488502    2.642045    2.780748
     2.940622    2.997491    3.138759    3.283725    3.453036    3.536278    3.618109    3.767189    3.809193    3.837510
     3.894091    3.991423    4.074392    4.126547    4.229064    4.335236    4.483956    4.496563    4.670566    4.835410
     4.941873    5.286223    5.402088    5.479351    5.808782    5.814291    5.921769    6.025259    6.211857    6.401026
     6.547660    7.746026

 fdd(*) eigenvalues. symmetry block  2
    -1.311949
 i,qaaresolved                     1  -1.13438930951029     
 i,qaaresolved                     2  -1.01586811552647     
 i,qaaresolved                     3 -0.858410602900968     
 i,qaaresolved                     4 -0.515327892529175     
 i,qaaresolved                     5  6.262204338435189E-002
 i,qaaresolved                     6  0.551749724672211     
 i,qaaresolved                     7  0.811704300733178     
 i,qaaresolved                     8   1.67686234686270     

 qvv(*) eigenvalues. symmetry block  2
     0.640890    1.192162    1.247617    1.371687    1.541058    1.597538    1.667923    1.815354    2.048187    2.235717
     2.411109    2.562477    2.973944    3.226961    3.358021    3.412042    3.623140    3.711429    3.794182    4.035844
     4.205826    4.649048    4.724933    5.136365    5.785789    5.874965

 fdd(*) eigenvalues. symmetry block  3
   -22.650813  -22.559092  -22.552421   -2.132940   -2.064768   -1.744716   -1.426882   -1.280881   -1.264611   -1.171854
    -1.065881   -0.930761

 qvv(*) eigenvalues. symmetry block  3
     0.332538    0.373806    0.418890    0.492311    0.547478    0.711594    0.842303    0.931354    1.143085    1.265971
     1.359229    1.424926    1.611226    1.653135    1.684459    1.700682    1.761053    1.834574    1.942861    2.183935
     2.238499    2.371534    2.565338    2.606931    2.805187    2.888966    3.141042    3.219271    3.481357    3.608049
     3.736513    3.783069    3.916536    4.012163    4.196360    4.259085    4.423054    4.444133    4.535392    4.675310
     4.912897    5.028681    5.263735    5.410311    5.500124    5.569554    5.698551    6.339732    6.499871    6.799653

 fdd(*) eigenvalues. symmetry block  4
    -1.235981
 i,qaaresolved                     1 -0.728258689828706     
 i,qaaresolved                     2  0.166014476059821     

 qvv(*) eigenvalues. symmetry block  4
     0.444794    1.273597    1.390679    1.536566    1.689509    2.084406    2.222181    2.396797    2.468509    2.762682
     3.243183    3.318406    3.517886    3.748240    3.825083    4.034127    4.354859    4.442139    4.462733    4.910751
     5.427909    5.664647

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=   101.483 walltime=   101.761

 not all mcscf convergence criteria are satisfied.
 iter=    5 emc=   -455.4948252469 demc= 1.8054E-09 wnorm= 4.5441E-05 knorm= 1.3339E-04 apxde= 4.0722E-10    *not conv.*     

               starting mcscf iteration...   6
 !timer:                                 cpu_time=   252.089 walltime=   254.819

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    82, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 20863, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222611142

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222489942
 address segment size,           sizesg = 222465072
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:   11673278 transformed 1/r12    array elements were written in    2138 records.

 !timer: 2-e transformation              cpu_time=    10.010 walltime=    10.070

 Size of orbital-Hessian matrix B:                  2368463
 Size of the orbital-state Hessian matrix C:      125620200
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:      127988663


 mosort: allocated sort2 space, avc2is=   222472992 available sort2 space, avcisx=   222473244
 !timer: mosrt1                          cpu_time=     0.856 walltime=     0.856
 !timer: mosort                          cpu_time=     1.032 walltime=     1.032

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  10 noldhv= 12 noldv= 12
 !timer: hmc(*) diagonalization          cpu_time=     1.810 walltime=     1.811

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4788225377    -1001.6523368804        0.0000008734        0.0000010000
    2      -455.3828269737    -1001.5563413164        0.0053285435        0.0100000000
 !timer: hmcvec                          cpu_time=     1.816 walltime=     1.817

   4 trial vectors read from nvfile (unit= 29).
 ciiter=  14 noldhv=  8 noldv=  8
 !timer: hmc(*) diagonalization          cpu_time=     4.320 walltime=     4.323

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503120687    -1001.8238264114        0.0000003739        0.0000010000
    2      -455.4319313211    -1001.6054456638        0.0000008405        0.0000010000
    3      -455.3816054723    -1001.5551198150        0.0051754828        0.0100000000
 !timer: hmcvec                          cpu_time=     4.330 walltime=     4.332

   3 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.601 walltime=     0.606
 ciiter=  10 noldhv= 12 noldv= 12
 !timer: hmc(*) diagonalization          cpu_time=     4.245 walltime=     4.254

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917729614    -1001.5652873041        0.0000006439        0.0000010000
    2      -455.2931107222    -1001.4666250649        0.0045676722        0.0100000000
 !timer: hmcvec                          cpu_time=     4.255 walltime=     4.264

   3 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.722 walltime=     0.722
 ciiter=  12 noldhv= 14 noldv= 14
 !timer: hmc(*) diagonalization          cpu_time=     6.078 walltime=     6.082

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212873474    -1001.6948016901        0.0000005895        0.0000010000
    2      -455.4778288117    -1001.6513431544        0.0051159372        0.0100000000
 !timer: hmcvec                          cpu_time=     6.090 walltime=     6.094
 !timer: rdft                            cpu_time=     7.051 walltime=     7.061
 !timer: rdft                            cpu_time=    16.237 walltime=    16.256
 !timer: rdft                            cpu_time=    11.735 walltime=    11.754
 !timer: rdft                            cpu_time=    12.561 walltime=    12.582
 !timer: hbcon                           cpu_time=    52.977 walltime=    53.301
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.026769026511917E-007
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 1.4153E-07 rpnorm= 9.5341E-08 noldr=  2 nnewr=  2 nolds=  0 nnews=  0
 
 !timer: solvek total                    cpu_time=     1.558 walltime=     1.559

 fdd(*) eigenvalues. symmetry block  1
   -31.333821  -31.171913  -22.650764  -22.644419  -22.594464  -22.572601  -22.559029  -22.552357   -2.672675   -2.443540
    -2.368680   -2.150001   -1.930177   -1.715935   -1.560253   -1.498249   -1.332639   -1.228548   -1.179187   -1.110471

 qvv(*) eigenvalues. symmetry block  1
     0.260051    0.325928    0.351929    0.464950    0.518308    0.648560    0.820038    0.846378    0.892622    1.080479
     1.174830    1.254725    1.300223    1.317481    1.379686    1.434610    1.584736    1.647320    1.731872    1.770197
     1.828173    1.860520    1.989889    2.091973    2.127055    2.304728    2.415007    2.488502    2.642044    2.780748
     2.940622    2.997491    3.138759    3.283725    3.453036    3.536278    3.618109    3.767189    3.809193    3.837510
     3.894090    3.991423    4.074393    4.126547    4.229064    4.335236    4.483956    4.496563    4.670566    4.835410
     4.941873    5.286223    5.402089    5.479351    5.808782    5.814291    5.921769    6.025258    6.211855    6.401025
     6.547659    7.746026

 fdd(*) eigenvalues. symmetry block  2
    -1.311945
 i,qaaresolved                     1  -1.13439091451170     
 i,qaaresolved                     2  -1.01587158797657     
 i,qaaresolved                     3 -0.858414845913307     
 i,qaaresolved                     4 -0.515330400347569     
 i,qaaresolved                     5  6.262308865754521E-002
 i,qaaresolved                     6  0.551748709160364     
 i,qaaresolved                     7  0.811705041663907     
 i,qaaresolved                     8   1.67702067763458     

 qvv(*) eigenvalues. symmetry block  2
     0.640811    1.192118    1.247617    1.371680    1.541060    1.597536    1.667924    1.815347    2.048190    2.235717
     2.411100    2.562474    2.973938    3.226961    3.358021    3.412038    3.623141    3.711429    3.794181    4.035844
     4.205823    4.649047    4.724931    5.136365    5.785788    5.874965

 fdd(*) eigenvalues. symmetry block  3
   -22.650813  -22.559089  -22.552420   -2.132940   -2.064768   -1.744716   -1.426882   -1.280882   -1.264611   -1.171854
    -1.065881   -0.930762

 qvv(*) eigenvalues. symmetry block  3
     0.332539    0.373806    0.418890    0.492312    0.547477    0.711594    0.842303    0.931354    1.143084    1.265972
     1.359229    1.424926    1.611226    1.653135    1.684460    1.700682    1.761052    1.834574    1.942862    2.183935
     2.238499    2.371534    2.565338    2.606931    2.805188    2.888966    3.141042    3.219271    3.481357    3.608049
     3.736514    3.783069    3.916537    4.012163    4.196359    4.259086    4.423053    4.444133    4.535392    4.675310
     4.912898    5.028680    5.263735    5.410312    5.500124    5.569555    5.698551    6.339733    6.499871    6.799651

 fdd(*) eigenvalues. symmetry block  4
    -1.235980
 i,qaaresolved                     1 -0.728256738576589     
 i,qaaresolved                     2  0.166015847597705     

 qvv(*) eigenvalues. symmetry block  4
     0.444795    1.273597    1.390680    1.536566    1.689510    2.084407    2.222181    2.396797    2.468509    2.762682
     3.243184    3.318406    3.517886    3.748241    3.825083    4.034127    4.354859    4.442139    4.462732    4.910751
     5.427910    5.664646

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    82.136 walltime=    82.535

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    6 emc=   -455.4948252473 demc= 4.0825E-10 wnorm= 8.2142E-07 knorm= 1.5230E-06 apxde= 1.9312E-13    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.200 total energy=     -455.478822538, rel. (eV)=   4.666470
   DRT #2 state # 1 wt 0.200 total energy=     -455.650312069, rel. (eV)=   0.000000
   DRT #2 state # 2 wt 0.200 total energy=     -455.431931321, rel. (eV)=   5.942445
   DRT #3 state # 1 wt 0.200 total energy=     -455.391772961, rel. (eV)=   7.035210
   DRT #4 state # 1 wt 0.200 total energy=     -455.521287347, rel. (eV)=   3.510943
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69        MO   70        MO   71        MO   72
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   73        MO   74        MO   75        MO   76        MO   77        MO   78        MO   79        MO   80
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   81        MO   82
  occ(*)=     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  B1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.95954280     1.94292532     1.89804503     1.31038545     0.49779266     0.07440343     0.04898956
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.01102394     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  3    -  B2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  4    -  A2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.80143257     0.25545924     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
       4724 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     1
 Number of unique bra states (ndbra):                     1
 qind: F
 (Transition) density matrices:
 d1(*) written to the 1-particle density matrix file.
       4724 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           

          state spec. NOs: DRT 1, State  1

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69        MO   70        MO   71        MO   72
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   73        MO   74        MO   75        MO   76        MO   77        MO   78        MO   79        MO   80
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   81        MO   82
  occ(*)=     0.00000000     2.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     1.97992811     1.93905710     1.86983980     1.32254978     0.44844442     0.10456855     0.05564243     0.01039584
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     2.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000

          block  4
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     1.58025653     0.68931746     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     1.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    18_ s       0.000000   0.000065   0.000000   0.000013   1.991878   0.003180
    18_ p       0.000000   0.000164   0.000000   0.000080   0.000002   0.000812
    18_ d       0.000000   0.000000   0.000000   0.000000   0.000004  -0.000180
    18_ s      -0.000001   0.000000   0.000000  -0.000160   0.003307   0.000042
    18_ p      -0.000004  -0.000001   0.000000  -0.000259   0.001263   0.000203
    18_ d       0.000000   0.000000   0.000000   0.000000  -0.000057  -0.000002
    18_ s       0.000102   0.000000   0.000005   0.002435  -0.000023  -0.000001
    18_ p      -0.000159   0.000000   0.000007   0.001061  -0.000016  -0.000002
    18_ d       0.000000   0.000000   0.000000  -0.000060   0.000001   0.000000
    18_ s       0.000180   0.000000   0.000016   1.998787   0.000017  -0.000001
    18_ p       0.001532   0.000000   0.000156  -0.000027   0.000021   0.000000
    18_ d      -0.000039   0.000000   0.000000   0.000001   0.000000   0.000000
    19_ s       0.000000  -0.000399   0.000000   0.000000   0.003391   1.997312
    19_ p       0.000000   0.002237   0.000000   0.000000   0.000281   0.000070
    19_ d       0.000000  -0.000506   0.000000   0.000000  -0.000115  -0.000010
    19_ s       0.000343   0.000000   1.999594   0.000007   0.000000   0.000000
    19_ p       0.000249   0.000000   0.000044   0.000030   0.000000   0.000000
    19_ d      -0.000065   0.000000   0.000000   0.000001   0.000000   0.000000
    19_ s       0.000000   1.998433   0.000000   0.000000   0.000002  -0.000891
    19_ p       0.000000   0.000006   0.000000   0.000000  -0.000002  -0.000575
    19_ d       0.000000   0.000001   0.000000   0.000000   0.000001   0.000034
    19_ s       1.997876   0.000000  -0.000210  -0.000708   0.000000   0.000000
    19_ p       0.000000   0.000000  -0.000062  -0.001256   0.000000   0.000000
    19_ d       0.000000   0.000000  -0.000024  -0.000050   0.000000   0.000000
    19_ s       0.000000   0.000000   0.000000  -0.000002   0.000047   0.000007
    19_ p       0.000000   0.000000   0.000000   0.000000   0.000001   0.000000
    19_ s       0.000016   0.000000   0.000007   0.000113  -0.000002   0.000000
    19_ p       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
    19_ s       0.000001   0.000000   0.000080   0.000003   0.000000   0.000000
    19_ p       0.000000   0.000000  -0.000066   0.000000   0.000000   0.000000
    20_ s      -0.000030   0.000000   0.000581  -0.000010   0.000000   0.000000
    20_ p       0.000000   0.000000  -0.000128   0.000000   0.000000   0.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    18_ s       0.000056   0.002946   0.001496   0.150086   0.258717   0.399198
    18_ p       0.000142   0.000703  -0.004326   0.001394   0.002252  -0.000160
    18_ d       0.000000  -0.000079   0.000013   0.002366   0.002234   0.002388
    18_ s       0.063499   1.932128   0.020761   0.130917   0.469249   0.131143
    18_ p       0.000331  -0.000023   0.011452   0.010263   0.062276   0.101766
    18_ d      -0.000025  -0.000013   0.000202   0.001756   0.004078   0.008656
    18_ s       1.934684   0.063701   0.011183   0.030270   0.407434   0.211930
    18_ p      -0.000125   0.000620   0.033488   0.001977   0.060838   0.088121
    18_ d       0.000006  -0.000051   0.002680   0.000432   0.003243   0.008536
    18_ s       0.001144   0.000030   0.246206   0.009840   0.133858   0.356263
    18_ p       0.000257  -0.000016  -0.010835   0.005825   0.038397   0.011926
    18_ d      -0.000076  -0.000005   0.009363   0.000155   0.001733   0.002640
    19_ s       0.000006   0.000053  -0.000491   0.580802   0.046220   0.019184
    19_ p       0.000013   0.000103  -0.000466   0.075514   0.067462   0.113775
    19_ d       0.000000  -0.000001   0.000003   0.019774   0.000783   0.001017
    19_ s       0.000001   0.000000   0.237165   0.004115   0.038854   0.150903
    19_ p       0.000004  -0.000001   0.085957   0.000818   0.006351   0.006988
    19_ d       0.000000   0.000000   0.014147   0.000146   0.001104   0.001198
    19_ s       0.000000   0.000000  -0.000005   0.809682   0.223755   0.048460
    19_ p       0.000000   0.000000   0.000009   0.136381   0.030728   0.003718
    19_ d       0.000000   0.000000   0.000000   0.005535   0.001336   0.000194
    19_ s       0.000000   0.000000   1.267789   0.007368   0.048996   0.012446
    19_ p       0.000002  -0.000001   0.004232   0.001320   0.023779   0.185694
    19_ d       0.000000   0.000000   0.003289   0.000031   0.000379   0.001970
    19_ s       0.000040   0.000010   0.000538   0.009424   0.024940   0.018262
    19_ p       0.000001  -0.000098  -0.000007   0.000831   0.002533   0.001768
    19_ s       0.000138  -0.000001  -0.001305   0.001394   0.021268   0.022968
    19_ p      -0.000097  -0.000007   0.000624   0.000200   0.002118   0.002309
    19_ s       0.000000   0.000000   0.021628   0.000760   0.007740   0.046865
    19_ p       0.000000   0.000000   0.002489   0.000049   0.000520   0.002406
    20_ s       0.000000   0.000000   0.037944   0.000497   0.006008   0.033959
    20_ p       0.000000   0.000000   0.004776   0.000077   0.000817   0.003508
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
    18_ s       0.155685   0.063443   0.008790   0.000040   0.045392   0.027465
    18_ p       0.031090   0.118281   0.298221   0.134186   0.065419   0.055808
    18_ d       0.000447   0.003893   0.000772   0.008009   0.002509   0.005279
    18_ s       0.029834   0.213171   0.008771   0.070642   0.079076   0.001670
    18_ p       0.138502   0.041355   0.347207   0.038634   0.397700   0.136738
    18_ d       0.003339   0.000978   0.006615   0.006683   0.013772   0.008915
    18_ s       0.196127   0.000040   0.080578   0.078402   0.056612   0.001955
    18_ p       0.036210   0.327784   0.405231   0.006719   0.153677   0.345260
    18_ d       0.001209   0.010086   0.000423   0.004503   0.018169   0.002271
    18_ s       0.034138   0.120273   0.024129   0.003492   0.082268   0.003889
    18_ p       0.118977   0.050793   0.077192   0.173397   0.009312   0.048929
    18_ d       0.004730   0.006306   0.000555   0.004658   0.000791   0.003258
    19_ s       0.045728   0.099780   0.078263   0.089274   0.001528   0.062865
    19_ p       0.094796   0.132561   0.081954   0.077592   0.001242   0.015879
    19_ d       0.001083   0.001091   0.000060   0.000154   0.000056  -0.000077
    19_ s       0.357840   0.157050   0.000755   0.048034  -0.000057   0.002225
    19_ p       0.044763   0.159855   0.049612   0.422553   0.198418   0.444183
    19_ d       0.002892   0.008770   0.000622   0.011567   0.002518   0.011108
    19_ s       0.022923   0.028016   0.025805   0.024248   0.002477   0.060304
    19_ p       0.001073   0.002291   0.005486   0.007312   0.002169   0.089801
    19_ d       0.000033  -0.000006   0.000001   0.000010   0.000008   0.000549
    19_ s       0.072823   0.110700   0.000383   0.015014   0.003825   0.003718
    19_ p       0.236724   0.014771   0.026903   0.446396   0.005541   0.017128
    19_ d       0.001988   0.000100   0.000023   0.000158   0.002178   0.005712
    19_ s       0.002850   0.078915   0.121749   0.040707   0.439958   0.000083
    19_ p       0.000599   0.003690   0.003758   0.001274   0.007954   0.000652
    19_ s       0.038221   0.000621   0.310972   0.010016   0.237643   0.196486
    19_ p       0.001566   0.001244   0.008262   0.000692   0.003144   0.003834
    19_ s       0.151699   0.066773   0.009006   0.003755   0.137364   0.325701
    19_ p       0.006274   0.002798   0.000282   0.001383   0.001634   0.002748
    20_ s       0.155927   0.167174   0.016994   0.261402   0.025626   0.110383
    20_ p       0.009910   0.007405   0.000627   0.009090   0.002077   0.005280
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
    18_ s       0.004919  -0.001849   0.000000   0.000000   0.000000   0.000000
    18_ p       0.003190   0.122029   0.000000   0.000000   0.000000   0.000000
    18_ d       0.009220   0.006900   0.000000   0.000000   0.000000   0.000000
    18_ s       0.005123   0.001581   0.000000   0.000000   0.000000   0.000000
    18_ p       0.316040   0.147457   0.000000   0.000000   0.000000   0.000000
    18_ d       0.005625   0.006558   0.000000   0.000000   0.000000   0.000000
    18_ s      -0.000018   0.000233   0.000000   0.000000   0.000000   0.000000
    18_ p       0.272047   0.147770   0.000000   0.000000   0.000000   0.000000
    18_ d       0.009275   0.002456   0.000000   0.000000   0.000000   0.000000
    18_ s       0.004765   0.001896   0.000000   0.000000   0.000000   0.000000
    18_ p       0.158335   0.010248   0.000000   0.000000   0.000000   0.000000
    18_ d       0.013693   0.004574   0.000000   0.000000   0.000000   0.000000
    19_ s       0.057050   0.006994   0.000000   0.000000   0.000000   0.000000
    19_ p       0.018700   0.266553   0.000000   0.000000   0.000000   0.000000
    19_ d       0.000043   0.001122   0.000000   0.000000   0.000000   0.000000
    19_ s      -0.000359   0.000022   0.000000   0.000000   0.000000   0.000000
    19_ p       0.235037   0.008685   0.000000   0.000000   0.000000   0.000000
    19_ d       0.008640   0.000321   0.000000   0.000000   0.000000   0.000000
    19_ s       0.110027   0.289084   0.000000   0.000000   0.000000   0.000000
    19_ p       0.219017   0.884699   0.000000   0.000000   0.000000   0.000000
    19_ d       0.001434   0.005895   0.000000   0.000000   0.000000   0.000000
    19_ s       0.000193   0.000416   0.000000   0.000000   0.000000   0.000000
    19_ p       0.191455   0.021013   0.000000   0.000000   0.000000   0.000000
    19_ d       0.004186   0.000107   0.000000   0.000000   0.000000   0.000000
    19_ s       0.133966   0.025903   0.000000   0.000000   0.000000   0.000000
    19_ p       0.002244   0.001019   0.000000   0.000000   0.000000   0.000000
    19_ s       0.016628   0.034472   0.000000   0.000000   0.000000   0.000000
    19_ p       0.001643   0.000796   0.000000   0.000000   0.000000   0.000000
    19_ s       0.180459   0.002712   0.000000   0.000000   0.000000   0.000000
    19_ p       0.001027   0.000028   0.000000   0.000000   0.000000   0.000000
    20_ s       0.014073   0.000168   0.000000   0.000000   0.000000   0.000000
    20_ p       0.002325   0.000138   0.000000   0.000000   0.000000   0.000000
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1       70A1       71A1       72A1 
 
   ao class      73A1       74A1       75A1       76A1       77A1       78A1 
 
   ao class      79A1       80A1       81A1       82A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    18_ p       0.000276   0.023019   0.353872   0.256369   0.280058   0.110724
    18_ d       0.000000   0.000239   0.002918   0.008358   0.000164   0.002555
    18_ p       0.000008   0.093549   0.145070   0.554830   0.047151   0.121802
    18_ d       0.000032   0.002717   0.005751   0.004892   0.013762   0.005299
    18_ p       0.000726   0.364840   0.003288   0.232163   0.327841   0.020464
    18_ d       0.000025   0.009029   0.002358   0.006866   0.005284   0.008918
    18_ p      -0.009438   0.670100   0.023941  -0.004276   0.148028   0.124942
    18_ d       0.000637   0.002650   0.001620   0.010041   0.012428   0.000059
    19_ p      -0.000005   0.010955   0.738907   0.156696  -0.002078   0.015473
    19_ d       0.000007   0.000000   0.002123   0.017250   0.009724   0.006195
    19_ p       1.016420  -0.010875  -0.000789  -0.000512   0.000932   0.000354
    19_ d       0.008360   0.016883   0.002267   0.007971   0.012202   0.001625
    19_ p       0.000035   0.008334   0.556199   0.343213   0.075147   0.048657
    19_ d       0.000000   0.000144   0.009977   0.003241   0.000217  -0.000024
    19_ p       0.085798   0.735202   0.089983   0.275862   0.327619   0.020798
    19_ d       0.001385   0.003683   0.000194   0.000058   0.000742   0.001183
    19_ p       0.000001   0.000400   0.000808   0.002743   0.000341   0.001086
    19_ p       0.000021   0.001686   0.000008   0.001147   0.002297   0.000191
    19_ p       0.006072   0.000075   0.000008   0.000025   0.000020  -0.000009
    20_ s       0.875783   0.027126   0.004451   0.021217   0.048748   0.007505
    20_ p       0.013857  -0.000213  -0.000029  -0.000111  -0.000242  -0.000005
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    18_ p       0.000032   0.009175   0.000054   0.000000   0.000000   0.000000
    18_ d       0.000470  -0.000004   0.000000   0.000000   0.000000   0.000000
    18_ p       0.006372   0.009606   0.000010   0.000000   0.000000   0.000000
    18_ d       0.000306   0.000143   0.000003   0.000000   0.000000   0.000000
    18_ p       0.018887   0.006126   0.000074   0.000000   0.000000   0.000000
    18_ d       0.000256   0.000055   0.000030   0.000000   0.000000   0.000000
    18_ p       0.013339   0.002916  -0.000077   0.000000   0.000000   0.000000
    18_ d       0.000064   0.000036   0.000148   0.000000   0.000000   0.000000
    19_ p       0.016847   0.012153   0.000001   0.000000   0.000000   0.000000
    19_ d       0.000129  -0.000007   0.000000   0.000000   0.000000   0.000000
    19_ p      -0.000005   0.000001   0.001074   0.000000   0.000000   0.000000
    19_ d       0.000059   0.000007   0.000299   0.000000   0.000000   0.000000
    19_ p       0.015277   0.008236   0.000009   0.000000   0.000000   0.000000
    19_ d      -0.000050  -0.000035   0.000000   0.000000   0.000000   0.000000
    19_ p       0.001967   0.000453   0.008238   0.000000   0.000000   0.000000
    19_ d       0.000155   0.000038  -0.000004   0.000000   0.000000   0.000000
    19_ p       0.000024   0.000051   0.000000   0.000000   0.000000   0.000000
    19_ p       0.000114   0.000028   0.000000   0.000000   0.000000   0.000000
    19_ p       0.000000   0.000000  -0.000004   0.000000   0.000000   0.000000
    20_ s       0.000163   0.000012   0.001071   0.000000   0.000000   0.000000
    20_ p      -0.000002   0.000000   0.000098   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    18_ p       0.000000   0.000018   0.000843   0.097715   0.031118   0.292012
    18_ d       0.000000   0.000002  -0.000030   0.004310   0.001882   0.002705
    18_ s       0.000000   0.066468   1.932166   0.477553   0.211190   0.356569
    18_ p       0.000000   0.000208   0.000178   0.067689  -0.003259   0.124413
    18_ d       0.000000  -0.000030  -0.000007   0.006756   0.002059   0.001651
    18_ s      -0.000033   1.932013   0.066606   0.644200   0.074796   0.337738
    18_ p      -0.000050   0.000207   0.000350  -0.005328   0.104640   0.140643
    18_ d       0.000000   0.000004  -0.000047   0.006904   0.001799   0.001835
    18_ p       0.000309   0.001111   0.000010   0.076436  -0.004588   0.276867
    18_ d       0.000000  -0.000044  -0.000003   0.002567   0.004888   0.002846
    19_ p       0.000000   0.000001  -0.000016   0.000373   0.000589   0.007123
    19_ d       0.000000   0.000000   0.000000   0.000323   0.000205   0.001204
    19_ s       2.000195   0.000003   0.000000   0.188440   0.745124   0.041285
    19_ p       0.000009   0.000005  -0.000001   0.004549   0.007599   0.018474
    19_ d       0.000000   0.000000   0.000000   0.000294   0.000563   0.000227
    19_ p       0.000000   0.000000   0.000000   0.000089   0.000064   0.001669
    19_ d       0.000000   0.000000   0.000000   0.000010   0.000007   0.000100
    19_ p      -0.000910  -0.000018   0.000000   0.129722   0.325683   0.002553
    19_ d      -0.000021   0.000000   0.000000   0.000190   0.003859   0.001516
    19_ s       0.000000   0.000042   0.000055   0.064355   0.037108   0.165539
    19_ p       0.000000   0.000001  -0.000095   0.005002   0.002524   0.008308
    19_ s      -0.000021   0.000102  -0.000002   0.109288   0.006688   0.163846
    19_ p       0.000000  -0.000093  -0.000007   0.006588   0.001326   0.008118
    19_ s       0.000279   0.000000   0.000000   0.024484   0.116102   0.023691
    19_ p      -0.000062   0.000000   0.000000   0.002101   0.009257   0.001196
    20_ s       0.000431   0.000001   0.000000   0.080144   0.298331   0.016431
    20_ p      -0.000125   0.000000   0.000000   0.005245   0.020447   0.001439
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    18_ p       0.172443   0.202873   0.033232   0.015927   0.159469   0.014990
    18_ d       0.000046  -0.000002   0.005888   0.001864   0.001221   0.015445
    18_ s       0.000941   0.006751  -0.003977  -0.002920   0.003367   0.006368
    18_ p       0.278351   0.357395   0.479040   0.155408   0.320136   0.062990
    18_ d       0.001222   0.002332   0.003829   0.002228   0.026757   0.001460
    18_ s       0.017152   0.000157  -0.001898   0.000577   0.003771   0.001329
    18_ p       0.260737   0.390609   0.556319   0.054768   0.386484   0.020492
    18_ d       0.001072   0.002283   0.004302   0.002518   0.026371   0.001969
    18_ p       0.145184   0.123592   0.069958   0.010895   0.230610   0.002026
    18_ d       0.002808   0.005846   0.008669   0.000451   0.007754   0.000385
    19_ p       0.013583   0.033005  -0.000008  -0.000101   0.054645   0.805180
    19_ d       0.000549   0.000446   0.000192   0.000100   0.000055   0.031556
    19_ s       0.033897   0.002835   0.001409  -0.000026  -0.000744   0.000021
    19_ p       0.422867   0.354663   0.141621   0.847666   0.095276   0.001170
    19_ d       0.012306   0.011557   0.000937   0.014249   0.001968   0.000055
    19_ p       0.004435   0.012968   0.000031   0.000003   0.038324   0.988083
    19_ d       0.000175   0.000415  -0.000002   0.000002   0.000922   0.013984
    19_ p       0.387402   0.319535  -0.001520   0.030599   0.032119   0.000048
    19_ d       0.000027   0.003474  -0.000023   0.002317   0.003855   0.000030
    19_ s       0.007698   0.001796   0.272030   0.097866   0.292432   0.013082
    19_ p       0.001308   0.001921   0.003751   0.001073   0.002936   0.000146
    19_ s       0.015668   0.056189   0.315682   0.008811   0.265777   0.018134
    19_ p       0.001561   0.002182   0.003031   0.000523   0.002836   0.000040
    19_ s       0.153864   0.056261   0.076654   0.401833   0.005378   0.000476
    19_ p       0.004750   0.002011   0.001493   0.006563   0.000442   0.000005
    20_ s       0.055644   0.046327   0.027802   0.336608   0.036931   0.000517
    20_ p       0.004311   0.002577   0.001559   0.010198   0.000907   0.000018
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2       57B2       58B2       59B2       60B2 
 
   ao class      61B2       62B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    18_ d       0.000000   0.011592   0.002914   0.000000   0.000000   0.000000
    18_ p      -0.000040   0.819778   0.130250   0.000000   0.000000   0.000000
    18_ d       0.000000   0.008706   0.001613   0.000000   0.000000   0.000000
    18_ p       0.001210   0.928600   0.113820   0.000000   0.000000   0.000000
    18_ d       0.000046   0.007203   0.001715   0.000000   0.000000   0.000000
    18_ d       0.000135   0.012962   0.002740   0.000000   0.000000   0.000000
    19_ d       0.000000   0.000040   0.000030   0.000000   0.000000   0.000000
    19_ p       1.050263   0.000065   0.000019   0.000000   0.000000   0.000000
    19_ d       0.013623   0.000042   0.000016   0.000000   0.000000   0.000000
    19_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
    19_ d       0.005746   0.000007   0.000005   0.000000   0.000000   0.000000
    19_ p       0.000000   0.005135   0.001012   0.000000   0.000000   0.000000
    19_ p       0.000071   0.006046   0.000973   0.000000   0.000000   0.000000
    19_ p       0.005242   0.000005   0.000002   0.000000   0.000000   0.000000
    20_ s       0.909701   0.001199   0.000325   0.000000   0.000000   0.000000
    20_ p       0.014001   0.000053   0.000025   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           18_        18_        18_        18_        19_        19_
      s         3.111521   6.215225   6.152059   3.021192   3.087562   6.008931
      p         2.883504   5.521836   5.808423   2.596305   2.811592   5.614391
      d         0.106313   0.158563   0.153975   0.132023   0.094596   0.168479
    total       6.101338  11.895624  12.114458   5.749520   5.993750  11.791801
 
 
     ao           19_        19_        19_        19_        19_        20_
      s         3.642319   3.540627   1.849400   1.849817   1.813569   3.627165
      p         3.482886   3.944773   0.064694   0.065011   0.060766   0.119911
      d         0.044108   0.048463   0.000000   0.000000   0.000000   0.000000
    total       7.169314   7.533863   1.914094   1.914828   1.874335   3.747076
 

 Total number of electrons:   77.80000000

 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    18_ s       0.000000   0.000065   0.000000   0.000013   1.991878   0.003180
    18_ p       0.000000   0.000164   0.000000   0.000080   0.000002   0.000812
    18_ d       0.000000   0.000000   0.000000   0.000000   0.000004  -0.000180
    18_ s      -0.000001   0.000000   0.000000  -0.000160   0.003307   0.000042
    18_ p      -0.000004  -0.000001   0.000000  -0.000259   0.001263   0.000203
    18_ d       0.000000   0.000000   0.000000   0.000000  -0.000057  -0.000002
    18_ s       0.000102   0.000000   0.000005   0.002435  -0.000023  -0.000001
    18_ p      -0.000159   0.000000   0.000007   0.001061  -0.000016  -0.000002
    18_ d       0.000000   0.000000   0.000000  -0.000060   0.000001   0.000000
    18_ s       0.000180   0.000000   0.000016   1.998787   0.000017  -0.000001
    18_ p       0.001532   0.000000   0.000156  -0.000027   0.000021   0.000000
    18_ d      -0.000039   0.000000   0.000000   0.000001   0.000000   0.000000
    19_ s       0.000000  -0.000399   0.000000   0.000000   0.003391   1.997313
    19_ p       0.000000   0.002237   0.000000   0.000000   0.000281   0.000070
    19_ d       0.000000  -0.000506   0.000000   0.000000  -0.000115  -0.000010
    19_ s       0.000343   0.000000   1.999594   0.000008   0.000000   0.000000
    19_ p       0.000249   0.000000   0.000044   0.000030   0.000000   0.000000
    19_ d      -0.000065   0.000000   0.000000   0.000001   0.000000   0.000000
    19_ s       0.000000   1.998433   0.000000   0.000000   0.000002  -0.000891
    19_ p       0.000000   0.000006   0.000000   0.000000  -0.000002  -0.000575
    19_ d       0.000000   0.000001   0.000000   0.000000   0.000001   0.000034
    19_ s       1.997876   0.000000  -0.000210  -0.000708   0.000000   0.000000
    19_ p       0.000000   0.000000  -0.000062  -0.001256   0.000000   0.000000
    19_ d       0.000000   0.000000  -0.000024  -0.000050   0.000000   0.000000
    19_ s       0.000000   0.000000   0.000000  -0.000002   0.000047   0.000007
    19_ p       0.000000   0.000000   0.000000   0.000000   0.000001   0.000000
    19_ s       0.000016   0.000000   0.000007   0.000113  -0.000002   0.000000
    19_ p       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
    19_ s       0.000001   0.000000   0.000080   0.000003   0.000000   0.000000
    19_ p       0.000000   0.000000  -0.000066   0.000000   0.000000   0.000000
    20_ s      -0.000030   0.000000   0.000581  -0.000010   0.000000   0.000000
    20_ p       0.000000   0.000000  -0.000128   0.000000   0.000000   0.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    18_ s       0.000056   0.002946   0.001496   0.150089   0.258713   0.399199
    18_ p       0.000142   0.000703  -0.004327   0.001394   0.002252  -0.000160
    18_ d       0.000000  -0.000079   0.000013   0.002366   0.002234   0.002388
    18_ s       0.063469   1.932158   0.020761   0.130921   0.469244   0.131144
    18_ p       0.000331  -0.000023   0.011452   0.010264   0.062275   0.101766
    18_ d      -0.000025  -0.000013   0.000202   0.001756   0.004078   0.008656
    18_ s       1.934714   0.063671   0.011184   0.030272   0.407433   0.211930
    18_ p      -0.000125   0.000620   0.033488   0.001978   0.060839   0.088121
    18_ d       0.000006  -0.000051   0.002680   0.000432   0.003243   0.008536
    18_ s       0.001144   0.000030   0.246208   0.009840   0.133857   0.356262
    18_ p       0.000257  -0.000016  -0.010835   0.005825   0.038398   0.011927
    18_ d      -0.000076  -0.000005   0.009363   0.000155   0.001733   0.002640
    19_ s       0.000006   0.000053  -0.000491   0.580800   0.046223   0.019184
    19_ p       0.000013   0.000103  -0.000466   0.075513   0.067463   0.113775
    19_ d       0.000000  -0.000001   0.000003   0.019774   0.000783   0.001017
    19_ s       0.000001   0.000000   0.237165   0.004116   0.038855   0.150903
    19_ p       0.000004  -0.000001   0.085957   0.000818   0.006351   0.006988
    19_ d       0.000000   0.000000   0.014147   0.000146   0.001104   0.001198
    19_ s       0.000000   0.000000  -0.000005   0.809675   0.223760   0.048460
    19_ p       0.000000   0.000000   0.000009   0.136380   0.030729   0.003718
    19_ d       0.000000   0.000000   0.000000   0.005535   0.001336   0.000194
    19_ s       0.000000   0.000000   1.267785   0.007368   0.048997   0.012446
    19_ p       0.000002  -0.000001   0.004232   0.001321   0.023779   0.185693
    19_ d       0.000000   0.000000   0.003289   0.000031   0.000379   0.001970
    19_ s       0.000040   0.000010   0.000538   0.009425   0.024939   0.018262
    19_ p       0.000001  -0.000098  -0.000007   0.000831   0.002533   0.001768
    19_ s       0.000138  -0.000001  -0.001305   0.001394   0.021268   0.022968
    19_ p      -0.000097  -0.000007   0.000624   0.000200   0.002118   0.002309
    19_ s       0.000000   0.000000   0.021628   0.000760   0.007740   0.046865
    19_ p       0.000000   0.000000   0.002489   0.000049   0.000520   0.002406
    20_ s       0.000000   0.000000   0.037945   0.000497   0.006008   0.033959
    20_ p       0.000000   0.000000   0.004776   0.000077   0.000817   0.003508
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
    18_ s       0.155685   0.063444   0.008790   0.000040   0.045392   0.027465
    18_ p       0.031090   0.118280   0.298221   0.134187   0.065419   0.055809
    18_ d       0.000447   0.003893   0.000772   0.008009   0.002509   0.005279
    18_ s       0.029834   0.213170   0.008771   0.070641   0.079076   0.001670
    18_ p       0.138502   0.041356   0.347206   0.038634   0.397700   0.136739
    18_ d       0.003339   0.000978   0.006615   0.006683   0.013772   0.008915
    18_ s       0.196126   0.000040   0.080578   0.078402   0.056613   0.001955
    18_ p       0.036210   0.327785   0.405230   0.006719   0.153676   0.345262
    18_ d       0.001209   0.010087   0.000423   0.004503   0.018169   0.002271
    18_ s       0.034139   0.120273   0.024129   0.003492   0.082268   0.003889
    18_ p       0.118977   0.050793   0.077192   0.173397   0.009312   0.048930
    18_ d       0.004730   0.006306   0.000555   0.004658   0.000791   0.003258
    19_ s       0.045727   0.099780   0.078264   0.089274   0.001528   0.062865
    19_ p       0.094796   0.132561   0.081954   0.077592   0.001242   0.015880
    19_ d       0.001083   0.001091   0.000060   0.000154   0.000056  -0.000077
    19_ s       0.357841   0.157049   0.000755   0.048034  -0.000057   0.002225
    19_ p       0.044764   0.159855   0.049612   0.422555   0.198418   0.444181
    19_ d       0.002892   0.008770   0.000622   0.011567   0.002518   0.011108
    19_ s       0.022923   0.028016   0.025805   0.024248   0.002477   0.060303
    19_ p       0.001073   0.002291   0.005486   0.007312   0.002169   0.089799
    19_ d       0.000033  -0.000006   0.000001   0.000010   0.000008   0.000549
    19_ s       0.072824   0.110701   0.000383   0.015014   0.003825   0.003718
    19_ p       0.236724   0.014770   0.026903   0.446396   0.005541   0.017128
    19_ d       0.001988   0.000100   0.000023   0.000158   0.002178   0.005712
    19_ s       0.002850   0.078915   0.121748   0.040706   0.439959   0.000083
    19_ p       0.000599   0.003690   0.003758   0.001274   0.007954   0.000652
    19_ s       0.038221   0.000621   0.310972   0.010015   0.237642   0.196487
    19_ p       0.001566   0.001244   0.008262   0.000692   0.003144   0.003834
    19_ s       0.151700   0.066772   0.009006   0.003756   0.137364   0.325700
    19_ p       0.006274   0.002798   0.000282   0.001383   0.001634   0.002748
    20_ s       0.155928   0.167174   0.016994   0.261402   0.025626   0.110383
    20_ p       0.009910   0.007405   0.000627   0.009090   0.002077   0.005279
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
    18_ s       0.004919  -0.001849   0.000000   0.000000   0.000000   0.000000
    18_ p       0.003190   0.122028   0.000000   0.000000   0.000000   0.000000
    18_ d       0.009220   0.006900   0.000000   0.000000   0.000000   0.000000
    18_ s       0.005123   0.001581   0.000000   0.000000   0.000000   0.000000
    18_ p       0.316042   0.147453   0.000000   0.000000   0.000000   0.000000
    18_ d       0.005625   0.006558   0.000000   0.000000   0.000000   0.000000
    18_ s      -0.000018   0.000233   0.000000   0.000000   0.000000   0.000000
    18_ p       0.272049   0.147766   0.000000   0.000000   0.000000   0.000000
    18_ d       0.009275   0.002456   0.000000   0.000000   0.000000   0.000000
    18_ s       0.004765   0.001896   0.000000   0.000000   0.000000   0.000000
    18_ p       0.158335   0.010248   0.000000   0.000000   0.000000   0.000000
    18_ d       0.013693   0.004574   0.000000   0.000000   0.000000   0.000000
    19_ s       0.057050   0.006994   0.000000   0.000000   0.000000   0.000000
    19_ p       0.018699   0.266554   0.000000   0.000000   0.000000   0.000000
    19_ d       0.000043   0.001122   0.000000   0.000000   0.000000   0.000000
    19_ s      -0.000359   0.000022   0.000000   0.000000   0.000000   0.000000
    19_ p       0.235037   0.008685   0.000000   0.000000   0.000000   0.000000
    19_ d       0.008640   0.000321   0.000000   0.000000   0.000000   0.000000
    19_ s       0.110025   0.289087   0.000000   0.000000   0.000000   0.000000
    19_ p       0.219011   0.884707   0.000000   0.000000   0.000000   0.000000
    19_ d       0.001434   0.005895   0.000000   0.000000   0.000000   0.000000
    19_ s       0.000193   0.000416   0.000000   0.000000   0.000000   0.000000
    19_ p       0.191457   0.021012   0.000000   0.000000   0.000000   0.000000
    19_ d       0.004186   0.000107   0.000000   0.000000   0.000000   0.000000
    19_ s       0.133966   0.025902   0.000000   0.000000   0.000000   0.000000
    19_ p       0.002244   0.001019   0.000000   0.000000   0.000000   0.000000
    19_ s       0.016628   0.034471   0.000000   0.000000   0.000000   0.000000
    19_ p       0.001643   0.000796   0.000000   0.000000   0.000000   0.000000
    19_ s       0.180458   0.002712   0.000000   0.000000   0.000000   0.000000
    19_ p       0.001027   0.000028   0.000000   0.000000   0.000000   0.000000
    20_ s       0.014073   0.000168   0.000000   0.000000   0.000000   0.000000
    20_ p       0.002325   0.000138   0.000000   0.000000   0.000000   0.000000
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1       70A1       71A1       72A1 
 
   ao class      73A1       74A1       75A1       76A1       77A1       78A1 
 
   ao class      79A1       80A1       81A1       82A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    18_ p       0.000276   0.007012   0.218690   0.288289   0.368177   0.099675
    18_ d       0.000000   0.000009   0.004458   0.006278   0.000349   0.002088
    18_ p       0.000008  -0.001997   0.070611   0.602364   0.101126   0.114798
    18_ d       0.000032   0.001161   0.003193   0.006641   0.014813   0.005314
    18_ p       0.000726   0.092859   0.015992   0.502354   0.279025   0.030387
    18_ d       0.000025   0.008723   0.000458   0.006123   0.007714   0.008266
    18_ p      -0.009438   0.549291   0.004487   0.103850   0.200621   0.113202
    18_ d       0.000637   0.011232   0.000144   0.008889   0.009188   0.000012
    19_ p      -0.000005   0.000559   0.857069   0.048739  -0.004502   0.008600
    19_ d       0.000007   0.000141   0.005709   0.011599   0.011460   0.005185
    19_ p       1.016420  -0.010582  -0.000015   0.000057   0.000116   0.000296
    19_ d       0.008360   0.031182   0.000012   0.003850   0.008773   0.001377
    19_ p       0.000035   0.000028   0.745549   0.164777   0.081109   0.032571
    19_ d       0.000000   0.000004   0.012085   0.001282   0.000198  -0.000008
    19_ p       0.085798   1.227292   0.000039   0.093041   0.201072   0.018088
    19_ d       0.001385   0.003312   0.000015   0.000354   0.001083   0.001120
    19_ p       0.000001   0.000010   0.000401   0.002839   0.000669   0.000998
    19_ p       0.000021   0.000398   0.000061   0.002495   0.002000   0.000252
    19_ p       0.006072   0.000123   0.000000   0.000003   0.000005  -0.000008
    20_ s       0.875783   0.059581   0.000099   0.016066   0.039722   0.006241
    20_ p       0.013857  -0.000412   0.000000  -0.000051  -0.000167  -0.000007
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    18_ p       0.003346   0.008385   0.000040   0.000000   0.000000   0.000000
    18_ d       0.000561   0.000088   0.000000   0.000000   0.000000   0.000000
    18_ p       0.022800   0.003923  -0.000006   0.000000   0.000000   0.000000
    18_ d       0.000248   0.000212   0.000004   0.000000   0.000000   0.000000
    18_ p       0.038052   0.000947   0.000038   0.000000   0.000000   0.000000
    18_ d       0.000372   0.000029   0.000025   0.000000   0.000000   0.000000
    18_ p       0.021890   0.000340  -0.000073   0.000000   0.000000   0.000000
    18_ d       0.000121   0.000007   0.000139   0.000000   0.000000   0.000000
    19_ p       0.006192   0.023880   0.000000   0.000000   0.000000   0.000000
    19_ d       0.000383  -0.000084   0.000000   0.000000   0.000000   0.000000
    19_ p       0.000010   0.000000   0.001009   0.000000   0.000000   0.000000
    19_ d       0.000115   0.000001   0.000270   0.000000   0.000000   0.000000
    19_ p       0.007475   0.017915   0.000007   0.000000   0.000000   0.000000
    19_ d      -0.000019  -0.000069   0.000000   0.000000   0.000000   0.000000
    19_ p       0.002154   0.000036   0.007883   0.000000   0.000000   0.000000
    19_ d       0.000264   0.000005  -0.000003   0.000000   0.000000   0.000000
    19_ p       0.000101   0.000022   0.000001   0.000000   0.000000   0.000000
    19_ p       0.000211   0.000003   0.000000   0.000000   0.000000   0.000000
    19_ p       0.000000   0.000000  -0.000004   0.000000   0.000000   0.000000
    20_ s       0.000296   0.000001   0.000973   0.000000   0.000000   0.000000
    20_ p      -0.000003   0.000000   0.000093   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    18_ p       0.000000   0.000018   0.000843   0.097715   0.031117   0.292012
    18_ d       0.000000   0.000002  -0.000030   0.004310   0.001882   0.002705
    18_ s       0.000000   0.066437   1.932197   0.477556   0.211186   0.356570
    18_ p       0.000000   0.000208   0.000178   0.067689  -0.003259   0.124412
    18_ d       0.000000  -0.000030  -0.000007   0.006756   0.002059   0.001651
    18_ s      -0.000033   1.932044   0.066575   0.644204   0.074794   0.337737
    18_ p      -0.000050   0.000207   0.000350  -0.005328   0.104640   0.140644
    18_ d       0.000000   0.000004  -0.000047   0.006904   0.001799   0.001835
    18_ p       0.000309   0.001111   0.000010   0.076438  -0.004588   0.276868
    18_ d       0.000000  -0.000044  -0.000003   0.002567   0.004888   0.002846
    19_ p       0.000000   0.000001  -0.000016   0.000373   0.000589   0.007123
    19_ d       0.000000   0.000000   0.000000   0.000323   0.000205   0.001204
    19_ s       2.000195   0.000003   0.000000   0.188436   0.745128   0.041285
    19_ p       0.000009   0.000005  -0.000001   0.004549   0.007599   0.018474
    19_ d       0.000000   0.000000   0.000000   0.000294   0.000563   0.000227
    19_ p       0.000000   0.000000   0.000000   0.000089   0.000064   0.001668
    19_ d       0.000000   0.000000   0.000000   0.000010   0.000007   0.000100
    19_ p      -0.000910  -0.000018   0.000000   0.129719   0.325684   0.002554
    19_ d      -0.000021   0.000000   0.000000   0.000190   0.003859   0.001516
    19_ s       0.000000   0.000042   0.000055   0.064355   0.037107   0.165539
    19_ p       0.000000   0.000001  -0.000095   0.005002   0.002524   0.008308
    19_ s      -0.000021   0.000102  -0.000002   0.109288   0.006688   0.163845
    19_ p       0.000000  -0.000093  -0.000007   0.006588   0.001326   0.008118
    19_ s       0.000279   0.000000   0.000000   0.024483   0.116103   0.023691
    19_ p      -0.000062   0.000000   0.000000   0.002101   0.009257   0.001196
    20_ s       0.000431   0.000001   0.000000   0.080142   0.298334   0.016431
    20_ p      -0.000125   0.000000   0.000000   0.005245   0.020447   0.001439
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    18_ p       0.172445   0.202871   0.033231   0.015928   0.159469   0.014990
    18_ d       0.000046  -0.000002   0.005888   0.001864   0.001221   0.015445
    18_ s       0.000941   0.006751  -0.003977  -0.002920   0.003367   0.006368
    18_ p       0.278355   0.357392   0.479037   0.155410   0.320138   0.062990
    18_ d       0.001222   0.002332   0.003829   0.002228   0.026757   0.001460
    18_ s       0.017152   0.000158  -0.001898   0.000577   0.003771   0.001329
    18_ p       0.260741   0.390606   0.556320   0.054768   0.386483   0.020492
    18_ d       0.001072   0.002283   0.004302   0.002518   0.026371   0.001969
    18_ p       0.145186   0.123592   0.069957   0.010895   0.230609   0.002026
    18_ d       0.002808   0.005846   0.008669   0.000451   0.007754   0.000385
    19_ p       0.013583   0.033005  -0.000008  -0.000101   0.054645   0.805181
    19_ d       0.000549   0.000446   0.000192   0.000100   0.000055   0.031556
    19_ s       0.033896   0.002835   0.001409  -0.000026  -0.000744   0.000021
    19_ p       0.422863   0.354667   0.141622   0.847664   0.095276   0.001170
    19_ d       0.012306   0.011557   0.000937   0.014249   0.001968   0.000055
    19_ p       0.004435   0.012968   0.000031   0.000003   0.038323   0.988084
    19_ d       0.000175   0.000415  -0.000002   0.000002   0.000922   0.013984
    19_ p       0.387399   0.319539  -0.001521   0.030600   0.032120   0.000048
    19_ d       0.000027   0.003474  -0.000023   0.002317   0.003855   0.000030
    19_ s       0.007698   0.001796   0.272029   0.097867   0.292433   0.013082
    19_ p       0.001308   0.001921   0.003751   0.001073   0.002936   0.000146
    19_ s       0.015668   0.056189   0.315684   0.008811   0.265777   0.018134
    19_ p       0.001561   0.002182   0.003031   0.000523   0.002836   0.000040
    19_ s       0.153863   0.056263   0.076655   0.401831   0.005378   0.000476
    19_ p       0.004750   0.002011   0.001493   0.006563   0.000442   0.000005
    20_ s       0.055642   0.046326   0.027802   0.336609   0.036931   0.000517
    20_ p       0.004311   0.002577   0.001559   0.010198   0.000907   0.000018
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2       57B2       58B2       59B2       60B2 
 
   ao class      61B2       62B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    18_ d       0.000000   0.009531   0.008142   0.000000   0.000000   0.000000
    18_ p      -0.000040   0.679858   0.368589   0.000000   0.000000   0.000000
    18_ d       0.000000   0.008072   0.004162   0.000000   0.000000   0.000000
    18_ p       0.001210   0.853709   0.290063   0.000000   0.000000   0.000000
    18_ d       0.000046   0.005913   0.004804   0.000000   0.000000   0.000000
    18_ d       0.000135   0.012032   0.007104   0.000000   0.000000   0.000000
    19_ d       0.000000   0.000031   0.000082   0.000000   0.000000   0.000000
    19_ p       1.050263   0.000061   0.000050   0.000000   0.000000   0.000000
    19_ d       0.013623   0.000040   0.000041   0.000000   0.000000   0.000000
    19_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
    19_ d       0.005746   0.000007   0.000014   0.000000   0.000000   0.000000
    19_ p       0.000000   0.004253   0.002841   0.000000   0.000000   0.000000
    19_ p       0.000071   0.005573   0.002508   0.000000   0.000000   0.000000
    19_ p       0.005242   0.000005   0.000006   0.000000   0.000000   0.000000
    20_ s       0.909701   0.001121   0.000846   0.000000   0.000000   0.000000
    20_ p       0.014001   0.000050   0.000065   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           18_        18_        18_        18_        19_        19_
      s         3.111521   6.215225   6.152059   3.021192   3.087562   6.008931
      p         2.843816   5.555486   5.895745   2.611000   2.803174   5.615129
      d         0.108610   0.159190   0.154689   0.138144   0.093620   0.172769
    total       6.063947  11.929901  12.202493   5.770335   5.984356  11.796828
 
 
     ao           19_        19_        19_        19_        19_        20_
      s         3.642319   3.540627   1.849400   1.849817   1.813569   3.640294
      p         3.477243   4.034257   0.065229   0.066022   0.060773   0.119904
      d         0.044111   0.048572   0.000000   0.000000   0.000000   0.000000
    total       7.163674   7.623456   1.914630   1.915839   1.874342   3.760199
 

 Total number of electrons:   78.00000000

 !timer: mcscf                           cpu_time=   334.632 walltime=   337.762
