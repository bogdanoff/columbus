 total ao core energy =  546.173514343
 MCSCF calculation performed for  4 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             0.200
  2   ground state          2             0.200 0.200
  3   ground state          1             0.200
  4   ground state          1             0.200

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:    78
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9408

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    78
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9996

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:    77
 Spin multiplicity:            2
 Number of active orbitals:   10
 Number of active electrons:   9
 Total number of CSFs:     14112

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    78
 Spin multiplicity:            3
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:     14916

 Number of active-double rotations:        10
 Number of active-active rotations:         0
 Number of double-virtual rotations:     1888
 Number of active-virtual rotations:      252
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -455.4948248790  4.555E+02  1.104E-03  2.884E-03  2.127E-07  F   *not conv.*     
    2   -455.4948251810  3.021E-07  5.114E-04  1.138E-03  3.803E-08  F   *not conv.*     
    3   -455.4948252352  5.417E-08  2.312E-04  4.571E-04  6.896E-09  F   *not conv.*     
    4   -455.4948252451  9.843E-09  1.030E-04  1.864E-04  1.263E-09  F   *not conv.*     
    5   -455.4948252469  1.805E-09  4.544E-05  1.334E-04  4.072E-10  T   *not conv.*     

 final mcscf convergence values:
    6   -455.4948252473  4.082E-10  8.214E-07  1.523E-06  1.931E-13  T   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.200 total energy=     -455.478822538, rel. (eV)=   4.666470
   DRT #2 state # 1 wt 0.200 total energy=     -455.650312069, rel. (eV)=   0.000000
   DRT #2 state # 2 wt 0.200 total energy=     -455.431931321, rel. (eV)=   5.942445
   DRT #3 state # 1 wt 0.200 total energy=     -455.391772961, rel. (eV)=   7.035210
   DRT #4 state # 1 wt 0.200 total energy=     -455.521287347, rel. (eV)=   3.510943
   ------------------------------------------------------------


