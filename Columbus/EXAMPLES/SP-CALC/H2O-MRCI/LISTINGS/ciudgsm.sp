1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:02 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:02 2003

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:02 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:02 2003
  h2o                                                                            

 297 dimension of the ci-matrix ->>>      9263


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    12)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0282863112  0.0000E+00  2.5131E-01  9.9728E-01  1.0000E-04
 mr-sdci #  1  2    -75.5863700138  4.2633E-14  0.0000E+00  1.0034E+00  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -76.0282863112  0.0000E+00  2.5131E-01  9.9728E-01  1.0000E-04
 mr-sdci #  1  2    -75.5863700138  4.2633E-14  0.0000E+00  1.0034E+00  1.0000E-04

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.2262883907  1.9800E-01  6.6454E-03  1.6287E-01  1.0000E-04
 mr-sdci #  1  2    -75.5864108894  4.0876E-05  0.0000E+00  1.0038E+00  1.0000E-04
 mr-sdci #  2  1    -76.2323975099  6.1091E-03  2.6406E-04  3.1307E-02  1.0000E-04
 mr-sdci #  2  2    -75.5867922444  3.8136E-04  0.0000E+00  1.0038E+00  1.0000E-04
 mr-sdci #  3  1    -76.2326663821  2.6887E-04  1.9452E-05  8.1427E-03  1.0000E-04
 mr-sdci #  3  2    -75.5904212788  3.6290E-03  0.0000E+00  9.9940E-01  1.0000E-04
 mr-sdci #  4  1    -76.2326839866  1.7605E-05  1.3154E-06  2.2537E-03  1.0000E-04
 mr-sdci #  4  2    -75.5908500610  4.2878E-04  0.0000E+00  1.0007E+00  1.0000E-04
 mr-sdci #  5  1    -76.2326853564  1.3698E-06  1.1345E-07  6.1780E-04  1.0000E-04
 mr-sdci #  5  2    -75.5909249120  7.4851E-05  0.0000E+00  1.0010E+00  1.0000E-04
 mr-sdci #  6  1    -76.2326854759  1.1954E-07  1.6926E-08  2.4819E-04  1.0000E-04
 mr-sdci #  6  2    -75.5911391795  2.1427E-04  0.0000E+00  1.0011E+00  1.0000E-04
 mr-sdci #  7  1    -76.2326854938  1.7906E-08  0.0000E+00  8.1852E-05  1.0000E-04
 mr-sdci #  7  2    -75.5947446816  3.6055E-03  2.9722E-01  9.9578E-01  1.0000E-04
 mr-sdci #  8  1    -76.2326854938  3.6863E-11  0.0000E+00  8.1212E-05  1.0000E-04
 mr-sdci #  8  2    -75.8113672275  2.1662E-01  1.7514E-02  2.5094E-01  1.0000E-04
 mr-sdci #  9  1    -76.2326854939  1.7678E-11  0.0000E+00  8.1042E-05  1.0000E-04
 mr-sdci #  9  2    -75.8261563331  1.4789E-02  1.2719E-03  6.4451E-02  1.0000E-04
 mr-sdci # 10  1    -76.2326854939  1.3074E-12  0.0000E+00  8.0979E-05  1.0000E-04
 mr-sdci # 10  2    -75.8274004457  1.2441E-03  2.3192E-04  2.5618E-02  1.0000E-04
 mr-sdci # 11  1    -76.2326854940  8.9756E-11  0.0000E+00  8.0352E-05  1.0000E-04
 mr-sdci # 11  2    -75.8276338363  2.3339E-04  2.7828E-05  1.0126E-02  1.0000E-04
 mr-sdci # 12  1    -76.2326854940  4.6214E-11  0.0000E+00  8.0468E-05  1.0000E-04
 mr-sdci # 12  2    -75.8276643344  3.0498E-05  4.5271E-06  3.8572E-03  1.0000E-04
 mr-sdci # 13  1    -76.2326854941  1.3084E-10  0.0000E+00  7.6826E-05  1.0000E-04
 mr-sdci # 13  2    -75.8276687816  4.4472E-06  6.4295E-07  1.4452E-03  1.0000E-04
 mr-sdci # 14  1    -76.2326854942  6.1135E-11  0.0000E+00  7.6785E-05  1.0000E-04
 mr-sdci # 14  2    -75.8276694356  6.5397E-07  1.0813E-07  5.7734E-04  1.0000E-04
 mr-sdci # 15  1    -76.2326854942  4.5659E-11  0.0000E+00  7.4790E-05  1.0000E-04
 mr-sdci # 15  2    -75.8276695237  8.8083E-08  2.4160E-08  2.4148E-04  1.0000E-04
 mr-sdci # 16  1    -76.2326854942  6.6365E-12  0.0000E+00  7.4817E-05  1.0000E-04
 mr-sdci # 16  2    -75.8276695512  2.7559E-08  4.6684E-09  1.2695E-04  1.0000E-04
 mr-sdci # 17  1    -76.2326854942  2.9559E-12  0.0000E+00  7.4837E-05  1.0000E-04
 mr-sdci # 17  2    -75.8276695560  4.7995E-09  6.8634E-10  4.8838E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after 17 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 17  1    -76.2326854942  2.9559E-12  0.0000E+00  7.4837E-05  1.0000E-04
 mr-sdci # 17  2    -75.8276695560  4.7995E-09  6.8634E-10  4.8838E-05  1.0000E-04

 number of reference csfs (nref) is    12.  root number (iroot) is  1.

 eref      =    -76.028229007330   "relaxed" cnot**2         =   0.951084236895
 eci       =    -76.232685494247   deltae = eci - eref       =  -0.204456486916
 eci+dv1   =    -76.242686639326   dv1 = (1-cnot**2)*deltae  =  -0.010001145079
 eci+dv2   =    -76.243201013999   dv2 = dv1 / cnot**2       =  -0.010515519752
 eci+dv3   =    -76.243771167677   dv3 = dv1 / (2*cnot**2-1) =  -0.011085673430
 eci+pople =    -76.241361866407   ( 10e- scaled deltae )    =  -0.213132859076

 number of reference csfs (nref) is    12.  root number (iroot) is  2.

 eref      =    -75.584920712505   "relaxed" cnot**2         =   0.922464051950
 eci       =    -75.827669556030   deltae = eci - eref       =  -0.242748843525
 eci+dv1   =    -75.846491317751   dv1 = (1-cnot**2)*deltae  =  -0.018821761721
 eci+dv2   =    -75.848073344862   dv2 = dv1 / cnot**2       =  -0.020403788832
 eci+dv3   =    -75.849945725604   dv3 = dv1 / (2*cnot**2-1) =  -0.022276169574
 eci+pople =    -75.844838011931   ( 10e- scaled deltae )    =  -0.259917299426
