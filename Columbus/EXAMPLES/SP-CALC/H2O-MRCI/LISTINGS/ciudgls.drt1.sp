1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 9263
global arrays:       305679   (    2.33 MB)
vdisk:                    0   (    0.00 MB)
drt:                 484939   (    1.85 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 0,
  NROOT= 2,
   NBKITR= 1,
  RTOLCI=  0.000100,  0.000100,
  VOUT=10,
  DAVCOR=10,
  CSFPRN=10,
  NITER=  30,
  IDEN=   1,
   IVMODE=   3,
  NVCIMX = 16
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    2
 lvlprt =    0      nroot  =    2      noldv  =    0      noldhv =    0
 nunitv =    2      ntype  =    0      nbkitr =    1      niter  =   30
 ivmode =    3      vout   =   10      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264082666

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:02 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:02 2003

 core energy values from the integral file:
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  9.187211027960E+00

 drt header information:
  h2o                                                                            
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     6 nfct  =     0 nfvt  =     0
 nrow  =    38 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        292       12       70      105      105
 nvalwt,nvalw:      292       12       70      105      105
 ncsft:            9263
 total number of valid internal walks:     292
 nvalz,nvaly,nvalx,nvalw =       12      70     105     105

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   342   216    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     4791     5559     3156      936      138        0        0        0
 minbl4,minbl3,maxbl2   171   162   174
 maxbuf 32767
 number of external orbitals per symmetry block:   7   6   3   2
 nmsym   4 number of internal orbitals   6

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:02 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:02 2003
  h2o                                                                            
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:    12    70   105   105 total internal walks:     292
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 3 1

 setref:       12 references kept,
                0 references were marked as invalid, out of
               12 total.
 limcnvrt: found 12 valid internal walksout of  12
  walks (skipping trailing invalids)
  ... adding  12 segmentation marks segtype= 1
 limcnvrt: found 70 valid internal walksout of  70
  walks (skipping trailing invalids)
  ... adding  70 segmentation marks segtype= 2
 limcnvrt: found 105 valid internal walksout of  105
  walks (skipping trailing invalids)
  ... adding  105 segmentation marks segtype= 3
 limcnvrt: found 105 valid internal walksout of  105
  walks (skipping trailing invalids)
  ... adding  105 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x      40      48      33      32
 vertex w      58      48      33      32



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          12|        12|         0|        12|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       365|        12|        70|        12|         2|
 -------------------------------------------------------------------------------
  X 3         105|      4052|       377|       105|        82|         3|
 -------------------------------------------------------------------------------
  W 4         105|      4834|      4429|       105|       187|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      9263


 297 dimension of the ci-matrix ->>>      9263


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1     105      12       4052         12     105      12
     2  4   1    25      two-ext wz   2X  4 1     105      12       4834         12     105      12
     3  4   3    26      two-ext wx   2X  4 3     105     105       4834       4052     105     105
     4  2   1    11      one-ext yz   1X  2 1      70      12        365         12      70      12
     5  3   2    15      1ex3ex  yx   3X  3 2     105      70       4052        365     105      70
     6  4   2    16      1ex3ex  yw   3X  4 2     105      70       4834        365     105      70
     7  1   1     1      allint zz    OX  1 1      12      12         12         12      12      12
     8  2   2     5      0ex2ex yy    OX  2 2      70      70        365        365      70      70
     9  3   3     6      0ex2ex xx    OX  3 3     105     105       4052       4052     105     105
    10  4   4     7      0ex2ex ww    OX  4 4     105     105       4834       4834     105     105
    11  1   1    75      dg-024ext z  DG  1 1      12      12         12         12      12      12
    12  2   2    45      4exdg024 y   DG  2 2      70      70        365        365      70      70
    13  3   3    46      4exdg024 x   DG  3 3     105     105       4052       4052     105     105
    14  4   4    47      4exdg024 w   DG  4 4     105     105       4834       4834     105     105
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 5031 1500 280
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      2 vectors will be written to unit 11 beginning with logical record   1

            2 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       216 2x:         0 4x:         0
All internal counts: zz :       162 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       160    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       184    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension      12

    root           eigenvalues
    ----           ------------
       1         -76.0282863112
       2         -75.5863700138

 strefv generated    2 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  2 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    12)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              9263
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             2
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:         0 xx:         0 ww:         0
One-external counts: yz :       884 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:       192 wz:       227 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:         0    task #   4:       834
task #   5:         0    task #   6:         0    task #   7:       160    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       184    task #  12:       273
task #  13:       392    task #  14:       365    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:         0 xx:         0 ww:         0
One-external counts: yz :       884 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:       192 wz:       227 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:         0    task #   4:       834
task #   5:         0    task #   6:         0    task #   7:       160    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       184    task #  12:       273
task #  13:       392    task #  14:       365    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000
 ref:   2     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0282863112  0.0000E+00  2.5131E-01  9.9728E-01  1.0000E-04
 mr-sdci #  1  2    -75.5863700138  4.2633E-14  0.0000E+00  1.0034E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.00   0.00   0.00   0.00
    6   16    0   0.00   0.00   0.00   0.00
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.00   0.00   0.00   0.00
    9    6    0   0.00   0.00   0.00   0.00
   10    7    0   0.00   0.00   0.00   0.00
   11   75    0   0.01   0.01   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.01   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0500s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0282863112  0.0000E+00  2.5131E-01  9.9728E-01  1.0000E-04
 mr-sdci #  1  2    -75.5863700138  4.2633E-14  0.0000E+00  1.0034E+00  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              9263
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             2
 number of iterations:                                   30
 residual norm convergence criteria:               0.000100  0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97640895     0.00566797    -0.21585512
 ref:   2    -0.00493066     0.99998003     0.00395416

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.2262883907  1.9800E-01  6.6454E-03  1.6287E-01  1.0000E-04
 mr-sdci #  1  2    -75.5864108894  4.0876E-05  0.0000E+00  1.0038E+00  1.0000E-04
 mr-sdci #  1  3    -71.9771591283  8.1164E+01  0.0000E+00  1.7250E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.00
    5   15    0   0.05   0.02   0.00   0.05
    6   16    0   0.03   0.01   0.00   0.03
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.06   0.00   0.00   0.05
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3400s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.97579197    -0.00317560    -0.00947383     0.21847242
 ref:   2     0.00278096    -0.99992373    -0.01174430    -0.00262270

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.2323975099  6.1091E-03  2.6406E-04  3.1307E-02  1.0000E-04
 mr-sdci #  2  2    -75.5867922444  3.8136E-04  0.0000E+00  1.0038E+00  1.0000E-04
 mr-sdci #  2  3    -72.6700936674  6.9293E-01  0.0000E+00  1.7561E+00  1.0000E-04
 mr-sdci #  2  4    -71.9628792193  8.1150E+01  0.0000E+00  1.8849E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.06   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.02   0.00   0.04
    6   16    0   0.04   0.00   0.00   0.04
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.03   0.01   0.00   0.03
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3400s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97530167    -0.00704715    -0.09621151     0.09263603    -0.17578081
 ref:   2    -0.00413398    -0.99910101     0.03561421    -0.01894571    -0.01235977

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.2326663821  2.6887E-04  1.9452E-05  8.1427E-03  1.0000E-04
 mr-sdci #  3  2    -75.5904212788  3.6290E-03  0.0000E+00  9.9940E-01  1.0000E-04
 mr-sdci #  3  3    -73.7748587554  1.1048E+00  0.0000E+00  1.4535E+00  1.0000E-04
 mr-sdci #  3  4    -72.4721413359  5.0926E-01  0.0000E+00  1.3093E+00  1.0000E-04
 mr-sdci #  3  5    -71.3999237293  8.0587E+01  0.0000E+00  2.1963E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.01   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.00   0.00   0.04
    6   16    0   0.07   0.01   0.00   0.05
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.01   0.00   0.02
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.08   0.00   0.00   0.08
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.03   0.00   0.00   0.03
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.4100s 
time spent in multnx:                   0.3900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.4400s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97521456    -0.00625482     0.08152371     0.06437307    -0.09249446     0.17196574
 ref:   2    -0.00399941    -0.99912898    -0.01112014    -0.03817320     0.00708145     0.00970999

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.2326839866  1.7605E-05  1.3154E-06  2.2537E-03  1.0000E-04
 mr-sdci #  4  2    -75.5908500610  4.2878E-04  0.0000E+00  1.0007E+00  1.0000E-04
 mr-sdci #  4  3    -74.3123606584  5.3750E-01  0.0000E+00  1.1321E+00  1.0000E-04
 mr-sdci #  4  4    -73.0049273759  5.3279E-01  0.0000E+00  1.9455E+00  1.0000E-04
 mr-sdci #  4  5    -72.3642884749  9.6436E-01  0.0000E+00  1.2938E+00  1.0000E-04
 mr-sdci #  4  6    -71.3247186109  8.0512E+01  0.0000E+00  2.2814E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.01   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3500s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97520609     0.00609589     0.06554524    -0.04191245    -0.08271641    -0.10688400    -0.15689775
 ref:   2    -0.00398222     0.99915105    -0.00593636     0.03498536     0.01556356     0.00753477    -0.01109575

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.2326853564  1.3698E-06  1.1345E-07  6.1780E-04  1.0000E-04
 mr-sdci #  5  2    -75.5909249120  7.4851E-05  0.0000E+00  1.0010E+00  1.0000E-04
 mr-sdci #  5  3    -74.6044805507  2.9212E-01  0.0000E+00  9.7188E-01  1.0000E-04
 mr-sdci #  5  4    -73.0116567624  6.7294E-03  0.0000E+00  1.9518E+00  1.0000E-04
 mr-sdci #  5  5    -72.9308376662  5.6655E-01  0.0000E+00  2.0083E+00  1.0000E-04
 mr-sdci #  5  6    -72.3508777014  1.0262E+00  0.0000E+00  1.2143E+00  1.0000E-04
 mr-sdci #  5  7    -71.1636421411  8.0351E+01  0.0000E+00  2.1658E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3500s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97520783    -0.00609711    -0.03272919    -0.08700148    -0.03533986    -0.11780227    -0.00119473     0.15863277
 ref:   2    -0.00398921    -0.99908289     0.01331943     0.00645108     0.03373614     0.01253162     0.01469639     0.00934244

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2326854759  1.1954E-07  1.6926E-08  2.4819E-04  1.0000E-04
 mr-sdci #  6  2    -75.5911391795  2.1427E-04  0.0000E+00  1.0011E+00  1.0000E-04
 mr-sdci #  6  3    -74.9048182091  3.0034E-01  0.0000E+00  9.5233E-01  1.0000E-04
 mr-sdci #  6  4    -73.9783249719  9.6667E-01  0.0000E+00  1.3490E+00  1.0000E-04
 mr-sdci #  6  5    -73.0111726333  8.0335E-02  0.0000E+00  1.9546E+00  1.0000E-04
 mr-sdci #  6  6    -72.3694727186  1.8595E-02  0.0000E+00  1.2213E+00  1.0000E-04
 mr-sdci #  6  7    -72.0751096258  9.1147E-01  0.0000E+00  2.1119E+00  1.0000E-04
 mr-sdci #  6  8    -71.1362494735  8.0323E+01  0.0000E+00  2.2691E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.00
    3   26    0   0.06   0.00   0.00   0.06
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.05   0.01   0.00   0.04
    6   16    0   0.03   0.00   0.00   0.03
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.03   0.00   0.00   0.03
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.01
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.2900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3600s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97520855     0.00636309    -0.01317686     0.08124035    -0.02136604    -0.03281958    -0.13681644    -0.01353005
 ref:   2    -0.00397909     0.99731038    -0.04540614    -0.03492447     0.03765124    -0.01704390     0.00722507    -0.01692796

                ci   9
 ref:   1    -0.14737281
 ref:   2    -0.00527907

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -76.2326854938  1.7906E-08  0.0000E+00  8.1852E-05  1.0000E-04
 mr-sdci #  7  2    -75.5947446816  3.6055E-03  2.9722E-01  9.9578E-01  1.0000E-04
 mr-sdci #  7  3    -75.1130643373  2.0825E-01  0.0000E+00  8.2632E-01  1.0000E-04
 mr-sdci #  7  4    -74.3278830115  3.4956E-01  0.0000E+00  9.9325E-01  1.0000E-04
 mr-sdci #  7  5    -73.0312885334  2.0116E-02  0.0000E+00  1.9427E+00  1.0000E-04
 mr-sdci #  7  6    -72.4777016007  1.0823E-01  0.0000E+00  2.0551E+00  1.0000E-04
 mr-sdci #  7  7    -72.3557078686  2.8060E-01  0.0000E+00  1.2171E+00  1.0000E-04
 mr-sdci #  7  8    -72.0666983040  9.3045E-01  0.0000E+00  2.0588E+00  1.0000E-04
 mr-sdci #  7  9    -71.0405602125  8.0228E+01  0.0000E+00  2.2075E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.02   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3300s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3900s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97520858    -0.00669476     0.01314000     0.08123816     0.02109824    -0.00698130    -0.10660713     0.09231644
 ref:   2    -0.00397414    -0.96388142     0.03794279    -0.03613527    -0.10392242    -0.17364215     0.10393664     0.12595554

                ci   9         ci  10
 ref:   1     0.00852061    -0.14729976
 ref:   2    -0.02326398    -0.00333219

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -76.2326854938  3.6863E-11  0.0000E+00  8.1212E-05  1.0000E-04
 mr-sdci #  8  2    -75.8113672275  2.1662E-01  1.7514E-02  2.5094E-01  1.0000E-04
 mr-sdci #  8  3    -75.1131374729  7.3136E-05  0.0000E+00  8.2575E-01  1.0000E-04
 mr-sdci #  8  4    -74.3278904339  7.4224E-06  0.0000E+00  9.9380E-01  1.0000E-04
 mr-sdci #  8  5    -73.0586535890  2.7365E-02  0.0000E+00  1.8964E+00  1.0000E-04
 mr-sdci #  8  6    -72.5690815023  9.1380E-02  0.0000E+00  1.8781E+00  1.0000E-04
 mr-sdci #  8  7    -72.3950290943  3.9321E-02  0.0000E+00  1.8316E+00  1.0000E-04
 mr-sdci #  8  8    -72.3080054450  2.4131E-01  0.0000E+00  1.4610E+00  1.0000E-04
 mr-sdci #  8  9    -72.0543446859  1.0138E+00  0.0000E+00  2.0861E+00  1.0000E-04
 mr-sdci #  8 10    -71.0403803554  8.0228E+01  0.0000E+00  2.2070E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.00   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.01   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.01
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3600s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97520857    -0.00679414    -0.01298600    -0.08066347     0.02608263     0.01042668    -0.00344537     0.12302618
 ref:   2     0.00397528    -0.96247115    -0.02984458     0.02826432    -0.10207812     0.09750993    -0.12535195    -0.09162778

                ci   9         ci  10         ci  11
 ref:   1     0.06779818    -0.00707570     0.14718648
 ref:   2     0.16056055     0.04545367     0.00782882

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -76.2326854939  1.7678E-11  0.0000E+00  8.1042E-05  1.0000E-04
 mr-sdci #  9  2    -75.8261563331  1.4789E-02  1.2719E-03  6.4451E-02  1.0000E-04
 mr-sdci #  9  3    -75.1151367780  1.9993E-03  0.0000E+00  8.2522E-01  1.0000E-04
 mr-sdci #  9  4    -74.3336650352  5.7746E-03  0.0000E+00  9.8170E-01  1.0000E-04
 mr-sdci #  9  5    -73.1073157839  4.8662E-02  0.0000E+00  1.9626E+00  1.0000E-04
 mr-sdci #  9  6    -72.7647488719  1.9567E-01  0.0000E+00  1.6731E+00  1.0000E-04
 mr-sdci #  9  7    -72.5280248339  1.3300E-01  0.0000E+00  2.0453E+00  1.0000E-04
 mr-sdci #  9  8    -72.3860902280  7.8085E-02  0.0000E+00  1.7164E+00  1.0000E-04
 mr-sdci #  9  9    -72.2686616520  2.1432E-01  0.0000E+00  1.8544E+00  1.0000E-04
 mr-sdci #  9 10    -72.0420125769  1.0016E+00  0.0000E+00  2.1115E+00  1.0000E-04
 mr-sdci #  9 11    -71.0333407760  8.0221E+01  0.0000E+00  2.2102E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.00
    5   15    0   0.05   0.01   0.00   0.04
    6   16    0   0.03   0.01   0.00   0.03
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.06   0.00   0.00   0.05
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.2900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3700s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97520858    -0.00692114    -0.01343447    -0.08031116     0.00437220    -0.02397460     0.00794224     0.02580250
 ref:   2    -0.00397523    -0.96075206    -0.01811102     0.04307588     0.13538972     0.07414162     0.16166197     0.01968604

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.13874764     0.01253920     0.03886239     0.14159377
 ref:   2     0.00217930     0.04883928    -0.14390734     0.03615663

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1    -76.2326854939  1.3074E-12  0.0000E+00  8.0979E-05  1.0000E-04
 mr-sdci # 10  2    -75.8274004457  1.2441E-03  2.3192E-04  2.5618E-02  1.0000E-04
 mr-sdci # 10  3    -75.1236011519  8.4644E-03  0.0000E+00  8.0864E-01  1.0000E-04
 mr-sdci # 10  4    -74.3392913801  5.6263E-03  0.0000E+00  1.0096E+00  1.0000E-04
 mr-sdci # 10  5    -73.9342693578  8.2695E-01  0.0000E+00  1.6936E+00  1.0000E-04
 mr-sdci # 10  6    -73.0837444303  3.1900E-01  0.0000E+00  1.9288E+00  1.0000E-04
 mr-sdci # 10  7    -72.7223675507  1.9434E-01  0.0000E+00  1.5286E+00  1.0000E-04
 mr-sdci # 10  8    -72.4864854457  1.0040E-01  0.0000E+00  2.0345E+00  1.0000E-04
 mr-sdci # 10  9    -72.3535024413  8.4841E-02  0.0000E+00  1.2397E+00  1.0000E-04
 mr-sdci # 10 10    -72.0887799957  4.6767E-02  0.0000E+00  2.0558E+00  1.0000E-04
 mr-sdci # 10 11    -71.4536991487  4.2036E-01  0.0000E+00  2.1035E+00  1.0000E-04
 mr-sdci # 10 12    -71.0028935745  8.0190E+01  0.0000E+00  2.2067E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.05   0.01   0.00   0.05
    6   16    0   0.03   0.01   0.00   0.03
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.01   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3300s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3700s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97520854     0.00698324    -0.01361561    -0.00163629    -0.08026377     0.02796831    -0.00965783     0.01006537
 ref:   2     0.00397506     0.95964657     0.00075939    -0.13306246     0.04793824    -0.03132803    -0.15152174    -0.09909594

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.01923387     0.13883461     0.01542231     0.05283254     0.13652205
 ref:   2    -0.04270042     0.00248468     0.02180048    -0.14208712     0.05295847

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1    -76.2326854940  8.9756E-11  0.0000E+00  8.0352E-05  1.0000E-04
 mr-sdci # 11  2    -75.8276338363  2.3339E-04  2.7828E-05  1.0126E-02  1.0000E-04
 mr-sdci # 11  3    -75.1339971036  1.0396E-02  0.0000E+00  8.1420E-01  1.0000E-04
 mr-sdci # 11  4    -74.7391884453  3.9990E-01  0.0000E+00  1.0247E+00  1.0000E-04
 mr-sdci # 11  5    -74.3390190896  4.0475E-01  0.0000E+00  1.0164E+00  1.0000E-04
 mr-sdci # 11  6    -73.1287461290  4.5002E-02  0.0000E+00  1.9165E+00  1.0000E-04
 mr-sdci # 11  7    -72.8362724811  1.1390E-01  0.0000E+00  1.8818E+00  1.0000E-04
 mr-sdci # 11  8    -72.6082489210  1.2176E-01  0.0000E+00  1.5811E+00  1.0000E-04
 mr-sdci # 11  9    -72.4716996116  1.1820E-01  0.0000E+00  2.0391E+00  1.0000E-04
 mr-sdci # 11 10    -72.3527575029  2.6398E-01  0.0000E+00  1.2244E+00  1.0000E-04
 mr-sdci # 11 11    -72.0400258066  5.8633E-01  0.0000E+00  2.0569E+00  1.0000E-04
 mr-sdci # 11 12    -71.1639274742  1.6103E-01  0.0000E+00  2.2294E+00  1.0000E-04
 mr-sdci # 11 13    -70.9994964090  8.0187E+01  0.0000E+00  2.1983E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.06   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.00   0.00   0.04
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.03   0.00   0.00   0.03
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.02   0.01   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.040000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3700s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97520853     0.00700324     0.01270846    -0.00554436     0.08001350    -0.02597730    -0.01178879     0.00909486
 ref:   2     0.00397506     0.95951064    -0.02773609    -0.10922781    -0.05354624    -0.10426702     0.13942211    -0.08603174

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.02965312     0.09855285    -0.10011912     0.01250446     0.11973639    -0.07949460
 ref:   2     0.02051921    -0.03936040    -0.03444609    -0.02338195    -0.07666806    -0.12830902

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -76.2326854940  4.6214E-11  0.0000E+00  8.0468E-05  1.0000E-04
 mr-sdci # 12  2    -75.8276643344  3.0498E-05  4.5271E-06  3.8572E-03  1.0000E-04
 mr-sdci # 12  3    -75.1829801128  4.8983E-02  0.0000E+00  7.6333E-01  1.0000E-04
 mr-sdci # 12  4    -74.9139722770  1.7478E-01  0.0000E+00  8.0460E-01  1.0000E-04
 mr-sdci # 12  5    -74.3400257824  1.0067E-03  0.0000E+00  1.0271E+00  1.0000E-04
 mr-sdci # 12  6    -73.3931194521  2.6437E-01  0.0000E+00  1.7803E+00  1.0000E-04
 mr-sdci # 12  7    -73.0297681394  1.9350E-01  0.0000E+00  1.8574E+00  1.0000E-04
 mr-sdci # 12  8    -72.6114755259  3.2266E-03  0.0000E+00  1.6337E+00  1.0000E-04
 mr-sdci # 12  9    -72.4795371008  7.8375E-03  0.0000E+00  2.0582E+00  1.0000E-04
 mr-sdci # 12 10    -72.3951878215  4.2430E-02  0.0000E+00  1.4639E+00  1.0000E-04
 mr-sdci # 12 11    -72.2318092168  1.9178E-01  0.0000E+00  1.6560E+00  1.0000E-04
 mr-sdci # 12 12    -71.9937244708  8.2980E-01  0.0000E+00  2.1385E+00  1.0000E-04
 mr-sdci # 12 13    -71.0040101620  4.5138E-03  0.0000E+00  2.1423E+00  1.0000E-04
 mr-sdci # 12 14    -70.9965252747  8.0184E+01  0.0000E+00  2.1695E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.040000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3800s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97520853    -0.00700443     0.01060816     0.00921926    -0.07913230    -0.01934660     0.02269183     0.01721248
 ref:   2     0.00397515    -0.95949942    -0.03901701     0.08507964     0.06526443    -0.13954858     0.00168930    -0.14456392

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.03120281     0.08635320    -0.10838694     0.02291377    -0.01206284    -0.14013985    -0.02804267
 ref:   2     0.02103831    -0.03257502    -0.02942860    -0.03172700     0.00543586    -0.03940584     0.14488135

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1    -76.2326854941  1.3084E-10  0.0000E+00  7.6826E-05  1.0000E-04
 mr-sdci # 13  2    -75.8276687816  4.4472E-06  6.4295E-07  1.4452E-03  1.0000E-04
 mr-sdci # 13  3    -75.2234104285  4.0430E-02  0.0000E+00  6.7205E-01  1.0000E-04
 mr-sdci # 13  4    -74.9876431383  7.3671E-02  0.0000E+00  7.6857E-01  1.0000E-04
 mr-sdci # 13  5    -74.3454460420  5.4203E-03  0.0000E+00  1.0306E+00  1.0000E-04
 mr-sdci # 13  6    -73.7639571369  3.7084E-01  0.0000E+00  1.4793E+00  1.0000E-04
 mr-sdci # 13  7    -73.2777095860  2.4794E-01  0.0000E+00  1.8726E+00  1.0000E-04
 mr-sdci # 13  8    -72.7272452614  1.1577E-01  0.0000E+00  1.4874E+00  1.0000E-04
 mr-sdci # 13  9    -72.4797816597  2.4456E-04  0.0000E+00  2.0550E+00  1.0000E-04
 mr-sdci # 13 10    -72.4144214797  1.9234E-02  0.0000E+00  1.4778E+00  1.0000E-04
 mr-sdci # 13 11    -72.2355330404  3.7238E-03  0.0000E+00  1.6442E+00  1.0000E-04
 mr-sdci # 13 12    -72.0962871387  1.0256E-01  0.0000E+00  2.0671E+00  1.0000E-04
 mr-sdci # 13 13    -71.8094764943  8.0547E-01  0.0000E+00  1.9628E+00  1.0000E-04
 mr-sdci # 13 14    -70.9999758782  3.4506E-03  0.0000E+00  2.1967E+00  1.0000E-04
 mr-sdci # 13 15    -70.9009491525  8.0088E+01  0.0000E+00  2.2449E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.040000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3800s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97520851     0.00700636    -0.00837393     0.01133563    -0.04952313     0.06582966     0.02001872    -0.00893208
 ref:   2    -0.00397524     0.95950622     0.03164496     0.07164438     0.11582412     0.05021152    -0.05269379     0.17416267

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.02539494     0.07758788    -0.11073878    -0.00112142    -0.04578579     0.01195877     0.13715817     0.03838231
 ref:   2    -0.01597024    -0.02258691    -0.01520184    -0.03332760     0.00952302    -0.00534433     0.05175700    -0.13873323

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1    -76.2326854942  6.1135E-11  0.0000E+00  7.6785E-05  1.0000E-04
 mr-sdci # 14  2    -75.8276694356  6.5397E-07  1.0813E-07  5.7734E-04  1.0000E-04
 mr-sdci # 14  3    -75.2930735765  6.9663E-02  0.0000E+00  5.6002E-01  1.0000E-04
 mr-sdci # 14  4    -74.9992263341  1.1583E-02  0.0000E+00  7.5584E-01  1.0000E-04
 mr-sdci # 14  5    -74.4781833657  1.3274E-01  0.0000E+00  1.0757E+00  1.0000E-04
 mr-sdci # 14  6    -74.1944503193  4.3049E-01  0.0000E+00  1.0120E+00  1.0000E-04
 mr-sdci # 14  7    -73.3280927494  5.0383E-02  0.0000E+00  1.8210E+00  1.0000E-04
 mr-sdci # 14  8    -72.8224669771  9.5222E-02  0.0000E+00  1.5381E+00  1.0000E-04
 mr-sdci # 14  9    -72.4944436247  1.4662E-02  0.0000E+00  2.0358E+00  1.0000E-04
 mr-sdci # 14 10    -72.4277224646  1.3301E-02  0.0000E+00  1.6323E+00  1.0000E-04
 mr-sdci # 14 11    -72.2771779545  4.1645E-02  0.0000E+00  1.5905E+00  1.0000E-04
 mr-sdci # 14 12    -72.1032277127  6.9406E-03  0.0000E+00  2.0104E+00  1.0000E-04
 mr-sdci # 14 13    -72.0670965919  2.5762E-01  0.0000E+00  2.0485E+00  1.0000E-04
 mr-sdci # 14 14    -71.8094710672  8.0950E-01  0.0000E+00  1.9636E+00  1.0000E-04
 mr-sdci # 14 15    -70.9966778599  9.5729E-02  0.0000E+00  2.2135E+00  1.0000E-04
 mr-sdci # 14 16    -70.8782172552  8.0065E+01  0.0000E+00  2.2009E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.03
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.01   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3300s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3900s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97520851    -0.00700634    -0.00010568
 ref:   2    -0.00397532    -0.95950999     0.01978579

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1    -76.2326854942  4.5659E-11  0.0000E+00  7.4790E-05  1.0000E-04
 mr-sdci # 15  2    -75.8276695237  8.8083E-08  2.4160E-08  2.4148E-04  1.0000E-04
 mr-sdci # 15  3    -73.3758538017 -1.9172E+00  0.0000E+00  2.0651E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.02   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.01   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3300s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3400s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97520851    -0.00700637    -0.00012884    -0.00021065
 ref:   2    -0.00397532    -0.95950999    -0.00491040     0.02038431

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1    -76.2326854942  6.6365E-12  0.0000E+00  7.4817E-05  1.0000E-04
 mr-sdci # 16  2    -75.8276695512  2.7559E-08  4.6684E-09  1.2695E-04  1.0000E-04
 mr-sdci # 16  3    -74.7055287801  1.3297E+00  0.0000E+00  1.3100E+00  1.0000E-04
 mr-sdci # 16  4    -72.8036485934 -2.1956E+00  0.0000E+00  2.0806E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.06   0.00   0.00   0.06
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3500s 
time spent in multnx:                   0.3500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3700s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97520851    -0.00700646     0.00081744     0.00020810    -0.00169730
 ref:   2    -0.00397529    -0.95951117     0.01273109     0.02396038    -0.01205029

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1    -76.2326854942  2.9559E-12  0.0000E+00  7.4837E-05  1.0000E-04
 mr-sdci # 17  2    -75.8276695560  4.7995E-09  6.8634E-10  4.8838E-05  1.0000E-04
 mr-sdci # 17  3    -75.0704833439  3.6495E-01  0.0000E+00  7.0853E-01  1.0000E-04
 mr-sdci # 17  4    -72.8281221793  2.4474E-02  0.0000E+00  2.0642E+00  1.0000E-04
 mr-sdci # 17  5    -72.4293704504 -2.0488E+00  0.0000E+00  2.1089E+00  1.0000E-04


 mr-sdci  convergence criteria satisfied after 17 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1    -76.2326854942  2.9559E-12  0.0000E+00  7.4837E-05  1.0000E-04
 mr-sdci # 17  2    -75.8276695560  4.7995E-09  6.8634E-10  4.8838E-05  1.0000E-04
 mr-sdci # 17  3    -75.0704833439  3.6495E-01  0.0000E+00  7.0853E-01  1.0000E-04
 mr-sdci # 17  4    -72.8281221793  2.4474E-02  0.0000E+00  2.0642E+00  1.0000E-04
 mr-sdci # 17  5    -72.4293704504 -2.0488E+00  0.0000E+00  2.1089E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.232685494247
   ci vector at position   2 energy=  -75.827669556030

################END OF CIUDGINFO################


    2 of the   6 expansion vectors are transformed.
    2 of the   5 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.975208514)

information on vector: 1from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference  2(overlap=  0.959511175)

information on vector: 2from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2326854942

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     1    2    3   12   19    4

                                         symmetry   a1   a1   a1   b1   b2   a1 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.974616                        +-   +-   +-   +-   +-      
 z*  1  1       2  0.012776                        +-   +-   +-   +-        +- 
 z*  1  1       3  0.014569                        +-   +-   +-        +-   +- 
 z*  1  1       5  0.017046                        +-   +-        +-   +-   +- 
 z*  1  1       7 -0.016870                        +-   +     -   +-   +-   +- 
 z*  1  1       8  0.012436                        +-        +-   +-   +-   +- 
 y   1  1      16 -0.010584              1( a1 )   +-   +-   +-   +-         - 
 y   1  1      39 -0.018765              1( a1 )   +-   +-   +-        +-    - 
 y   1  1      81  0.014830              1( b2 )   +-   +-   +    +-    -    - 
 y   1  1      84 -0.023297              1( b1 )   +-   +-   +     -   +-    - 
 y   1  1      85 -0.017985              2( b1 )   +-   +-   +     -   +-    - 
 y   1  1      86  0.011678              3( b1 )   +-   +-   +     -   +-    - 
 y   1  1      92 -0.015752              1( a1 )   +-   +-        +-   +-    - 
 y   1  1      93  0.012401              2( a1 )   +-   +-        +-   +-    - 
 y   1  1     163 -0.029747              1( b2 )   +-   +    +-   +-    -    - 
 y   1  1     166  0.023414              1( b1 )   +-   +    +-    -   +-    - 
 y   1  1     167  0.010194              2( b1 )   +-   +    +-    -   +-    - 
 y   1  1     168 -0.023683              3( b1 )   +-   +    +-    -   +-    - 
 y   1  1     174  0.019291              1( a1 )   +-   +     -   +-   +-    - 
 y   1  1     175 -0.019087              2( a1 )   +-   +     -   +-   +-    - 
 y   1  1     190 -0.014287              1( a1 )   +-        +-   +-   +-    - 
 x   1  1     378  0.026142    1( b1 )   1( b2 )   +-   +-   +-    -    -      
 x   1  1     379  0.014852    2( b1 )   1( b2 )   +-   +-   +-    -    -      
 x   1  1     380 -0.026809    3( b1 )   1( b2 )   +-   +-   +-    -    -      
 x   1  1     385  0.013845    2( b1 )   2( b2 )   +-   +-   +-    -    -      
 x   1  1     396 -0.020251    1( a1 )   1( a2 )   +-   +-   +-    -    -      
 x   1  1     399  0.011371    4( a1 )   1( a2 )   +-   +-   +-    -    -      
 x   1  1     400  0.010923    5( a1 )   1( a2 )   +-   +-   +-    -    -      
 x   1  1     403 -0.010179    1( a1 )   2( a2 )   +-   +-   +-    -    -      
 x   1  1     407  0.011211    5( a1 )   2( a2 )   +-   +-   +-    -    -      
 x   1  1     409  0.012124    7( a1 )   2( a2 )   +-   +-   +-    -    -      
 x   1  1     491  0.017959    1( a1 )   1( b2 )   +-   +-    -   +-    -      
 x   1  1     492 -0.032014    2( a1 )   1( b2 )   +-   +-    -   +-    -      
 x   1  1     500 -0.011250    3( a1 )   2( b2 )   +-   +-    -   +-    -      
 x   1  1     501 -0.011653    4( a1 )   2( b2 )   +-   +-    -   +-    -      
 x   1  1     507 -0.010423    3( a1 )   3( b2 )   +-   +-    -   +-    -      
 x   1  1     508 -0.010491    4( a1 )   3( b2 )   +-   +-    -   +-    -      
 x   1  1     510 -0.013885    6( a1 )   3( b2 )   +-   +-    -   +-    -      
 x   1  1     513 -0.014727    2( b1 )   1( a2 )   +-   +-    -   +-    -      
 x   1  1     565  0.024667    2( a1 )   1( b1 )   +-   +-    -    -   +-      
 x   1  1     572  0.022630    2( a1 )   2( b1 )   +-   +-    -    -   +-      
 x   1  1     573  0.020013    3( a1 )   2( b1 )   +-   +-    -    -   +-      
 x   1  1     576  0.011475    6( a1 )   2( b1 )   +-   +-    -    -   +-      
 x   1  1     578  0.012934    1( a1 )   3( b1 )   +-   +-    -    -   +-      
 x   1  1     579 -0.018292    2( a1 )   3( b1 )   +-   +-    -    -   +-      
 x   1  1     593 -0.012222    2( a1 )   5( b1 )   +-   +-    -    -   +-      
 x   1  1     945 -0.015070    4( a1 )   1( b2 )   +-    -   +-   +-    -      
 w   1  1    4439  0.012877    4( a1 )   4( a1 )   +-   +-   +-   +-           
 w   1  1    4440 -0.010650    1( a1 )   5( a1 )   +-   +-   +-   +-           
 w   1  1    4455  0.010043    5( a1 )   7( a1 )   +-   +-   +-   +-           
 w   1  1    4479  0.047501    1( b2 )   1( b2 )   +-   +-   +-   +-           
 w   1  1    4484  0.012502    3( b2 )   3( b2 )   +-   +-   +-   +-           
 w   1  1    4486  0.010855    1( a2 )   2( a2 )   +-   +-   +-   +-           
 w   1  1    4487  0.012195    2( a2 )   2( a2 )   +-   +-   +-   +-           
 w   1  1    4488 -0.024577    1( b1 )   1( b2 )   +-   +-   +-   +     -      
 w   1  1    4489 -0.014932    2( b1 )   1( b2 )   +-   +-   +-   +     -      
 w   1  1    4490  0.026969    3( b1 )   1( b2 )   +-   +-   +-   +     -      
 w   1  1    4492  0.011203    5( b1 )   1( b2 )   +-   +-   +-   +     -      
 w   1  1    4495 -0.010777    2( b1 )   2( b2 )   +-   +-   +-   +     -      
 w   1  1    4568  0.026594    1( a1 )   1( a1 )   +-   +-   +-        +-      
 w   1  1    4596  0.026439    1( b1 )   1( b1 )   +-   +-   +-        +-      
 w   1  1    4597  0.027027    1( b1 )   2( b1 )   +-   +-   +-        +-      
 w   1  1    4598  0.033592    2( b1 )   2( b1 )   +-   +-   +-        +-      
 w   1  1    4599 -0.024833    1( b1 )   3( b1 )   +-   +-   +-        +-      
 w   1  1    4600 -0.011862    2( b1 )   3( b1 )   +-   +-   +-        +-      
 w   1  1    4601  0.020773    3( b1 )   3( b1 )   +-   +-   +-        +-      
 w   1  1    4607 -0.014477    2( b1 )   5( b1 )   +-   +-   +-        +-      
 w   1  1    4610  0.010131    5( b1 )   5( b1 )   +-   +-   +-        +-      
 w   1  1    4623  0.014390    1( a2 )   1( a2 )   +-   +-   +-        +-      
 w   1  1    4717 -0.013234    1( a1 )   1( b2 )   +-   +-   +    +-    -      
 w   1  1    4718  0.034505    2( a1 )   1( b2 )   +-   +-   +    +-    -      
 w   1  1    4739  0.012107    2( b1 )   1( a2 )   +-   +-   +    +-    -      
 w   1  1    4745  0.010118    2( b1 )   2( a2 )   +-   +-   +    +-    -      
 w   1  1    4808  0.021791    1( a1 )   1( b1 )   +-   +-   +     -   +-      
 w   1  1    4809 -0.018642    2( a1 )   1( b1 )   +-   +-   +     -   +-      
 w   1  1    4815  0.034973    1( a1 )   2( b1 )   +-   +-   +     -   +-      
 w   1  1    4816 -0.010280    2( a1 )   2( b1 )   +-   +-   +     -   +-      
 w   1  1    4819 -0.012270    5( a1 )   2( b1 )   +-   +-   +     -   +-      
 w   1  1    4822 -0.011329    1( a1 )   3( b1 )   +-   +-   +     -   +-      
 w   1  1    4823  0.019644    2( a1 )   3( b1 )   +-   +-   +     -   +-      
 w   1  1    4851 -0.013277    2( b2 )   1( a2 )   +-   +-   +     -   +-      
 w   1  1    5059  0.014483    1( a1 )   1( a1 )   +-   +-        +-   +-      
 w   1  1    5060 -0.016868    1( a1 )   2( a1 )   +-   +-        +-   +-      
 w   1  1    5061  0.032858    2( a1 )   2( a1 )   +-   +-        +-   +-      
 w   1  1    5064  0.014520    3( a1 )   3( a1 )   +-   +-        +-   +-      
 w   1  1    5066  0.016047    2( a1 )   4( a1 )   +-   +-        +-   +-      
 w   1  1    5079  0.011366    6( a1 )   6( a1 )   +-   +-        +-   +-      
 w   1  1    5088  0.017079    1( b1 )   2( b1 )   +-   +-        +-   +-      
 w   1  1    5089  0.023767    2( b1 )   2( b1 )   +-   +-        +-   +-      
 w   1  1    5098 -0.012682    2( b1 )   5( b1 )   +-   +-        +-   +-      
 w   1  1    5113  0.010390    3( b2 )   3( b2 )   +-   +-        +-   +-      
 w   1  1    5346  0.017504    1( a1 )   1( b2 )   +-   +    +-   +-    -      
 w   1  1    5348 -0.013707    3( a1 )   1( b2 )   +-   +    +-   +-    -      
 w   1  1    5349  0.020676    4( a1 )   1( b2 )   +-   +    +-   +-    -      
 w   1  1    5350 -0.016433    5( a1 )   1( b2 )   +-   +    +-   +-    -      
 w   1  1    5360 -0.010729    1( a1 )   3( b2 )   +-   +    +-   +-    -      
 w   1  1    5373  0.011077    1( b1 )   2( a2 )   +-   +    +-   +-    -      
 w   1  1    5437 -0.011273    1( a1 )   1( b1 )   +-   +    +-    -   +-      
 w   1  1    5439  0.012341    3( a1 )   1( b1 )   +-   +    +-    -   +-      
 w   1  1    5440 -0.011624    4( a1 )   1( b1 )   +-   +    +-    -   +-      
 w   1  1    5444 -0.020141    1( a1 )   2( b1 )   +-   +    +-    -   +-      
 w   1  1    5446  0.013658    3( a1 )   2( b1 )   +-   +    +-    -   +-      
 w   1  1    5450 -0.010151    7( a1 )   2( b1 )   +-   +    +-    -   +-      
 w   1  1    5451  0.014046    1( a1 )   3( b1 )   +-   +    +-    -   +-      
 w   1  1    5454  0.017180    4( a1 )   3( b1 )   +-   +    +-    -   +-      
 w   1  1    5480  0.011936    2( b2 )   1( a2 )   +-   +    +-    -   +-      
 w   1  1    5688 -0.018080    1( a1 )   1( a1 )   +-   +     -   +-   +-      
 w   1  1    5692 -0.012163    2( a1 )   3( a1 )   +-   +     -   +-   +-      
 w   1  1    5695  0.019279    2( a1 )   4( a1 )   +-   +     -   +-   +-      
 w   1  1    6227  0.013056    1( a1 )   1( a1 )   +-        +-   +-   +-      

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             109
     0.01> rq > 0.001            600
    0.001> rq > 0.0001          1904
   0.0001> rq > 0.00001         2296
  0.00001> rq > 0.000001        2959
 0.000001> rq                   1394
           all                  9263
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       216 2x:         0 4x:         0
All internal counts: zz :       162 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       160    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       184    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.974615779876     74.098566285369
     2     2      0.012776319838     -0.966963311519
     3     3      0.014568602197     -1.102853204676
     4     4     -0.009013372450      0.683597003189
     5     5      0.017046007762     -1.295288702891
     6     6      0.004660032380     -0.351598759433
     7     7     -0.016869750291      1.280004246396
     8     8      0.012436324533     -0.942366642098
     9     9      0.000081995923     -0.004158934800
    10    10      0.000133974892     -0.008861717074
    11    11     -0.000050875830      0.002424056095
    12    12      0.000197294662     -0.014595615930

 number of reference csfs (nref) is    12.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.028229007330   "relaxed" cnot**2         =   0.951084236895
 eci       =    -76.232685494247   deltae = eci - eref       =  -0.204456486916
 eci+dv1   =    -76.242686639326   dv1 = (1-cnot**2)*deltae  =  -0.010001145079
 eci+dv2   =    -76.243201013999   dv2 = dv1 / cnot**2       =  -0.010515519752
 eci+dv3   =    -76.243771167677   dv3 = dv1 / (2*cnot**2-1) =  -0.011085673430
 eci+pople =    -76.241361866407   ( 10e- scaled deltae )    =  -0.213132859076


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =       -75.8276695560

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     1    2    3   12   19    4

                                         symmetry   a1   a1   a1   b1   b2   a1 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.013624                        +-   +-   +-   +-   +-      
 z*  1  1       3 -0.014991                        +-   +-   +-        +-   +- 
 z*  1  1       4 -0.955147                        +-   +-   +    +-   +-    - 
 z*  1  1       5  0.083198                        +-   +-        +-   +-   +- 
 z*  1  1       6 -0.041835                        +-   +    +-   +-   +-    - 
 z*  1  1       7 -0.031387                        +-   +     -   +-   +-   +- 
 y   1  1      13  0.030171              1( b2 )   +-   +-   +-   +-    -      
 y   1  1      23 -0.045752              1( b1 )   +-   +-   +-    -   +-      
 y   1  1      24 -0.026166              2( b1 )   +-   +-   +-    -   +-      
 y   1  1      25  0.017408              3( b1 )   +-   +-   +-    -   +-      
 y   1  1      39  0.016895              1( a1 )   +-   +-   +-        +-    - 
 y   1  1      49  0.072048              1( a1 )   +-   +-    -   +-   +-      
 y   1  1      51 -0.016851              3( a1 )   +-   +-    -   +-   +-      
 y   1  1      52  0.027777              4( a1 )   +-   +-    -   +-   +-      
 y   1  1      53 -0.010356              5( a1 )   +-   +-    -   +-   +-      
 y   1  1      56 -0.018337              1( b2 )   +-   +-    -   +-   +     - 
 y   1  1      67  0.020483              2( b1 )   +-   +-    -   +    +-    - 
 y   1  1      68 -0.010702              3( b1 )   +-   +-    -   +    +-    - 
 y   1  1      81  0.060041              1( b2 )   +-   +-   +    +-    -    - 
 y   1  1      82  0.017510              2( b2 )   +-   +-   +    +-    -    - 
 y   1  1      83  0.021964              3( b2 )   +-   +-   +    +-    -    - 
 y   1  1      84 -0.090629              1( b1 )   +-   +-   +     -   +-    - 
 y   1  1      85 -0.053719              2( b1 )   +-   +-   +     -   +-    - 
 y   1  1      86  0.042826              3( b1 )   +-   +-   +     -   +-    - 
 y   1  1      88  0.023437              5( b1 )   +-   +-   +     -   +-    - 
 y   1  1      92 -0.037014              1( a1 )   +-   +-        +-   +-    - 
 y   1  1      93  0.065605              2( a1 )   +-   +-        +-   +-    - 
 y   1  1      95  0.026924              4( a1 )   +-   +-        +-   +-    - 
 y   1  1      97  0.020432              6( a1 )   +-   +-        +-   +-    - 
 y   1  1     108  0.014036              1( a1 )   +-    -   +-   +-   +-      
 y   1  1     140  0.020656              1( a1 )   +-    -   +    +-   +-    - 
 y   1  1     142 -0.016865              3( a1 )   +-    -   +    +-   +-    - 
 y   1  1     143  0.020271              4( a1 )   +-    -   +    +-   +-    - 
 y   1  1     144 -0.010453              5( a1 )   +-    -   +    +-   +-    - 
 y   1  1     163  0.012063              1( b2 )   +-   +    +-   +-    -    - 
 y   1  1     164  0.020869              2( b2 )   +-   +    +-   +-    -    - 
 y   1  1     165  0.022163              3( b2 )   +-   +    +-   +-    -    - 
 y   1  1     166 -0.019734              1( b1 )   +-   +    +-    -   +-    - 
 y   1  1     167 -0.038995              2( b1 )   +-   +    +-    -   +-    - 
 y   1  1     170  0.018882              5( b1 )   +-   +    +-    -   +-    - 
 y   1  1     171 -0.010882              6( b1 )   +-   +    +-    -   +-    - 
 y   1  1     174 -0.010073              1( a1 )   +-   +     -   +-   +-    - 
 y   1  1     175  0.025930              2( a1 )   +-   +     -   +-   +-    - 
 y   1  1     176  0.020044              3( a1 )   +-   +     -   +-   +-    - 
 y   1  1     177  0.014355              4( a1 )   +-   +     -   +-   +-    - 
 y   1  1     178  0.011270              5( a1 )   +-   +     -   +-   +-    - 
 y   1  1     179  0.017816              6( a1 )   +-   +     -   +-   +-    - 
 y   1  1     181  0.018779              1( b2 )   +-   +     -   +-    -   +- 
 y   1  1     184 -0.014138              1( b1 )   +-   +     -    -   +-   +- 
 y   1  1     186  0.014231              3( b1 )   +-   +     -    -   +-   +- 
 x   1  1     494 -0.011336    4( a1 )   1( b2 )   +-   +-    -   +-    -      
 x   1  1     512 -0.011822    1( b1 )   1( a2 )   +-   +-    -   +-    -      
 x   1  1     564  0.027893    1( a1 )   1( b1 )   +-   +-    -    -   +-      
 x   1  1     578 -0.015372    1( a1 )   3( b1 )   +-   +-    -    -   +-      
 x   1  1     612  0.010489    1( b1 )   1( b2 )   +-   +-    -    -   +     - 
 x   1  1     614 -0.012882    3( b1 )   1( b2 )   +-   +-    -    -   +     - 
 x   1  1     797  0.011148    1( b1 )   1( b2 )   +-   +-   +     -    -    - 
 x   1  1     799 -0.020603    3( b1 )   1( b2 )   +-   +-   +     -    -    - 
 x   1  1     815 -0.013605    1( a1 )   1( a2 )   +-   +-   +     -    -    - 
 x   1  1     826  0.010228    5( a1 )   2( a2 )   +-   +-   +     -    -    - 
 x   1  1     828  0.011610    7( a1 )   2( a2 )   +-   +-   +     -    -    - 
 x   1  1     829  0.011574    1( a1 )   1( b2 )   +-   +-        +-    -    - 
 x   1  1     830 -0.018948    2( a1 )   1( b2 )   +-   +-        +-    -    - 
 x   1  1     848 -0.010457    6( a1 )   3( b2 )   +-   +-        +-    -    - 
 x   1  1     870  0.010337    2( a1 )   2( b1 )   +-   +-         -   +-    - 
 x   1  1     871  0.011232    3( a1 )   2( b1 )   +-   +-         -   +-    - 
 x   1  1     877 -0.012221    2( a1 )   3( b1 )   +-   +-         -   +-    - 
 x   1  1    1484 -0.010740    4( a1 )   1( b2 )   +-    -   +    +-    -    - 
 w   1  1    4717  0.012352    1( a1 )   1( b2 )   +-   +-   +    +-    -      
 w   1  1    4759 -0.011539    4( a1 )   4( a1 )   +-   +-   +    +-         - 
 w   1  1    4775 -0.010475    5( a1 )   7( a1 )   +-   +-   +    +-         - 
 w   1  1    4799 -0.041691    1( b2 )   1( b2 )   +-   +-   +    +-         - 
 w   1  1    4803 -0.010106    2( b2 )   3( b2 )   +-   +-   +    +-         - 
 w   1  1    4804 -0.013154    3( b2 )   3( b2 )   +-   +-   +    +-         - 
 w   1  1    4806 -0.010721    1( a2 )   2( a2 )   +-   +-   +    +-         - 
 w   1  1    4807 -0.012731    2( a2 )   2( a2 )   +-   +-   +    +-         - 
 w   1  1    4808  0.017613    1( a1 )   1( b1 )   +-   +-   +     -   +-      
 w   1  1    4815  0.013777    1( a1 )   2( b1 )   +-   +-   +     -   +-      
 w   1  1    4858 -0.013146    3( b1 )   1( b2 )   +-   +-   +     -   +     - 
 w   1  1    4936  0.016712    1( b1 )   1( b2 )   +-   +-   +    +     -    - 
 w   1  1    4937  0.010034    2( b1 )   1( b2 )   +-   +-   +    +     -    - 
 w   1  1    4938 -0.021938    3( b1 )   1( b2 )   +-   +-   +    +     -    - 
 w   1  1    4940 -0.010313    5( b1 )   1( b2 )   +-   +-   +    +     -    - 
 w   1  1    4968 -0.019878    1( a1 )   1( a1 )   +-   +-   +         +-    - 
 w   1  1    4996 -0.017008    1( b1 )   1( b1 )   +-   +-   +         +-    - 
 w   1  1    4997 -0.020912    1( b1 )   2( b1 )   +-   +-   +         +-    - 
 w   1  1    4998 -0.028015    2( b1 )   2( b1 )   +-   +-   +         +-    - 
 w   1  1    4999  0.020833    1( b1 )   3( b1 )   +-   +-   +         +-    - 
 w   1  1    5000  0.011034    2( b1 )   3( b1 )   +-   +-   +         +-    - 
 w   1  1    5001 -0.021663    3( b1 )   3( b1 )   +-   +-   +         +-    - 
 w   1  1    5007  0.016388    2( b1 )   5( b1 )   +-   +-   +         +-    - 
 w   1  1    5010 -0.011303    5( b1 )   5( b1 )   +-   +-   +         +-    - 
 w   1  1    5023 -0.012399    1( a2 )   1( a2 )   +-   +-   +         +-    - 
 w   1  1    5060  0.010186    1( a1 )   2( a1 )   +-   +-        +-   +-      
 w   1  1    5087  0.021250    1( b1 )   1( b1 )   +-   +-        +-   +-      
 w   1  1    5088  0.025286    1( b1 )   2( b1 )   +-   +-        +-   +-      
 w   1  1    5089  0.010957    2( b1 )   2( b1 )   +-   +-        +-   +-      
 w   1  1    5091 -0.012183    2( b1 )   3( b1 )   +-   +-        +-   +-      
 w   1  1    5117  0.010152    1( a1 )   1( b2 )   +-   +-        +-   +     - 
 w   1  1    5118 -0.021373    2( a1 )   1( b2 )   +-   +-        +-   +     - 
 w   1  1    5208 -0.012022    1( a1 )   1( b1 )   +-   +-        +    +-    - 
 w   1  1    5209  0.011902    2( a1 )   1( b1 )   +-   +-        +    +-    - 
 w   1  1    5215 -0.017833    1( a1 )   2( b1 )   +-   +-        +    +-    - 
 w   1  1    5223 -0.013708    2( a1 )   3( b1 )   +-   +-        +    +-    - 
 w   1  1    5975  0.011440    1( a1 )   1( b2 )   +-   +    +    +-    -    - 
 w   1  1    5978  0.015623    4( a1 )   1( b2 )   +-   +    +    +-    -    - 
 w   1  1    5979 -0.013289    5( a1 )   1( b2 )   +-   +    +    +-    -    - 
 w   1  1    6015 -0.013854    1( a1 )   2( b1 )   +-   +    +     -   +-    - 
 w   1  1    6022  0.010478    1( a1 )   3( b1 )   +-   +    +     -   +-    - 
 w   1  1    6025  0.014038    4( a1 )   3( b1 )   +-   +    +     -   +-    - 
 w   1  1    6095  0.012149    2( a1 )   4( a1 )   +-   +         +-   +-    - 
 w   1  1    6514  0.011625    1( a1 )   1( a1 )   +-        +    +-   +-    - 
 w   1  1    6544  0.010243    2( b1 )   2( b1 )   +-        +    +-   +-    - 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             112
     0.01> rq > 0.001           1105
    0.001> rq > 0.0001          2319
   0.0001> rq > 0.00001         2450
  0.00001> rq > 0.000001        2434
 0.000001> rq                    842
           all                  9263
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       216 2x:         0 4x:         0
All internal counts: zz :       162 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       160    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       184    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.013624112062     -1.033355007424
     2     2     -0.006457378079      0.487349522692
     3     3     -0.014991072785      1.131823336428
     4     4     -0.955147072608     72.194050428298
     5     5      0.083198210112     -6.304682559873
     6     6     -0.041834839924      3.136627956136
     7     7     -0.031386986992      2.384075736493
     8     8      0.006947615608     -0.520017651887
     9     9     -0.000296121429      0.021884806789
    10    10      0.000123628367     -0.004068640337
    11    11     -0.000679817921      0.052838541345
    12    12     -0.000042148228      0.003229053523

 number of reference csfs (nref) is    12.  root number (iroot) is  2.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -75.584920712505   "relaxed" cnot**2         =   0.922464051950
 eci       =    -75.827669556030   deltae = eci - eref       =  -0.242748843525
 eci+dv1   =    -75.846491317751   dv1 = (1-cnot**2)*deltae  =  -0.018821761721
 eci+dv2   =    -75.848073344862   dv2 = dv1 / cnot**2       =  -0.020403788832
 eci+dv3   =    -75.849945725604   dv3 = dv1 / (2*cnot**2-1) =  -0.022276169574
 eci+pople =    -75.844838011931   ( 10e- scaled deltae )    =  -0.259917299426
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999629

 space required:

    space required for calls in multd2:
       onex           91
       allin           0
       diagon        290
    max.      ---------
       maxnex        290

    total core space usage:
       maxnex        290
    max k-seg       4834
    max k-ind        105
              ---------
       totmax      10168
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      86  DYX=     445  DYW=     430
   D0Z=      24  D0Y=     216  D0X=     377  D0W=     330
  DDZI=      66 DDYI=     390 DDXI=     570 DDWI=     540
  DDZE=       0 DDYE=      70 DDXE=     105 DDWE=     105
================================================================================


*****   symmetry block  A1    *****

 occupation numbers of nos
   1.9999206      1.9862223      1.9701468      0.0221301      0.0100982
   0.0051826      0.0042085      0.0010721      0.0006973      0.0004651
   0.0001011


*****   symmetry block  B1    *****

 occupation numbers of nos
   1.9681161      0.0233349      0.0053810      0.0010906      0.0006471
   0.0004706      0.0000386


*****   symmetry block  B2    *****

 occupation numbers of nos
   1.9758902      0.0147240      0.0041076      0.0005115


*****   symmetry block  A2    *****

 occupation numbers of nos
   0.0047485      0.0006944


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       2.000548   1.693745  -0.053138   0.001666   0.005005   0.001169
    O1_ p       0.000094   0.045638   1.451201   0.009663   0.004045   0.000034
    O1_ d       0.000000   0.000182   0.002029   0.000267   0.000309   0.002639
    H1_ s      -0.000169   0.194563   0.526106   0.010973   0.000331   0.000311
    H1_ p      -0.000552   0.052094   0.043949  -0.000439   0.000408   0.001029

   ao class       7A1        8A1        9A1       10A1       11A1 
    O1_ s       0.000167   0.000188   0.000035   0.000011   0.000009
    O1_ p       0.000237   0.000157   0.000022   0.000043   0.000007
    O1_ d       0.003355   0.000415   0.000008   0.000109   0.000002
    H1_ s       0.000058   0.000365   0.000214   0.000006   0.000038
    H1_ p       0.000391  -0.000054   0.000419   0.000297   0.000046

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.130036   0.012036   0.001823   0.000305   0.000003   0.000065
    O1_ d       0.008406   0.000430   0.002605   0.000372   0.000089   0.000000
    H1_ s       0.802088   0.011300   0.000567   0.000136   0.000278   0.000010
    H1_ p       0.027587  -0.000431   0.000387   0.000278   0.000278   0.000396

   ao class       7B1 
    O1_ p       0.000006
    O1_ d       0.000001
    H1_ s       0.000017
    H1_ p       0.000016

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    O1_ p       1.928055   0.013968   0.000098   0.000026
    O1_ d       0.001147   0.000043   0.003531   0.000070
    H1_ p       0.046688   0.000713   0.000478   0.000415

                        A2  partial gross atomic populations
   ao class       1A2        2A2 
    O1_ d       0.003562   0.000174
    H1_ p       0.001187   0.000521


                        gross atomic populations
     ao           O1_        H1_
      s         3.649405   1.547191
      p         4.597564   0.176099
      d         0.029742   0.000000
    total       8.276710   1.723290


 Total number of electrons:   10.00000000

NO coefficients and occupation numbers are written to nocoef_ci.2
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=      86  DYX=     445  DYW=     430
   D0Z=      24  D0Y=     216  D0X=     377  D0W=     330
  DDZI=      66 DDYI=     390 DDXI=     570 DDWI=     540
  DDZE=       0 DDYE=      70 DDXE=     105 DDWE=     105
================================================================================


*****   symmetry block  A1    *****

 occupation numbers of nos
   1.9999249      1.9834651      1.1194782      0.8717979      0.0109487
   0.0052670      0.0041834      0.0038278      0.0011777      0.0003055
   0.0002803


*****   symmetry block  B1    *****

 occupation numbers of nos
   1.9643562      0.0261941      0.0052441      0.0022650      0.0008489
   0.0003589      0.0000823


*****   symmetry block  B2    *****

 occupation numbers of nos
   1.9767948      0.0133439      0.0043522      0.0003806


*****   symmetry block  A2    *****

 occupation numbers of nos
   0.0044682      0.0006541


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       2.000748   1.431698  -0.107855   0.113670   0.001844   0.000438
    O1_ p       0.000013   0.056096   0.749045   0.103736   0.003076   0.002800
    O1_ d       0.000000   0.000491  -0.000017  -0.000076   0.000682   0.001207
    H1_ s      -0.000432   0.417185   0.459476   0.662663   0.005637   0.000034
    H1_ p      -0.000404   0.077996   0.018829  -0.008195  -0.000290   0.000788

   ao class       7A1        8A1        9A1       10A1       11A1 
    O1_ s       0.001877   0.000900   0.000216   0.000038   0.000000
    O1_ p       0.000215   0.000046   0.000168   0.000016   0.000031
    O1_ d       0.001801   0.002750   0.000358   0.000015   0.000058
    H1_ s      -0.000122  -0.000125   0.000324  -0.000046   0.000010
    H1_ p       0.000412   0.000256   0.000111   0.000282   0.000182

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.255168   0.011676   0.001318   0.000585   0.000117   0.000065
    O1_ d       0.003814   0.000388   0.002529   0.000038   0.000346   0.000011
    H1_ s       0.669074   0.014384   0.000942   0.001007   0.000214   0.000023
    H1_ p       0.036300  -0.000254   0.000455   0.000636   0.000172   0.000260

   ao class       7B1 
    O1_ p       0.000007
    O1_ d       0.000004
    H1_ s       0.000014
    H1_ p       0.000057

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    O1_ p       1.938380   0.012646   0.000074   0.000021
    O1_ d       0.000184   0.000031   0.003873   0.000041
    H1_ p       0.038230   0.000666   0.000405   0.000319

                        A2  partial gross atomic populations
   ao class       1A2        2A2 
    O1_ d       0.003583   0.000130
    H1_ p       0.000886   0.000524


                        gross atomic populations
     ao           O1_        H1_
      s         3.443574   2.230261
      p         4.135301   0.168624
      d         0.022240   0.000000
    total       7.601115   2.398885


 Total number of electrons:   10.00000000

