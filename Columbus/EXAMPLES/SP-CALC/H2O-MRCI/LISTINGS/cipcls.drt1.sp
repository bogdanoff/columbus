
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=   8000000 mem1=1108557832 ifirst=-264082350

 drt header information:
  h2o                                                                            
 nmot  =    24 niot  =     6 nfct  =     0 nfvt  =     0
 nrow  =    38 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:        292       12       70      105      105
 nvalwt,nvalw:      292       12       70      105      105
 ncsft:            9263
 map(*)=    19 20 21 24  1  2  3  4  5  6  7 22  8  9 10 11 12 13 23 14
            15 16 17 18
 mu(*)=      0  0  0  0  0  0
 syml(*) =   1  1  1  2  3  1
 rmo(*)=     1  2  3  1  1  4

 indx01:   292 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:02 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:02 2003
 energy computed by program ciudg.       hochtor2        Thu Jun  5 14:27:09 2003

 lenrec =    4096 lenci =      9263 ninfo =  6 nenrgy =  4 ntitle =  5

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       98% overlap
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.623268549425E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  7.483695792984E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)=  2.955857780762E-12, ietype=-2056, cnvginf energy of type: CI-D.E. 
==================================================================================
===================================ROOT # 2===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:02 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:02 2003
 energy computed by program ciudg.       hochtor2        Thu Jun  5 14:27:09 2003

 lenrec =    4096 lenci =      9263 ninfo =  6 nenrgy =  5 ntitle =  5

 Max. overlap with ref vector #        2
 Valid ci vector #        2
 Method:        0       96% overlap
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.582766955603E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  4.883805791190E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)=  4.799460384675E-09, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 5)=  6.863437193849E-10, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   3997672 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:     110 coefficients were selected.
 workspace: ncsfmx=    9263
 ncsfmx= 9263

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    9263 ncsft =    9263 ncsf =     110
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       6 ***
 1.5625E-02 <= |c| < 3.1250E-02      40 ********************
 7.8125E-03 <= |c| < 1.5625E-02      63 ********************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =      110 total stored =     110

 from the selected csfs,
 min(|csfvec(:)|) = 1.0043E-02    max(|csfvec(:)|) = 9.7462E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    9263 ncsft =    9263 ncsf =     110

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 

 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    1
 label          =  a1   a1   a1   b1   b2   a1 
 rmo(*)         =    1    2    3    1    1    4

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1 -0.97462 0.94988 z*                    333330
     4479  0.04750 0.00226 w           b2 :  2  3333300
     4815  0.03497 0.00122 w  a1 :  5  b1 :  3 12331230
     4718  0.03451 0.00119 w  a1 :  6  b2 :  2 12331320
     4598  0.03359 0.00113 w           b1 :  3  3333030
     5061  0.03286 0.00108 w           a1 :  6  3330330
      492 -0.03201 0.00102 x  a1 :  6  b2 :  2 11332320
      163 -0.02975 0.00088 y           b2 :  2  1313322
     4597  0.02703 0.00073 w  b1 :  2  b1 :  3 12333030
     4490  0.02697 0.00073 w  b1 :  4  b2 :  2 12333120
      380 -0.02681 0.00072 x  b1 :  4  b2 :  2 11333220
     4568  0.02659 0.00071 w           a1 :  5  3333030
     4596  0.02644 0.00070 w           b1 :  2  3333030
      378  0.02614 0.00068 x  b1 :  2  b2 :  2 11333220
     4599 -0.02483 0.00062 w  b1 :  2  b1 :  4 12333030
      565  0.02467 0.00061 x  a1 :  6  b1 :  2 11332230
     4488 -0.02458 0.00060 w  b1 :  2  b2 :  2 12333120
     5089  0.02377 0.00056 w           b1 :  3  3330330
      168 -0.02368 0.00056 y           b1 :  4  1313232
      166  0.02341 0.00055 y           b1 :  2  1313232
       84 -0.02330 0.00054 y           b1 :  2  1331232
      572  0.02263 0.00051 x  a1 :  6  b1 :  3 11332230
     4808  0.02179 0.00047 w  a1 :  5  b1 :  2 12331230
     4601  0.02077 0.00043 w           b1 :  4  3333030
     5349  0.02068 0.00043 w  a1 :  8  b2 :  2 12313320
      396 -0.02025 0.00041 x  a1 :  5  a2 :  1 11333220
     5444 -0.02014 0.00041 w  a1 :  5  b1 :  3 12313230
      573  0.02001 0.00040 x  a1 :  7  b1 :  3 11332230
     4823  0.01964 0.00039 w  a1 :  6  b1 :  4 12331230
      174  0.01929 0.00037 y           a1 :  5  1312332
     5695  0.01928 0.00037 w  a1 :  6  a1 :  8 12312330
      175 -0.01909 0.00036 y           a1 :  6  1312332
       39 -0.01876 0.00035 y           a1 :  5  1333032
     4809 -0.01864 0.00035 w  a1 :  6  b1 :  2 12331230
      579 -0.01829 0.00033 x  a1 :  6  b1 :  4 11332230
     5688 -0.01808 0.00033 w           a1 :  5  3312330
       85 -0.01798 0.00032 y           b1 :  3  1331232
      491  0.01796 0.00032 x  a1 :  5  b2 :  2 11332320
     5346  0.01750 0.00031 w  a1 :  5  b2 :  2 12313320
     5454  0.01718 0.00030 w  a1 :  8  b1 :  4 12313230
     5088  0.01708 0.00029 w  b1 :  2  b1 :  3 12330330
        5  0.01705 0.00029 z*                    330333
        7 -0.01687 0.00028 z*                    312333
     5060 -0.01687 0.00028 w  a1 :  5  a1 :  6 12330330
     5350 -0.01643 0.00027 w  a1 :  9  b2 :  2 12313320
     5066  0.01605 0.00026 w  a1 :  6  a1 :  8 12330330
       92 -0.01575 0.00025 y           a1 :  5  1330332
      945 -0.01507 0.00023 x  a1 :  8  b2 :  2 11323320
     4489 -0.01493 0.00022 w  b1 :  3  b2 :  2 12333120
      379  0.01485 0.00022 x  b1 :  3  b2 :  2 11333220
       81  0.01483 0.00022 y           b2 :  2  1331322
      513 -0.01473 0.00022 x  b1 :  3  a2 :  1 11332320
        3  0.01457 0.00021 z*                    333033
     5064  0.01452 0.00021 w           a1 :  7  3330330
     5059  0.01448 0.00021 w           a1 :  5  3330330
     4607 -0.01448 0.00021 w  b1 :  3  b1 :  6 12333030
     4623  0.01439 0.00021 w           a2 :  1  3333030
      190 -0.01429 0.00020 y           a1 :  5  1303332
     5451  0.01405 0.00020 w  a1 :  5  b1 :  4 12313230
      510 -0.01389 0.00019 x  a1 : 10  b2 :  4 11332320
      385  0.01385 0.00019 x  b1 :  3  b2 :  3 11333220
     5348 -0.01371 0.00019 w  a1 :  7  b2 :  2 12313320
     5446  0.01366 0.00019 w  a1 :  7  b1 :  3 12313230
     4851 -0.01328 0.00018 w  b2 :  3  a2 :  1 12331230
     4717 -0.01323 0.00018 w  a1 :  5  b2 :  2 12331320
     6227  0.01306 0.00017 w           a1 :  5  3303330
      578  0.01293 0.00017 x  a1 :  5  b1 :  4 11332230
     4439  0.01288 0.00017 w           a1 :  8  3333300
        2  0.01278 0.00016 z*                    333303
     5098 -0.01268 0.00016 w  b1 :  3  b1 :  6 12330330
     4484  0.01250 0.00016 w           b2 :  4  3333300
        8  0.01244 0.00015 z*                    303333
       93  0.01240 0.00015 y           a1 :  6  1330332
     5439  0.01234 0.00015 w  a1 :  7  b1 :  2 12313230
     4819 -0.01227 0.00015 w  a1 :  9  b1 :  3 12331230
      593 -0.01222 0.00015 x  a1 :  6  b1 :  6 11332230
     4487  0.01220 0.00015 w           a2 :  2  3333300
     5692 -0.01216 0.00015 w  a1 :  6  a1 :  7 12312330
      409  0.01212 0.00015 x  a1 : 11  a2 :  2 11333220
     4739  0.01211 0.00015 w  b1 :  3  a2 :  1 12331320
     5480  0.01194 0.00014 w  b2 :  3  a2 :  1 12313230
     4600 -0.01186 0.00014 w  b1 :  3  b1 :  4 12333030
       86  0.01168 0.00014 y           b1 :  4  1331232
      501 -0.01165 0.00014 x  a1 :  8  b2 :  3 11332320
     5440 -0.01162 0.00014 w  a1 :  8  b1 :  2 12313230
      576  0.01147 0.00013 x  a1 : 10  b1 :  3 11332230
      399  0.01137 0.00013 x  a1 :  8  a2 :  1 11333220
     5079  0.01137 0.00013 w           a1 : 10  3330330
     4822 -0.01133 0.00013 w  a1 :  5  b1 :  4 12331230
     5437 -0.01127 0.00013 w  a1 :  5  b1 :  2 12313230
      500 -0.01125 0.00013 x  a1 :  7  b2 :  3 11332320
      407  0.01121 0.00013 x  a1 :  9  a2 :  2 11333220
     4492  0.01120 0.00013 w  b1 :  6  b2 :  2 12333120
     5373  0.01108 0.00012 w  b1 :  2  a2 :  2 12313320
      400  0.01092 0.00012 x  a1 :  9  a2 :  1 11333220
     4486  0.01085 0.00012 w  a2 :  1  a2 :  2 12333300
     4495 -0.01078 0.00012 w  b1 :  3  b2 :  3 12333120
     5360 -0.01073 0.00012 w  a1 :  5  b2 :  4 12313320
     4440 -0.01065 0.00011 w  a1 :  5  a1 :  9 12333300
       16 -0.01058 0.00011 y           a1 :  5  1333302
      508 -0.01049 0.00011 x  a1 :  8  b2 :  4 11332320
      507 -0.01042 0.00011 x  a1 :  7  b2 :  4 11332320
     5113  0.01039 0.00011 w           b2 :  4  3330330
     4816 -0.01028 0.00011 w  a1 :  6  b1 :  3 12331230
      167  0.01019 0.00010 y           b1 :  3  1313232
      403 -0.01018 0.00010 x  a1 :  5  a2 :  2 11333220
     5450 -0.01015 0.00010 w  a1 : 11  b1 :  3 12313230
     4610  0.01013 0.00010 w           b1 :  6  3333030
     4745  0.01012 0.00010 w  b1 :  3  a2 :  2 12331320
     4455  0.01004 0.00010 w  a1 :  9  a1 : 11 12333300
          110 csfs were printed in this range.

================================================================================
===================================VECTOR # 2===================================
================================================================================


 rdcivnew:     113 coefficients were selected.
 workspace: ncsfmx=    9263
 ncsfmx= 9263

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    9263 ncsft =    9263 ncsf =     113
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       4 **
 3.1250E-02 <= |c| < 6.2500E-02       9 *****
 1.5625E-02 <= |c| < 3.1250E-02      41 *********************
 7.8125E-03 <= |c| < 1.5625E-02      58 *****************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =      113 total stored =     113

 from the selected csfs,
 min(|csfvec(:)|) = 1.0034E-02    max(|csfvec(:)|) = 9.5515E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    9263 ncsft =    9263 ncsf =     113

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 

 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    1
 label          =  a1   a1   a1   b1   b2   a1 
 rmo(*)         =    1    2    3    1    1    4

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        4 -0.95515 0.91231 z*                    331332
       84 -0.09063 0.00821 y           b1 :  2  1331232
        5  0.08320 0.00692 z*                    330333
       49  0.07205 0.00519 y           a1 :  5  1332330
       93  0.06561 0.00430 y           a1 :  6  1330332
       81  0.06004 0.00360 y           b2 :  2  1331322
       85 -0.05372 0.00289 y           b1 :  3  1331232
       23 -0.04575 0.00209 y           b1 :  2  1333230
       86  0.04283 0.00183 y           b1 :  4  1331232
        6 -0.04183 0.00175 z*                    313332
     4799 -0.04169 0.00174 w           b2 :  2  3331302
      167 -0.03899 0.00152 y           b1 :  3  1313232
       92 -0.03701 0.00137 y           a1 :  5  1330332
        7 -0.03139 0.00099 z*                    312333
       13  0.03017 0.00091 y           b2 :  2  1333320
     4998 -0.02802 0.00078 w           b1 :  3  3331032
      564  0.02789 0.00078 x  a1 :  5  b1 :  2 11332230
       52  0.02778 0.00077 y           a1 :  8  1332330
       95  0.02692 0.00072 y           a1 :  8  1330332
       24 -0.02617 0.00068 y           b1 :  3  1333230
      175  0.02593 0.00067 y           a1 :  6  1312332
     5088  0.02529 0.00064 w  b1 :  2  b1 :  3 12330330
       88  0.02344 0.00055 y           b1 :  6  1331232
      165  0.02216 0.00049 y           b2 :  4  1313322
       83  0.02196 0.00048 y           b2 :  4  1331322
     4938 -0.02194 0.00048 w  b1 :  4  b2 :  2 12331122
     5001 -0.02166 0.00047 w           b1 :  4  3331032
     5118 -0.02137 0.00046 w  a1 :  6  b2 :  2 12330312
     5087  0.02125 0.00045 w           b1 :  2  3330330
     4997 -0.02091 0.00044 w  b1 :  2  b1 :  3 12331032
      164  0.02087 0.00044 y           b2 :  3  1313322
     4999  0.02083 0.00043 w  b1 :  2  b1 :  4 12331032
      140  0.02066 0.00043 y           a1 :  5  1321332
      799 -0.02060 0.00042 x  b1 :  4  b2 :  2 11331222
       67  0.02048 0.00042 y           b1 :  3  1332132
       97  0.02043 0.00042 y           a1 : 10  1330332
      143  0.02027 0.00041 y           a1 :  8  1321332
      176  0.02004 0.00040 y           a1 :  7  1312332
     4968 -0.01988 0.00040 w           a1 :  5  3331032
      166 -0.01973 0.00039 y           b1 :  2  1313232
      830 -0.01895 0.00036 x  a1 :  6  b2 :  2 11330322
      170  0.01888 0.00036 y           b1 :  6  1313232
      181  0.01878 0.00035 y           b2 :  2  1312323
       56 -0.01834 0.00034 y           b2 :  2  1332312
     5215 -0.01783 0.00032 w  a1 :  5  b1 :  3 12330132
      179  0.01782 0.00032 y           a1 : 10  1312332
     4808  0.01761 0.00031 w  a1 :  5  b1 :  2 12331230
       82  0.01751 0.00031 y           b2 :  3  1331322
       25  0.01741 0.00030 y           b1 :  4  1333230
     4996 -0.01701 0.00029 w           b1 :  2  3331032
       39  0.01689 0.00029 y           a1 :  5  1333032
      142 -0.01686 0.00028 y           a1 :  7  1321332
       51 -0.01685 0.00028 y           a1 :  7  1332330
     4936  0.01671 0.00028 w  b1 :  2  b2 :  2 12331122
     5007  0.01639 0.00027 w  b1 :  3  b1 :  6 12331032
     5978  0.01562 0.00024 w  a1 :  8  b2 :  2 12311322
      578 -0.01537 0.00024 x  a1 :  5  b1 :  4 11332230
        3 -0.01499 0.00022 z*                    333033
      177  0.01435 0.00021 y           a1 :  8  1312332
      186  0.01423 0.00020 y           b1 :  4  1312233
      184 -0.01414 0.00020 y           b1 :  2  1312233
     6025  0.01404 0.00020 w  a1 :  8  b1 :  4 12311232
      108  0.01404 0.00020 y           a1 :  5  1323330
     6015 -0.01385 0.00019 w  a1 :  5  b1 :  3 12311232
     4815  0.01378 0.00019 w  a1 :  5  b1 :  3 12331230
     5223 -0.01371 0.00019 w  a1 :  6  b1 :  4 12330132
        1  0.01362 0.00019 z*                    333330
      815 -0.01360 0.00019 x  a1 :  5  a2 :  1 11331222
     5979 -0.01329 0.00018 w  a1 :  9  b2 :  2 12311322
     4804 -0.01315 0.00017 w           b2 :  4  3331302
     4858 -0.01315 0.00017 w  b1 :  4  b2 :  2 12331212
      614 -0.01288 0.00017 x  b1 :  4  b2 :  2 11332212
     4807 -0.01273 0.00016 w           a2 :  2  3331302
     5023 -0.01240 0.00015 w           a2 :  1  3331032
     4717  0.01235 0.00015 w  a1 :  5  b2 :  2 12331320
      877 -0.01222 0.00015 x  a1 :  6  b1 :  4 11330232
     5091 -0.01218 0.00015 w  b1 :  3  b1 :  4 12330330
     6095  0.01215 0.00015 w  a1 :  6  a1 :  8 12310332
      163  0.01206 0.00015 y           b2 :  2  1313322
     5208 -0.01202 0.00014 w  a1 :  5  b1 :  2 12330132
     5209  0.01190 0.00014 w  a1 :  6  b1 :  2 12330132
      512 -0.01182 0.00014 x  b1 :  2  a2 :  1 11332320
     6514  0.01163 0.00014 w           a1 :  5  3301332
      828  0.01161 0.00013 x  a1 : 11  a2 :  2 11331222
      829  0.01157 0.00013 x  a1 :  5  b2 :  2 11330322
     4759 -0.01154 0.00013 w           a1 :  8  3331302
     5975  0.01144 0.00013 w  a1 :  5  b2 :  2 12311322
      494 -0.01134 0.00013 x  a1 :  8  b2 :  2 11332320
     5010 -0.01130 0.00013 w           b1 :  6  3331032
      178  0.01127 0.00013 y           a1 :  9  1312332
      871  0.01123 0.00013 x  a1 :  7  b1 :  3 11330232
      797  0.01115 0.00012 x  b1 :  2  b2 :  2 11331222
     5000  0.01103 0.00012 w  b1 :  3  b1 :  4 12331032
     5089  0.01096 0.00012 w           b1 :  3  3330330
      171 -0.01088 0.00012 y           b1 :  7  1313232
     1484 -0.01074 0.00012 x  a1 :  8  b2 :  2 11321322
     4806 -0.01072 0.00011 w  a2 :  1  a2 :  2 12331302
       68 -0.01070 0.00011 y           b1 :  4  1332132
      612  0.01049 0.00011 x  b1 :  2  b2 :  2 11332212
     6022  0.01048 0.00011 w  a1 :  5  b1 :  4 12311232
     4775 -0.01047 0.00011 w  a1 :  9  a1 : 11 12331302
      848 -0.01046 0.00011 x  a1 : 10  b2 :  4 11330322
      144 -0.01045 0.00011 y           a1 :  9  1321332
       53 -0.01036 0.00011 y           a1 :  9  1332330
      870  0.01034 0.00011 x  a1 :  6  b1 :  3 11330232
     4940 -0.01031 0.00011 w  b1 :  6  b2 :  2 12331122
     6544  0.01024 0.00010 w           b1 :  3  3301332
      826  0.01023 0.00010 x  a1 :  9  a2 :  2 11331222
     5060  0.01019 0.00010 w  a1 :  5  a1 :  6 12330330
     5117  0.01015 0.00010 w  a1 :  5  b2 :  2 12330312
     4803 -0.01011 0.00010 w  b2 :  3  b2 :  4 12331302
      174 -0.01007 0.00010 y           a1 :  5  1312332
     4937  0.01003 0.00010 w  b1 :  3  b2 :  2 12331122
          113 csfs were printed in this range.
