1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 1311
global arrays:        43263   (    0.33 MB)
vdisk:                    0   (    0.00 MB)
drt:                 252658   (    0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 0,
   RTOLCI=0.00010000,
   VOUT  = 1,
   NBKITR= 1,
   NITER= 20,
  IDEN=   1,
   IVMODE=   3,
   NVCIMX = 16
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   20
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =   1      ctol   = 1.00E-02  davcor  =   1


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264082618

 integral file titles:
 h2o pvdz                                                                        
 aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:55 2003
 cidrt_title                                                                     
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:55 2003

 core energy values from the integral file:
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.903692770082E+01, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -6.984971667286E+01

 drt header information:
 cidrt_title                                                                     
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            1311
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   380   190    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     5955     5445     2496      618       66        0        0        0
 minbl4,minbl3,maxbl2   195   195   198
 maxbuf 32767
 number of external orbitals per symmetry block:   8   2   6   3
 nmsym   4 number of internal orbitals   5

 formula file title:
 h2o pvdz                                                                        
 aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:55 2003
 cidrt_title                                                                     
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:55 2003
 cidrt_title                                                                     
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 3 1 4

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x      47      34      54      36
 vertex w      66      34      54      36



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        33|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|       445|        34|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|       832|       479|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      1311


 297 dimension of the ci-matrix ->>>      1311


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1        445          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1        832          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10        832        445      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         33          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5        445         33      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5        832         33      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         33         33       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10        445        445      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15        832        832      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         33         33       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10        445        445      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15        832        832      15      15
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 185 88 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         3 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:         2    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

    root           eigenvalues
    ----           ------------
       1         -76.0267601011

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     1)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1311
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       185 2x:        88 4x:        30
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :        13 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:        10 wz:        15 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:         0    task #   4:        12
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:         2    task #  12:        17
task #  13:        35    task #  14:        51    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0267601011 -5.3291E-15  2.5326E-01  9.9884E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.00   0.00   0.00   0.00
    6   16    0   0.00   0.00   0.00   0.00
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.00   0.00   0.00   0.00
    9    6    0   0.00   0.00   0.00   0.00
   10    7    0   0.00   0.00   0.00   0.00
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0400s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0500s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0267601011 -5.3291E-15  2.5326E-01  9.9884E-01  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1311
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       185 2x:        88 4x:        30
All internal counts: zz :         0 yy:         7 xx:        26 ww:        59
One-external counts: yz :        13 yx:        94 yw:       127
Two-external counts: yy :        23 ww:        93 xx:        62 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        55 yw:        70

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        12
task #   5:        87    task #   6:       120    task #   7:        -1    task #   8:         6
task #   9:        23    task #  10:        56    task #  11:         2    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97616924    -0.21701065

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.2257785482  1.9902E-01  6.4055E-03  1.5663E-01  1.0000E-04
 mr-sdci #  1  2    -71.9997616038  2.1500E+00  0.0000E+00  1.7212E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1300s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       185 2x:        88 4x:        30
All internal counts: zz :         0 yy:         7 xx:        26 ww:        59
One-external counts: yz :        13 yx:        94 yw:       127
Two-external counts: yy :        23 ww:        93 xx:        62 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        55 yw:        70

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        12
task #   5:        87    task #   6:       120    task #   7:        -1    task #   8:         6
task #   9:        23    task #  10:        56    task #  11:         2    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97558659     0.01000293    -0.21938720

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.2317276139  5.9491E-03  2.3226E-04  2.8782E-02  1.0000E-04
 mr-sdci #  2  2    -72.9379300652  9.3817E-01  0.0000E+00  1.7061E+00  1.0000E-04
 mr-sdci #  2  3    -71.9800179490  2.1303E+00  0.0000E+00  1.8932E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       185 2x:        88 4x:        30
All internal counts: zz :         0 yy:         7 xx:        26 ww:        59
One-external counts: yz :        13 yx:        94 yw:       127
Two-external counts: yy :        23 ww:        93 xx:        62 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        55 yw:        70

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        12
task #   5:        87    task #   6:       120    task #   7:        -1    task #   8:         6
task #   9:        23    task #  10:        56    task #  11:         2    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97510011     0.09647724     0.04948920     0.19344957

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.2319684910  2.4088E-04  1.4882E-05  6.5159E-03  1.0000E-04
 mr-sdci #  3  2    -73.9232902583  9.8536E-01  0.0000E+00  1.2842E+00  1.0000E-04
 mr-sdci #  3  3    -72.8305583503  8.5054E-01  0.0000E+00  1.4907E+00  1.0000E-04
 mr-sdci #  3  4    -71.5452755215  1.6956E+00  0.0000E+00  2.1605E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.01   0.00   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       185 2x:        88 4x:        30
All internal counts: zz :         0 yy:         7 xx:        26 ww:        59
One-external counts: yz :        13 yx:        94 yw:       127
Two-external counts: yy :        23 ww:        93 xx:        62 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        55 yw:        70

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        12
task #   5:        87    task #   6:       120    task #   7:        -1    task #   8:         6
task #   9:        23    task #  10:        56    task #  11:         2    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97502718     0.05616328     0.08278773    -0.09102085     0.17615075

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.2319815449  1.3054E-05  8.0176E-07  1.6385E-03  1.0000E-04
 mr-sdci #  4  2    -74.5188567316  5.9557E-01  0.0000E+00  1.0585E+00  1.0000E-04
 mr-sdci #  4  3    -73.8274583999  9.9690E-01  0.0000E+00  1.4850E+00  1.0000E-04
 mr-sdci #  4  4    -72.4654137378  9.2014E-01  0.0000E+00  1.2283E+00  1.0000E-04
 mr-sdci #  4  5    -71.3290872606  1.4794E+00  0.0000E+00  2.3082E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.01
   10    7    0   0.03   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1400s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       185 2x:        88 4x:        30
All internal counts: zz :         0 yy:         7 xx:        26 ww:        59
One-external counts: yz :        13 yx:        94 yw:       127
Two-external counts: yy :        23 ww:        93 xx:        62 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        55 yw:        70

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        12
task #   5:        87    task #   6:       120    task #   7:        -1    task #   8:         6
task #   9:        23    task #  10:        56    task #  11:         2    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97502069     0.06638699     0.02818410     0.09708278     0.09136448     0.16235929

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.2319823359  7.9102E-07  3.0083E-08  3.0800E-04  1.0000E-04
 mr-sdci #  5  2    -74.5681757883  4.9319E-02  0.0000E+00  8.6143E-01  1.0000E-04
 mr-sdci #  5  3    -74.2934275629  4.6597E-01  0.0000E+00  1.2513E+00  1.0000E-04
 mr-sdci #  5  4    -72.8519059544  3.8649E-01  0.0000E+00  1.8533E+00  1.0000E-04
 mr-sdci #  5  5    -72.4654093854  1.1363E+00  0.0000E+00  1.2252E+00  1.0000E-04
 mr-sdci #  5  6    -71.1846352408  1.3349E+00  0.0000E+00  2.1658E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.02   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1400s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       185 2x:        88 4x:        30
All internal counts: zz :         0 yy:         7 xx:        26 ww:        59
One-external counts: yz :        13 yx:        94 yw:       127
Two-external counts: yy :        23 ww:        93 xx:        62 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        55 yw:        70

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        12
task #   5:        87    task #   6:       120    task #   7:        -1    task #   8:         6
task #   9:        23    task #  10:        56    task #  11:         2    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97502167     0.04142297     0.04687932    -0.05772259    -0.10854234     0.06796575     0.16027019

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2319823658  2.9864E-08  1.9255E-09  7.7720E-05  1.0000E-04
 mr-sdci #  6  2    -74.7288768040  1.6070E-01  0.0000E+00  8.6878E-01  1.0000E-04
 mr-sdci #  6  3    -74.3093970066  1.5969E-02  0.0000E+00  1.1721E+00  1.0000E-04
 mr-sdci #  6  4    -74.0071094644  1.1552E+00  0.0000E+00  1.3264E+00  1.0000E-04
 mr-sdci #  6  5    -72.6825880035  2.1718E-01  0.0000E+00  1.5961E+00  1.0000E-04
 mr-sdci #  6  6    -72.3457811408  1.1611E+00  0.0000E+00  1.6591E+00  1.0000E-04
 mr-sdci #  6  7    -71.1228380629  1.2731E+00  0.0000E+00  2.2754E+00  1.0000E-04


 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2319823658  2.9864E-08  1.9255E-09  7.7720E-05  1.0000E-04
 mr-sdci #  6  2    -74.7288768040  1.6070E-01  0.0000E+00  8.6878E-01  1.0000E-04
 mr-sdci #  6  3    -74.3093970066  1.5969E-02  0.0000E+00  1.1721E+00  1.0000E-04
 mr-sdci #  6  4    -74.0071094644  1.1552E+00  0.0000E+00  1.3264E+00  1.0000E-04
 mr-sdci #  6  5    -72.6825880035  2.1718E-01  0.0000E+00  1.5961E+00  1.0000E-04
 mr-sdci #  6  6    -72.3457811408  1.1611E+00  0.0000E+00  1.6591E+00  1.0000E-04
 mr-sdci #  6  7    -71.1228380629  1.2731E+00  0.0000E+00  2.2754E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.231982365803

################END OF CIUDGINFO################


    1 of the   8 expansion vectors are transformed.
    1 of the   7 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.975021668)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2319823658

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2   14    3   21

                                         symmetry   a1   a1   b1   a1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.975022                        +-   +-   +-   +-   +- 
 x   1  1      37  0.014689    1( a2 )   2( b1 )   +-   +-   +-    -    - 
 x   1  1      47 -0.014230    1( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      48  0.017965    2( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      49 -0.032036    3( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      58 -0.011238    4( a1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      59 -0.011639    5( a1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      66 -0.010431    4( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      67 -0.010501    5( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      69  0.013889    7( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      72 -0.020201    2( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      75  0.011353    5( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      76  0.010943    6( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      80 -0.010186    2( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1      84  0.011202    6( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1      86  0.012123    8( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1      87  0.026078    1( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      88  0.014842    2( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      89 -0.026797    3( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      94  0.013810    2( b1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1     107  0.024576    3( a1 )   1( b1 )   +-   +-    -    -   +- 
 x   1  1     115  0.022574    3( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     116  0.019976    4( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     119 -0.011489    7( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     122  0.012944    2( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1     123 -0.018268    3( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1     139 -0.012254    3( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     171 -0.015023    1( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1     175  0.015101    5( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1     196  0.010796    1( a1 )   3( a1 )   +-    -   +-    -   +- 
 x   1  1     242 -0.010764    1( a1 )   1( b1 )   +-    -    -   +-   +- 
 x   1  1     258  0.010307    1( a1 )   3( b1 )   +-    -    -   +-   +- 
 w   1  1     480 -0.010477    1( a1 )   1( a1 )   +-   +-   +-   +-      
 w   1  1     481  0.010036    1( a1 )   2( a1 )   +-   +-   +-   +-      
 w   1  1     494 -0.012885    5( a1 )   5( a1 )   +-   +-   +-   +-      
 w   1  1     496  0.010648    2( a1 )   6( a1 )   +-   +-   +-   +-      
 w   1  1     513 -0.010044    6( a1 )   8( a1 )   +-   +-   +-   +-      
 w   1  1     517 -0.010854    1( a2 )   2( a2 )   +-   +-   +-   +-      
 w   1  1     518 -0.012199    2( a2 )   2( a2 )   +-   +-   +-   +-      
 w   1  1     540 -0.047510    1( b2 )   1( b2 )   +-   +-   +-   +-      
 w   1  1     545 -0.012507    3( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1     548  0.012083    1( a2 )   2( b1 )   +-   +-   +-   +     - 
 w   1  1     549  0.010112    2( a2 )   2( b1 )   +-   +-   +-   +     - 
 w   1  1     559 -0.013266    2( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     560  0.034509    3( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     582 -0.014333    1( a1 )   1( a1 )   +-   +-   +-        +- 
 w   1  1     583  0.015337    1( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     584 -0.014526    2( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     585 -0.012069    1( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     586  0.016877    2( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     587 -0.032877    3( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     591 -0.014536    4( a1 )   4( a1 )   +-   +-   +-        +- 
 w   1  1     594 -0.016044    3( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1     609 -0.011376    7( a1 )   7( a1 )   +-   +-   +-        +- 
 w   1  1     622 -0.016979    1( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     623 -0.023764    2( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     632  0.012683    2( b1 )   5( b1 )   +-   +-   +-        +- 
 w   1  1     647 -0.010397    3( b2 )   3( b2 )   +-   +-   +-        +- 
 w   1  1     664 -0.024554    1( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     665 -0.014947    2( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     666  0.026969    3( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     668  0.011226    5( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     671 -0.010757    2( b1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1     682  0.019191    1( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     683 -0.021753    2( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     684  0.018680    3( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     690  0.017946    1( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     691 -0.034948    2( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     692  0.010297    3( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     695  0.012285    6( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     699  0.011312    2( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     700 -0.019658    3( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     732  0.013276    1( a2 )   2( b2 )   +-   +-   +     -   +- 
 w   1  1     736 -0.013152    1( a1 )   1( a1 )   +-   +-        +-   +- 
 w   1  1     737  0.018338    1( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     738 -0.026603    2( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     772 -0.014386    1( a2 )   1( a2 )   +-   +-        +-   +- 
 w   1  1     775 -0.026296    1( b1 )   1( b1 )   +-   +-        +-   +- 
 w   1  1     776 -0.026980    1( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     777 -0.033547    2( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     778  0.024787    1( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     779  0.011859    2( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     780 -0.020744    3( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     786  0.014470    2( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1     789 -0.010123    5( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1     803 -0.011069    2( a2 )   1( b1 )   +-   +    +-   +-    - 
 w   1  1     814  0.024947    1( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     815 -0.017546    2( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     817  0.013726    4( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     818 -0.020705    5( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     819  0.016474    6( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     831  0.010733    2( a1 )   3( b2 )   +-   +    +-   +-    - 
 w   1  1     838 -0.015003    1( a1 )   1( a1 )   +-   +    +-    -   +- 
 w   1  1     839  0.020333    1( a1 )   2( a1 )   +-   +    +-    -   +- 
 w   1  1     840 -0.018121    2( a1 )   2( a1 )   +-   +    +-    -   +- 
 w   1  1     841 -0.015426    1( a1 )   3( a1 )   +-   +    +-    -   +- 
 w   1  1     846 -0.012158    3( a1 )   4( a1 )   +-   +    +-    -   +- 
 w   1  1     850  0.019293    3( a1 )   5( a1 )   +-   +    +-    -   +- 
 w   1  1     904  0.019691    1( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     905 -0.011327    2( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     907  0.012324    4( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     908 -0.011641    5( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     912  0.011828    1( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     913 -0.020146    2( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     915  0.013650    4( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     919 -0.010146    8( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     920 -0.020826    1( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     921  0.014078    2( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     924  0.017188    5( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     954  0.011935    1( a2 )   2( b2 )   +-   +     -   +-   +- 
 w   1  1     958 -0.011774    1( a1 )   1( a1 )   +-        +-   +-   +- 
 w   1  1     959  0.014209    1( a1 )   2( a1 )   +-        +-   +-   +- 
 w   1  1     960 -0.013086    2( a1 )   2( a1 )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             112
     0.01> rq > 0.001            586
    0.001> rq > 0.0001           427
   0.0001> rq > 0.00001          140
  0.00001> rq > 0.000001          42
 0.000001> rq                      3
           all                  1311
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         3 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:         2    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.975021667520    -74.127738409903

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.026760101122   "relaxed" cnot**2         =   0.950667252133
 eci       =    -76.231982365803   deltae = eci - eref       =  -0.205222264681
 eci+dv1   =    -76.242106544043   dv1 = (1-cnot**2)*deltae  =  -0.010124178240
 eci+dv2   =    -76.242631915598   dv2 = dv1 / cnot**2       =  -0.010649549795
 eci+dv3   =    -76.243214797483   dv3 = dv1 / (2*cnot**2-1) =  -0.011232431680
 eci+pople =    -76.240771762386   ( 10e- scaled deltae )    =  -0.214011661265
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999634

 space required:

    space required for calls in multd2:
       onex          112
       allin           0
       diagon        330
    max.      ---------
       maxnex        330

    total core space usage:
       maxnex        330
    max k-seg        832
    max k-ind         15
              ---------
       totmax       2024
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       2 DDYI=      13 DDXI=      32 DDWI=      43
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block   a1   *****

 occupation numbers of nos
   1.9999209      1.9863868      1.9709198      0.0212880      0.0100134
   0.0051365      0.0041796      0.0010546      0.0005437      0.0004587
   0.0000508


*****   symmetry block   a2   *****

 occupation numbers of nos
   0.0047323      0.0006866


*****   symmetry block   b1   *****

 occupation numbers of nos
   1.9686090      0.0230773      0.0053417      0.0010708      0.0006130
   0.0004648      0.0000365


*****   symmetry block   b2   *****

 occupation numbers of nos
   1.9761792      0.0146372      0.0040919      0.0005071


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                         a1 partial gross atomic populations
   ao class       1 a1       2 a1       3 a1       4 a1       5 a1       6 a1
    O__ s       2.000565   1.690162  -0.049751   0.001580   0.005047   0.001202
    O__ p       0.000094   0.039312   1.459912   0.009658   0.003874   0.000035
    O__ d       0.000000   0.000183   0.002033   0.000268   0.000324   0.002625
    H__ s      -0.000182   0.203965   0.515446   0.010251   0.000365   0.000252
    H__ p      -0.000555   0.052764   0.043280  -0.000469   0.000403   0.001023

   ao class       7 a1       8 a1       9 a1      10 a1      11 a1
    O__ s       0.000185   0.000206   0.000011   0.000010   0.000004
    O__ p       0.000244   0.000173  -0.000006   0.000045   0.000004
    O__ d       0.003341   0.000399   0.000016   0.000103   0.000001
    H__ s       0.000038   0.000321   0.000102   0.000006   0.000028
    H__ p       0.000373  -0.000044   0.000420   0.000295   0.000014

                         a2 partial gross atomic populations
   ao class       1 a2       2 a2
    O__ d       0.003554   0.000171
    H__ p       0.001179   0.000516

                         b1 partial gross atomic populations
   ao class       1 b1       2 b1       3 b1       4 b1       5 b1       6 b1
    O__ p       1.130069   0.011938   0.001818   0.000292   0.000002   0.000065
    O__ d       0.008427   0.000435   0.002599   0.000382   0.000073   0.000000
    H__ s       0.802646   0.011158   0.000546   0.000144   0.000253   0.000009
    H__ p       0.027467  -0.000453   0.000379   0.000253   0.000286   0.000391

   ao class       7 b1
    O__ p       0.000006
    O__ d       0.000001
    H__ s       0.000016
    H__ p       0.000014

                         b2 partial gross atomic populations
   ao class       1 b2       2 b2       3 b2       4 b2
    O__ p       1.928297   0.013884   0.000099   0.000026
    O__ d       0.001143   0.000045   0.003519   0.000069
    H__ p       0.046739   0.000708   0.000474   0.000412


                        gross atomic populations
     ao           O__        H__
      s         3.649221   1.545363
      p         4.599840   0.175866
      d         0.029710   0.000000
    total       8.278771   1.721229


 Total number of electrons:   10.00000000

