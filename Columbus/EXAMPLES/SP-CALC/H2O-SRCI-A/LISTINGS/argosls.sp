echo of the argos input file:
 ------------------------------------------------------------------------
 h2o pvdz
   1  2  5  5  5 20  9  0  0  0  0  0  0  0  0
   4  1 a1  1 a2  1 b1  1 b2
   1
   2  3  4
     1    1
     3    1    3    4
     5    1    3    4    1    2
     2    1    3
     6    1    3    4    3    1    2
     1    1    1
     1
     3    3    2
     0    0    1
     1    0    0
     0    1    0
     5    6    3
    -1   -1    4    0    0    0
     0    0    0    0    1    0
     0    0    0    0    0    1
     1   -1    0    0    0    0
     0    0    0    1    0    0
     2    2    4
     1    1
     1   -1
     6    6    5
     0    0    1    0    0    1
     1    0    0    1    0    0
     0    1    0    0    1    0
     0    0    1    0    0   -1
     1    0    0   -1    0    0
     0    1    0    0   -1    0
     9    1    3
   11720.0000000        0.0007100  -0.0001600   0.0000000
    1759.0000000        0.0054700  -0.0012630   0.0000000
     400.8000000        0.0278370  -0.0062670   0.0000000
     113.7000000        0.1048000  -0.0257160   0.0000000
      37.0300000        0.2830620  -0.0709240   0.0000000
      13.2700000        0.4487190  -0.1654110   0.0000000
       5.0250000        0.2709520  -0.1169550   0.0000000
       1.0130000        0.0154580   0.5573680   0.0000000
       0.3023000       -0.0025850   0.5727590   1.0000000
     4    2    2
      17.7000000        0.0430180   0.0000000
       3.8540000        0.2289130   0.0000000
       1.0460000        0.5087280   0.0000000
       0.2753000        0.4605310   1.0000000
     1    3    1
       1.1850000        1.0000000
     4    1    2
      13.0100000        0.0196850   0.0000000
       1.9620000        0.1379770   0.0000000
       0.4446000        0.4781480   0.0000000
       0.1220000        0.5012400   1.0000000
     1    2    1
       0.7270000        1.0000000
 O    3  1 8.
     0.00000000    0.00000000    0.00000000
   1  1
   2  2
   3  3
 H    2  2 1.
     1.43145000    0.00000000    1.10834700
    -1.43145000    0.00000000    1.10834700
   2  1
   4  4
   5  5
 ------------------------------------------------------------------------
                              program "argos" 5.4
                            columbus program system

             this program computes integrals over symmetry orbitals
               of generally contracted gaussian atomic orbitals.
                        programmed by russell m. pitzer

                           version date: 03-feb-1999

references:

    symmetry analysis (equal contributions):
    r. m. pitzer, j. chem. phys. 58, 3111 (1973).

    ao integral evaluation (hondo):
    m. dupuis, j. rys, and h. f. king, j. chem. phys. 65, 111 (1976).

    general contraction of gaussian orbitals:
    r. c. raffenetti, j. chem. phys. 58, 4452 (1973).

    core potential ao integrals (meldps):
    l. e. mcmurchie and e. r. davidson, j. comput. phys. 44, 289 (1981)

    spin-orbit and core potential integrals:
    r. m. pitzer and n. w. winter, int. j. quantum chem. 40, 773 (1991)

 This Version of Program ARGOS is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at


 workspace allocation parameters: lcore=   8000000 mem1=1108557832 ifirst=-264082718

 filenames and unit numbers:
 unit description                   filename
 ---- -----------                   ----------
   6  listing file:                 argosls                                                     
   4  1-e integral file:            aoints                                                      
   8  2-e integral file [fsplit=2]: aoints2                                                     
   5  input file:                   argosin                                                     

 argos input parameters and titles:
 ngen   =  1 ns     =  2 naords =  5 ncons  =  5 ngcs   =  5 itol   = 20
 icut   =  9 aoints =  4 only1e =  0 inrm   =  0 ncrs   =  0
 l1rec  =         0      l2rec  =         0      aoint2 =  8 fsplit =  2

h2o pvdz                                                                        
aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:55 2003


irrep            1       2       3       4
degeneracy       1       1       1       1
label            a1      a2      b1      b2


direct product table
   ( a1) ( a1) =  a1
   ( a1) ( a2) =  a2
   ( a1) ( b1) =  b1
   ( a1) ( b2) =  b2
   ( a2) ( a1) =  a2
   ( a2) ( a2) =  a1
   ( a2) ( b1) =  b2
   ( a2) ( b2) =  b1
   ( b1) ( a1) =  b1
   ( b1) ( a2) =  b2
   ( b1) ( b1) =  a1
   ( b1) ( b2) =  a2
   ( b2) ( a1) =  b2
   ( b2) ( a2) =  b1
   ( b2) ( b1) =  a2
   ( b2) ( b2) =  a1


                     nuclear repulsion energy    9.18721103


primitive ao integrals neglected if exponential factor below 10**(-20)
contracted ao and so integrals neglected if value below 10**(- 9)
symmetry orbital integrals written on units  4  8


                                    O   atoms

                              nuclear charge   8.00

           center            x               y               z
             1             0.00000000      0.00000000      0.00000000

                    1s orbitals

 orbital exponents  contraction coefficients
    11720.00       7.1000025E-04  -1.6000002E-04    0.000000    
    1759.000       5.4700019E-03  -1.2630001E-03    0.000000    
    400.8000       2.7837010E-02  -6.2670006E-03    0.000000    
    113.7000       0.1048000      -2.5716002E-02    0.000000    
    37.03000       0.2830621      -7.0924007E-02    0.000000    
    13.27000       0.4487192      -0.1654110        0.000000    
    5.025000       0.2709521      -0.1169550        0.000000    
    1.013000       1.5458005E-02   0.5573681        0.000000    
   0.3023000      -2.5850009E-03   0.5727591        1.000000    

                     symmetry orbital labels
                     1 a11           2 a11           3 a11

           symmetry orbitals
 ctr, ao    a11
  1, 000  1.000

                    2p orbitals

 orbital exponents  contraction coefficients
    17.70000       4.3017992E-02    0.000000    
    3.854000       0.2289130        0.000000    
    1.046000       0.5087279        0.000000    
   0.2753000       0.4605309        1.000000    

                     symmetry orbital labels
                     4 a11           5 a11
                     1 b11           2 b11
                     1 b21           2 b21

           symmetry orbitals
 ctr, ao    a11    b11    b21
  1, 100  0.000  1.000  0.000
  1, 010  0.000  0.000  1.000
  1, 001  1.000  0.000  0.000

                    3d orbitals

 orbital exponents  contraction coefficients
    1.185000        1.000000    

                     symmetry orbital labels
                     6 a11
                     3 b11
                     3 b21
                     7 a11
                     1 a21

           symmetry orbitals
 ctr, ao    a11    b11    b21    a11    a21
  1, 200 -1.000  0.000  0.000  1.000  0.000
  1, 020 -1.000  0.000  0.000 -1.000  0.000
  1, 002  2.000  0.000  0.000  0.000  0.000
  1, 110  0.000  0.000  0.000  0.000  1.000
  1, 101  0.000  1.000  0.000  0.000  0.000
  1, 011  0.000  0.000  1.000  0.000  0.000


                                    H   atoms

                              nuclear charge   1.00

           center            x               y               z
             1             1.43145000      0.00000000      1.10834700
             2            -1.43145000      0.00000000      1.10834700
 operator      center interchanges
  1(id.)        1  2
  2(gen.)       2  1

                    1s orbitals

 orbital exponents  contraction coefficients
    13.01000       1.9684990E-02    0.000000    
    1.962000       0.1379769        0.000000    
   0.4446000       0.4781478        0.000000    
   0.1220000       0.5012397        1.000000    

                     symmetry orbital labels
                     8 a11           9 a11
                     4 b11           5 b11

           symmetry orbitals
 ctr, ao    a11    b11
  1, 000  1.000  1.000
  2, 000  1.000 -1.000

                    2p orbitals

 orbital exponents  contraction coefficients
   0.7270000        1.000000    

                     symmetry orbital labels
                    10 a11
                     6 b11
                     4 b21
                     7 b11
                    11 a11
                     2 a21

           symmetry orbitals
 ctr, ao    a11    b11    b21    b11    a11    a21
  1, 100  0.000  1.000  0.000  0.000  1.000  0.000
  1, 010  0.000  0.000  1.000  0.000  0.000  1.000
  1, 001  1.000  0.000  0.000  1.000  0.000  0.000
  2, 100  0.000  1.000  0.000  0.000 -1.000  0.000
  2, 010  0.000  0.000  1.000  0.000  0.000 -1.000
  2, 001  1.000  0.000  0.000 -1.000  0.000  0.000

lx:  b2          ly:  b1          lz:  a2

output SIFS file header information:
h2o pvdz                                                                        
aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:55 2003

output energy(*) values:
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.

total core energy =  9.187211027960E+00

nsym = 4 nbft=  24

symmetry  =    1    2    3    4
slabel(*) =   a1   a2   b1   b2
nbpsy(*)  =   11    2    7    4

info(*) =         2      4096      3272      4096      2700

output orbital labels, i:bfnlab(i)=
   1:  1O__1s   2:  2O__1s   3:  3O__1s   4:  4O__2p   5:  5O__2p   6:  6O__3d
   7:  7O__3d   8:  8H__1s   9:  9H__1s  10: 10H__2p  11: 11H__2p  12: 12O__3d
  13: 13H__2p  14: 14O__2p  15: 15O__2p  16: 16O__3d  17: 17H__1s  18: 18H__1s
  19: 19H__2p  20: 20H__2p  21: 21O__2p  22: 22O__2p  23: 23O__3d  24: 24H__2p

bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  2   9:  2  10:  2
  11:  2  12:  1  13:  2  14:  1  15:  1  16:  1  17:  2  18:  2  19:  2  20:  2
  21:  1  22:  1  23:  1  24:  2

bfn_to_orbital_type map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  2   5:  2   6:  4   7:  4   8:  1   9:  1  10:  2
  11:  2  12:  4  13:  2  14:  2  15:  2  16:  4  17:  1  18:  1  19:  2  20:  2
  21:  2  22:  2  23:  4  24:  2


       24 symmetry orbitals,       a1:  11    a2:   2    b1:   7    b2:   4

 !timer: syminp required                 user+sys=     0.010 walltime=     0.000

 socfpd: mcxu=     3755 mcxu2=     2953 left=  7996245
 !timer: socfpd required                 user+sys=     0.000 walltime=     0.000

oneint:    80 S1(*)    integrals were written in  1 records.
oneint:    80 T1(*)    integrals were written in  1 records.
oneint:   107 V1(*)    integrals were written in  1 records.
oneint:    69 X(*)     integrals were written in  1 records.
oneint:    46 Y(*)     integrals were written in  1 records.
oneint:    78 Z(*)     integrals were written in  1 records.
oneint:    69 Im(px)   integrals were written in  1 records.
oneint:    46 Im(py)   integrals were written in  1 records.
oneint:    62 Im(pz)   integrals were written in  1 records.
oneint:    36 Im(lx)   integrals were written in  1 records.
oneint:    54 Im(ly)   integrals were written in  1 records.
oneint:    32 Im(lz)   integrals were written in  1 records.
oneint:    87 XX(*)    integrals were written in  1 records.
oneint:    41 XY(*)    integrals were written in  1 records.
oneint:    71 XZ(*)    integrals were written in  1 records.
oneint:    87 YY(*)    integrals were written in  1 records.
oneint:    44 YZ(*)    integrals were written in  1 records.
oneint:    89 ZZ(*)    integrals were written in  1 records.

 !timer: oneint required                 user+sys=     0.020 walltime=     0.000
 !timer: seg1mn required                 user+sys=     0.030 walltime=     0.000

twoint:       11412 1/r12    integrals and       63 pk flags
                                 were written in     5 records.

 twoint: maximum mblu needed =      2830
 !timer: twoint required                 user+sys=     0.100 walltime=     0.000

 driver: 1-e integral  workspace high-water mark =     24720
 driver: 2-e integral  workspace high-water mark =     20322
 driver: overall argos workspace high-water mark =     24720
 !timer: argos required                  user+sys=     0.130 walltime=     0.000
