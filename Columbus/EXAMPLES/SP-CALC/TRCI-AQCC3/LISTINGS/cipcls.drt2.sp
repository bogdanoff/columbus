
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans@itc.univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=   8000000 mem1=1075314696 ifirst=-268238500

 drt header information:
 cidrt_title                                                                     
 *** warning: rdhdrt: drt version .ne. 4 *** vrsion= 3
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     2 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       42        2       15       12       13
 ncsft:             280
 map(*)=    -1  9 10  1  2  3  4 11  5 12  6  7  8
 mu(*)=      0  0  0  0
 syml(*) =   1  1  2  3
 rmo(*)=     2  3  1  1

 indx01:    42 indices saved in indxv(*)
===================================ROOT #-1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Sep  5 16:35:22 2002
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Sep  5 16:35:22 2002
 cidrt_title                                                                     
 energy computed by program ciudg.       hochtor2        Thu Sep  5 16:35:27 2002

 lenrec =    4096 lenci =       280 ninfo =  6 nenrgy =  8 ntitle =  8

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        3       97% overlap
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -3.862410000473E+01, ietype=-1038,   total energy of type: MR-AQCC 
 energy( 4)=  3.952921928941E-07, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  8.899547765395E-13, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  7.630497089961E-14, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 7)=  1.020237629659E+00, ietype=-1039,   total energy of type: a4den   
 energy( 8)= -3.853601971762E+01, ietype=-1041,   total energy of type: Unknown 
==================================================================================

 space is available for   3997904 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      53 coefficients were selected.
 workspace: ncsfmx=     280
 ncsfmx= 280

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     280 ncsft =     280 ncsf =      53
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       4 ****
 3.1250E-02 <= |c| < 6.2500E-02       9 *********
 1.5625E-02 <= |c| < 3.1250E-02      20 ********************
 7.8125E-03 <= |c| < 1.5625E-02      19 *******************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       53 total stored =      53

 from the selected csfs,
 min(|csfvec(:)|) = 1.0278E-02    max(|csfvec(:)|) = 9.7479E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     280 ncsft =     280 ncsf =      53

 i:slabel(i) =  1:   1  2:   2  3:   3  4:   4

 frozen orbital =    1
 symfc(*)       =    1
 label          =    1
 rmo(*)         =    1

 internal level =    1    2    3    4
 syml(*)        =    1    1    2    3
 label          =    1    1    2    3
 rmo(*)         =    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1  0.97479 0.95022 z*                    3123
      158 -0.08211 0.00674 w             3:  2  33120
       32  0.07255 0.00526 y             1:  4  11223
       15  0.07125 0.00508 y             1:  4  13023
       29 -0.06267 0.00393 y             3:  2  11322
      219 -0.05020 0.00252 w    1:  4    3:  2 121122
      180 -0.04667 0.00218 w    1:  5    2:  2 123003
       11 -0.04322 0.00187 y             2:  2  13203
      264 -0.04120 0.00170 w             1:  4  30123
      203 -0.03745 0.00140 w    1:  4    3:  2 121212
      167  0.03291 0.00108 w    1:  4    3:  2 123012
        3  0.03284 0.00108 y             1:  4  13320
      147 -0.03180 0.00101 w             1:  4  33120
      215 -0.03178 0.00101 w    1:  4    2:  2 121203
      164 -0.02721 0.00074 w    2:  2    3:  2 123102
       50  0.02643 0.00070 x    2:  2    3:  2 113202
      160 -0.02622 0.00069 w             3:  3  33120
       35  0.02607 0.00068 y             1:  7  11223
      275 -0.02491 0.00062 w             3:  2  30123
       18  0.02127 0.00045 y             1:  7  13023
      225  0.02116 0.00045 w    1:  6    3:  3 121122
       51  0.02093 0.00044 x    2:  2    3:  3 113202
      168 -0.02077 0.00043 w    1:  5    3:  2 123012
       30  0.01977 0.00039 y             3:  3  11322
       34  0.01963 0.00039 y             1:  6  11223
       54  0.01887 0.00036 x    1:  5    3:  2 113022
       89  0.01883 0.00035 x    1:  4    2:  2 112203
       13 -0.01798 0.00032 y             3:  3  13122
      105  0.01767 0.00031 x    1:  4    1:  5 112023
       97  0.01733 0.00030 x    1:  4    3:  3 112122
       10  0.01715 0.00029 y             3:  4  13212
      165 -0.01711 0.00029 w    2:  2    3:  3 123102
      172 -0.01649 0.00027 w    1:  5    3:  3 123012
        8 -0.01648 0.00027 y             3:  2  13212
       58  0.01542 0.00024 x    1:  5    3:  3 113022
       20  0.01529 0.00023 y             3:  2  12312
      231  0.01472 0.00022 w             1:  4  31023
       93  0.01434 0.00021 x    1:  4    3:  2 112122
       16 -0.01395 0.00019 y             1:  5  13023
        2 -0.01394 0.00019 z*                    1323
      152 -0.01344 0.00018 w             1:  6  33120
      233 -0.01343 0.00018 w             1:  5  31023
      209  0.01297 0.00017 w    1:  6    3:  3 121212
      216 -0.01279 0.00016 w    1:  5    2:  2 121203
       14  0.01252 0.00016 y             3:  4  13122
       24  0.01198 0.00014 y             1:  4  12123
      269 -0.01164 0.00014 w             1:  6  30123
      220 -0.01108 0.00012 w    1:  5    3:  2 121122
       81  0.01101 0.00012 x    1:  4    3:  3 112212
        5  0.01087 0.00012 y             1:  6  13320
      223 -0.01069 0.00011 w    1:  4    3:  3 121122
       38 -0.01064 0.00011 y             1:  6  10323
      277 -0.01028 0.00011 w             3:  3  30123
           53 csfs were printed in this range.
