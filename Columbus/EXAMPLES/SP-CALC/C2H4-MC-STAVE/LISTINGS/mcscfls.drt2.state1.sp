

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
         8000000 of real*8 words (   61.04 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
  NPATH =  1, 2, 3, 9,10,12,13,17,19,21,23,
  
  
  
  
  
  NSTATE =0, NUNITV=1 ,
  NITER=1,NMITER=0
  TOL(1)= .00000001,
  NCOUPL =   1,
  FCIORB=
          2   1 40, 2   2 40, 2   3 40, 6   1 40, 6   2 40,
  6   3 40,
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:25:31 2003

 Core type energy values:
 energy( 1)=  3.329664742176E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   33.296647422


   ******  Basis set informations:  ******

 Number of irreps:                  8
 Total number of basis functions:  48

 irrep no.              1    2    3    4    5    6    7    8
 irrep label           Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
 no. of bas.fcions.    11    4    7    2   11    4    7    2


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=     1

 maximum number of psci micro-iterations:   nmiter=    0
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  Ag         2

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       1( 12)    40
       2       2( 13)    40
       2       3( 14)    40
       6       1( 36)    40
       6       2( 37)    40
       6       3( 38)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  1
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=25 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:    ag 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:        12

 !timer: initialization                  user+sys=     0.000 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   6 possible.


 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    54
 Number of active-virtual rotations:     6

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:            720
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2736



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is 106
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  2826

 twoao_o processed      92764 two electron ao integrals.

      92764 ao integrals were written into   87 records

 srtinc_o read in      93892 integrals and wrote out      93892 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.080 walltime=     0.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.090 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:            720
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2736

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   6 noldhv= 10 noldv= 10
 !timer: hmc(*) diagonalization          user+sys=     0.010 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688172865     -111.3654647083        0.0000002482        0.0000010000
    2       -77.5246981642     -110.8213455859        0.0003776188        0.0100000000
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.0177298711
 *** psci convergence not reached ***
 Total number of micro iterations:    0

 ***  micro: final psci convergence values:  ***
    imxov=  0 z0= 0.00000000 pnorm= 0.0000E+00 rznorm= 0.0000E+00 rpnorm= 0.0000E+00 noldr=  0 nnewr=  1 nolds=  0 nnews=  0
 *** warning *** small z0.

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.402882   -2.025400   -1.130168

 qvv(*) eigenvalues. symmetry block  1
     0.413589    1.169627    1.431881    2.034845    3.221215    3.731714    5.014811    5.437681
 i,qaaresolved 1 -0.690972938
 i,qaaresolved 2  1.40285748
 i,qaaresolved 3  2.65208674

 qvv(*) eigenvalues. symmetry block  2
     3.858821

 fdd(*) eigenvalues. symmetry block  3
    -1.257138

 qvv(*) eigenvalues. symmetry block  3
     0.472192    1.242292    1.744742    3.504779    3.848287    5.648616

 qvv(*) eigenvalues. symmetry block  4
     2.283403    3.964111

 fdd(*) eigenvalues. symmetry block  5
   -22.399677   -1.556575

 qvv(*) eigenvalues. symmetry block  5
     0.482419    0.930720    1.575781    1.965442    2.486413    3.739653    4.529181    5.265378    6.277236
 i,qaaresolved 1  0.330153586
 i,qaaresolved 2  1.63277087
 i,qaaresolved 3  3.99911657

 qvv(*) eigenvalues. symmetry block  6
     4.102311

 fdd(*) eigenvalues. symmetry block  7
    -0.988761

 qvv(*) eigenvalues. symmetry block  7
     0.609641    1.888588    2.512803    3.457043    4.909906    6.789673

 qvv(*) eigenvalues. symmetry block  8
     2.599130    4.755172
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.2809E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.1220E-05
 *** warning *** small active-orbital occupation. i=  2 nocc= 1.8888E-04
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.110 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -78.0688172865 demc= 7.8069E+01 wnorm= 1.4184E-01 knorm= 0.0000E+00 apxde=-8.8649E-03    *not converged* 

 final mcscf convergence values:
 iter=    1 emc=  -78.0688172865 demc= 7.8069E+01 wnorm= 1.4184E-01 knorm= 0.0000E+00 apxde=-8.8649E-03    *not converged* 




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -78.068817287
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  Ag  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1C1s       0.70789518    -0.02125812    -0.00419445     0.04430833     0.08307301    -0.18292772     0.48704798    -0.13481072
   2C1s       0.00581352     0.60895592    -0.04527383    -0.12265239     0.35863539    -0.63805014     2.25414431    -0.47025737
   3C1s      -0.00547862    -0.16589846     0.04523067    -1.31481302     0.17135522     0.87512771    -3.58613701     1.23194586
   4C1pz      0.00009656    -0.19215706     0.60524810    -0.19758765    -0.62248467     0.39670575     0.34528030     0.00446522
   5C1pz     -0.00034704     0.06340722    -0.14182451    -0.43686892     1.30687341    -0.26120556    -0.82020586     0.58755204
   6C1d0      0.00005798     0.00626140    -0.00649825    -0.00111976     0.01568525    -0.00887776     0.03693396    -0.00141581
   7C1d2+    -0.00013011    -0.00045070    -0.00596111     0.00709739     0.01743193     0.10416193     0.03786626    -0.31728394
   8H1s      -0.00029124     0.18611069     0.28707364     0.01914449    -0.39766694    -0.82103730    -0.05394613    -0.90606010
   9H1s       0.00123292    -0.08687924    -0.08065989     0.92002316     0.16984638     0.55627963     0.85471213     0.29957990
  10H1py      0.00026532    -0.01400472    -0.01377760    -0.00951912    -0.00544873    -0.08674980    -0.14849809    -0.00318511
  11H1pz      0.00005891    -0.01122019     0.00216074    -0.01003212     0.03168852    -0.02487705    -0.05390124    -0.17152526

               MO    9        MO   10        MO   11
   1C1s      -0.34971450    -0.18329942     0.38539758
   2C1s      -1.41194045    -0.62456947     1.81165451
   3C1s       1.10255577     1.03905435    -0.29708401
   4C1pz      0.37263706     0.56080416     0.81853354
   5C1pz     -0.86083433    -0.06780179    -0.14021489
   6C1d0      0.14899851     0.21481558    -0.07376167
   7C1d2+    -0.11166505     0.10298365    -0.30122434
   8H1s       0.02447667    -0.06193176    -1.24170895
   9H1s      -0.01491009    -0.20760172     0.37429927
  10H1py     -0.16434376     0.43109355     0.62560333
  11H1pz      0.28379097    -0.40700884     0.54466797

          mcscf orbitals of the final iteration,  B3u block   2

               MO   12        MO   13        MO   14        MO   15
  12C1px      0.55554859    -1.02179412     0.39636809    -0.27254125
  13C1px      0.04336479     1.08506687    -0.27439335    -0.09046099
  14C1d1+    -0.02801880     0.10915493     0.57900662    -0.27863586
  15H1px      0.01283761    -0.00908218     0.03954878     0.58469177

          mcscf orbitals of the final iteration,  B2u block   3

               MO   16        MO   17        MO   18        MO   19        MO   20        MO   21        MO   22
  16C1py      0.47042675    -0.33298454    -0.09388374     0.88527301     0.03173955    -0.12953450     1.61867682
  17C1py     -0.10000397    -0.61887841     0.44593272    -1.80014931    -0.62745284     0.26171177    -0.11109381
  18C1d1-    -0.00317254     0.00605824    -0.15277815    -0.23917063    -0.35471794    -0.45718470     0.78834029
  19H1s       0.35459512     0.05570864    -0.87738569    -0.02167612     0.71217866     0.37110562    -1.46892220
  20H1s      -0.12374527     1.14843080     0.66300896     1.11237275    -0.21015399    -0.46196206     0.29500536
  21H1py     -0.01160227    -0.01036981    -0.01506969    -0.14307120     0.34445877    -0.08179831     0.69655815
  22H1pz     -0.01152725    -0.00685101    -0.06786469    -0.06700664    -0.20784379     0.45500240     0.42185114

          mcscf orbitals of the final iteration,  B1g block   4

               MO   23        MO   24
  23C1d2-     0.48444497    -0.59271976
  24H1px      0.19466564     0.55631469

          mcscf orbitals of the final iteration,  B1u block   5

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  25C1s       0.70855286    -0.01340781     0.04957164     0.06712526    -0.00265026     0.11792502     0.63736523     0.19741054
  26C1s       0.00733862     0.47983603    -0.17797654     0.02399408    -0.43511677     0.46080648     2.75322050     0.94911157
  27C1s      -0.00863951    -0.10291268    -1.46014848    -3.53706913    -0.27286196     0.40020848    -7.69384757     0.15726372
  28C1pz     -0.00041518     0.23643329    -0.11392983     0.23623729     0.26276425     0.99063643    -0.11844411    -0.50693321
  29C1pz      0.00125221    -0.04570261    -0.26761926     3.17972852     0.18443628    -2.58411140     2.19910640    -0.95389396
  30C1d0      0.00015912    -0.00357693     0.00085945    -0.01103128     0.02996459     0.08147676    -0.06643666     0.02893278
  31C1d2+    -0.00033814    -0.00302584     0.01225314     0.01809291    -0.09247722     0.05553872    -0.04158997     0.16300339
  32H1s      -0.00059218     0.33394434     0.03304199    -0.18414621     0.90536724    -0.14168963     0.41288788     0.46566414
  33H1s       0.00101711    -0.13267896     1.06271345    -0.60904854    -0.89405861     0.99460089     0.07369108    -0.03778478
  34H1py      0.00045712    -0.02080505    -0.01753765    -0.01815173     0.09108746    -0.13358612    -0.09325182    -0.06654849
  35H1pz      0.00025877    -0.00930595    -0.00528388    -0.00288432     0.05891838     0.00124859    -0.08301979     0.43804277

               MO   33        MO   34        MO   35
  25C1s      -0.15261112     0.44546851    -0.05184853
  26C1s      -0.38768118     2.08504832     0.12585198
  27C1s       4.46681380    -0.86057892     2.94599375
  28C1pz     -1.32117745    -0.42221342     1.15502950
  29C1pz     -1.63861053     0.60881006    -1.14096970
  30C1d0      0.23222081     0.26577348    -0.15668465
  31C1d2+    -0.24406196    -0.00407239    -0.57292515
  32H1s      -0.46012827    -0.44423080    -1.73308582
  33H1s       0.52651807    -0.08155227     0.60429476
  34H1py     -0.25663262     0.62928046     0.43108333
  35H1pz      0.06431219    -0.14738179     0.69593616

          mcscf orbitals of the final iteration,  B2g block   6

               MO   36        MO   37        MO   38        MO   39
  36C1px      0.54721127    -1.13513676    -0.40746041     0.03501410
  37C1px      0.46705047     1.67057417    -0.36222421     0.32643906
  38C1d1+     0.02218513    -0.08183134     1.00284503     0.56819168
  39H1px      0.01667897    -0.02662566     0.13533991    -0.59088408

          mcscf orbitals of the final iteration,  B3g block   7

               MO   40        MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  40C1py      0.44984201    -0.29584348    -1.02821525     0.25254009     0.10916849     1.19420809     0.86200186
  41C1py     -0.08398036    -1.49710054     2.57439494    -3.10809629    -2.17097898     0.00472679     0.25520307
  42C1d1-     0.03737217    -0.02945877     0.12792420     0.25520872    -0.05252463    -0.29906002     1.94531629
  43H1s       0.43044374    -0.02923889     0.21514072     1.12190930     0.91815107    -0.35986859    -1.99134944
  44H1s      -0.09564102     1.68862659    -1.38213846     0.34516886     0.18813390    -0.29172929     0.79500990
  45H1py     -0.00747523    -0.01458727     0.12306619    -0.03082209     0.34835783     0.39398100     0.77370936
  46H1pz     -0.01274156    -0.01608321     0.13342660     0.14373917    -0.33825644     0.49060157     0.08832708

          mcscf orbitals of the final iteration,  Au  block   8

               MO   47        MO   48
  47C1d2-     0.39770309    -0.87759076
  48H1px      0.30607526     0.54314409

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1C1s       0.70789428    -0.02140781    -0.00353267     0.05018943     0.06795547    -0.18223403     0.48741429    -0.13518567
   2C1s       0.00580572     0.60681897    -0.06817527    -0.09631500     0.30089834    -0.63270564     2.25683001    -0.47545316
   3C1s      -0.00546990    -0.16407740     0.05144718    -1.34712250     0.27611555     0.83371011    -3.57338497     1.23114672
   4C1pz      0.00021749    -0.16922799     0.61205510    -0.19957263    -0.60735871     0.42778388     0.33198044    -0.00354296
   5C1pz     -0.00037533     0.05802134    -0.14411166    -0.43316338     1.31325832    -0.32528403    -0.79435666     0.59625020
   6C1d0      0.00005669     0.00601225    -0.00672944    -0.00134058     0.01397451    -0.00883128     0.03762956    -0.00196363
   7C1d2+    -0.00013131    -0.00067486    -0.00593988     0.00798717     0.01846466     0.10260551     0.03566537    -0.31340363
   8H1s      -0.00023331     0.19678946     0.27986140     0.01536692    -0.44257336    -0.80603876    -0.05197266    -0.88927909
   9H1s       0.00121657    -0.08985516    -0.07733117     0.92862532     0.17953114     0.55814934     0.84420058     0.28882337
  10H1py      0.00026253    -0.01451363    -0.01324048    -0.01024455    -0.00680398    -0.08844367    -0.15009221    -0.00157771
  11H1pz      0.00005932    -0.01113086     0.00258173    -0.00986946     0.03184771    -0.02926428    -0.05352350    -0.18248343

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

   1C1s      -0.35068581    -0.17775846     0.38906830
   2C1s      -1.41791722    -0.59706355     1.82548352
   3C1s       1.11656253     1.03332270    -0.31656641
   4C1pz      0.36326923     0.58183712     0.80874183
   5C1pz     -0.84822489    -0.08430313    -0.13802937
   6C1d0      0.14586709     0.21529154    -0.07846477
   7C1d2+    -0.11805177     0.09677311    -0.30559775
   8H1s       0.01068254    -0.08312803    -1.24755359
   9H1s      -0.00769296    -0.19966614     0.38250842
  10H1py     -0.16960286     0.44214243     0.61577509
  11H1pz      0.28698988    -0.39010597     0.55156842

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     1.91339258     0.00249788     0.00032809     0.00000000

  12C1px      0.58425635    -0.85639827     0.65956928    -0.27254125
  13C1px      0.01406197     0.96403137    -0.57007728    -0.09046099
  14C1d1+    -0.02514733     0.26765051     0.52505174    -0.27863586
  15H1px      0.01343496     0.00272884     0.04029208     0.58469177

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  16C1py      0.47042675    -0.32123434    -0.08428387     0.88634795     0.05107491    -0.10780988     1.62208595
  17C1py     -0.10000397    -0.63933442     0.45650146    -1.78820693    -0.64472859     0.23046818    -0.11540939
  18C1d1-    -0.00317254    -0.00277528    -0.14619350    -0.23691109    -0.33283346    -0.46697802     0.79407168
  19H1s       0.35459512     0.04240914    -0.88633573    -0.02395488     0.68514275     0.37805782    -1.47503135
  20H1s      -0.12374527     1.17552899     0.64771736     1.09686704    -0.18400278    -0.45885420     0.30252512
  21H1py     -0.01160227    -0.01269906    -0.01414310    -0.14618851     0.35081798    -0.06245503     0.69471850
  22H1pz     -0.01152725    -0.00798171    -0.07053115    -0.06970164    -0.22425358     0.44891461     0.41906609

          natural orbitals of the final iteration, block  4

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  23C1d2-     0.47645393    -0.59916215
  24H1px      0.20210748     0.55365450

          natural orbitals of the final iteration, block  5

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  25C1s       0.70855223    -0.01344098     0.05465263     0.06929486     0.00138778     0.13390278     0.63073634     0.19685490
  26C1s       0.00736109     0.47983569    -0.15015839     0.04257272    -0.42373906     0.50791488     2.72938273     0.95361796
  27C1s      -0.00864433    -0.10291228    -1.50626395    -3.58972944    -0.43236800     0.17245684    -7.69388884     0.13052492
  28C1pz     -0.00040411     0.23643331    -0.11760047     0.20691134     0.21614791     0.98650573    -0.12678217    -0.50303717
  29C1pz      0.00125007    -0.04570267    -0.26357858     3.24146588     0.36431015    -2.47166816     2.24804724    -0.93676061
  30C1d0      0.00015895    -0.00357694    -0.00019902    -0.01329731     0.02632011     0.08374466    -0.07159821     0.02355345
  31C1d2+    -0.00033829    -0.00302583     0.01317049     0.01799431    -0.09245173     0.05124077    -0.04103988     0.15842944
  32H1s      -0.00057655     0.33394436     0.02664603    -0.19843287     0.91503365    -0.09531065     0.41467575     0.44972466
  33H1s       0.00101090    -0.13267901     1.07502496    -0.60653643    -0.93699112     0.94594549     0.05170091    -0.02928025
  34H1py      0.00045615    -0.02080507    -0.01869137    -0.01763165     0.09620988    -0.13400687    -0.09251108    -0.07030281
  35H1pz      0.00025834    -0.00930597    -0.00575850    -0.00478589     0.06225802     0.00373817    -0.08772164     0.44399769

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

  25C1s      -0.15802038     0.44734412    -0.05580765
  26C1s      -0.41524575     2.10326686     0.10746301
  27C1s       4.42028191    -0.79003910     2.95326039
  28C1pz     -1.32395466    -0.45005976     1.16059710
  29C1pz     -1.61623356     0.57277659    -1.14344644
  30C1d0      0.22601314     0.26909594    -0.15799816
  31C1d2+    -0.24329492    -0.01141022    -0.57485911
  32H1s      -0.44152762    -0.45919014    -1.73443523
  33H1s       0.52078783    -0.06655713     0.60767060
  34H1py     -0.27014678     0.62555975     0.42651068
  35H1pz      0.06466127    -0.13762363     0.69323231

          natural orbitals of the final iteration, block  6

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     0.08357134     0.00018888     0.00002122     0.00000000

  36C1px      0.85420148    -1.01024739    -0.06115284     0.03501410
  37C1px      0.00422092     1.53516377    -0.88510731     0.32643906
  38C1d1+     0.00308008     0.24401150     0.97638907     0.56819168
  39H1px      0.01784452     0.02249418     0.13593965    -0.59088408

          natural orbitals of the final iteration, block  7

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  40C1py      0.44984201    -0.28077190    -1.02353225     0.24876695     0.11083511     1.20706363     0.85557072
  41C1py     -0.08398036    -1.52752295     2.56680252    -3.10308349    -2.16660337    -0.00291905     0.24954995
  42C1d1-     0.03737217    -0.03353414     0.12250030     0.24811686    -0.04921160    -0.29094226     1.94783292
  43H1s       0.43044374    -0.03329260     0.20806605     1.12848491     0.91428689    -0.37579181    -1.98715430
  44H1s      -0.09564102     1.70608138    -1.36311203     0.34003569     0.18724143    -0.27962399     0.79736302
  45H1py     -0.00747523    -0.01629251     0.12515091    -0.03211332     0.35057053     0.39632830     0.77108572
  46H1pz     -0.01274156    -0.01744255     0.13745800     0.14507875    -0.33713508     0.49003851     0.08710155

          natural orbitals of the final iteration, block  8

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  47C1d2-     0.38760914    -0.88209548
  48H1px      0.31228610     0.53959703
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        304 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       1.999304   1.481007   0.009120   0.000000   0.000000   0.000000
    C1_ p       0.000085   0.145132   1.419813   0.000000   0.000000   0.000000
    C1_ d       0.000066   0.014602   0.016922   0.000000   0.000000   0.000000
    H1_ s       0.000699   0.331510   0.538438   0.000000   0.000000   0.000000
    H1_ p      -0.000154   0.027749   0.015707   0.000000   0.000000   0.000000

   ao class       7Ag        8Ag        9Ag       10Ag       11Ag 

                        B3u partial gross atomic populations
   ao class       1B3u       2B3u       3B3u       4B3u
    C1_ p       1.872601   0.001938   0.000076   0.000000
    C1_ d       0.020039   0.000556   0.000243   0.000000
    H1_ p       0.020753   0.000004   0.000009   0.000000

                        B2u partial gross atomic populations
   ao class       1B2u       2B2u       3B2u       4B2u       5B2u       6B2u
    C1_ p       1.165733   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.000142   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.815930   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.018195   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7B2u

                        B1g partial gross atomic populations
   ao class       1B1g       2B1g

                        B1u partial gross atomic populations
   ao class       1B1u       2B1u       3B1u       4B1u       5B1u       6B1u
    C1_ s       1.999943   0.762504   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.000370   0.387371   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000181   0.007791   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.000184   0.805458   0.000000   0.000000   0.000000   0.000000
    H1_ p      -0.000316   0.036875   0.000000   0.000000   0.000000   0.000000

   ao class       7B1u       8B1u       9B1u      10B1u      11B1u

                        B2g partial gross atomic populations
   ao class       1B2g       2B2g       3B2g       4B2g
    C1_ p       0.082026   0.000167   0.000003   0.000000
    C1_ d       0.000138   0.000019   0.000016   0.000000
    H1_ p       0.001407   0.000003   0.000002   0.000000

                        B3g partial gross atomic populations
   ao class       1B3g       2B3g       3B3g       4B3g       5B3g       6B3g
    C1_ p       0.816159   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.052081   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.120003   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011757   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7B3g

                        Au  partial gross atomic populations
   ao class       1Au        2Au 


                        gross atomic populations
     ao           C1_        H1_
      s         6.251879   3.612222
      p         5.891473   0.131992
      d         0.112433   0.000000
    total      12.255786   3.744214


 Total number of electrons:   16.00000000

 !timer: mcscf                           user+sys=     0.200 walltime=     0.000
