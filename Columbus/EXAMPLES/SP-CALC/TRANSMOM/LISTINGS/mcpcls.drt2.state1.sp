

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:40 2003
    mdrt2_title                                                                     
    mdrt2_title                                                                     


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
    mdrt2_title                                                                     
 Molecular symmetry group:   sym5
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:         9

   ***  Informations from the DRT number:   1


 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag   b3u   b2u   b1g   b1u   b2g   b3g    au 

 List of doubly occupied orbitals:
  1 ag   2 ag   3 ag   1b2u   1b1u   2b1u   1b3g 

 List of active orbitals:
  1b3u   2b3u   3b3u   1b2g   2b2g   3b2g 

 Informations for the DRT no.  2
 Header form the DRT file: 
    mdrt2_title                                                                     
 Molecular symmetry group:    ag 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:        12

   ***  Informations from the DRT number:   2


 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag   b3u   b2u   b1g   b1u   b2g   b3g    au 

 List of doubly occupied orbitals:
  1 ag   2 ag   3 ag   1b2u   1b1u   2b1u   1b3g 

 List of active orbitals:
  1b3u   2b3u   3b3u   1b2g   2b2g   3b2g 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -77.6752100409    nuclear repulsion=    33.2966474218
 demc=             0.0000000001    wnorm=                 0.0000006851
 knorm=            0.0000000221    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1   b1u     2   0.3333 0.3333
  2    ag     1   0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 2 there are  1 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry b3u  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  1b3u   2b3u   3b3u   1b2g   2b2g   3b2g 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1  0.9774096544  0.9553296325  300000
      7 -0.1893715729  0.0358615926  000300
      8  0.0728357484  0.0053050463  000120
      2 -0.0350579641  0.0012290608  120000
      4 -0.0329345282  0.0010846832  030000
     10 -0.0238033237  0.0005665982  000030
      6 -0.0145149878  0.0002106849  003000
      3  0.0132249559  0.0001748995  102000
      9  0.0103185422  0.0001064723  000102
      5 -0.0089619820  0.0000803171  012000
     11 -0.0057652544  0.0000332382  000012
     12 -0.0042159767  0.0000177745  000003

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: