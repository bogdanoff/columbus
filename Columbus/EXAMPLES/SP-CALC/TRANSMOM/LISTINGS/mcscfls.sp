

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
         8000000 of real*8 words (   61.04 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
  NPATH =  1, 2, 3, 9,10,12,13,17,19,21,23,
  navst(1)= 2
  wavst(1,1)=  1.000
  wavst(1,2)=  1.000
  navst(2)= 1
  wavst(2,1)=  1.000
  NSTATE=  0,
  NITER =  30,
  TOL(1)= .00000001,
  NCOUPL =   1,
  FCIORB=
          2   1 40, 2   2 40, 2   3 40, 6   1 40, 6   2 40,
  6   3 40,
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:40 2003

 Core type energy values:
 energy( 1)=  3.329664742176E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   33.296647422


   ******  Basis set informations:  ******

 Number of irreps:                  8
 Total number of basis functions:  48

 irrep no.              1    2    3    4    5    6    7    8
 irrep label           Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
 no. of bas.fcions.    11    4    7    2   11    4    7    2


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    30

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          2             0.333 0.333
  2   ground state          1             0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  Ag         3
  B3u        2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       1( 12)    40
       2       2( 13)    40
       2       3( 14)    40
       6       1( 36)    40
       6       2( 37)    40
       6       3( 38)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  1
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=25 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:   b1u 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:         9

 Informations for the DRT no.  2

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:    ag 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:        12

 !timer: initialization                  user+sys=     0.010 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   6 possible.


 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    54
 Number of active-virtual rotations:     6

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:           1800
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           3816



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is 106
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  3040

 twoao_o processed      92764 two electron ao integrals.

      92764 ao integrals were written into   87 records

 srtinc_o read in      93892 integrals and wrote out      93892 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.090 walltime=     0.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.080 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:           1800
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           3816

 !timer: mosrt1                          user+sys=     0.020 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.020 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   5 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7009677595     -110.9976151812        0.0000000000        0.0000010000
    2       -77.2472911438     -110.5439385656        0.0000000000        0.0000010000
    3       -76.9092311666     -110.2058785884        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   6 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          user+sys=     0.010 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0701098997     -111.3667573215        0.0000000697        0.0000010000
    2       -77.5161086483     -110.8127560701        0.0059774936        0.0100000000
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.0182418324
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.010 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99670670 pnorm= 6.2476E-02 rznorm= 3.0442E-06 rpnorm= 6.3342E-06 noldr=  7 nnewr=  7 nolds=  5 nnews=  5

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.010 walltime=     0.000
 !timer: solvek total                    user+sys=     0.010 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.524813   -2.113714   -1.212268

 qvv(*) eigenvalues. symmetry block  1
     0.392109    1.129958    1.389502    1.986319    3.151983    3.668730    4.962510    5.385463
 i,qaaresolved 1 -0.537937739
 i,qaaresolved 2  1.30284299
 i,qaaresolved 3  2.46792649

 qvv(*) eigenvalues. symmetry block  2
     3.918245

 fdd(*) eigenvalues. symmetry block  3
    -1.320776

 qvv(*) eigenvalues. symmetry block  3
     0.446307    1.190266    1.678057    3.466645    3.785468    5.581471

 qvv(*) eigenvalues. symmetry block  4
     2.223752    3.909774

 fdd(*) eigenvalues. symmetry block  5
   -22.522003   -1.606246

 qvv(*) eigenvalues. symmetry block  5
     0.467532    0.907332    1.519688    1.905153    2.420368    3.682212    4.456410    5.215994    6.213109
 i,qaaresolved 1  0.189372407
 i,qaaresolved 2  1.4919519
 i,qaaresolved 3  3.60101266

 qvv(*) eigenvalues. symmetry block  6
     4.392950

 fdd(*) eigenvalues. symmetry block  7
    -1.039608

 qvv(*) eigenvalues. symmetry block  7
     0.596505    1.828958    2.473164    3.432065    4.852400    6.718832

 qvv(*) eigenvalues. symmetry block  8
     2.547223    4.689789
 *** warning *** small active-orbital occupation. i=  1 nocc= 1.7863E-04
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.120 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -77.6727896010 demc= 7.7673E+01 wnorm= 1.4593E-01 knorm= 8.0933E-02 apxde= 1.9608E-03    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 user+sys=     0.210 walltime=     0.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.090 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:           1800
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           3816

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   3 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7047532251     -111.0014006469        0.0000000000        0.0000100000
    2       -77.2511221439     -110.5477695657        0.0000000000        0.0000100000
    3       -76.9127937280     -110.2094411498        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   4 noldhv=  8 noldv=  8
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0685998382     -111.3652472599        0.0000097312        0.0000100000
    2       -77.5236729080     -110.8203203298        0.0010658118        0.0100000000
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.000310891085
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.010 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   10

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.87058680 pnorm= 3.9944E-02 rznorm= 9.4007E-08 rpnorm= 3.4392E-07 noldr=  9 nnewr=  9 nolds=  8 nnews=  8

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.010 walltime=     0.000
 !timer: solvek total                    user+sys=     0.010 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.482788   -2.088462   -1.193365

 qvv(*) eigenvalues. symmetry block  1
     0.396515    1.137876    1.400252    1.996238    3.168411    3.681803    4.972762    5.395854
 i,qaaresolved 1 -0.513530323
 i,qaaresolved 2  1.32302347
 i,qaaresolved 3  2.48935613

 qvv(*) eigenvalues. symmetry block  2
     3.920109

 fdd(*) eigenvalues. symmetry block  3
    -1.308656

 qvv(*) eigenvalues. symmetry block  3
     0.451615    1.202182    1.693687    3.473501    3.798379    5.597780

 qvv(*) eigenvalues. symmetry block  4
     2.239079    3.920577

 fdd(*) eigenvalues. symmetry block  5
   -22.479979   -1.596185

 qvv(*) eigenvalues. symmetry block  5
     0.470010    0.911838    1.531308    1.919512    2.433170    3.694032    4.473030    5.225339    6.227285
 i,qaaresolved 1  0.202515537
 i,qaaresolved 2  1.50972164
 i,qaaresolved 3  3.60801458

 qvv(*) eigenvalues. symmetry block  6
     4.404827

 fdd(*) eigenvalues. symmetry block  7
    -1.029389

 qvv(*) eigenvalues. symmetry block  7
     0.598161    1.842901    2.480858    3.435073    4.865257    6.735120

 qvv(*) eigenvalues. symmetry block  8
     2.557021    4.702401
 *** warning *** small active-orbital occupation. i=  1 nocc= 1.8278E-04
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.120 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    2 emc=  -77.6748250691 demc= 2.0355E-03 wnorm= 2.4871E-03 knorm= 4.9162E-01 apxde= 2.9632E-04    *not converged* 

               starting mcscf iteration...   3
 !timer:                                 user+sys=     0.330 walltime=     0.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.090 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:           1800
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           3816

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   3 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7054260322     -111.0020734540        0.0000000000        0.0000010000
    2       -77.2512849196     -110.5479323413        0.0000000000        0.0000010000
    3       -76.9157955037     -110.2124429255        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.010 walltime=     0.000
 ciiter=   5 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          user+sys=     0.010 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688236918     -111.3654711136        0.0000001109        0.0000010000
    2       -77.5245923697     -110.8212397914        0.0030845847        0.0100000000
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  6.13708559E-05
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.010 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 Total number of micro iterations:    8

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.97859917 pnorm= 5.9886E-03 rznorm= 1.5439E-07 rpnorm= 5.3833E-07 noldr=  8 nnewr=  8 nolds=  7 nnews=  7

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.010 walltime=     0.000
 !timer: solvek total                    user+sys=     0.010 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.482133   -2.088015   -1.193021

 qvv(*) eigenvalues. symmetry block  1
     0.396602    1.138036    1.400467    1.996446    3.168692    3.682056    4.972989    5.396074
 i,qaaresolved 1 -0.513251686
 i,qaaresolved 2  1.32298091
 i,qaaresolved 3  2.55685348

 qvv(*) eigenvalues. symmetry block  2
     3.853345

 fdd(*) eigenvalues. symmetry block  3
    -1.308387

 qvv(*) eigenvalues. symmetry block  3
     0.451728    1.202376    1.693962    3.473663    3.798625    5.598075

 qvv(*) eigenvalues. symmetry block  4
     2.239353    3.920797

 fdd(*) eigenvalues. symmetry block  5
   -22.479324   -1.595947

 qvv(*) eigenvalues. symmetry block  5
     0.470080    0.911949    1.531524    1.919791    2.433406    3.694256    4.473345    5.225555    6.227550
 i,qaaresolved 1  0.202823877
 i,qaaresolved 2  1.50946356
 i,qaaresolved 3  3.77293494

 qvv(*) eigenvalues. symmetry block  6
     4.240734

 fdd(*) eigenvalues. symmetry block  7
    -1.029177

 qvv(*) eigenvalues. symmetry block  7
     0.598223    1.843174    2.480996    3.435172    4.865506    6.735414

 qvv(*) eigenvalues. symmetry block  8
     2.557244    4.702640
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.6120E-04
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.120 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    3 emc=  -77.6751782145 demc= 3.5315E-04 wnorm= 4.9097E-04 knorm= 2.0577E-01 apxde= 2.9489E-05    *not converged* 

               starting mcscf iteration...   4
 !timer:                                 user+sys=     0.450 walltime=     0.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.090 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:           1800
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           3816

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   3 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7055259523     -111.0021733741        0.0000000000        0.0000010000
    2       -77.2512836047     -110.5479310265        0.0000000000        0.0000010000
    3       -76.9158872593     -110.2125346811        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   5 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688197161     -111.3654671378        0.0000001405        0.0000010000
    2       -77.5246836403     -110.8213310620        0.0032723448        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  1.03511142E-05
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99975733 pnorm= 7.0941E-04 rznorm= 2.9090E-07 rpnorm= 3.8724E-07 noldr=  7 nnewr=  7 nolds=  6 nnews=  6

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.010 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.481979   -2.087928   -1.192952

 qvv(*) eigenvalues. symmetry block  1
     0.396619    1.138066    1.400504    1.996489    3.168746    3.682106    4.973031    5.396114
 i,qaaresolved 1 -0.513171914
 i,qaaresolved 2  1.32297227
 i,qaaresolved 3  2.57681532

 qvv(*) eigenvalues. symmetry block  2
     3.833541

 fdd(*) eigenvalues. symmetry block  3
    -1.308338

 qvv(*) eigenvalues. symmetry block  3
     0.451747    1.202411    1.694013    3.473691    3.798667    5.598133

 qvv(*) eigenvalues. symmetry block  4
     2.239412    3.920837

 fdd(*) eigenvalues. symmetry block  5
   -22.479170   -1.595898

 qvv(*) eigenvalues. symmetry block  5
     0.470092    0.911970    1.531560    1.919851    2.433454    3.694295    4.473410    5.225595    6.227600
 i,qaaresolved 1  0.202876739
 i,qaaresolved 2  1.50961794
 i,qaaresolved 3  3.91992745

 qvv(*) eigenvalues. symmetry block  6
     4.093718

 fdd(*) eigenvalues. symmetry block  7
    -1.029137

 qvv(*) eigenvalues. symmetry block  7
     0.598233    1.843226    2.481020    3.435188    4.865552    6.735471

 qvv(*) eigenvalues. symmetry block  8
     2.557295    4.702682
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.5873E-04
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.120 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    4 emc=  -77.6752097577 demc= 3.1543E-05 wnorm= 8.2809E-05 knorm= 2.2029E-02 apxde= 2.7959E-07    *not converged* 

               starting mcscf iteration...   5
 !timer:                                 user+sys=     0.570 walltime=     0.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.080 walltime=     1.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:           1800
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           3816

 !timer: mosrt1                          user+sys=     0.020 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.020 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   3 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7055318460     -111.0021792678        0.0000000000        0.0000010000
    2       -77.2512809581     -110.5479283798        0.0000000000        0.0000010000
    3       -76.9158601004     -110.2125075221        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   5 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688173184     -111.3654647401        0.0000001250        0.0000010000
    2       -77.5246839636     -110.8213313854        0.0038404213        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.010 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  1.41540415E-07
 performing all-state projection
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999995 pnorm= 8.4931E-06 rznorm= 6.2158E-07 rpnorm= 9.8240E-07 noldr=  4 nnewr=  4 nolds=  1 nnews=  1

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.481960   -2.087919   -1.192944

 qvv(*) eigenvalues. symmetry block  1
     0.396620    1.138070    1.400507    1.996494    3.168752    3.682111    4.973035    5.396117
 i,qaaresolved 1 -0.513160555
 i,qaaresolved 2  1.32297344
 i,qaaresolved 3  2.57793254

 qvv(*) eigenvalues. symmetry block  2
     3.832439

 fdd(*) eigenvalues. symmetry block  3
    -1.308334

 qvv(*) eigenvalues. symmetry block  3
     0.451748    1.202414    1.694018    3.473693    3.798671    5.598139

 qvv(*) eigenvalues. symmetry block  4
     2.239419    3.920840

 fdd(*) eigenvalues. symmetry block  5
   -22.479150   -1.595892

 qvv(*) eigenvalues. symmetry block  5
     0.470093    0.911973    1.531563    1.919858    2.433460    3.694299    4.473417    5.225599    6.227605
 i,qaaresolved 1  0.202882209
 i,qaaresolved 2  1.50964985
 i,qaaresolved 3  3.93710585

 qvv(*) eigenvalues. symmetry block  6
     4.076524

 fdd(*) eigenvalues. symmetry block  7
    -1.029133

 qvv(*) eigenvalues. symmetry block  7
     0.598233    1.843231    2.481022    3.435189    4.865557    6.735477

 qvv(*) eigenvalues. symmetry block  8
     2.557301    4.702686
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.5718E-04
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.110 walltime=     1.000

 all mcscf convergence criteria are not satisfied.
 iter=    5 emc=  -77.6752100408 demc= 2.8311E-07 wnorm= 1.1323E-06 knorm= 3.2723E-04 apxde= 5.9968E-11    *not converged* 

               starting mcscf iteration...   6
 !timer:                                 user+sys=     0.680 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.090 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:           1800
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           3816

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.010 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   3 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          user+sys=     0.010 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7055319238     -111.0021793456        0.0000000000        0.0000010000
    2       -77.2512809123     -110.5479283341        0.0000000000        0.0000010000
    3       -76.9158595738     -110.2125069955        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   5 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688172865     -111.3654647083        0.0000001011        0.0000010000
    2       -77.5246876944     -110.8213351161        0.0035386651        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  8.56327947E-08
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 4.7170E-07 rpnorm= 4.1944E-09 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.481960   -2.087918   -1.192944

 qvv(*) eigenvalues. symmetry block  1
     0.396620    1.138070    1.400507    1.996494    3.168752    3.682111    4.973035    5.396117
 i,qaaresolved 1 -0.513160453
 i,qaaresolved 2  1.32297343
 i,qaaresolved 3  2.57793981

 qvv(*) eigenvalues. symmetry block  2
     3.832432

 fdd(*) eigenvalues. symmetry block  3
    -1.308334

 qvv(*) eigenvalues. symmetry block  3
     0.451748    1.202414    1.694018    3.473693    3.798671    5.598139

 qvv(*) eigenvalues. symmetry block  4
     2.239419    3.920840

 fdd(*) eigenvalues. symmetry block  5
   -22.479150   -1.595892

 qvv(*) eigenvalues. symmetry block  5
     0.470093    0.911973    1.531563    1.919858    2.433460    3.694299    4.473417    5.225599    6.227605
 i,qaaresolved 1  0.202882246
 i,qaaresolved 2  1.50965028
 i,qaaresolved 3  3.93736261

 qvv(*) eigenvalues. symmetry block  6
     4.076267

 fdd(*) eigenvalues. symmetry block  7
    -1.029133

 qvv(*) eigenvalues. symmetry block  7
     0.598233    1.843231    2.481022    3.435189    4.865557    6.735477

 qvv(*) eigenvalues. symmetry block  8
     2.557301    4.702686
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.5716E-04
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.120 walltime=     0.000

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    6 emc=  -77.6752100409 demc= 6.0552E-11 wnorm= 6.8506E-07 knorm= 2.2051E-08 apxde= 7.2801E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=  -77.705531924
   DRT #1 state # 2 weight 0.333333 total energy=  -77.251280912
   DRT #2 state # 1 weight 0.333333 total energy=  -78.068817287
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  Ag  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1C1s       0.70789518    -0.02125812    -0.00419445     0.04430833     0.08307301    -0.18292772     0.48704798    -0.13481072
   2C1s       0.00581352     0.60895592    -0.04527383    -0.12265239     0.35863539    -0.63805014     2.25414431    -0.47025737
   3C1s      -0.00547862    -0.16589846     0.04523067    -1.31481302     0.17135522     0.87512771    -3.58613701     1.23194586
   4C1pz      0.00009656    -0.19215706     0.60524810    -0.19758765    -0.62248467     0.39670575     0.34528030     0.00446522
   5C1pz     -0.00034704     0.06340722    -0.14182451    -0.43686892     1.30687341    -0.26120556    -0.82020586     0.58755204
   6C1d0      0.00005798     0.00626140    -0.00649825    -0.00111976     0.01568525    -0.00887776     0.03693396    -0.00141581
   7C1d2+    -0.00013011    -0.00045070    -0.00596111     0.00709739     0.01743193     0.10416193     0.03786626    -0.31728394
   8H1s      -0.00029124     0.18611069     0.28707364     0.01914449    -0.39766694    -0.82103730    -0.05394613    -0.90606010
   9H1s       0.00123292    -0.08687924    -0.08065989     0.92002316     0.16984638     0.55627963     0.85471213     0.29957990
  10H1py      0.00026532    -0.01400472    -0.01377760    -0.00951912    -0.00544873    -0.08674980    -0.14849809    -0.00318511
  11H1pz      0.00005891    -0.01122019     0.00216074    -0.01003212     0.03168852    -0.02487705    -0.05390124    -0.17152526

               MO    9        MO   10        MO   11
   1C1s      -0.34971450    -0.18329942     0.38539758
   2C1s      -1.41194045    -0.62456947     1.81165451
   3C1s       1.10255577     1.03905435    -0.29708401
   4C1pz      0.37263706     0.56080416     0.81853354
   5C1pz     -0.86083433    -0.06780179    -0.14021489
   6C1d0      0.14899851     0.21481558    -0.07376167
   7C1d2+    -0.11166505     0.10298365    -0.30122434
   8H1s       0.02447667    -0.06193176    -1.24170895
   9H1s      -0.01491009    -0.20760172     0.37429927
  10H1py     -0.16434376     0.43109355     0.62560333
  11H1pz      0.28379097    -0.40700884     0.54466797

          mcscf orbitals of the final iteration,  B3u block   2

               MO   12        MO   13        MO   14        MO   15
  12C1px      0.55554859    -1.02179412     0.39636809    -0.27254125
  13C1px      0.04336479     1.08506687    -0.27439335    -0.09046099
  14C1d1+    -0.02801880     0.10915493     0.57900662    -0.27863586
  15H1px      0.01283761    -0.00908218     0.03954878     0.58469177

          mcscf orbitals of the final iteration,  B2u block   3

               MO   16        MO   17        MO   18        MO   19        MO   20        MO   21        MO   22
  16C1py      0.47042675    -0.33298454    -0.09388374     0.88527301     0.03173955    -0.12953450     1.61867682
  17C1py     -0.10000397    -0.61887841     0.44593272    -1.80014931    -0.62745284     0.26171177    -0.11109381
  18C1d1-    -0.00317254     0.00605824    -0.15277815    -0.23917063    -0.35471794    -0.45718470     0.78834029
  19H1s       0.35459512     0.05570864    -0.87738569    -0.02167612     0.71217866     0.37110562    -1.46892220
  20H1s      -0.12374527     1.14843080     0.66300896     1.11237275    -0.21015399    -0.46196206     0.29500536
  21H1py     -0.01160227    -0.01036981    -0.01506969    -0.14307120     0.34445877    -0.08179831     0.69655815
  22H1pz     -0.01152725    -0.00685101    -0.06786469    -0.06700664    -0.20784379     0.45500240     0.42185114

          mcscf orbitals of the final iteration,  B1g block   4

               MO   23        MO   24
  23C1d2-     0.48444497    -0.59271976
  24H1px      0.19466564     0.55631469

          mcscf orbitals of the final iteration,  B1u block   5

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  25C1s       0.70855286    -0.01340781     0.04957164     0.06712526    -0.00265026     0.11792502     0.63736523     0.19741054
  26C1s       0.00733862     0.47983603    -0.17797654     0.02399408    -0.43511677     0.46080648     2.75322050     0.94911157
  27C1s      -0.00863951    -0.10291268    -1.46014848    -3.53706913    -0.27286196     0.40020848    -7.69384757     0.15726372
  28C1pz     -0.00041518     0.23643329    -0.11392983     0.23623729     0.26276425     0.99063643    -0.11844411    -0.50693321
  29C1pz      0.00125221    -0.04570261    -0.26761926     3.17972852     0.18443628    -2.58411140     2.19910640    -0.95389396
  30C1d0      0.00015912    -0.00357693     0.00085945    -0.01103128     0.02996459     0.08147676    -0.06643666     0.02893278
  31C1d2+    -0.00033814    -0.00302584     0.01225314     0.01809291    -0.09247722     0.05553872    -0.04158997     0.16300339
  32H1s      -0.00059218     0.33394434     0.03304199    -0.18414621     0.90536724    -0.14168963     0.41288788     0.46566414
  33H1s       0.00101711    -0.13267896     1.06271345    -0.60904854    -0.89405861     0.99460089     0.07369108    -0.03778478
  34H1py      0.00045712    -0.02080505    -0.01753765    -0.01815173     0.09108746    -0.13358612    -0.09325182    -0.06654849
  35H1pz      0.00025877    -0.00930595    -0.00528388    -0.00288432     0.05891838     0.00124859    -0.08301979     0.43804277

               MO   33        MO   34        MO   35
  25C1s      -0.15261112     0.44546851    -0.05184853
  26C1s      -0.38768118     2.08504832     0.12585198
  27C1s       4.46681380    -0.86057892     2.94599375
  28C1pz     -1.32117745    -0.42221342     1.15502950
  29C1pz     -1.63861053     0.60881006    -1.14096970
  30C1d0      0.23222081     0.26577348    -0.15668465
  31C1d2+    -0.24406196    -0.00407239    -0.57292515
  32H1s      -0.46012827    -0.44423080    -1.73308582
  33H1s       0.52651807    -0.08155227     0.60429476
  34H1py     -0.25663262     0.62928046     0.43108333
  35H1pz      0.06431219    -0.14738179     0.69593616

          mcscf orbitals of the final iteration,  B2g block   6

               MO   36        MO   37        MO   38        MO   39
  36C1px      0.54721127    -1.13513676    -0.40746041     0.03501410
  37C1px      0.46705047     1.67057417    -0.36222421     0.32643906
  38C1d1+     0.02218513    -0.08183134     1.00284503     0.56819168
  39H1px      0.01667897    -0.02662566     0.13533991    -0.59088408

          mcscf orbitals of the final iteration,  B3g block   7

               MO   40        MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  40C1py      0.44984201    -0.29584348    -1.02821525     0.25254009     0.10916849     1.19420809     0.86200186
  41C1py     -0.08398036    -1.49710054     2.57439494    -3.10809629    -2.17097898     0.00472679     0.25520307
  42C1d1-     0.03737217    -0.02945877     0.12792420     0.25520872    -0.05252463    -0.29906002     1.94531629
  43H1s       0.43044374    -0.02923889     0.21514072     1.12190930     0.91815107    -0.35986859    -1.99134944
  44H1s      -0.09564102     1.68862659    -1.38213846     0.34516886     0.18813390    -0.29172929     0.79500990
  45H1py     -0.00747523    -0.01458727     0.12306619    -0.03082209     0.34835783     0.39398100     0.77370936
  46H1pz     -0.01274156    -0.01608321     0.13342660     0.14373917    -0.33825644     0.49060157     0.08832708

          mcscf orbitals of the final iteration,  Au  block   8

               MO   47        MO   48
  47C1d2-     0.39770309    -0.87759076
  48H1px      0.30607526     0.54314409

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1C1s       0.70789518    -0.02125812    -0.00419445     0.04430834     0.08307301    -0.18292772     0.48704798    -0.13481071
   2C1s       0.00581353     0.60895592    -0.04527384    -0.12265237     0.35863539    -0.63805014     2.25414431    -0.47025734
   3C1s      -0.00547862    -0.16589845     0.04523068    -1.31481305     0.17135523     0.87512769    -3.58613701     1.23194583
   4C1pz      0.00009656    -0.19215705     0.60524810    -0.19758764    -0.62248465     0.39670578     0.34528030     0.00446520
   5C1pz     -0.00034704     0.06340722    -0.14182451    -0.43686894     1.30687340    -0.26120561    -0.82020586     0.58755207
   6C1d0      0.00005798     0.00626140    -0.00649825    -0.00111975     0.01568525    -0.00887776     0.03693396    -0.00141581
   7C1d2+    -0.00013011    -0.00045070    -0.00596111     0.00709739     0.01743193     0.10416192     0.03786626    -0.31728394
   8H1s      -0.00029124     0.18611070     0.28707364     0.01914449    -0.39766697    -0.82103729    -0.05394612    -0.90606009
   9H1s       0.00123292    -0.08687924    -0.08065989     0.92002317     0.16984641     0.55627963     0.85471211     0.29957990
  10H1py      0.00026532    -0.01400472    -0.01377760    -0.00951912    -0.00544873    -0.08674981    -0.14849809    -0.00318511
  11H1pz      0.00005891    -0.01122019     0.00216074    -0.01003212     0.03168852    -0.02487705    -0.05390124    -0.17152527

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

   1C1s      -0.34971450    -0.18329943     0.38539759
   2C1s      -1.41194044    -0.62456950     1.81165452
   3C1s       1.10255578     1.03905437    -0.29708402
   4C1pz      0.37263705     0.56080416     0.81853355
   5C1pz     -0.86083432    -0.06780180    -0.14021489
   6C1d0      0.14899851     0.21481558    -0.07376167
   7C1d2+    -0.11166506     0.10298365    -0.30122434
   8H1s       0.02447665    -0.06193176    -1.24170895
   9H1s      -0.01491008    -0.20760172     0.37429927
  10H1py     -0.16434377     0.43109354     0.62560333
  11H1pz      0.28379097    -0.40700883     0.54466796

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     1.19907189     0.10444987     0.00188443     0.00000000

  12C1px      0.54986055    -1.03254703     0.37590576    -0.27254125
  13C1px      0.04671188     1.09271608    -0.24151997    -0.09046099
  14C1d1+    -0.03841385     0.09202694     0.58138106    -0.27863586
  15H1px      0.01214816    -0.01023218     0.03948570     0.58469177

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  16C1py      0.47042675    -0.33298453    -0.09388373     0.88527300     0.03173957    -0.12953449     1.61867683
  17C1py     -0.10000397    -0.61887843     0.44593272    -1.80014930    -0.62745286     0.26171175    -0.11109382
  18C1d1-    -0.00317254     0.00605824    -0.15277815    -0.23917063    -0.35471792    -0.45718471     0.78834029
  19H1s       0.35459512     0.05570863    -0.87738570    -0.02167611     0.71217863     0.37110563    -1.46892220
  20H1s      -0.12374527     1.14843082     0.66300896     1.11237273    -0.21015396    -0.46196206     0.29500537
  21H1py     -0.01160227    -0.01036981    -0.01506969    -0.14307120     0.34445878    -0.08179829     0.69655815
  22H1pz     -0.01152725    -0.00685101    -0.06786469    -0.06700664    -0.20784380     0.45500239     0.42185114

          natural orbitals of the final iteration, block  4

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  23C1d2-     0.48444496    -0.59271976
  24H1px      0.19466564     0.55631468

          natural orbitals of the final iteration, block  5

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  25C1s       0.70855286    -0.01340781     0.04957164     0.06712527    -0.00265026     0.11792502     0.63736522     0.19741054
  26C1s       0.00733862     0.47983603    -0.17797651     0.02399410    -0.43511676     0.46080649     2.75322049     0.94911157
  27C1s      -0.00863951    -0.10291268    -1.46014848    -3.53706921    -0.27286214     0.40020838    -7.69384755     0.15726368
  28C1pz     -0.00041518     0.23643329    -0.11392983     0.23623727     0.26276420     0.99063643    -0.11844412    -0.50693320
  29C1pz      0.00125221    -0.04570261    -0.26761930     3.17972857     0.18443648    -2.58411133     2.19910640    -0.95389394
  30C1d0      0.00015912    -0.00357693     0.00085945    -0.01103128     0.02996458     0.08147676    -0.06643666     0.02893278
  31C1d2+    -0.00033814    -0.00302584     0.01225314     0.01809291    -0.09247722     0.05553872    -0.04158997     0.16300338
  32H1s      -0.00059218     0.33394434     0.03304199    -0.18414622     0.90536725    -0.14168958     0.41288787     0.46566413
  33H1s       0.00101711    -0.13267896     1.06271347    -0.60904853    -0.89405866     0.99460083     0.07369108    -0.03778478
  34H1py      0.00045712    -0.02080505    -0.01753765    -0.01815173     0.09108746    -0.13358612    -0.09325183    -0.06654849
  35H1pz      0.00025877    -0.00930595    -0.00528388    -0.00288432     0.05891838     0.00124860    -0.08301980     0.43804277

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

  25C1s      -0.15261113     0.44546851    -0.05184853
  26C1s      -0.38768124     2.08504833     0.12585197
  27C1s       4.46681382    -0.86057881     2.94599372
  28C1pz     -1.32117744    -0.42221345     1.15502952
  29C1pz     -1.63861053     0.60881000    -1.14096970
  30C1d0      0.23222080     0.26577349    -0.15668465
  31C1d2+    -0.24406196    -0.00407240    -0.57292515
  32H1s      -0.46012826    -0.44423081    -1.73308582
  33H1s       0.52651807    -0.08155225     0.60429476
  34H1py     -0.25663264     0.62928045     0.43108332
  35H1pz      0.06431220    -0.14738179     0.69593616

          natural orbitals of the final iteration, block  6

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     0.45836289     0.23597376     0.00025716     0.00000000

  36C1px      0.61389549    -1.11123759    -0.37720413     0.03501410
  37C1px      0.36836792     1.68507462    -0.40618861     0.32643906
  38C1d1+     0.02522844    -0.05389050     1.00466216     0.56819168
  39H1px      0.01798459    -0.02201508     0.13599961    -0.59088408

          natural orbitals of the final iteration, block  7

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  40C1py      0.44984201    -0.29584347    -1.02821524     0.25254009     0.10916849     1.19420811     0.86200186
  41C1py     -0.08398036    -1.49710057     2.57439490    -3.10809631    -2.17097898     0.00472677     0.25520306
  42C1d1-     0.03737217    -0.02945877     0.12792420     0.25520872    -0.05252462    -0.29906002     1.94531629
  43H1s       0.43044374    -0.02923890     0.21514072     1.12190931     0.91815106    -0.35986860    -1.99134943
  44H1s      -0.09564102     1.68862660    -1.38213843     0.34516887     0.18813391    -0.29172928     0.79500990
  45H1py     -0.00747523    -0.01458727     0.12306620    -0.03082210     0.34835783     0.39398101     0.77370936
  46H1pz     -0.01274156    -0.01608321     0.13342661     0.14373917    -0.33825644     0.49060157     0.08832708

          natural orbitals of the final iteration, block  8

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  47C1d2-     0.39770308    -0.87759076
  48H1px      0.30607526     0.54314409
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        340 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       1.999302   1.478936   0.011194   0.000000   0.000000   0.000000
    C1_ p       0.000107   0.183883   1.381039   0.000000   0.000000   0.000000
    C1_ d       0.000067   0.015453   0.016069   0.000000   0.000000   0.000000
    H1_ s       0.000679   0.294765   0.575203   0.000000   0.000000   0.000000
    H1_ p      -0.000156   0.026963   0.016495   0.000000   0.000000   0.000000

   ao class       7Ag        8Ag        9Ag       10Ag       11Ag 

                        B3u partial gross atomic populations
   ao class       1B3u       2B3u       3B3u       4B3u
    C1_ p       1.166776   0.101259   0.000084   0.000000
    C1_ d       0.020740   0.003488   0.001741   0.000000
    H1_ p       0.011556  -0.000297   0.000059   0.000000

                        B2u partial gross atomic populations
   ao class       1B2u       2B2u       3B2u       4B2u       5B2u       6B2u
    C1_ p       1.165733   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.000142   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.815930   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.018195   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7B2u

                        B1g partial gross atomic populations
   ao class       1B1g       2B1g

                        B1u partial gross atomic populations
   ao class       1B1u       2B1u       3B1u       4B1u       5B1u       6B1u
    C1_ s       1.999950   0.762497   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.000368   0.387374   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000182   0.007791   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.000180   0.805462   0.000000   0.000000   0.000000   0.000000
    H1_ p      -0.000316   0.036876   0.000000   0.000000   0.000000   0.000000

   ao class       7B1u       8B1u       9B1u      10B1u      11B1u

                        B2g partial gross atomic populations
   ao class       1B2g       2B2g       3B2g       4B2g
    C1_ p       0.442909   0.237783   0.000006   0.000000
    C1_ d       0.006773  -0.000483   0.000223   0.000000
    H1_ p       0.008680  -0.001326   0.000029   0.000000

                        B3g partial gross atomic populations
   ao class       1B3g       2B3g       3B3g       4B3g       5B3g       6B3g
    C1_ p       0.816159   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.052081   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.120003   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011757   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7B3g

                        Au  partial gross atomic populations
   ao class       1Au        2Au 


                        gross atomic populations
     ao           C1_        H1_
      s         6.251879   3.612222
      p         5.883480   0.128515
      d         0.123904   0.000000
    total      12.259263   3.740737


 Total number of electrons:   16.00000000

 !timer: mcscf                           user+sys=     0.810 walltime=     1.000
