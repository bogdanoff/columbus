1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:51 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:51 2003

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:51 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:51 2003
  h2o                                                                            

 297 dimension of the ci-matrix ->>>      9263


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -76.0282863112  0.0000E+00  2.5131E-01  9.9728E-01  1.0000E-04
 mraqcc  #  2  1    -76.2262728342  1.9799E-01  6.7480E-03  1.6511E-01  1.0000E-04
 mraqcc  #  3  1    -76.2385488232  1.2276E-02  3.7153E-04  3.5161E-02  1.0000E-04
 mraqcc  #  4  1    -76.2392857910  7.3697E-04  4.7445E-05  1.1370E-02  1.0000E-04
 mraqcc  #  5  1    -76.2393475984  6.1807E-05  3.5482E-06  3.6039E-03  1.0000E-04
 mraqcc  #  6  1    -76.2393530977  5.4993E-06  4.2214E-07  1.1013E-03  1.0000E-04
 mraqcc  #  7  1    -76.2393536909  5.9318E-07  5.7928E-08  4.3722E-04  1.0000E-04
 mraqcc  #  8  1    -76.2393537783  8.7472E-08  7.8285E-09  1.6843E-04  1.0000E-04
 mraqcc  #  9  1    -76.2393537892  1.0846E-08  9.4296E-10  5.7539E-05  1.0000E-04

 mraqcc   convergence criteria satisfied after  9 iterations.

 final mraqcc   convergence information:
 mraqcc  #  9  1    -76.2393537892  1.0846E-08  9.4296E-10  5.7539E-05  1.0000E-04
