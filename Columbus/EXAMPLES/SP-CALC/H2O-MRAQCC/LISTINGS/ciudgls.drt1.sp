1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 9263
global arrays:       305679   (    2.33 MB)
vdisk:                    0   (    0.00 MB)
drt:                 484939   (    1.85 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 3,
   GSET = 3,
  NROOT= 1,
   NBKITR= 1,
  RTOLCI=  0.000100,  0.000100,
  VOUT=10,
  DAVCOR=10,
  CSFPRN=10,
  NITER=  30,
  IDEN=   1,
   IVMODE=   3,
  NVCIMX = 16
 /&end
 ------------------------------------------------------------------------
 bummer (warning):2:changed keyword: nbkitr=         0
 bummer (warning):changed keyword: davcor=         0

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    3      nbkitr =    0      niter  =   30
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =  10      ctol   = 1.00E-02  davcor  =   0


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264082186

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:51 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:51 2003

 core energy values from the integral file:
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  9.187211027960E+00

 drt header information:
  h2o                                                                            
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     6 nfct  =     0 nfvt  =     0
 nrow  =    38 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        292       12       70      105      105
 nvalwt,nvalw:      292       12       70      105      105
 ncsft:            9263
 total number of valid internal walks:     292
 nvalz,nvaly,nvalx,nvalw =       12      70     105     105

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   342   216    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     4791     5559     3156      936      138        0        0        0
 minbl4,minbl3,maxbl2   171   162   174
 maxbuf 32767
 number of external orbitals per symmetry block:   7   6   3   2
 nmsym   4 number of internal orbitals   6

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:51 2003
  h2o                                                                            
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:51 2003
  h2o                                                                            
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:    12    70   105   105 total internal walks:     292
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 3 1

 setref:       12 references kept,
                0 references were marked as invalid, out of
               12 total.
 limcnvrt: found 12 valid internal walksout of  12
  walks (skipping trailing invalids)
  ... adding  12 segmentation marks segtype= 1
 limcnvrt: found 70 valid internal walksout of  70
  walks (skipping trailing invalids)
  ... adding  70 segmentation marks segtype= 2
 limcnvrt: found 105 valid internal walksout of  105
  walks (skipping trailing invalids)
  ... adding  105 segmentation marks segtype= 3
 limcnvrt: found 105 valid internal walksout of  105
  walks (skipping trailing invalids)
  ... adding  105 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x      40      48      33      32
 vertex w      58      48      33      32



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          12|        12|         0|        12|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       365|        12|        70|        12|         2|
 -------------------------------------------------------------------------------
  X 3         105|      4052|       377|       105|        82|         3|
 -------------------------------------------------------------------------------
  W 4         105|      4834|      4429|       105|       187|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      9263


 297 dimension of the ci-matrix ->>>      9263


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1     105      12       4052         12     105      12
     2  4   1    25      two-ext wz   2X  4 1     105      12       4834         12     105      12
     3  4   3    26      two-ext wx   2X  4 3     105     105       4834       4052     105     105
     4  2   1    11      one-ext yz   1X  2 1      70      12        365         12      70      12
     5  3   2    15      1ex3ex  yx   3X  3 2     105      70       4052        365     105      70
     6  4   2    16      1ex3ex  yw   3X  4 2     105      70       4834        365     105      70
     7  1   1     1      allint zz    OX  1 1      12      12         12         12      12      12
     8  2   2     5      0ex2ex yy    OX  2 2      70      70        365        365      70      70
     9  3   3     6      0ex2ex xx    OX  3 3     105     105       4052       4052     105     105
    10  4   4     7      0ex2ex ww    OX  4 4     105     105       4834       4834     105     105
    11  1   1    75      dg-024ext z  DG  1 1      12      12         12         12      12      12
    12  2   2    45      4exdg024 y   DG  2 2      70      70        365        365      70      70
    13  3   3    46      4exdg024 x   DG  3 3     105     105       4052       4052     105     105
    14  4   4    47      4exdg024 w   DG  4 4     105     105       4834       4834     105     105
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 5031 1500 280
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       216 2x:         0 4x:         0
All internal counts: zz :       162 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       160    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       184    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension      12

    root           eigenvalues
    ----           ------------
       1         -76.0282863112

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.377777778

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -76.0282863112

  ### active excitation selection ###

 Inactive orbitals:
    there are   12 all-active excitations of which   12 are references.

    the   12 reference all-active excitation csfs

    ------------------------------------------------
       1   2   3   4   5   6   7   8   9  10  11  12
    ------------------------------------------------
  1:   2   2   2   2   2   2   2   2   1   1   1   0
  2:   2   2   2   2   2   1   1   0   2   2  -1   2
  3:   2   2   2   1   0   2  -1   2   2  -1   2   2
  4:   2   2   0   2   2   2   2   2   2   2   2   2
  5:   2   0   2   2   2   2   2   2   2   2   2   2
  6:   0   2   2  -1   2  -1   2   2  -1   2   2   2

  ### end active excitation selection ###


################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999986
 the acpf density matrix will be calculated

 space required:

    space required for calls in multd2:
       onex           91
       allin           0
       diagon        290
    max.      ---------
       maxnex        290

    total core space usage:
       maxnex        290
    max k-seg       4834
    max k-ind        105
              ---------
       totmax      10168
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      24  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      66 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              9263
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -76.0282863112  0.0000E+00  2.5131E-01  9.9728E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.00   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.01   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97642440    -0.21585968

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -76.2262728342  1.9799E-01  6.7480E-03  1.6511E-01  1.0000E-04
 mraqcc  #  2  2    -71.9772155604  8.1164E+01  0.0000E+00  1.7253E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.01   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3300s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97414074    -0.03907025     0.22253839

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -76.2385488232  1.2276E-02  3.7153E-04  3.5161E-02  1.0000E-04
 mraqcc  #  3  2    -72.9480017130  9.7079E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  3  3    -72.0942491620  8.1281E+01  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.00
    3   26    0   0.06   0.00   0.00   0.06
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.05   0.01   0.00   0.04
    6   16    0   0.03   0.00   0.00   0.03
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.06   0.00   0.00   0.05
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.01
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.2800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3400s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97344315     0.10240034     0.06372835     0.19457983

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -76.2392857910  7.3697E-04  4.7445E-05  1.1370E-02  1.0000E-04
 mraqcc  #  4  2    -73.9573904485  1.0094E+00  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  3    -72.7085272351  6.1428E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  4    -71.6770445350  8.0864E+01  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.01   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3400s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97327267    -0.08189662     0.06788685    -0.09267314     0.18120792

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -76.2393475984  6.1807E-05  3.5482E-06  3.6039E-03  1.0000E-04
 mraqcc  #  5  2    -74.5244869989  5.6710E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -73.6819930502  9.7347E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  4    -72.5325227530  8.5548E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  5    -71.4903861297  8.0678E+01  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.00   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.01   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3400s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97324978    -0.07000427    -0.05543098    -0.08132911     0.09234640     0.17224806

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -76.2393530977  5.4993E-06  4.2214E-07  1.1013E-03  1.0000E-04
 mraqcc  #  6  2    -74.8060982325  2.8161E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -73.6955504995  1.3557E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  4    -73.0465267179  5.1400E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  5    -72.5325537200  1.0422E+00  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  6    -71.4028153886  8.0590E+01  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3500s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97324913     0.04205275    -0.07928475    -0.01275104     0.08484265     0.10223455     0.16406965

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -76.2393536909  5.9318E-07  5.7928E-08  4.3722E-04  1.0000E-04
 mraqcc  #  7  2    -75.0933598902  2.8726E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  3    -74.2148392204  5.1929E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  4    -73.3692751158  3.2275E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  5    -73.0267908735  4.9424E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  6    -72.4400017303  1.0372E+00  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  7    -71.2981131690  8.0485E+01  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.01   0.00   0.01
    5   15    0   0.04   0.00   0.00   0.04
    6   16    0   0.09   0.01   0.00   0.06
    7    1    0   0.02   0.00   0.00   0.02
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.06   0.01   0.00   0.06
   10    7    0   0.05   0.01   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.01   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.4000s 
time spent in multnx:                   0.3700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.4200s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97324985     0.01469129    -0.08661277     0.01176755     0.04653982    -0.12454522    -0.04172941    -0.15971801

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -76.2393537783  8.7472E-08  7.8285E-09  1.6843E-04  1.0000E-04
 mraqcc  #  8  2    -75.4562380319  3.6288E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  3    -74.5168692393  3.0203E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  4    -73.3853667908  1.6092E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  5    -73.1552292617  1.2844E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  6    -72.5304029256  9.0401E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  7    -72.3972960638  1.0992E+00  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  8    -71.2734389197  8.0461E+01  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.05   0.00   0.00   0.05
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3300s 
time spent in multnx:                   0.3300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0400s 
total time per CI iteration:            0.3900s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5031 2x:      1500 4x:       280
All internal counts: zz :       162 yy:      1462 xx:      2689 ww:      2296
One-external counts: yz :       884 yx:      5644 yw:      5302
Two-external counts: yy :      1075 ww:      1368 xx:      1857 xz:       192 wz:       227 wx:      2145
Three-ext.   counts: yx :      1244 yw:      1231

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       178    task #   2:       198    task #   3:      1642    task #   4:       834
task #   5:      4732    task #   6:      4414    task #   7:       160    task #   8:      1248
task #   9:      2277    task #  10:      1925    task #  11:       184    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97324935     0.01181240     0.07185474     0.07363942     0.05011722    -0.03408321     0.11760194    -0.02614624

                ci   9
 ref:   1     0.15450906

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1    -76.2393537892  1.0846E-08  9.4296E-10  5.7539E-05  1.0000E-04
 mraqcc  #  9  2    -75.5620464095  1.0581E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  3    -74.6702924706  1.5342E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  4    -73.4468899627  6.1523E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  5    -73.3712316886  2.1600E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  6    -72.9206905894  3.9029E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  7    -72.4802376010  8.2942E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  8    -72.0579938419  7.8455E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  9    -71.1364512589  8.0324E+01  0.0000E+00  0.0000E+00  1.0000E-04


 mraqcc   convergence criteria satisfied after  9 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1    -76.2393537892  1.0846E-08  9.4296E-10  5.7539E-05  1.0000E-04
 mraqcc  #  9  2    -75.5620464095  1.0581E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  3    -74.6702924706  1.5342E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  4    -73.4468899627  6.1523E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  5    -73.3712316886  2.1600E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  6    -72.9206905894  3.9029E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  7    -72.4802376010  8.2942E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  8    -72.0579938419  7.8455E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  9    -71.1364512589  8.0324E+01  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc vector at position   1 energy=  -76.239353789195

################END OF CIUDGINFO################


 a4den factor =  1.033917031 for root   1
    1 of the  10 expansion vectors are transformed.
    1 of the   9 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.973249353)
 reference energy= -76.0282863
 total aqcc energy= -76.2393538
 diagonal element shift (lrtshift) =  -0.211067478

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2393537892

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     1    2    3   12   19    4

                                         symmetry   a1   a1   a1   b1   b2   a1 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.972643                        +-   +-   +-   +-   +-      
 z*  1  1       2  0.013031                        +-   +-   +-   +-        +- 
 z*  1  1       3  0.014766                        +-   +-   +-        +-   +- 
 z*  1  1       5  0.017115                        +-   +-        +-   +-   +- 
 z*  1  1       7 -0.017003                        +-   +     -   +-   +-   +- 
 z*  1  1       8  0.012469                        +-        +-   +-   +-   +- 
 y   1  1      16 -0.011502              1( a1 )   +-   +-   +-   +-         - 
 y   1  1      39 -0.020109              1( a1 )   +-   +-   +-        +-    - 
 y   1  1      81  0.015490              1( b2 )   +-   +-   +    +-    -    - 
 y   1  1      84 -0.025609              1( b1 )   +-   +-   +     -   +-    - 
 y   1  1      85 -0.019494              2( b1 )   +-   +-   +     -   +-    - 
 y   1  1      86  0.012294              3( b1 )   +-   +-   +     -   +-    - 
 y   1  1      92 -0.016907              1( a1 )   +-   +-        +-   +-    - 
 y   1  1      93  0.013076              2( a1 )   +-   +-        +-   +-    - 
 y   1  1     163 -0.031001              1( b2 )   +-   +    +-   +-    -    - 
 y   1  1     165  0.010191              3( b2 )   +-   +    +-   +-    -    - 
 y   1  1     166  0.024782              1( b1 )   +-   +    +-    -   +-    - 
 y   1  1     167  0.010737              2( b1 )   +-   +    +-    -   +-    - 
 y   1  1     168 -0.024647              3( b1 )   +-   +    +-    -   +-    - 
 y   1  1     174  0.020411              1( a1 )   +-   +     -   +-   +-    - 
 y   1  1     175 -0.019799              2( a1 )   +-   +     -   +-   +-    - 
 y   1  1     190 -0.014760              1( a1 )   +-        +-   +-   +-    - 
 x   1  1     378  0.026945    1( b1 )   1( b2 )   +-   +-   +-    -    -      
 x   1  1     379  0.015098    2( b1 )   1( b2 )   +-   +-   +-    -    -      
 x   1  1     380 -0.027360    3( b1 )   1( b2 )   +-   +-   +-    -    -      
 x   1  1     382 -0.010005    5( b1 )   1( b2 )   +-   +-   +-    -    -      
 x   1  1     385  0.014295    2( b1 )   2( b2 )   +-   +-   +-    -    -      
 x   1  1     396 -0.020954    1( a1 )   1( a2 )   +-   +-   +-    -    -      
 x   1  1     399  0.011680    4( a1 )   1( a2 )   +-   +-   +-    -    -      
 x   1  1     400  0.011166    5( a1 )   1( a2 )   +-   +-   +-    -    -      
 x   1  1     403 -0.010376    1( a1 )   2( a2 )   +-   +-   +-    -    -      
 x   1  1     407  0.011361    5( a1 )   2( a2 )   +-   +-   +-    -    -      
 x   1  1     409  0.012252    7( a1 )   2( a2 )   +-   +-   +-    -    -      
 x   1  1     491  0.018432    1( a1 )   1( b2 )   +-   +-    -   +-    -      
 x   1  1     492 -0.032735    2( a1 )   1( b2 )   +-   +-    -   +-    -      
 x   1  1     500 -0.011566    3( a1 )   2( b2 )   +-   +-    -   +-    -      
 x   1  1     501 -0.011951    4( a1 )   2( b2 )   +-   +-    -   +-    -      
 x   1  1     507 -0.010620    3( a1 )   3( b2 )   +-   +-    -   +-    -      
 x   1  1     508 -0.010682    4( a1 )   3( b2 )   +-   +-    -   +-    -      
 x   1  1     510 -0.014059    6( a1 )   3( b2 )   +-   +-    -   +-    -      
 x   1  1     513 -0.015265    2( b1 )   1( a2 )   +-   +-    -   +-    -      
 x   1  1     565  0.025475    2( a1 )   1( b1 )   +-   +-    -    -   +-      
 x   1  1     572  0.023221    2( a1 )   2( b1 )   +-   +-    -    -   +-      
 x   1  1     573  0.020742    3( a1 )   2( b1 )   +-   +-    -    -   +-      
 x   1  1     576  0.011698    6( a1 )   2( b1 )   +-   +-    -    -   +-      
 x   1  1     578  0.013195    1( a1 )   3( b1 )   +-   +-    -    -   +-      
 x   1  1     579 -0.018641    2( a1 )   3( b1 )   +-   +-    -    -   +-      
 x   1  1     593 -0.012426    2( a1 )   5( b1 )   +-   +-    -    -   +-      
 x   1  1     945 -0.015270    4( a1 )   1( b2 )   +-    -   +-   +-    -      
 w   1  1    4439  0.013237    4( a1 )   4( a1 )   +-   +-   +-   +-           
 w   1  1    4440 -0.011026    1( a1 )   5( a1 )   +-   +-   +-   +-           
 w   1  1    4455  0.010210    5( a1 )   7( a1 )   +-   +-   +-   +-           
 w   1  1    4479  0.049428    1( b2 )   1( b2 )   +-   +-   +-   +-           
 w   1  1    4483  0.010152    2( b2 )   3( b2 )   +-   +-   +-   +-           
 w   1  1    4484  0.012682    3( b2 )   3( b2 )   +-   +-   +-   +-           
 w   1  1    4486  0.011102    1( a2 )   2( a2 )   +-   +-   +-   +-           
 w   1  1    4487  0.012370    2( a2 )   2( a2 )   +-   +-   +-   +-           
 w   1  1    4488 -0.025921    1( b1 )   1( b2 )   +-   +-   +-   +     -      
 w   1  1    4489 -0.015620    2( b1 )   1( b2 )   +-   +-   +-   +     -      
 w   1  1    4490  0.027979    3( b1 )   1( b2 )   +-   +-   +-   +     -      
 w   1  1    4492  0.011508    5( b1 )   1( b2 )   +-   +-   +-   +     -      
 w   1  1    4495 -0.011189    2( b1 )   2( b2 )   +-   +-   +-   +     -      
 w   1  1    4568  0.027890    1( a1 )   1( a1 )   +-   +-   +-        +-      
 w   1  1    4596  0.028678    1( b1 )   1( b1 )   +-   +-   +-        +-      
 w   1  1    4597  0.028946    1( b1 )   2( b1 )   +-   +-   +-        +-      
 w   1  1    4598  0.035354    2( b1 )   2( b1 )   +-   +-   +-        +-      
 w   1  1    4599 -0.026160    1( b1 )   3( b1 )   +-   +-   +-        +-      
 w   1  1    4600 -0.012455    2( b1 )   3( b1 )   +-   +-   +-        +-      
 w   1  1    4601  0.021406    3( b1 )   3( b1 )   +-   +-   +-        +-      
 w   1  1    4607 -0.014948    2( b1 )   5( b1 )   +-   +-   +-        +-      
 w   1  1    4610  0.010255    5( b1 )   5( b1 )   +-   +-   +-        +-      
 w   1  1    4623  0.014779    1( a2 )   1( a2 )   +-   +-   +-        +-      
 w   1  1    4717 -0.013843    1( a1 )   1( b2 )   +-   +-   +    +-    -      
 w   1  1    4718  0.035889    2( a1 )   1( b2 )   +-   +-   +    +-    -      
 w   1  1    4719 -0.010025    3( a1 )   1( b2 )   +-   +-   +    +-    -      
 w   1  1    4720  0.010032    4( a1 )   1( b2 )   +-   +-   +    +-    -      
 w   1  1    4739  0.012617    2( b1 )   1( a2 )   +-   +-   +    +-    -      
 w   1  1    4745  0.010379    2( b1 )   2( a2 )   +-   +-   +    +-    -      
 w   1  1    4808  0.023443    1( a1 )   1( b1 )   +-   +-   +     -   +-      
 w   1  1    4809 -0.019693    2( a1 )   1( b1 )   +-   +-   +     -   +-      
 w   1  1    4815  0.036978    1( a1 )   2( b1 )   +-   +-   +     -   +-      
 w   1  1    4816 -0.010844    2( a1 )   2( b1 )   +-   +-   +     -   +-      
 w   1  1    4818 -0.010154    4( a1 )   2( b1 )   +-   +-   +     -   +-      
 w   1  1    4819 -0.012734    5( a1 )   2( b1 )   +-   +-   +     -   +-      
 w   1  1    4822 -0.011886    1( a1 )   3( b1 )   +-   +-   +     -   +-      
 w   1  1    4823  0.020302    2( a1 )   3( b1 )   +-   +-   +     -   +-      
 w   1  1    4851 -0.013630    2( b2 )   1( a2 )   +-   +-   +     -   +-      
 w   1  1    5059  0.015281    1( a1 )   1( a1 )   +-   +-        +-   +-      
 w   1  1    5060 -0.017656    1( a1 )   2( a1 )   +-   +-        +-   +-      
 w   1  1    5061  0.034134    2( a1 )   2( a1 )   +-   +-        +-   +-      
 w   1  1    5064  0.014961    3( a1 )   3( a1 )   +-   +-        +-   +-      
 w   1  1    5066  0.016595    2( a1 )   4( a1 )   +-   +-        +-   +-      
 w   1  1    5079  0.011498    6( a1 )   6( a1 )   +-   +-        +-   +-      
 w   1  1    5088  0.018443    1( b1 )   2( b1 )   +-   +-        +-   +-      
 w   1  1    5089  0.025122    2( b1 )   2( b1 )   +-   +-        +-   +-      
 w   1  1    5098 -0.013176    2( b1 )   5( b1 )   +-   +-        +-   +-      
 w   1  1    5113  0.010521    3( b2 )   3( b2 )   +-   +-        +-   +-      
 w   1  1    5346  0.018185    1( a1 )   1( b2 )   +-   +    +-   +-    -      
 w   1  1    5348 -0.014149    3( a1 )   1( b2 )   +-   +    +-   +-    -      
 w   1  1    5349  0.021272    4( a1 )   1( b2 )   +-   +    +-   +-    -      
 w   1  1    5350 -0.016853    5( a1 )   1( b2 )   +-   +    +-   +-    -      
 w   1  1    5360 -0.011012    1( a1 )   3( b2 )   +-   +    +-   +-    -      
 w   1  1    5373  0.011420    1( b1 )   2( a2 )   +-   +    +-   +-    -      
 w   1  1    5437 -0.011958    1( a1 )   1( b1 )   +-   +    +-    -   +-      
 w   1  1    5439  0.012863    3( a1 )   1( b1 )   +-   +    +-    -   +-      
 w   1  1    5440 -0.012054    4( a1 )   1( b1 )   +-   +    +-    -   +-      
 w   1  1    5444 -0.021071    1( a1 )   2( b1 )   +-   +    +-    -   +-      
 w   1  1    5446  0.014128    3( a1 )   2( b1 )   +-   +    +-    -   +-      
 w   1  1    5450 -0.010405    7( a1 )   2( b1 )   +-   +    +-    -   +-      
 w   1  1    5451  0.014626    1( a1 )   3( b1 )   +-   +    +-    -   +-      
 w   1  1    5454  0.017616    4( a1 )   3( b1 )   +-   +    +-    -   +-      
 w   1  1    5480  0.012216    2( b2 )   1( a2 )   +-   +    +-    -   +-      
 w   1  1    5688 -0.018953    1( a1 )   1( a1 )   +-   +     -   +-   +-      
 w   1  1    5692 -0.012550    2( a1 )   3( a1 )   +-   +     -   +-   +-      
 w   1  1    5695  0.019812    2( a1 )   4( a1 )   +-   +     -   +-   +-      
 w   1  1    6227  0.013464    1( a1 )   1( a1 )   +-        +-   +-   +-      

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             115
     0.01> rq > 0.001            603
    0.001> rq > 0.0001          1973
   0.0001> rq > 0.00001         2266
  0.00001> rq > 0.000001        2927
 0.000001> rq                   1378
           all                  9263
NO coefficients and occupation numbers are written to nocoef_ci.1
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      86  DYX=     445  DYW=     430
   D0Z=      24  D0Y=     216  D0X=     377  D0W=     330
  DDZI=      66 DDYI=     390 DDXI=     570 DDWI=     540
  DDZE=       0 DDYE=      70 DDXE=     105 DDWE=     105
================================================================================
 root #  1: Scaling(2) with   1.03391703corresponding to ref #  1


*****   symmetry block  A1    *****

 occupation numbers of nos
   1.9999180      1.9849459      1.9667219      0.0248927      0.0110935
   0.0056479      0.0045399      0.0011651      0.0007282      0.0005051
   0.0000955


*****   symmetry block  B1    *****

 occupation numbers of nos
   1.9641642      0.0265547      0.0059078      0.0011997      0.0007203
   0.0005112      0.0000433


*****   symmetry block  B2    *****

 occupation numbers of nos
   1.9734538      0.0163021      0.0044294      0.0005554


*****   symmetry block  A2    *****

 occupation numbers of nos
   0.0051492      0.0007554


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       2.000556   1.692317  -0.053064   0.001851   0.005416   0.001296
    O1_ p       0.000089   0.045382   1.447788   0.010753   0.004515   0.000036
    O1_ d       0.000000   0.000187   0.002023   0.000306   0.000336   0.002824
    H1_ s      -0.000173   0.194881   0.526413   0.012384   0.000374   0.000336
    H1_ p      -0.000553   0.052178   0.043562  -0.000402   0.000454   0.001156

   ao class       7A1        8A1        9A1       10A1       11A1 
    O1_ s       0.000199   0.000210   0.000032   0.000012   0.000009
    O1_ p       0.000261   0.000174   0.000018   0.000046   0.000007
    O1_ d       0.003596   0.000457   0.000010   0.000122   0.000002
    H1_ s       0.000059   0.000375   0.000211   0.000006   0.000039
    H1_ p       0.000426  -0.000052   0.000458   0.000320   0.000039

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.125009   0.013468   0.002038   0.000340   0.000005   0.000070
    O1_ d       0.008344   0.000488   0.002785   0.000407   0.000108   0.000000
    H1_ s       0.803691   0.013003   0.000654   0.000135   0.000326   0.000010
    H1_ p       0.027121  -0.000404   0.000431   0.000317   0.000282   0.000431

   ao class       7B1 
    O1_ p       0.000006
    O1_ d       0.000001
    H1_ s       0.000018
    H1_ p       0.000019

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    O1_ p       1.925218   0.015474   0.000106   0.000029
    O1_ d       0.001153   0.000044   0.003791   0.000078
    H1_ p       0.047082   0.000784   0.000533   0.000449

                        A2  partial gross atomic populations
   ao class       1A2        2A2 
    O1_ d       0.003826   0.000194
    H1_ p       0.001323   0.000561


                        gross atomic populations
     ao           O1_        H1_
      s         3.648834   1.552741
      p         4.590832   0.176513
      d         0.031081   0.000000
    total       8.270747   1.729253


 Total number of electrons:   10.00000000

