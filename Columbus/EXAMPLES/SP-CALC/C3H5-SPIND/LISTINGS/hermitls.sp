
 Work memory size (LMWORK) :    13107200 =100   megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :    9



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    8

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  C  1        2       6      26      14      [9s4p1d|3s2p1d]                              
  C  2        1       6      26      14      [9s4p1d|3s2p1d]                              
  H  1        2       1       7       5      [4s1p|2s1p]                                  
  H  2        2       1       7       5      [4s1p|2s1p]                                  
  H  3        1       1       7       5      [4s1p|2s1p]                                  
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      8      23     113      67

  Spherical harmonic basis used.
  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 24


   1   C  1 1   x      0.0000000000
   2            y     -2.3306492900
   3            z     -0.3747233900

   4   C  1 2   x      0.0000000000
   5            y      2.3306492900
   6            z     -0.3747233900

   7   C  2     x      0.0000000000
   8            y      0.0000000000
   9            z      0.8453617600

  10   H  1 1   x      0.0000000000
  11            y     -2.4562518800
  12            z     -2.4169928000

  13   H  1 2   x      0.0000000000
  14            y      2.4562518800
  15            z     -2.4169928000

  16   H  2 1   x      0.0000000000
  17            y     -4.0791606600
  18            z      0.6810934700

  19   H  2 2   x      0.0000000000
  20            y      4.0791606600
  21            z      0.6810934700

  22   H  3     x      0.0000000000
  23            y      0.0000000000
  24            z      2.8962917900



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   8  5  8  3


  Symmetry 1

   1   C  1  y    [ 2  -  5 ]/2
   2   C  1  z    [ 3  +  6 ]/2
   3   C  2  z    9
   4   H  1  y    [11  - 14 ]/2
   5   H  1  z    [12  + 15 ]/2
   6   H  2  y    [17  - 20 ]/2
   7   H  2  z    [18  + 21 ]/2
   8   H  3  z   24


  Symmetry 2

   9   C  1  x    [ 1  +  4 ]/2
  10   C  2  x    7
  11   H  1  x    [10  + 13 ]/2
  12   H  2  x    [16  + 19 ]/2
  13   H  3  x   22


  Symmetry 3

  14   C  1  y    [ 2  +  5 ]/2
  15   C  1  z    [ 3  -  6 ]/2
  16   C  2  y    8
  17   H  1  y    [11  + 14 ]/2
  18   H  1  z    [12  - 15 ]/2
  19   H  2  y    [17  + 20 ]/2
  20   H  2  z    [18  - 21 ]/2
  21   H  3  y   23


  Symmetry 4

  22   C  1  x    [ 1  -  4 ]/2
  23   H  1  x    [10  - 13 ]/2
  24   H  2  x    [16  - 19 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            C  1        C  2        C  2        H  1        H  2        H  1

   C  1    0.000000
   C  2    2.466652    0.000000
   C  2    1.392101    1.392101    0.000000
   H  1    1.082764    2.754024    2.160970    0.000000
   H  2    2.754024    1.082764    2.160970    2.599584    0.000000
   H  1    1.080875    3.437632    2.160348    1.850756    3.827299    0.000000
   H  2    3.437632    1.080875    2.160348    3.827299    1.850756    4.317196
   H  3    2.125387    2.125387    1.085305    3.097570    3.097570    2.456354

            H  2        H  3

   H  2    0.000000
   H  3    2.456354    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    C  2       C  1                           1.392101
  bond distance:    C  2       C  2                           1.392101
  bond distance:    H  1       C  1                           1.082764
  bond distance:    H  2       C  2                           1.082764
  bond distance:    H  1       C  1                           1.080875
  bond distance:    H  2       C  2                           1.080875
  bond distance:    H  3       C  2                           1.085305


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       C  1       C  2                 121.151
  bond angle:       H  1       C  1       C  2                 121.243
  bond angle:       H  1       C  1       H  1                 117.606
  bond angle:       H  2       C  2       C  2                 121.151
  bond angle:       H  2       C  2       C  2                 121.243
  bond angle:       H  2       C  2       H  2                 117.606
  bond angle:       C  2       C  2       C  1                 124.736
  bond angle:       H  3       C  2       C  1                 117.632
  bond angle:       H  3       C  2       C  2                 117.632


  Nuclear repulsion energy :   64.723385570857


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  C  1#1 1s    1     6665.000000    0.0007 -0.0001  0.0000
   gen. cont.  2     1000.000000    0.0053 -0.0012  0.0000
               3      228.000000    0.0271 -0.0057  0.0000
               4       64.710000    0.1017 -0.0233  0.0000
               5       21.060000    0.2747 -0.0640  0.0000
               6        7.495000    0.4486 -0.1500  0.0000
               7        2.797000    0.2851 -0.1273  0.0000
               8        0.521500    0.0152  0.5445  0.0000
               9        0.159600   -0.0032  0.5805  1.0000

  C  1#2 1s   10     6665.000000    0.0007 -0.0001  0.0000
   gen. cont. 11     1000.000000    0.0053 -0.0012  0.0000
              12      228.000000    0.0271 -0.0057  0.0000
              13       64.710000    0.1017 -0.0233  0.0000
              14       21.060000    0.2747 -0.0640  0.0000
              15        7.495000    0.4486 -0.1500  0.0000
              16        2.797000    0.2851 -0.1273  0.0000
              17        0.521500    0.0152  0.5445  0.0000
              18        0.159600   -0.0032  0.5805  1.0000

  C  1#1 2px  19        9.439000    0.0381  0.0000
   gen. cont. 20        2.002000    0.2095  0.0000
              21        0.545600    0.5086  0.0000
              22        0.151700    0.4688  1.0000

  C  1#2 2px  23        9.439000    0.0381  0.0000
   gen. cont. 24        2.002000    0.2095  0.0000
              25        0.545600    0.5086  0.0000
              26        0.151700    0.4688  1.0000

  C  1#1 2py  27        9.439000    0.0381  0.0000
   gen. cont. 28        2.002000    0.2095  0.0000
              29        0.545600    0.5086  0.0000
              30        0.151700    0.4688  1.0000

  C  1#2 2py  31        9.439000    0.0381  0.0000
   gen. cont. 32        2.002000    0.2095  0.0000
              33        0.545600    0.5086  0.0000
              34        0.151700    0.4688  1.0000

  C  1#1 2pz  35        9.439000    0.0381  0.0000
   gen. cont. 36        2.002000    0.2095  0.0000
              37        0.545600    0.5086  0.0000
              38        0.151700    0.4688  1.0000

  C  1#2 2pz  39        9.439000    0.0381  0.0000
   gen. cont. 40        2.002000    0.2095  0.0000
              41        0.545600    0.5086  0.0000
              42        0.151700    0.4688  1.0000

  C  1#1 3d2- 43        0.550000    1.0000

  C  1#2 3d2- 44        0.550000    1.0000

  C  1#1 3d1- 45        0.550000    1.0000

  C  1#2 3d1- 46        0.550000    1.0000

  C  1#1 3d0  47        0.550000    1.0000

  C  1#2 3d0  48        0.550000    1.0000

  C  1#1 3d1+ 49        0.550000    1.0000

  C  1#2 3d1+ 50        0.550000    1.0000

  C  1#1 3d2+ 51        0.550000    1.0000

  C  1#2 3d2+ 52        0.550000    1.0000

  C  2   1s   53     6665.000000    0.0007 -0.0001  0.0000
   gen. cont. 54     1000.000000    0.0053 -0.0012  0.0000
              55      228.000000    0.0271 -0.0057  0.0000
              56       64.710000    0.1017 -0.0233  0.0000
              57       21.060000    0.2747 -0.0640  0.0000
              58        7.495000    0.4486 -0.1500  0.0000
              59        2.797000    0.2851 -0.1273  0.0000
              60        0.521500    0.0152  0.5445  0.0000
              61        0.159600   -0.0032  0.5805  1.0000

  C  2   2px  62        9.439000    0.0381  0.0000
   gen. cont. 63        2.002000    0.2095  0.0000
              64        0.545600    0.5086  0.0000
              65        0.151700    0.4688  1.0000

  C  2   2py  66        9.439000    0.0381  0.0000
   gen. cont. 67        2.002000    0.2095  0.0000
              68        0.545600    0.5086  0.0000
              69        0.151700    0.4688  1.0000

  C  2   2pz  70        9.439000    0.0381  0.0000
   gen. cont. 71        2.002000    0.2095  0.0000
              72        0.545600    0.5086  0.0000
              73        0.151700    0.4688  1.0000

  C  2   3d2- 74        0.550000    1.0000

  C  2   3d1- 75        0.550000    1.0000

  C  2   3d0  76        0.550000    1.0000

  C  2   3d1+ 77        0.550000    1.0000

  C  2   3d2+ 78        0.550000    1.0000

  H  1#1 1s   79       13.010000    0.0197  0.0000
   gen. cont. 80        1.962000    0.1380  0.0000
              81        0.444600    0.4781  0.0000
              82        0.122000    0.5012  1.0000

  H  1#2 1s   83       13.010000    0.0197  0.0000
   gen. cont. 84        1.962000    0.1380  0.0000
              85        0.444600    0.4781  0.0000
              86        0.122000    0.5012  1.0000

  H  1#1 2px  87        0.727000    1.0000

  H  1#2 2px  88        0.727000    1.0000

  H  1#1 2py  89        0.727000    1.0000

  H  1#2 2py  90        0.727000    1.0000

  H  1#1 2pz  91        0.727000    1.0000

  H  1#2 2pz  92        0.727000    1.0000

  H  2#1 1s   93       13.010000    0.0197  0.0000
   gen. cont. 94        1.962000    0.1380  0.0000
              95        0.444600    0.4781  0.0000
              96        0.122000    0.5012  1.0000

  H  2#2 1s   97       13.010000    0.0197  0.0000
   gen. cont. 98        1.962000    0.1380  0.0000
              99        0.444600    0.4781  0.0000
             100        0.122000    0.5012  1.0000

  H  2#1 2px 101        0.727000    1.0000

  H  2#2 2px 102        0.727000    1.0000

  H  2#1 2py 103        0.727000    1.0000

  H  2#2 2py 104        0.727000    1.0000

  H  2#1 2pz 105        0.727000    1.0000

  H  2#2 2pz 106        0.727000    1.0000

  H  3   1s  107       13.010000    0.0197  0.0000
   gen. cont.108        1.962000    0.1380  0.0000
             109        0.444600    0.4781  0.0000
             110        0.122000    0.5012  1.0000

  H  3   2px 111        0.727000    1.0000

  H  3   2py 112        0.727000    1.0000

  H  3   2pz 113        0.727000    1.0000


  Contracted Orbitals
  -------------------

   1  C  1#1  1s     1   2   3   4   5   6   7   8   9
   2  C  1#2  1s    10  11  12  13  14  15  16  17  18
   3  C  1#1  1s     1   2   3   4   5   6   7   8   9
   4  C  1#2  1s    10  11  12  13  14  15  16  17  18
   5  C  1#1  1s     9
   6  C  1#2  1s    18
   7  C  1#1  2px   19  20  21  22
   8  C  1#2  2px   23  24  25  26
   9  C  1#1  2py   27  28  29  30
  10  C  1#2  2py   31  32  33  34
  11  C  1#1  2pz   35  36  37  38
  12  C  1#2  2pz   39  40  41  42
  13  C  1#1  2px   22
  14  C  1#2  2px   26
  15  C  1#1  2py   30
  16  C  1#2  2py   34
  17  C  1#1  2pz   38
  18  C  1#2  2pz   42
  19  C  1#1  3d2-  43
  20  C  1#2  3d2-  44
  21  C  1#1  3d1-  45
  22  C  1#2  3d1-  46
  23  C  1#1  3d0   47
  24  C  1#2  3d0   48
  25  C  1#1  3d1+  49
  26  C  1#2  3d1+  50
  27  C  1#1  3d2+  51
  28  C  1#2  3d2+  52
  29  C  2    1s      53    54    55    56    57    58    59    60    61
  30  C  2    1s      53    54    55    56    57    58    59    60    61
  31  C  2    1s      61
  32  C  2    2px     62    63    64    65
  33  C  2    2py     66    67    68    69
  34  C  2    2pz     70    71    72    73
  35  C  2    2px     65
  36  C  2    2py     69
  37  C  2    2pz     73
  38  C  2    3d2-    74
  39  C  2    3d1-    75
  40  C  2    3d0     76
  41  C  2    3d1+    77
  42  C  2    3d2+    78
  43  H  1#1  1s    79  80  81  82
  44  H  1#2  1s    83  84  85  86
  45  H  1#1  1s    82
  46  H  1#2  1s    86
  47  H  1#1  2px   87
  48  H  1#2  2px   88
  49  H  1#1  2py   89
  50  H  1#2  2py   90
  51  H  1#1  2pz   91
  52  H  1#2  2pz   92
  53  H  2#1  1s    93  94  95  96
  54  H  2#2  1s    97  98  99 100
  55  H  2#1  1s    96
  56  H  2#2  1s   100
  57  H  2#1  2px  101
  58  H  2#2  2px  102
  59  H  2#1  2py  103
  60  H  2#2  2py  104
  61  H  2#1  2pz  105
  62  H  2#2  2pz  106
  63  H  3    1s     107   108   109   110
  64  H  3    1s     110
  65  H  3    2px    111
  66  H  3    2py    112
  67  H  3    2pz    113




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        28 10 22  7


  Symmetry  A1 ( 1)

    1     C  1     1s         1  +   2
    2     C  1     1s         3  +   4
    3     C  1     1s         5  +   6
    4     C  1     2py        9  -  10
    5     C  1     2pz       11  +  12
    6     C  1     2py       15  -  16
    7     C  1     2pz       17  +  18
    8     C  1     3d1-      21  -  22
    9     C  1     3d0       23  +  24
   10     C  1     3d2+      27  +  28
   11     C  2     1s        29
   12     C  2     1s        30
   13     C  2     1s        31
   14     C  2     2pz       34
   15     C  2     2pz       37
   16     C  2     3d0       40
   17     C  2     3d2+      42
   18     H  1     1s        43  +  44
   19     H  1     1s        45  +  46
   20     H  1     2py       49  -  50
   21     H  1     2pz       51  +  52
   22     H  2     1s        53  +  54
   23     H  2     1s        55  +  56
   24     H  2     2py       59  -  60
   25     H  2     2pz       61  +  62
   26     H  3     1s        63
   27     H  3     1s        64
   28     H  3     2pz       67


  Symmetry  B1 ( 2)

   29     C  1     2px        7  +   8
   30     C  1     2px       13  +  14
   31     C  1     3d2-      19  -  20
   32     C  1     3d1+      25  +  26
   33     C  2     2px       32
   34     C  2     2px       35
   35     C  2     3d1+      41
   36     H  1     2px       47  +  48
   37     H  2     2px       57  +  58
   38     H  3     2px       65


  Symmetry  B2 ( 3)

   39     C  1     1s         1  -   2
   40     C  1     1s         3  -   4
   41     C  1     1s         5  -   6
   42     C  1     2py        9  +  10
   43     C  1     2pz       11  -  12
   44     C  1     2py       15  +  16
   45     C  1     2pz       17  -  18
   46     C  1     3d1-      21  +  22
   47     C  1     3d0       23  -  24
   48     C  1     3d2+      27  -  28
   49     C  2     2py       33
   50     C  2     2py       36
   51     C  2     3d1-      39
   52     H  1     1s        43  -  44
   53     H  1     1s        45  -  46
   54     H  1     2py       49  +  50
   55     H  1     2pz       51  -  52
   56     H  2     1s        53  -  54
   57     H  2     1s        55  -  56
   58     H  2     2py       59  +  60
   59     H  2     2pz       61  -  62
   60     H  3     2py       66


  Symmetry  A2 ( 4)

   61     C  1     2px        7  -   8
   62     C  1     2px       13  -  14
   63     C  1     3d2-      19  +  20
   64     C  1     3d1+      25  -  26
   65     C  2     3d2-      38
   66     H  1     2px       47  -  48
   67     H  2     2px       57  -  58

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2X   Y      0.10D-14                                                   
       6.0    2    3    1    1    1                                             
C  1   0.000000000000000  -2.330649290000000  -0.374723390000000       *        
C  2   0.000000000000000   0.000000000000000   0.845361760000000       *        
H   9   3                                                                       
       6665.00000000         0.00069200        -0.00014600         0.00000000   
       1000.00000000         0.00532900        -0.00115400         0.00000000   
        228.00000000         0.02707700        -0.00572500         0.00000000   
         64.71000000         0.10171800        -0.02331200         0.00000000   
         21.06000000         0.27474000        -0.06395500         0.00000000   
          7.49500000         0.44856400        -0.14998100         0.00000000   
          2.79700000         0.28507400        -0.12726200         0.00000000   
          0.52150000         0.01520400         0.54452900         0.00000000   
          0.15960000        -0.00319100         0.58049600         1.00000000   
H   4   2                                                                       
          9.43900000         0.03810900         0.00000000                      
          2.00200000         0.20948000         0.00000000                      
          0.54560000         0.50855700         0.00000000                      
          0.15170000         0.46884200         1.00000000                      
H   1   1                                                                       
          0.55000000         1.00000000                                         
       1.0    3    2    1    1                                                  
H  1   0.000000000000000  -2.456251880000000  -2.416992800000000       *        
H  2   0.000000000000000  -4.079160660000000   0.681093470000000       *        
H  3   0.000000000000000   0.000000000000000   2.896291790000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.72700000         1.00000000                                         




 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

found     656 non-vanashing overlap integrals
found     742 non-vanashing nuclear attraction integrals
found     656 non-vanashing kinetic energy integrals






 found     387 non-vanashing integrals ( typea=  1 typeb=  0)
 found     617 non-vanashing integrals ( typea=  1 typeb=  1)
 found     730 non-vanashing integrals ( typea=  1 typeb=  2)


 found     666 non-vanashing integrals ( typea=  1 typeb=  3)
 found     373 non-vanashing integrals ( typea=  1 typeb=  4)
 found     434 non-vanashing integrals ( typea=  1 typeb=  5)
 found     670 non-vanashing integrals ( typea=  1 typeb=  6)
 found     684 non-vanashing integrals ( typea=  1 typeb=  7)
 found     733 non-vanashing integrals ( typea=  1 typeb=  8)


 found     387 non-vanashing integrals ( typea=  1 typeb=  9)
 found     614 non-vanashing integrals ( typea=  1 typeb= 10)
 found     742 non-vanashing integrals ( typea=  1 typeb= 11)
 found     384 non-vanashing integrals ( typea=  1 typeb= 12)
 found     415 non-vanashing integrals ( typea=  1 typeb= 13)
 found     434 non-vanashing integrals ( typea=  1 typeb= 14)
 found     621 non-vanashing integrals ( typea=  1 typeb= 15)
 found     742 non-vanashing integrals ( typea=  1 typeb= 16)
 found     686 non-vanashing integrals ( typea=  1 typeb= 17)
 found     733 non-vanashing integrals ( typea=  1 typeb= 18)


 found     667 non-vanashing integrals ( typea=  1 typeb= 19)
 found     374 non-vanashing integrals ( typea=  1 typeb= 20)
 found     433 non-vanashing integrals ( typea=  1 typeb= 21)
 found     663 non-vanashing integrals ( typea=  1 typeb= 22)
 found     681 non-vanashing integrals ( typea=  1 typeb= 23)
 found     742 non-vanashing integrals ( typea=  1 typeb= 24)
 found     374 non-vanashing integrals ( typea=  1 typeb= 25)
 found     429 non-vanashing integrals ( typea=  1 typeb= 26)
 found     415 non-vanashing integrals ( typea=  1 typeb= 27)
 found     434 non-vanashing integrals ( typea=  1 typeb= 28)
 found     674 non-vanashing integrals ( typea=  1 typeb= 29)
 found     685 non-vanashing integrals ( typea=  1 typeb= 30)
 found     742 non-vanashing integrals ( typea=  1 typeb= 31)
 found     686 non-vanashing integrals ( typea=  1 typeb= 32)
 found     733 non-vanashing integrals ( typea=  1 typeb= 33)


 found     683 non-vanashing integrals ( typea=  2 typeb=  6)
 found     421 non-vanashing integrals ( typea=  2 typeb=  7)
 found     313 non-vanashing integrals ( typea=  2 typeb=  8)




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************


 Number of two-electron integrals written:    680649 (26.2%)
 Kilobytes written:                            10922




 >>>> Total CPU  time used in HERMIT:   0.75 seconds
 >>>> Total wall time used in HERMIT:   1.00 seconds

- End of Integral Section
