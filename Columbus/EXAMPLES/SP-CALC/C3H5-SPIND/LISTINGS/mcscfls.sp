

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        13107200 of real*8 words (  100.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,30
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  2,1,40,2,2,40,4,1,40
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /global/rene/tests/Columbus/test/SP-CALC/C3H5-SPIND/WORK/
 aoi

 Integral file header information:
 Hermit Integral Program : SIFS version  n001              23:34:17.480 20-Jun-22

 Core type energy values:
 energy( 1)=  6.472338557086E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   64.723385571


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions:  67

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    28   10   22    7


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        2

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       1( 29)    40
       2       2( 30)    40
       4       1( 61)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   23
 Spin multiplicity:            2
 Number of active orbitals:    3
 Number of active electrons:   3
 Total number of CSFs:         4
 

 faar:   0 active-active rotations allowed out of:   1 possible.


 Number of active-double rotations:         0
 Number of active-active rotations:         0
 Number of double-virtual rotations:      204
 Number of active-virtual rotations:       22
 lenbfsdef=                131071  lenbfs=                   908
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #          13
 class  2 (pq|ri):         #           0
 class  3 (pq|ia):         #        1208
 class  4 (pi|qa):         #        1804
 class  5 (pq|ra):         #         128
 class  6 (pq|ij)/(pi|qj): #         476
 class  7 (pq|ab):         #        2812
 class  8 (pa|qb):         #        5408
 class  9 p(bp,ai)         #        4488
 class 10p(ai,jp):        #           0
 class 11p(ai,bj):        #       22908

 Size of orbital-Hessian matrix B:                    27720
 Size of the orbital-state Hessian matrix C:            904
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:          28624


 Source of the initial MO coeficients:

 Input MO coefficient file: /global/rene/tests/Columbus/test/SP-CALC/C3H5-SPIND/WORK/moc
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    28, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13094682

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12926838
 address segment size,           sizesg =  12775352
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     121576 transformed 1/r12    array elements were written in      23 records.

 !timer: 2-e transformation              cpu_time=     0.151 walltime=     0.504

 mosort: allocated sort2 space, avc2is=    12956535 available sort2 space, avcisx=    12956787

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   3 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -116.4834463827     -181.2068319536        0.0000000000        0.0000010000
    2      -116.0528569382     -180.7762425090        0.0000000000        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  9.984354787760276E-003
 Total number of micro iterations:    8

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.95300517 pnorm= 0.0000E+00 rznorm= 3.1939E-06 rpnorm= 0.0000E+00 noldr=  8 nnewr=  8 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -22.490857  -22.485492   -2.132044   -1.515498   -1.299029   -1.019502

 qvv(*) eigenvalues. symmetry block  1
     0.398483    0.436831    0.557941    0.839403    1.180679    1.414870    1.516457    1.753740    1.884489    1.975574
     2.519808    2.737366    3.200662    3.512115    3.678943    3.793746    4.483481    5.021699    5.163190    5.524835
     6.013955    6.971601
 i,qaaresolved                     1 -0.796300948535020     
 i,qaaresolved                     2  0.402950592939721     

 qvv(*) eigenvalues. symmetry block  2
     1.381687    1.645003    2.259157    2.601963    3.498913    3.881930    4.078728    4.790931

 fdd(*) eigenvalues. symmetry block  3
   -22.486086   -1.822922   -1.198109   -1.077748

 qvv(*) eigenvalues. symmetry block  3
     0.468159    0.570882    1.022060    1.183451    1.479905    1.751500    1.957264    2.298286    2.372666    3.386490
     3.477233    3.769719    4.129991    4.357397    4.987195    5.261829    5.781704    6.310375
 i,qaaresolved                     1 -0.208168130075864     

 qvv(*) eigenvalues. symmetry block  4
     1.509275    2.357236    2.541441    3.753208    4.175966    4.661184

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.174 walltime=     0.620

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -116.4834463827 demc= 1.1648E+02 wnorm= 7.9875E-02 knorm= 3.0295E-01 apxde= 9.3741E-03    *not conv.*     

               starting mcscf iteration...   2
 !timer:                                 cpu_time=     0.178 walltime=     0.627

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    28, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13094682

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12926838
 address segment size,           sizesg =  12775352
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     121576 transformed 1/r12    array elements were written in      23 records.


 mosort: allocated sort2 space, avc2is=    12956535 available sort2 space, avcisx=    12956787

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   2 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -116.4943138333     -181.2176994042        0.0000000000        0.0000050880
    2      -115.9971608950     -180.7205464658        0.0000000000        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.675343375771866E-003
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99963412 pnorm= 0.0000E+00 rznorm= 7.0167E-06 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -22.467287  -22.456465   -2.119859   -1.505922   -1.288629   -1.010184

 qvv(*) eigenvalues. symmetry block  1
     0.401869    0.440326    0.561006    0.843906    1.186832    1.422674    1.523634    1.762215    1.894785    1.984423
     2.525706    2.741964    3.208575    3.518289    3.686305    3.801887    4.493456    5.031465    5.171479    5.534207
     6.023167    6.982150
 i,qaaresolved                     1 -0.783651191367614     
 i,qaaresolved                     2  0.505925070292939     

 qvv(*) eigenvalues. symmetry block  2
     1.366748    1.565836    2.267016    2.608783    3.498472    3.887038    4.085848    4.797544

 fdd(*) eigenvalues. symmetry block  3
   -22.456746   -1.811065   -1.186054   -1.066334

 qvv(*) eigenvalues. symmetry block  3
     0.471610    0.574914    1.026993    1.189043    1.487217    1.760886    1.968694    2.306857    2.378122    3.393891
     3.483271    3.777157    4.137719    4.365923    4.996373    5.271657    5.792264    6.320721
 i,qaaresolved                     1 -0.198746583298074     

 qvv(*) eigenvalues. symmetry block  4
     1.509270    2.366037    2.549686    3.759183    4.183298    4.669234

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=   -116.4943138333 demc= 1.0867E-02 wnorm= 1.3403E-02 knorm= 2.7049E-02 apxde= 9.4721E-05    *not conv.*     

               starting mcscf iteration...   3
 !timer:                                 cpu_time=     0.251 walltime=     0.823

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    28, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13094682

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12926838
 address segment size,           sizesg =  12775352
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     121576 transformed 1/r12    array elements were written in      23 records.


 mosort: allocated sort2 space, avc2is=    12956535 available sort2 space, avcisx=    12956787

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   2 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -116.4944098010     -181.2177953719        0.0000000000        0.0000010000
    2      -115.9880495789     -180.7114351498        0.0000000000        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.055721981118135E-004
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999969 pnorm= 0.0000E+00 rznorm= 5.1128E-07 rpnorm= 0.0000E+00 noldr=  5 nnewr=  5 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -22.463125  -22.453394   -2.118188   -1.504653   -1.287261   -1.008884

 qvv(*) eigenvalues. symmetry block  1
     0.402330    0.440825    0.561451    0.844470    1.187681    1.423741    1.524697    1.763351    1.896322    1.985466
     2.526508    2.742583    3.209601    3.518988    3.687249    3.803033    4.494813    5.032774    5.172458    5.535511
     6.024358    6.983602
 i,qaaresolved                     1 -0.783081857982784     
 i,qaaresolved                     2  0.524897128636432     

 qvv(*) eigenvalues. symmetry block  2
     1.361929    1.552827    2.267975    2.609674    3.499004    3.887779    4.086664    4.798523

 fdd(*) eigenvalues. symmetry block  3
   -22.453706   -1.809615   -1.184477   -1.064874

 qvv(*) eigenvalues. symmetry block  3
     0.472026    0.575450    1.027632    1.189749    1.488164    1.762032    1.970107    2.307940    2.378795    3.394836
     3.483940    3.778215    4.138773    4.366976    4.997520    5.272894    5.793501    6.322043
 i,qaaresolved                     1 -0.197604213672623     

 qvv(*) eigenvalues. symmetry block  4
     1.508634    2.367065    2.550636    3.759931    4.184165    4.670258

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    3 emc=   -116.4944098010 demc= 9.5968E-05 wnorm= 8.4458E-04 knorm= 7.8844E-04 apxde= 1.1706E-07    *not conv.*     

               starting mcscf iteration...   4
 !timer:                                 cpu_time=     0.326 walltime=     1.024

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    28, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13094682

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12926838
 address segment size,           sizesg =  12775352
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     121576 transformed 1/r12    array elements were written in      23 records.


 mosort: allocated sort2 space, avc2is=    12956535 available sort2 space, avcisx=    12956787

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   2 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -116.4944099361     -181.2177955070        0.0000000000        0.0000010000
    2      -115.9878367163     -180.7112222871        0.0000000000        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.360355333567459E-005
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-1.00000000 pnorm= 0.0000E+00 rznorm= 5.9100E-07 rpnorm= 0.0000E+00 noldr=  4 nnewr=  4 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -22.462938  -22.453467   -2.118162   -1.504651   -1.287258   -1.008878

 qvv(*) eigenvalues. symmetry block  1
     0.402328    0.440823    0.561449    0.844470    1.187688    1.423746    1.524714    1.763350    1.896340    1.985459
     2.526521    2.742588    3.209610    3.518977    3.687259    3.803038    4.494828    5.032778    5.172451    5.535525
     6.024357    6.983616
 i,qaaresolved                     1 -0.783058799815747     
 i,qaaresolved                     2  0.525456740589420     

 qvv(*) eigenvalues. symmetry block  2
     1.361715    1.552605    2.267990    2.609683    3.498988    3.887786    4.086660    4.798532

 fdd(*) eigenvalues. symmetry block  3
   -22.453787   -1.809617   -1.184481   -1.064874

 qvv(*) eigenvalues. symmetry block  3
     0.472022    0.575446    1.027637    1.189752    1.488165    1.762022    1.970103    2.307945    2.378794    3.394834
     3.483930    3.778226    4.138781    4.366980    4.997517    5.272885    5.793487    6.322045
 i,qaaresolved                     1 -0.197638307984626     

 qvv(*) eigenvalues. symmetry block  4
     1.508628    2.367066    2.550638    3.759934    4.184165    4.670267

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    4 emc=   -116.4944099361 demc= 1.3507E-07 wnorm= 1.8883E-04 knorm= 9.2289E-05 apxde= 3.4148E-09    *not conv.*     

               starting mcscf iteration...   5
 !timer:                                 cpu_time=     0.406 walltime=     1.228

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    28, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13094682

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12926838
 address segment size,           sizesg =  12775352
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     121576 transformed 1/r12    array elements were written in      23 records.


 mosort: allocated sort2 space, avc2is=    12956535 available sort2 space, avcisx=    12956787

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   2 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -116.4944099402     -181.2177955111        0.0000000000        0.0000010000
    2      -115.9878221162     -180.7112076871        0.0000000000        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.233074883696016E-006
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    3

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 1.0894E-05 rznorm= 9.4874E-07 rpnorm= 5.6481E-07 noldr=  3 nnewr=  3 nolds=  2 nnews=  2
 

 fdd(*) eigenvalues. symmetry block  1
   -22.462893  -22.453487   -2.118156   -1.504651   -1.287258   -1.008877

 qvv(*) eigenvalues. symmetry block  1
     0.402328    0.440822    0.561449    0.844470    1.187689    1.423747    1.524717    1.763349    1.896344    1.985456
     2.526523    2.742589    3.209611    3.518974    3.687261    3.803039    4.494830    5.032778    5.172448    5.535527
     6.024357    6.983618
 i,qaaresolved                     1 -0.783052098708629     
 i,qaaresolved                     2  0.525503909901100     

 qvv(*) eigenvalues. symmetry block  2
     1.361687    1.552604    2.267992    2.609684    3.498987    3.887787    4.086658    4.798533

 fdd(*) eigenvalues. symmetry block  3
   -22.453809   -1.809619   -1.184482   -1.064875

 qvv(*) eigenvalues. symmetry block  3
     0.472020    0.575445    1.027637    1.189752    1.488164    1.762018    1.970101    2.307945    2.378793    3.394833
     3.483927    3.778228    4.138783    4.366980    4.997516    5.272882    5.793483    6.322045
 i,qaaresolved                     1 -0.197646907226180     

 qvv(*) eigenvalues. symmetry block  4
     1.508624    2.367065    2.550637    3.759934    4.184164    4.670268

 restrt: restart information saved on the restart file (unit= 13).

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    5 emc=   -116.4944099402 demc= 4.1175E-09 wnorm= 4.1865E-05 knorm= 2.0208E-05 apxde= 1.9062E-10    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 1.000 total energy=     -116.494409940, rel. (eV)=   0.000000
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  B1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     1.90221449     0.09778551     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10
  occ(*)=     0.00000000     0.00000000

          natural orbitals of the final iteration,block  3    -  B2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  4    -  A2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        192 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     1
 Number of unique bra states (ndbra):                     1
 *** Starting rdft_grd
 qind: F
  
 Spin density matrix:

              < 1|SD| 1>            block   1

              < 1|SD| 1>            block   2

                mo   1         mo   2
   mo   1     0.03831236
   mo   2    -0.21528626     0.03787363

              < 1|SD| 1>            block   3

              < 1|SD| 1>            block   4

                mo   3
   mo   3     0.92381402
  
 Alpha density matrix:

              < 1|alpha| 1>         block   1

              < 1|alpha| 1>         block   2

                mo   1         mo   2
   mo   1     0.97026319
   mo   2    -0.10718348     0.06782980

              < 1|alpha| 1>         block   3

              < 1|alpha| 1>         block   4

                mo   3
   mo   3     0.96190701
  
  
 Beta density matrix:

              < 1|beta| 1>          block   1

              < 1|beta| 1>          block   2

                mo   1         mo   2
   mo   1     0.93195083
   mo   2     0.10810278     0.02995618

              < 1|beta| 1>          block   3

              < 1|beta| 1>          block   4

                mo   3
   mo   3     0.03809299
  
  
 One particle density matrix:

              < 1|E_s| 1>           block   1

              < 1|E_s| 1>           block   2

                mo   1         mo   2
   mo   1     1.90221402
   mo   2     0.00091931     0.09778598

              < 1|E_s| 1>           block   3

              < 1|E_s| 1>           block   4

                mo   3
   mo   3     1.00000000
  
 *** rdft_grd finished
 d1(*) and spind(*) written to the 1-particle density matrix file.
        192 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           

          state spec. NOs: DRT 1, State  1
 *** warning *** small active-orbital occupation. i=  1 nocc=-1.7719E-01

          state spec. NOs: DRT 1, State  1

          state spec. NOs: DRT 1, State  1

          state spec. NOs: DRT 1, State  1

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     1.90221449     0.09778551     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10
  occ(*)=     0.00000000     0.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       0.063593   1.936660   0.719932   0.304922   0.022305   0.003070
    C1_ p       0.000594   0.000009   0.092949   0.408058   0.816994   0.529834
    C1_ d      -0.000086  -0.000023   0.009651   0.008438   0.003565   0.031899
    C2_ s       1.935771   0.063315   0.868008   0.292127   0.006898   0.000561
    C2_ p      -0.000039   0.000064   0.043007   0.148797   0.401616   0.390644
    C2_ d       0.000015  -0.000095   0.007209   0.001081   0.000615   0.029357
    H1_ s       0.000026   0.000159   0.091094   0.364316   0.038736   0.467274
    H1_ p       0.000005  -0.000124   0.007337   0.012562   0.003591   0.002160
    H2_ s       0.000066   0.000183   0.055937   0.158572   0.545905   0.115413
    H2_ p       0.000001  -0.000132   0.006490   0.007214   0.009492   0.003564
    H3_ s       0.000155  -0.000004   0.091736   0.283759   0.148013   0.423174
    H3_ p      -0.000099  -0.000012   0.006651   0.010155   0.002271   0.003048
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    C1_ p       0.948597   0.048746   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.017040   0.000715   0.000000   0.000000   0.000000   0.000000
    C2_ p       0.917793   0.047402   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.003821   0.000015   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.005150   0.000303   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.004856   0.000294   0.000000   0.000000   0.000000   0.000000
    H3_ p       0.004958   0.000311   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1        8B1        9B1       10B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ s       1.999277   1.124436   0.006796  -0.000938   0.000000   0.000000
    C1_ p       0.000055   0.006502   0.988238   0.957520   0.000000   0.000000
    C1_ d       0.000000   0.002293   0.006718   0.020356   0.000000   0.000000
    C2_ p       0.000551   0.354771   0.384293   0.215315   0.000000   0.000000
    C2_ d      -0.000021   0.009276  -0.000106   0.014733   0.000000   0.000000
    H1_ s       0.000196   0.195420   0.593232   0.101864   0.000000   0.000000
    H1_ p      -0.000110   0.012817   0.009720   0.005505   0.000000   0.000000
    H2_ s       0.000165   0.277254   0.004870   0.678558   0.000000   0.000000
    H2_ p      -0.000112   0.016407   0.004497   0.005928   0.000000   0.000000
    H3_ p       0.000000   0.000824   0.001743   0.001159   0.000000   0.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    C1_ p       0.970615   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000028   0.000000   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.016494   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.006100   0.000000   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.006819   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7A2 


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s         6.180052   3.166680   1.852316   1.836923   0.946834
      p         5.768708   2.904212   0.065014   0.065320   0.031008
      d         0.100538   0.082395   0.000000   0.000000   0.000000
    total      12.049298   6.153288   1.917330   1.902243   0.977842
 

 Trace:   23.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       0.062705   1.937548   0.719922   0.304931   0.022307   0.003069
    C1_ p       0.000594   0.000009   0.092954   0.408045   0.816988   0.529847
    C1_ d      -0.000087  -0.000023   0.009652   0.008438   0.003565   0.031899
    C2_ s       1.936659   0.062427   0.868019   0.292116   0.006898   0.000561
    C2_ p      -0.000039   0.000064   0.043006   0.148807   0.401623   0.390628
    C2_ d       0.000015  -0.000094   0.007209   0.001081   0.000615   0.029357
    H1_ s       0.000026   0.000159   0.091090   0.364315   0.038729   0.467286
    H1_ p       0.000005  -0.000124   0.007336   0.012562   0.003591   0.002160
    H2_ s       0.000065   0.000183   0.055935   0.158571   0.545903   0.115418
    H2_ p       0.000001  -0.000132   0.006490   0.007214   0.009492   0.003564
    H3_ s       0.000155  -0.000004   0.091738   0.283765   0.148018   0.423161
    H3_ p      -0.000100  -0.000012   0.006651   0.010155   0.002271   0.003048
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    C1_ p       0.948597   0.048746   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.017040   0.000715   0.000000   0.000000   0.000000   0.000000
    C2_ p       0.917793   0.047402   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.003821   0.000015   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.005150   0.000303   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.004856   0.000294   0.000000   0.000000   0.000000   0.000000
    H3_ p       0.004958   0.000311   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1        8B1        9B1       10B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ s       1.999277   1.124436   0.006797  -0.000938   0.000000   0.000000
    C1_ p       0.000055   0.006502   0.988239   0.957519   0.000000   0.000000
    C1_ d       0.000000   0.002293   0.006718   0.020356   0.000000   0.000000
    C2_ p       0.000551   0.354778   0.384303   0.215296   0.000000   0.000000
    C2_ d      -0.000021   0.009276  -0.000106   0.014733   0.000000   0.000000
    H1_ s       0.000196   0.195416   0.593223   0.101877   0.000000   0.000000
    H1_ p      -0.000110   0.012817   0.009720   0.005505   0.000000   0.000000
    H2_ s       0.000165   0.277251   0.004866   0.678565   0.000000   0.000000
    H2_ p      -0.000112   0.016407   0.004497   0.005928   0.000000   0.000000
    H3_ p       0.000000   0.000824   0.001743   0.001159   0.000000   0.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    C1_ p       0.970615   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000028   0.000000   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.016494   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.006100   0.000000   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.006819   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7A2 


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s         6.180052   3.166680   1.852316   1.836923   0.946834
      p         5.768708   2.904212   0.065014   0.065320   0.031008
      d         0.100538   0.082395   0.000000   0.000000   0.000000
    total      12.049298   6.153288   1.917330   1.902243   0.977842
 

 Trace:   23.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population based on the Spin-Density for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    C1_ p       0.269303   0.011635   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000162  -0.002997   0.000000   0.000000   0.000000   0.000000
    C2_ p      -0.019283  -0.184873   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.000633   0.000061   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.001476   0.000003   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.001413   0.000003   0.000000   0.000000   0.000000   0.000000
    H3_ p      -0.000001  -0.001026   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1        8B1        9B1       10B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    C1_ p       0.896667   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000026   0.000000   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.015238   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.005635   0.000000   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.006300   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7A2 


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      p         1.177605  -0.204156   0.007114   0.007716  -0.001027
      d        -0.003184   0.015932   0.000000   0.000000   0.000000
    total       1.174421  -0.188225   0.007114   0.007716  -0.001027
 

 Trace:    1.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population based on the Alpha Spin-Density for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       0.031352   0.968774   0.359961   0.152466   0.011153   0.001535
    C1_ p       0.000297   0.000004   0.046477   0.204022   0.408494   0.264924
    C1_ d      -0.000043  -0.000011   0.004826   0.004219   0.001782   0.015950
    C2_ s       0.968330   0.031213   0.434009   0.146058   0.003449   0.000281
    C2_ p      -0.000019   0.000032   0.021503   0.074403   0.200811   0.195314
    C2_ d       0.000008  -0.000047   0.003605   0.000540   0.000308   0.014678
    H1_ s       0.000013   0.000079   0.045545   0.182158   0.019365   0.233643
    H1_ p       0.000002  -0.000062   0.003668   0.006281   0.001795   0.001080
    H2_ s       0.000033   0.000091   0.027967   0.079286   0.272952   0.057709
    H2_ p       0.000001  -0.000066   0.003245   0.003607   0.004746   0.001782
    H3_ s       0.000077  -0.000002   0.045869   0.141883   0.074009   0.211581
    H3_ p      -0.000050  -0.000006   0.003325   0.005077   0.001136   0.001524
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    C1_ p       0.618825   0.020315   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.006780   0.000518   0.000000   0.000000   0.000000   0.000000
    C2_ p       0.346546   0.033973   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.002273  -0.000009   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.003332   0.000133   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.003153   0.000130   0.000000   0.000000   0.000000   0.000000
    H3_ p       0.001909   0.000212   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1        8B1        9B1       10B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ s       0.999638   0.562218   0.003398  -0.000469   0.000000   0.000000
    C1_ p       0.000027   0.003251   0.494119   0.478759   0.000000   0.000000
    C1_ d       0.000000   0.001146   0.003359   0.010178   0.000000   0.000000
    C2_ p       0.000275   0.177389   0.192152   0.107648   0.000000   0.000000
    C2_ d      -0.000011   0.004638  -0.000053   0.007367   0.000000   0.000000
    H1_ s       0.000098   0.097708   0.296611   0.050938   0.000000   0.000000
    H1_ p      -0.000055   0.006409   0.004860   0.002752   0.000000   0.000000
    H2_ s       0.000083   0.138626   0.002433   0.339283   0.000000   0.000000
    H2_ p      -0.000056   0.008204   0.002249   0.002964   0.000000   0.000000
    H3_ p       0.000000   0.000412   0.000871   0.000580   0.000000   0.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    C1_ p       0.933641   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000027   0.000000   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.015866   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.005868   0.000000   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.006560   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7A2 


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s         3.090026   1.583340   0.926158   0.918461   0.473417
      p         3.473157   1.350028   0.036064   0.036518   0.014990
      d         0.048677   0.049163   0.000000   0.000000   0.000000
    total       6.611860   2.982531   0.962222   0.954979   0.488407
 

 Trace:   12.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population based on the Beta Spin-Density for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       0.031352   0.968774   0.359961   0.152466   0.011153   0.001535
    C1_ p       0.000297   0.000004   0.046477   0.204022   0.408494   0.264924
    C1_ d      -0.000043  -0.000011   0.004826   0.004219   0.001782   0.015950
    C2_ s       0.968330   0.031213   0.434009   0.146058   0.003449   0.000281
    C2_ p      -0.000019   0.000032   0.021503   0.074403   0.200811   0.195314
    C2_ d       0.000008  -0.000047   0.003605   0.000540   0.000308   0.014678
    H1_ s       0.000013   0.000079   0.045545   0.182158   0.019365   0.233643
    H1_ p       0.000002  -0.000062   0.003668   0.006281   0.001795   0.001080
    H2_ s       0.000033   0.000091   0.027967   0.079286   0.272952   0.057709
    H2_ p       0.000001  -0.000066   0.003245   0.003607   0.004746   0.001782
    H3_ s       0.000077  -0.000002   0.045869   0.141883   0.074009   0.211581
    H3_ p      -0.000050  -0.000006   0.003325   0.005077   0.001136   0.001524
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    C1_ p       0.347388   0.010815   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.010366   0.000091   0.000000   0.000000   0.000000   0.000000
    C2_ p       0.578580   0.006096   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.001562   0.000009   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.001922   0.000065   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.001804   0.000063   0.000000   0.000000   0.000000   0.000000
    H3_ p       0.003105   0.000043   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1        8B1        9B1       10B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ s       0.999638   0.562218   0.003398  -0.000469   0.000000   0.000000
    C1_ p       0.000027   0.003251   0.494119   0.478759   0.000000   0.000000
    C1_ d       0.000000   0.001146   0.003359   0.010178   0.000000   0.000000
    C2_ p       0.000275   0.177389   0.192152   0.107648   0.000000   0.000000
    C2_ d      -0.000011   0.004638  -0.000053   0.007367   0.000000   0.000000
    H1_ s       0.000098   0.097708   0.296611   0.050938   0.000000   0.000000
    H1_ p      -0.000055   0.006409   0.004860   0.002752   0.000000   0.000000
    H2_ s       0.000083   0.138626   0.002433   0.339283   0.000000   0.000000
    H2_ p      -0.000056   0.008204   0.002249   0.002964   0.000000   0.000000
    H3_ p       0.000000   0.000412   0.000871   0.000580   0.000000   0.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    C1_ p       0.036974   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000001   0.000000   0.000000   0.000000   0.000000   0.000000
    C2_ d       0.000628   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.000232   0.000000   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.000260   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7A2 


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s         3.090026   1.583340   0.926158   0.918461   0.473417
      p         2.295551   1.554184   0.028950   0.028802   0.016018
      d         0.051861   0.033232   0.000000   0.000000   0.000000
    total       5.437438   3.170756   0.955108   0.947263   0.489434
 

 Trace:   11.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 !timer: mcscf                           cpu_time=     0.507 walltime=     1.471
 *** cpu_time / walltime =      0.345
 bummer (warning):timer: cpu_time << walltime.  If possible, increase core memory. event =1
