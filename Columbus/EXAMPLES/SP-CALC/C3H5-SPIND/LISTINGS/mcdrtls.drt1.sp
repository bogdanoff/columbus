
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /global/rene/tests/Columbus/test/SP-CALC/C3H5-SPIND/WORK/mcdrtky                
 
 input the spin multiplicity [  0]: spin multiplicity:    2    doublet 
 input the total number of electrons [  0]: nelt:     23
 input the number of irreps (1-8) [  0]: nsym:      4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      4
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:     10
 number of inactive electrons:     20
 number of active electrons:      3
 level(*)        1   2   3   4   5   6   7   8   9  10
 symd(*)         1   1   1   1   1   1   3   3   3   3
 slabel(*)     a1  a1  a1  a1  a1  a1  b2  b2  b2  b2 
 doub(*)         1   2   3   4   5   6   1   2   3   4
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      3
 level(*)        1   2   3
 syml(*)         2   2   4
 slabel(*)     b1  b1  a2 
 modrt(*)        1   2   1
 input the minimum cumulative occupation for each active level:
  b1  b1  a2 
    1   2   1
 input the maximum cumulative occupation for each active level:
  b1  b1  a2 
    1   2   1
 slabel(*)     b1  b1  a2 
 modrt(*)        1   2   1
 occmin(*)       0   0   3
 occmax(*)       3   3   3
 input the minimum b value for each active level:
  b1  b1  a2 
    1   2   1
 input the maximum b value for each active level:
  b1  b1  a2 
    1   2   1
 slabel(*)     b1  b1  a2 
 modrt(*)        1   2   1
 bmin(*)         0   0   0
 bmax(*)         3   3   3
 input the step masks for each active level:
 modrt:smask=
   1:1111   2:1111   1:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:      9
 are any arcs to be manually removed?(y,[n])
 nwalk=       4
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   3

 level  0 through level  3 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

   9   3 1 1   4 a2    1    0   0   0   0   1     1     0     0     0     4     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     4     0
                                            4     0     0     0     0     0     0
 ........................................

   5   2 1 1   2 b1    2    9   0   0   0   1     1     0     0     0     0     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     2     0
                                            4     0     0     0     0     0     0

   6   2 1 0   2 b1    2    0   9   0   0   1     0     0     0     0     0     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     1     1     0     0     3     0

   7   2 0 2   2 b1    2    0   0   9   0   1     0     0     0     0     0     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     1     1     1     0     1     3

   8   2 0 1   2 b1    2    0   0   0   9   1     1     1     1     1     0     4
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     2     0
                                            4     0     0     0     0     0     0
 ........................................

   2   1 1 0   2 b1    1    6   5   0   0   1     0     0     0     0     0     0
                                            2     1     1     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     1     0     0     0     1     0

   3   1 0 1   2 b1    1    8   7   6   5   1     2     1     1     1     0     0
                                            2     0     0     0     0     0     0
                                            3     2     2     1     0     1     1
                                            4     0     0     0     0     0     0

   4   1 0 0   2 b1    1    0   8   0   6   1     0     0     0     0     0     0
                                            2     1     1     0     0     0     4
                                            3     0     0     0     0     0     0
                                            4     1     1     1     1     1     2
 ........................................

   1   0 0 0   0       0    4   3   0   2   1     0     0     0     0     0     0
                                            2     4     3     1     1     0     0
                                            3     0     0     0     0     0     0
                                            4     4     3     1     1     1     0
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=       4
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
        4 csfs selected from       4 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
        4 csfs selected from       4 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
        4 csfs selected from       4 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /global/rene/tests/Columbus/test/SP-CALC/C3H5-SPIND/WORK/mcdrtfl                
 
 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 0 1
   CSF#     2    1 2 1
   CSF#     3    1 1 2
   CSF#     4    0 3 1
