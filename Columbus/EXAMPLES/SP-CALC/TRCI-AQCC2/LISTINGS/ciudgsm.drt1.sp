1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:16 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:16 2003

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:16 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:16 2003
 /                                                                               

 297 dimension of the ci-matrix ->>>       358


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -38.6450051117 -7.1054E-15  0.0000E+00  4.2072E-01  1.0000E-06
 mraqcc  #  1  2    -38.4074031068 -1.4211E-14  1.0486E-01  4.3501E-01  1.0000E-06
 mraqcc  #  2  1    -38.6450536095  4.8498E-05  0.0000E+00  4.2053E-01  1.0000E-06
 mraqcc  #  2  2    -38.4887501007  8.1347E-02  5.5302E-03  1.0221E-01  1.0000E-06
 mraqcc  #  3  1    -38.6464754617  1.4219E-03  0.0000E+00  4.1678E-01  1.0000E-06
 mraqcc  #  3  2    -38.4954529758  6.7029E-03  9.1036E-04  3.9267E-02  1.0000E-06
 mraqcc  #  4  1    -38.6549279911  8.4525E-03  0.0000E+00  3.9224E-01  1.0000E-06
 mraqcc  #  4  2    -38.4964228680  9.6989E-04  1.9978E-04  1.8792E-02  1.0000E-06
 mraqcc  #  5  1    -38.6711596831  1.6232E-02  0.0000E+00  3.4357E-01  1.0000E-06
 mraqcc  #  5  2    -38.4966317236  2.0886E-04  4.3270E-05  8.5810E-03  1.0000E-06
 mraqcc  #  6  1    -38.6817180320  1.0558E-02  0.0000E+00  2.9492E-01  1.0000E-06
 mraqcc  #  6  2    -38.4966903436  5.8620E-05  1.6726E-05  5.4561E-03  1.0000E-06
 mraqcc  #  7  1    -38.6911691170  9.4511E-03  0.0000E+00  2.5934E-01  1.0000E-06
 mraqcc  #  7  2    -38.4967153688  2.5025E-05  6.8317E-06  3.5286E-03  1.0000E-06
 mraqcc  #  8  1    -38.6946194702  3.4504E-03  0.0000E+00  2.4219E-01  1.0000E-06
 mraqcc  #  8  2    -38.4967250238  9.6550E-06  2.3946E-06  2.0924E-03  1.0000E-06
 mraqcc  #  9  1    -38.6964365894  1.8171E-03  0.0000E+00  2.3652E-01  1.0000E-06
 mraqcc  #  9  2    -38.4967285377  3.5140E-06  5.9721E-07  1.0918E-03  1.0000E-06
 mraqcc  # 10  1    -38.6966214865  1.8490E-04  0.0000E+00  2.3465E-01  1.0000E-06
 mraqcc  # 10  2    -38.4967293422  8.0448E-07  9.6712E-08  4.3702E-04  1.0000E-06
 mraqcc  # 11  1    -38.6979665599  1.3451E-03  0.0000E+00  2.2827E-01  1.0000E-06
 mraqcc  # 11  2    -38.4967294534  1.1120E-07  1.3135E-08  1.5901E-04  1.0000E-06
 mraqcc  # 12  1    -38.6983996174  4.3306E-04  0.0000E+00  2.2448E-01  1.0000E-06
 mraqcc  # 12  2    -38.4967294687  1.5327E-08  1.7958E-09  5.7822E-05  1.0000E-06
 mraqcc  # 13  1    -38.6994200999  1.0205E-03  0.0000E+00  2.2047E-01  1.0000E-06
 mraqcc  # 13  2    -38.4967294708  2.0221E-09  1.9626E-10  1.9606E-05  1.0000E-06
 mraqcc  # 14  1    -38.6995476350  1.2754E-04  0.0000E+00  2.1989E-01  1.0000E-06
 mraqcc  # 14  2    -38.4967294710  2.4955E-10  2.8109E-11  7.1711E-06  1.0000E-06
 mraqcc  # 15  1    -38.6996065835  5.8948E-05  0.0000E+00  2.1917E-01  1.0000E-06
 mraqcc  # 15  2    -38.4967294710  3.3296E-11  3.8382E-12  2.7048E-06  1.0000E-06
 mraqcc  # 16  1    -38.7005419823  9.3540E-04  0.0000E+00  2.1376E-01  1.0000E-06
 mraqcc  # 16  2    -38.4967294711  4.1620E-12  7.8795E-13  1.1312E-06  1.0000E-06
 mraqcc  # 17  1    -38.7005490996  7.1173E-06  0.0000E+00  2.1373E-01  1.0000E-06
 mraqcc  # 17  2    -38.4967294711  9.9654E-13  1.6868E-13  5.5194E-07  1.0000E-06

 mraqcc   convergence criteria satisfied after 17 iterations.

 final mraqcc   convergence information:
 mraqcc  # 17  2    -38.4967294711  9.9654E-13  1.6868E-13  5.5194E-07  1.0000E-06
