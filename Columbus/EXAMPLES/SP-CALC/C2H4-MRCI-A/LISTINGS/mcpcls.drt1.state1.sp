

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    ethylen pvdz                                                                    
    aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:44 2003
    ethylen cas(2,2)                                                                


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
    ethylen cas(2,2)                                                                
 Molecular symmetry group:   sym1
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         2

   ***  Informations from the DRT number:   1


 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag   b1g   b2g   b3g    au   b1u   b2u   b3u 

 List of doubly occupied orbitals:
  1 ag   2 ag   3 ag   1b2g   1b1u   2b1u   1b3u 

 List of active orbitals:
  1b3g   1b2u 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -78.0668950712    nuclear repulsion=    33.2200605173
 demc=             0.0000000016    wnorm=                 0.0000006413
 knorm=            0.0000000373    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    ag     1   1.0000

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  1 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  ag  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  1b3g   1b2u 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2  0.9785787159  0.9576163031  03
      1 -0.2058730115  0.0423836969  30

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: