echo of the argos input file:
 ------------------------------------------------------------------------
 ethylen pvdz
   2  2  5  5  5 20  9  0  0  0  0  0  0  0  0
   8  1 ag  1b1g  1b2g  1b3g  1 au  1b1u  1b2u  1b3u
   7
   2  3  4
   2  5  6
   2  7  8
   3  5  7
   3  6  8
   4  5  8
   4  6  7
     2    1    6
     6    6    8    7    1    3    4
    10    1    3    4    1    2    6    8    7    6    5
     4    1    8    6    3
    12    6    8    7    3    1    2    1    3    4    8    6    5
     2    2    1
     1    1
     1   -1
     6    6    2
     0    0    1    0    0    1
     1    0    0    1    0    0
     0    1    0    0    1    0
     0    0    1    0    0   -1
     1    0    0   -1    0    0
     0    1    0    0   -1    0
    10   12    3
    -1   -1    4    0    0    0   -1   -1    4    0    0    0
     0    0    0    0    1    0    0    0    0    0    1    0
     0    0    0    0    0    1    0    0    0    0    0    1
     1   -1    0    0    0    0    1   -1    0    0    0    0
     0    0    0    1    0    0    0    0    0    1    0    0
    -1   -1    4    0    0    0    1    1   -4    0    0    0
     0    0    0    0    1    0    0    0    0    0   -1    0
     0    0    0    0    0    1    0    0    0    0    0   -1
     1   -1    0    0    0    0   -1    1    0    0    0    0
     0    0    0    1    0    0    0    0    0   -1    0    0
     4    4    4
     1    1    1    1
     1   -1    1   -1
     1    1   -1   -1
     1   -1   -1    1
    12   12    5
     0    0    1    0    0    1    0    0    1    0    0    1
     1    0    0    1    0    0    1    0    0    1    0    0
     0    1    0    0    1    0    0    1    0    0    1    0
     0    0    1    0    0   -1    0    0    1    0    0   -1
     1    0    0   -1    0    0    1    0    0   -1    0    0
     0    1    0    0   -1    0    0    1    0    0   -1    0
     0    0    1    0    0    1    0    0   -1    0    0   -1
     1    0    0    1    0    0   -1    0    0   -1    0    0
     0    1    0    0    1    0    0   -1    0    0   -1    0
     0    0    1    0    0   -1    0    0   -1    0    0    1
     1    0    0   -1    0    0   -1    0    0    1    0    0
     0    1    0    0   -1    0    0   -1    0    0    1    0
     9    1    3
    6665.0000000        0.0006920  -0.0001460   0.0000000
    1000.0000000        0.0053290  -0.0011540   0.0000000
     228.0000000        0.0270770  -0.0057250   0.0000000
      64.7100000        0.1017180  -0.0233120   0.0000000
      21.0600000        0.2747400  -0.0639550   0.0000000
       7.4950000        0.4485640  -0.1499810   0.0000000
       2.7970000        0.2850740  -0.1272620   0.0000000
       0.5215000        0.0152040   0.5445290   0.0000000
       0.1596000       -0.0031910   0.5804960   1.0000000
     4    2    2
       9.4390000        0.0381090   0.0000000
       2.0020000        0.2094800   0.0000000
       0.5456000        0.5085570   0.0000000
       0.1517000        0.4688420   1.0000000
     1    3    1
       0.5500000        1.0000000
     4    1    2
      13.0100000        0.0196850   0.0000000
       1.9620000        0.1379770   0.0000000
       0.4446000        0.4781480   0.0000000
       0.1220000        0.5012400   1.0000000
     1    2    1
       0.7270000        1.0000000
 C    3  2 6.
     0.00000000    0.00000000    1.25306000
     0.00000000    0.00000000   -1.25306000
   1  2
   2  1
   1  1
   2  2
   3  3
 H    2  4 1.
     1.74646000    0.00000000    2.37500000
    -1.74646000    0.00000000    2.37500000
     1.74646000    0.00000000   -2.37500000
    -1.74646000    0.00000000   -2.37500000
   2  1  4  3
   3  4  1  2
   4  4
   5  5
 ------------------------------------------------------------------------
                              program "argos" 5.4
                            columbus program system

             this program computes integrals over symmetry orbitals
               of generally contracted gaussian atomic orbitals.
                        programmed by russell m. pitzer

                           version date: 03-feb-1999

references:

    symmetry analysis (equal contributions):
    r. m. pitzer, j. chem. phys. 58, 3111 (1973).

    ao integral evaluation (hondo):
    m. dupuis, j. rys, and h. f. king, j. chem. phys. 65, 111 (1976).

    general contraction of gaussian orbitals:
    r. c. raffenetti, j. chem. phys. 58, 4452 (1973).

    core potential ao integrals (meldps):
    l. e. mcmurchie and e. r. davidson, j. comput. phys. 44, 289 (1981)

    spin-orbit and core potential integrals:
    r. m. pitzer and n. w. winter, int. j. quantum chem. 40, 773 (1991)

 This Version of Program ARGOS is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at


 workspace allocation parameters: lcore=   8000000 mem1=1108557832 ifirst=-264082478

 filenames and unit numbers:
 unit description                   filename
 ---- -----------                   ----------
   6  listing file:                 argosls                                                     
   4  1-e integral file:            aoints                                                      
   8  2-e integral file [fsplit=2]: aoints2                                                     
   5  input file:                   argosin                                                     

 argos input parameters and titles:
 ngen   =  2 ns     =  2 naords =  5 ncons  =  5 ngcs   =  5 itol   = 20
 icut   =  9 aoints =  4 only1e =  0 inrm   =  0 ncrs   =  0
 l1rec  =         0      l2rec  =         0      aoint2 =  8 fsplit =  2

ethylen pvdz                                                                    
aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:44 2003


irrep            1       2       3       4       5       6       7       8
degeneracy       1       1       1       1       1       1       1       1
label            ag     b1g     b2g     b3g      au     b1u     b2u     b3u


direct product table
   ( ag) ( ag) =  ag
   ( ag) (b1g) = b1g
   ( ag) (b2g) = b2g
   ( ag) (b3g) = b3g
   ( ag) ( au) =  au
   ( ag) (b1u) = b1u
   ( ag) (b2u) = b2u
   ( ag) (b3u) = b3u
   (b1g) ( ag) = b1g
   (b1g) (b1g) =  ag
   (b1g) (b2g) = b3g
   (b1g) (b3g) = b2g
   (b1g) ( au) = b1u
   (b1g) (b1u) =  au
   (b1g) (b2u) = b3u
   (b1g) (b3u) = b2u
   (b2g) ( ag) = b2g
   (b2g) (b1g) = b3g
   (b2g) (b2g) =  ag
   (b2g) (b3g) = b1g
   (b2g) ( au) = b2u
   (b2g) (b1u) = b3u
   (b2g) (b2u) =  au
   (b2g) (b3u) = b1u
   (b3g) ( ag) = b3g
   (b3g) (b1g) = b2g
   (b3g) (b2g) = b1g
   (b3g) (b3g) =  ag
   (b3g) ( au) = b3u
   (b3g) (b1u) = b2u
   (b3g) (b2u) = b1u
   (b3g) (b3u) =  au
   ( au) ( ag) =  au
   ( au) (b1g) = b1u
   ( au) (b2g) = b2u
   ( au) (b3g) = b3u
   ( au) ( au) =  ag
   ( au) (b1u) = b1g
   ( au) (b2u) = b2g
   ( au) (b3u) = b3g
   (b1u) ( ag) = b1u
   (b1u) (b1g) =  au
   (b1u) (b2g) = b3u
   (b1u) (b3g) = b2u
   (b1u) ( au) = b1g
   (b1u) (b1u) =  ag
   (b1u) (b2u) = b3g
   (b1u) (b3u) = b2g
   (b2u) ( ag) = b2u
   (b2u) (b1g) = b3u
   (b2u) (b2g) =  au
   (b2u) (b3g) = b1u
   (b2u) ( au) = b2g
   (b2u) (b1u) = b3g
   (b2u) (b2u) =  ag
   (b2u) (b3u) = b1g
   (b3u) ( ag) = b3u
   (b3u) (b1g) = b2u
   (b3u) (b2g) = b1u
   (b3u) (b3g) =  au
   (b3u) ( au) = b3g
   (b3u) (b1u) = b2g
   (b3u) (b2u) = b1g
   (b3u) (b3u) =  ag


                     nuclear repulsion energy   33.22006052


primitive ao integrals neglected if exponential factor below 10**(-20)
contracted ao and so integrals neglected if value below 10**(- 9)
symmetry orbital integrals written on units  4  8


                                    C   atoms

                              nuclear charge   6.00

           center            x               y               z
             1             0.00000000      0.00000000      1.25306000
             2             0.00000000      0.00000000     -1.25306000
 operator      center interchanges
  1(id.)        1  2
  2(gen.)       1  2
  3(gen.)       2  1
  4             2  1

                    1s orbitals

 orbital exponents  contraction coefficients
    6665.000       6.9200018E-04  -1.4600006E-04    0.000000    
    1000.000       5.3290014E-03  -1.1540005E-03    0.000000    
    228.0000       2.7077007E-02  -5.7250025E-03    0.000000    
    64.71000       0.1017180      -2.3312010E-02    0.000000    
    21.06000       0.2747401      -6.3955028E-02    0.000000    
    7.495000       0.4485641      -0.1499811        0.000000    
    2.797000       0.2850741      -0.1272621        0.000000    
   0.5215000       1.5204004E-02   0.5445292        0.000000    
   0.1596000      -3.1910008E-03   0.5804963        1.000000    

                     symmetry orbital labels
                     1 ag1           2 ag1           3 ag1
                     1b1u1           2b1u1           3b1u1

           symmetry orbitals
 ctr, ao    ag1   b1u1
  1, 000  1.000  1.000
  2, 000  1.000 -1.000

                    2p orbitals

 orbital exponents  contraction coefficients
    9.439000       3.8109021E-02    0.000000    
    2.002000       0.2094801        0.000000    
   0.5456000       0.5085573        0.000000    
   0.1517000       0.4688423        1.000000    

                     symmetry orbital labels
                     4b1u1           5b1u1
                     1b3u1           2b3u1
                     1b2u1           2b2u1
                     4 ag1           5 ag1
                     1b2g1           2b2g1
                     1b3g1           2b3g1

           symmetry orbitals
 ctr, ao   b1u1   b3u1   b2u1    ag1   b2g1   b3g1
  1, 100  0.000  1.000  0.000  0.000  1.000  0.000
  1, 010  0.000  0.000  1.000  0.000  0.000  1.000
  1, 001  1.000  0.000  0.000  1.000  0.000  0.000
  2, 100  0.000  1.000  0.000  0.000 -1.000  0.000
  2, 010  0.000  0.000  1.000  0.000  0.000 -1.000
  2, 001  1.000  0.000  0.000 -1.000  0.000  0.000

                    3d orbitals

 orbital exponents  contraction coefficients
   0.5500000        1.000000    

                     symmetry orbital labels
                     6 ag1
                     3b2g1
                     3b3g1
                     7 ag1
                     1b1g1
                     6b1u1
                     3b3u1
                     3b2u1
                     7b1u1
                     1 au1

           symmetry orbitals
 ctr, ao    ag1   b2g1   b3g1    ag1   b1g1   b1u1   b3u1   b2u1   b1u1    au1
  1, 200 -1.000  0.000  0.000  1.000  0.000 -1.000  0.000  0.000  1.000  0.000
  1, 020 -1.000  0.000  0.000 -1.000  0.000 -1.000  0.000  0.000 -1.000  0.000
  1, 002  2.000  0.000  0.000  0.000  0.000  2.000  0.000  0.000  0.000  0.000
  1, 110  0.000  0.000  0.000  0.000  1.000  0.000  0.000  0.000  0.000  1.000
  1, 101  0.000  1.000  0.000  0.000  0.000  0.000  1.000  0.000  0.000  0.000
  1, 011  0.000  0.000  1.000  0.000  0.000  0.000  0.000  1.000  0.000  0.000
  2, 200 -1.000  0.000  0.000  1.000  0.000  1.000  0.000  0.000 -1.000  0.000
  2, 020 -1.000  0.000  0.000 -1.000  0.000  1.000  0.000  0.000  1.000  0.000
  2, 002  2.000  0.000  0.000  0.000  0.000 -2.000  0.000  0.000  0.000  0.000
  2, 110  0.000  0.000  0.000  0.000  1.000  0.000  0.000  0.000  0.000 -1.000
  2, 101  0.000  1.000  0.000  0.000  0.000  0.000 -1.000  0.000  0.000  0.000
  2, 011  0.000  0.000  1.000  0.000  0.000  0.000  0.000 -1.000  0.000  0.000


                                    H   atoms

                              nuclear charge   1.00

           center            x               y               z
             1             1.74646000      0.00000000      2.37500000
             2            -1.74646000      0.00000000      2.37500000
             3             1.74646000      0.00000000     -2.37500000
             4            -1.74646000      0.00000000     -2.37500000
 operator      center interchanges
  1(id.)        1  2  3  4
  2(gen.)       2  1  4  3
  3(gen.)       3  4  1  2
  4             4  3  2  1

                    1s orbitals

 orbital exponents  contraction coefficients
    13.01000       1.9684990E-02    0.000000    
    1.962000       0.1379769        0.000000    
   0.4446000       0.4781478        0.000000    
   0.1220000       0.5012397        1.000000    

                     symmetry orbital labels
                     8 ag1           9 ag1
                     4b3u1           5b3u1
                     8b1u1           9b1u1
                     4b2g1           5b2g1

           symmetry orbitals
 ctr, ao    ag1   b3u1   b1u1   b2g1
  1, 000  1.000  1.000  1.000  1.000
  2, 000  1.000 -1.000  1.000 -1.000
  3, 000  1.000  1.000 -1.000 -1.000
  4, 000  1.000 -1.000 -1.000  1.000

                    2p orbitals

 orbital exponents  contraction coefficients
   0.7270000        1.000000    

                     symmetry orbital labels
                    10b1u1
                     6b3u1
                     4b2u1
                     6b2g1
                    10 ag1
                     2b1g1
                    11 ag1
                     7b2g1
                     4b3g1
                     7b3u1
                    11b1u1
                     2 au1

           symmetry orbitals
 ctr, ao   b1u1   b3u1   b2u1   b2g1    ag1   b1g1    ag1   b2g1   b3g1   b3u1
  1, 100  0.000  1.000  0.000  0.000  1.000  0.000  0.000  1.000  0.000  0.000
  1, 010  0.000  0.000  1.000  0.000  0.000  1.000  0.000  0.000  1.000  0.000
  1, 001  1.000  0.000  0.000  1.000  0.000  0.000  1.000  0.000  0.000  1.000
  2, 100  0.000  1.000  0.000  0.000 -1.000  0.000  0.000  1.000  0.000  0.000
  2, 010  0.000  0.000  1.000  0.000  0.000 -1.000  0.000  0.000  1.000  0.000
  2, 001  1.000  0.000  0.000 -1.000  0.000  0.000  1.000  0.000  0.000 -1.000
  3, 100  0.000  1.000  0.000  0.000  1.000  0.000  0.000 -1.000  0.000  0.000
  3, 010  0.000  0.000  1.000  0.000  0.000  1.000  0.000  0.000 -1.000  0.000
  3, 001  1.000  0.000  0.000  1.000  0.000  0.000 -1.000  0.000  0.000 -1.000
  4, 100  0.000  1.000  0.000  0.000 -1.000  0.000  0.000 -1.000  0.000  0.000
  4, 010  0.000  0.000  1.000  0.000  0.000 -1.000  0.000  0.000 -1.000  0.000
  4, 001  1.000  0.000  0.000 -1.000  0.000  0.000 -1.000  0.000  0.000  1.000
 ctr, ao   b1u1    au1
  1, 100  1.000  0.000
  1, 010  0.000  1.000
  1, 001  0.000  0.000
  2, 100 -1.000  0.000
  2, 010  0.000 -1.000
  2, 001  0.000  0.000
  3, 100 -1.000  0.000
  3, 010  0.000 -1.000
  3, 001  0.000  0.000
  4, 100  1.000  0.000
  4, 010  0.000  1.000
  4, 001  0.000  0.000

lx: b3g          ly: b2g          lz: b1g

output SIFS file header information:
ethylen pvdz                                                                    
aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:44 2003

output energy(*) values:
 energy( 1)=  3.322006051733E+01, ietype=   -1,    core energy of type: Nuc.Rep.

total core energy =  3.322006051733E+01

nsym = 8 nbft=  48

symmetry  =    1    2    3    4    5    6    7    8
slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
nbpsy(*)  =   11    2    7    4    2   11    4    7

info(*) =         2      4096      3272      4096      2700

output orbital labels, i:bfnlab(i)=
   1:  1C__1s   2:  2C__1s   3:  3C__1s   4:  4C__2p   5:  5C__2p   6:  6C__3d
   7:  7C__3d   8:  8H__1s   9:  9H__1s  10: 10H__2p  11: 11H__2p  12: 12C__3d
  13: 13H__2p  14: 14C__2p  15: 15C__2p  16: 16C__3d  17: 17H__1s  18: 18H__1s
  19: 19H__2p  20: 20H__2p  21: 21C__2p  22: 22C__2p  23: 23C__3d  24: 24H__2p
  25: 25C__3d  26: 26H__2p  27: 27C__1s  28: 28C__1s  29: 29C__1s  30: 30C__2p
  31: 31C__2p  32: 32C__3d  33: 33C__3d  34: 34H__1s  35: 35H__1s  36: 36H__2p
  37: 37H__2p  38: 38C__2p  39: 39C__2p  40: 40C__3d  41: 41H__2p  42: 42C__2p
  43: 43C__2p  44: 44C__3d  45: 45H__1s  46: 46H__1s  47: 47H__2p  48: 48H__2p

bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  2   9:  2  10:  2
  11:  2  12:  1  13:  2  14:  1  15:  1  16:  1  17:  2  18:  2  19:  2  20:  2
  21:  1  22:  1  23:  1  24:  2  25:  1  26:  2  27:  1  28:  1  29:  1  30:  1
  31:  1  32:  1  33:  1  34:  2  35:  2  36:  2  37:  2  38:  1  39:  1  40:  1
  41:  2  42:  1  43:  1  44:  1  45:  2  46:  2  47:  2  48:  2

bfn_to_orbital_type map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  2   5:  2   6:  4   7:  4   8:  1   9:  1  10:  2
  11:  2  12:  4  13:  2  14:  2  15:  2  16:  4  17:  1  18:  1  19:  2  20:  2
  21:  2  22:  2  23:  4  24:  2  25:  4  26:  2  27:  1  28:  1  29:  1  30:  2
  31:  2  32:  4  33:  4  34:  1  35:  1  36:  2  37:  2  38:  2  39:  2  40:  4
  41:  2  42:  2  43:  2  44:  4  45:  1  46:  1  47:  2  48:  2


       48 symmetry orbitals,       ag:  11   b1g:   2   b2g:   7   b3g:   4
                                   au:   2   b1u:  11   b2u:   4   b3u:   7
 !timer: syminp required                 user+sys=     0.010 walltime=     0.000

 socfpd: mcxu=    56264 mcxu2=    45340 left=  7943736
 !timer: socfpd required                 user+sys=     0.010 walltime=     0.000

oneint:   202 S1(*)    integrals were written in  1 records.
oneint:   202 T1(*)    integrals were written in  1 records.
oneint:   214 V1(*)    integrals were written in  1 records.
oneint:   170 X(*)     integrals were written in  1 records.
oneint:   116 Y(*)     integrals were written in  1 records.
oneint:   178 Z(*)     integrals were written in  1 records.
oneint:   170 Im(px)   integrals were written in  1 records.
oneint:   116 Im(py)   integrals were written in  1 records.
oneint:   178 Im(pz)   integrals were written in  1 records.
oneint:   116 Im(lx)   integrals were written in  1 records.
oneint:   170 Im(ly)   integrals were written in  1 records.
oneint:    76 Im(lz)   integrals were written in  1 records.
oneint:   214 XX(*)    integrals were written in  1 records.
oneint:    98 XY(*)    integrals were written in  1 records.
oneint:   170 XZ(*)    integrals were written in  1 records.
oneint:   214 YY(*)    integrals were written in  1 records.
oneint:   116 YZ(*)    integrals were written in  1 records.
oneint:   202 ZZ(*)    integrals were written in  1 records.

 !timer: oneint required                 user+sys=     0.020 walltime=     0.000
 !timer: seg1mn required                 user+sys=     0.040 walltime=     0.000

twoint:       92764 1/r12    integrals and       70 pk flags
                                 were written in    35 records.

 twoint: maximum mblu needed =      3898
 !timer: twoint required                 user+sys=     0.900 walltime=     1.000

 driver: 1-e integral  workspace high-water mark =     77667
 driver: 2-e integral  workspace high-water mark =     74337
 driver: overall argos workspace high-water mark =     77667
 !timer: argos required                  user+sys=     0.940 walltime=     1.000
