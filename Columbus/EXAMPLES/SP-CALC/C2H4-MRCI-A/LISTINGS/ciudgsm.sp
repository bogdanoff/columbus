1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 ethylen pvdz                                                                    
 aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:44 2003
  ethylen cas(2,2) reference                                                     
 ethylen cas(2,2)                                                                
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Jun  5 14:26:46 2003
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:46 2003

 formula file title:
 ethylen pvdz                                                                    
 aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:44 2003
  ethylen cas(2,2) reference                                                     
 ethylen cas(2,2)                                                                
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Jun  5 14:26:46 2003
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:46 2003
  ethylen cas(2,2) reference                                                     

 297 dimension of the ci-matrix ->>>     14558


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    13)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.0668950712 -4.2188E-15  3.0754E-01  9.4916E-01  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -78.0668950712 -4.2188E-15  3.0754E-01  9.4916E-01  1.0000E-04

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.3235468672  2.5665E-01  1.1023E-02  1.6978E-01  1.0000E-04
 mr-sdci #  2  1    -78.3319062860  8.3594E-03  1.0873E-03  5.3067E-02  1.0000E-04
 mr-sdci #  3  1    -78.3327676632  8.6138E-04  5.3231E-05  1.2047E-02  1.0000E-04
 mr-sdci #  4  1    -78.3328230834  5.5420E-05  3.3527E-06  3.1283E-03  1.0000E-04
 mr-sdci #  5  1    -78.3328264758  3.3924E-06  3.0582E-07  8.9229E-04  1.0000E-04
 mr-sdci #  6  1    -78.3328268118  3.3599E-07  4.1056E-08  3.2919E-04  1.0000E-04
 mr-sdci #  7  1    -78.3328268593  4.7418E-08  5.0672E-09  1.1751E-04  1.0000E-04
 mr-sdci #  8  1    -78.3328268645  5.2132E-09  4.6092E-10  3.5865E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  8  1    -78.3328268645  5.2132E-09  4.6092E-10  3.5865E-05  1.0000E-04

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 eref      =    -78.066304920239   "relaxed" cnot**2         =   0.912510698844
 eci       =    -78.332826864464   deltae = eci - eref       =  -0.266521944225
 eci+dv1   =    -78.356144683107   dv1 = (1-cnot**2)*deltae  =  -0.023317818643
 eci+dv2   =    -78.358380338711   dv2 = dv1 / cnot**2       =  -0.025553474247
 eci+dv3   =    -78.361090154008   dv3 = dv1 / (2*cnot**2-1) =  -0.028263289544
 eci+pople =    -78.356890487025   ( 16e- scaled deltae )    =  -0.290585566786
