

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
         8000000 of real*8 words (   61.04 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
  NPATH =  1, 2, 3, 9,10,12,13,17,19,21,23,
  NSTATE=  0,
  NITER =  30,
  TOL(1)= .00000001,
  NCOUPL =   1,
  FCIORB= 4   1  4, 7   1  4,
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 ethylen pvdz                                                                    
 aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:44 2003

 Core type energy values:
 energy( 1)=  3.322006051733E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   33.220060517


   ******  Basis set informations:  ******

 Number of irreps:                  8
 Total number of basis functions:  48

 irrep no.              1    2    3    4    5    6    7    8
 irrep label            ag  b1g  b2g  b3g   au  b1u  b2u  b3u
 no. of bas.fcions.    11    2    7    4    2   11    4    7


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    30

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
   ag        2

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       4       1( 21)     4
       7       1( 38)     4

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  1
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=25 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
 ethylen cas(2,2)                                                                
 Molecular symmetry group:    ag 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         2

 !timer: initialization                  user+sys=     0.010 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   0 possible.


 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    54
 Number of active-virtual rotations:     6

 Size of orbital-Hessian matrix B:                     2022
 Size of the orbital-state Hessian matrix C:            120
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2142



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is 106
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  2844

 twoao_o processed      92764 two electron ao integrals.

      92764 ao integrals were written into   87 records

 srtinc_o read in      93892 integrals and wrote out      93892 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.090 walltime=     0.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     15931
 number of records written:     6
 !timer: 2-e transformation              user+sys=     0.080 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2022
 Size of the orbital-state Hessian matrix C:            120
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2142

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     2
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   2 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0569742091     -111.2770347264        0.0000000000        0.0000010000
    2       -77.4716677901     -110.6917283074        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.00841513886
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.80901452 pnorm= 1.8991E-01 rznorm= 2.5789E-06 rpnorm= 2.5182E-17 noldr=  7 nnewr=  7 nolds=  1 nnews=  1

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.487674   -2.067699   -1.180927

 qvv(*) eigenvalues. symmetry block  1
     0.405325    1.161015    1.412264    2.008410    3.173530    3.676170    5.006681    5.316277

 qvv(*) eigenvalues. symmetry block  2
     2.259696    3.896987

 fdd(*) eigenvalues. symmetry block  3
    -0.991644

 qvv(*) eigenvalues. symmetry block  3
     0.606613    1.847946    2.441243    3.505473    4.813491    6.760956
 i,qaaresolved 1  0.319387027

 qvv(*) eigenvalues. symmetry block  4
     1.611052    3.610521    4.479135

 qvv(*) eigenvalues. symmetry block  5
     2.583799    4.682928

 fdd(*) eigenvalues. symmetry block  6
   -22.484285   -1.575144

 qvv(*) eigenvalues. symmetry block  6
     0.467964    0.934579    1.545908    1.947209    2.474913    3.700672    4.490346    5.296802    6.139434
 i,qaaresolved 1 -0.739266587

 qvv(*) eigenvalues. symmetry block  7
     1.370038    2.516111    3.959913

 fdd(*) eigenvalues. symmetry block  8
    -1.265056

 qvv(*) eigenvalues. symmetry block  8
     0.459325    1.199641    1.706696    3.510326    3.766895    5.589500
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.110 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -78.0569742091 demc= 7.8057E+01 wnorm= 6.7321E-02 knorm= 5.7747E-01 apxde= 1.8066E-02    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 user+sys=     0.200 walltime=     0.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     15931
 number of records written:     6
 !timer: 2-e transformation              user+sys=     0.080 walltime=     1.000

 Size of orbital-Hessian matrix B:                     2022
 Size of the orbital-state Hessian matrix C:            120
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2142

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0627172819     -111.2827777992        0.0000000000        0.0000025675
    2       -77.1674139043     -110.3874744216        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.00720345115
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.010 walltime=     0.000
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.92710620 pnorm= 8.7048E-02 rznorm= 2.1120E-06 rpnorm= 1.6466E-17 noldr=  7 nnewr=  7 nolds=  1 nnews=  1

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.010 walltime=     0.000
 !timer: solvek total                    user+sys=     0.010 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.440178   -2.046188   -1.160064

 qvv(*) eigenvalues. symmetry block  1
     0.410438    1.170266    1.422868    2.021409    3.186437    3.687891    5.019726    5.329875

 qvv(*) eigenvalues. symmetry block  2
     2.272825    3.906950

 fdd(*) eigenvalues. symmetry block  3
    -0.977990

 qvv(*) eigenvalues. symmetry block  3
     0.611507    1.865247    2.448045    3.512025    4.828824    6.777220
 i,qaaresolved 1  0.733418782

 qvv(*) eigenvalues. symmetry block  4
     1.249538    3.590863    4.482391

 qvv(*) eigenvalues. symmetry block  5
     2.594600    4.694546

 fdd(*) eigenvalues. symmetry block  6
   -22.436773   -1.559682

 qvv(*) eigenvalues. symmetry block  6
     0.472401    0.940498    1.556362    1.965954    2.487503    3.712709    4.507693    5.310279    6.154746
 i,qaaresolved 1 -0.728575295

 qvv(*) eigenvalues. symmetry block  7
     1.375286    2.531214    3.969140

 fdd(*) eigenvalues. symmetry block  8
    -1.249494

 qvv(*) eigenvalues. symmetry block  8
     0.465483    1.208333    1.721116    3.519713    3.779057    5.607692
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.100 walltime=     1.000

 all mcscf convergence criteria are not satisfied.
 iter=    2 emc=  -78.0627172819 demc= 5.7431E-03 wnorm= 5.7628E-02 knorm= 3.7339E-01 apxde= 5.7424E-03    *not converged* 

               starting mcscf iteration...   3
 !timer:                                 user+sys=     0.300 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     15931
 number of records written:     6
 !timer: 2-e transformation              user+sys=     0.080 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2022
 Size of the orbital-state Hessian matrix C:            120
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2142

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0649926051     -111.2850531224        0.0000000000        0.0000013786
    2       -77.4601659975     -110.6802265148        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.010 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  0.00577926122
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99228674 pnorm= 6.2261E-03 rznorm= 6.8131E-06 rpnorm= 2.2616E-18 noldr=  6 nnewr=  6 nolds=  1 nnews=  1

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.463690   -2.058335   -1.170278

 qvv(*) eigenvalues. symmetry block  1
     0.408376    1.165559    1.418087    2.015770    3.178942    3.681505    5.013598    5.324239

 qvv(*) eigenvalues. symmetry block  2
     2.265699    3.901841

 fdd(*) eigenvalues. symmetry block  3
    -0.983690

 qvv(*) eigenvalues. symmetry block  3
     0.609994    1.857350    2.444651    3.510125    4.821989    6.769157
 i,qaaresolved 1  0.363270065

 qvv(*) eigenvalues. symmetry block  4
     1.560864    3.614331    4.485719

 qvv(*) eigenvalues. symmetry block  5
     2.589022    4.688207

 fdd(*) eigenvalues. symmetry block  6
   -22.460300   -1.566194

 qvv(*) eigenvalues. symmetry block  6
     0.470896    0.938192    1.550484    1.957631    2.480287    3.706724    4.498869    5.304424    6.147270
 i,qaaresolved 1 -0.728345446

 qvv(*) eigenvalues. symmetry block  7
     1.368808    2.521783    3.964998

 fdd(*) eigenvalues. symmetry block  8
    -1.256808

 qvv(*) eigenvalues. symmetry block  8
     0.462567    1.203655    1.713217    3.516082    3.772839    5.599032
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.100 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    3 emc=  -78.0649926051 demc= 2.2753E-03 wnorm= 4.6234E-02 knorm= 1.2396E-01 apxde= 1.8375E-03    *not converged* 

               starting mcscf iteration...   4
 !timer:                                 user+sys=     0.400 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     15931
 number of records written:     6
 !timer: 2-e transformation              user+sys=     0.090 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2022
 Size of the orbital-state Hessian matrix C:            120
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2142

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.010 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0668863466     -111.2869468639        0.0000000000        0.0000010000
    2       -77.4006335858     -110.6206941031        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.000664531338
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99996817 pnorm= 1.3060E-03 rznorm= 2.3264E-06 rpnorm= 1.6332E-20 noldr=  5 nnewr=  5 nolds=  1 nnews=  1

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.455604   -2.053696   -1.166930

 qvv(*) eigenvalues. symmetry block  1
     0.408993    1.167279    1.419798    2.017650    3.181802    3.683901    5.015670    5.326090

 qvv(*) eigenvalues. symmetry block  2
     2.268488    3.903687

 fdd(*) eigenvalues. symmetry block  3
    -0.981901

 qvv(*) eigenvalues. symmetry block  3
     0.610319    1.859905    2.445854    3.510687    4.824215    6.771989
 i,qaaresolved 1  0.451480343

 qvv(*) eigenvalues. symmetry block  4
     1.488305    3.608433    4.485793

 qvv(*) eigenvalues. symmetry block  5
     2.591028    4.690449

 fdd(*) eigenvalues. symmetry block  6
   -22.452223   -1.564172

 qvv(*) eigenvalues. symmetry block  6
     0.471321    0.938972    1.552480    1.960323    2.482999    3.708733    4.501987    5.306386    6.149835
 i,qaaresolved 1 -0.725443629

 qvv(*) eigenvalues. symmetry block  7
     1.372739    2.525408    3.966435

 fdd(*) eigenvalues. symmetry block  8
    -1.254332

 qvv(*) eigenvalues. symmetry block  8
     0.463536    1.205279    1.715941    3.517265    3.774960    5.601950
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.110 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    4 emc=  -78.0668863466 demc= 1.8937E-03 wnorm= 5.3163E-03 knorm= 7.9782E-03 apxde= 8.6469E-06    *not converged* 

               starting mcscf iteration...   5
 !timer:                                 user+sys=     0.510 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     15931
 number of records written:     6
 !timer: 2-e transformation              user+sys=     0.080 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2022
 Size of the orbital-state Hessian matrix C:            120
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2142

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0668950696     -111.2869555869        0.0000000000        0.0000010000
    2       -77.3954977337     -110.6155582510        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  3.88822774E-06
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999999 pnorm= 1.7495E-05 rznorm= 6.4114E-07 rpnorm= 6.5164E-09 noldr=  4 nnewr=  4 nolds=  1 nnews=  1

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.455836   -2.053692   -1.167018

 qvv(*) eigenvalues. symmetry block  1
     0.408955    1.167284    1.419769    2.017580    3.181813    3.683917    5.015638    5.326029

 qvv(*) eigenvalues. symmetry block  2
     2.268495    3.903676

 fdd(*) eigenvalues. symmetry block  3
    -0.981984

 qvv(*) eigenvalues. symmetry block  3
     0.610271    1.859822    2.445845    3.510649    4.824144    6.771948
 i,qaaresolved 1  0.458188558

 qvv(*) eigenvalues. symmetry block  4
     1.482249    3.608208    4.485809

 qvv(*) eigenvalues. symmetry block  5
     2.591035    4.690448

 fdd(*) eigenvalues. symmetry block  6
   -22.452456   -1.564258

 qvv(*) eigenvalues. symmetry block  6
     0.471288    0.938949    1.552496    1.960217    2.483014    3.708707    4.501954    5.306337    6.149792
 i,qaaresolved 1 -0.725663582

 qvv(*) eigenvalues. symmetry block  7
     1.372937    2.525456    3.966413

 fdd(*) eigenvalues. symmetry block  8
    -1.254376

 qvv(*) eigenvalues. symmetry block  8
     0.463522    1.205261    1.715917    3.517224    3.774945    5.601879
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.100 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    5 emc=  -78.0668950696 demc= 8.7230E-06 wnorm= 3.1106E-05 knorm= 1.3523E-04 apxde= 1.6291E-09    *not converged* 

               starting mcscf iteration...   6
 !timer:                                 user+sys=     0.610 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     15931
 number of records written:     6
 !timer: 2-e transformation              user+sys=     0.080 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2022
 Size of the orbital-state Hessian matrix C:            120
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2142

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0668950712     -111.2869555885        0.0000000000        0.0000010000
    2       -77.3954082289     -110.6154687463        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.010 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  8.01649679E-08
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 6.4209E-07 rpnorm= 1.0039E-09 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.455829   -2.053688   -1.167015

 qvv(*) eigenvalues. symmetry block  1
     0.408956    1.167286    1.419770    2.017582    3.181816    3.683919    5.015641    5.326031

 qvv(*) eigenvalues. symmetry block  2
     2.268497    3.903678

 fdd(*) eigenvalues. symmetry block  3
    -0.981983

 qvv(*) eigenvalues. symmetry block  3
     0.610271    1.859824    2.445847    3.510649    4.824146    6.771950
 i,qaaresolved 1  0.458303849

 qvv(*) eigenvalues. symmetry block  4
     1.482145    3.608205    4.485811

 qvv(*) eigenvalues. symmetry block  5
     2.591037    4.690450

 fdd(*) eigenvalues. symmetry block  6
   -22.452449   -1.564256

 qvv(*) eigenvalues. symmetry block  6
     0.471288    0.938950    1.552498    1.960220    2.483016    3.708709    4.501957    5.306339    6.149794
 i,qaaresolved 1 -0.725664246

 qvv(*) eigenvalues. symmetry block  7
     1.372940    2.525459    3.966414

 fdd(*) eigenvalues. symmetry block  8
    -1.254373

 qvv(*) eigenvalues. symmetry block  8
     0.463523    1.205263    1.715920    3.517225    3.774947    5.601882
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.100 walltime=     0.000

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    6 emc=  -78.0668950712 demc= 1.6293E-09 wnorm= 6.4132E-07 knorm= 3.7312E-08 apxde= 1.1579E-14    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -78.066895071
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,   ag block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1C__1s     0.70774353    -0.02319324    -0.00260083     0.04861335     0.09139187     0.16887057     0.49092971    -0.13493455
   2C__1s     0.00520229     0.60203195    -0.05557212    -0.10186590     0.39081655     0.58364644     2.27092644    -0.47758591
   3C__1s    -0.00481820    -0.15565476     0.04633377    -1.33262844     0.17053841    -0.84461714    -3.57827411     1.26531683
   4C__2p     0.00034358    -0.18199308     0.59927739    -0.20691610    -0.64942727    -0.36081537     0.34747379    -0.02529730
   5C__2p    -0.00059326     0.05973619    -0.13730377    -0.45179072     1.35344988     0.16878669    -0.83718519     0.64495145
   6C__3d     0.00008278     0.00563275    -0.00528583    -0.00142549     0.01490242     0.00800780     0.03488179    -0.00491210
   7C__3d     0.00008269    -0.00101135     0.00609327    -0.00733923    -0.01170945     0.10332178    -0.03005547     0.31801580
   8H__1s    -0.00028563     0.18461164     0.28733042     0.01512875    -0.35562549     0.85923619    -0.05803705    -0.89898557
   9H__1s     0.00116998    -0.08473948    -0.07854669     0.92684561     0.10996637    -0.58325249     0.84828627     0.28127712
  10H__2p     0.00028979    -0.01398918    -0.01343026    -0.00999335    -0.00114071     0.08645157    -0.14559481    -0.00038998
  11H__2p     0.00005415    -0.01105710     0.00211073    -0.00975160     0.03816037     0.02317830    -0.05545850    -0.15854763

               MO    9        MO   10        MO   11
   1C__1s    -0.34452786    -0.25786205     0.31964641
   2C__1s    -1.38876516    -0.93826068     1.57192111
   3C__1s     1.03520771     1.19309744    -0.07289538
   4C__2p     0.34913764     0.45671443     0.88811777
   5C__2p    -0.87358143    -0.04732507    -0.13766932
   6C__3d     0.14329870     0.22975040    -0.03313219
   7C__3d     0.11080480    -0.12202047     0.26847243
   8H__1s     0.04175804     0.01518014    -1.15972595
   9H__1s    -0.00342900    -0.21654729     0.29796521
  10H__2p    -0.18740898     0.35523740     0.65276842
  11H__2p     0.28804792    -0.45183131     0.48710767

          mcscf orbitals of the final iteration,  b1g block   2

               MO   12        MO   13
  12C__3d     0.48344585    -0.57730430
  13H__2p     0.19852230     0.54626464

          mcscf orbitals of the final iteration,  b2g block   3

               MO   14        MO   15        MO   16        MO   17        MO   18        MO   19        MO   20
  14C__2p     0.43517854    -0.29608301    -1.03114713     0.29663810    -0.13052174     1.16009364     0.77818682
  15C__2p    -0.07517028    -1.49563490     2.41902152    -3.12453468     2.15010543     0.06756016     0.32396578
  16C__3d     0.04032471    -0.03626740     0.13451840     0.24453469     0.05226768    -0.26159481     1.98385392
  17H__1s     0.42988517    -0.03969460     0.26544110     1.09667913    -0.88875951    -0.36797814    -1.99587939
  18H__1s    -0.08324484     1.66770969    -1.33209740     0.33874001    -0.17280292    -0.29925398     0.79084192
  19H__2p    -0.01182921    -0.01848801     0.14107045     0.13258139     0.34049677     0.49186355     0.14080195
  20H__2p    -0.00663400    -0.01600109     0.12039217    -0.03735772    -0.35963649     0.38874136     0.76018464

          mcscf orbitals of the final iteration,  b3g block   4

               MO   21        MO   22        MO   23        MO   24
  21C__2p     0.87951658    -0.92233927    -0.28801184    -0.24640580
  22C__2p    -0.02633089     1.74146366    -0.50397609     0.03108994
  23C__3d     0.00589578    -0.07278901     0.40496189     1.09598617
  24H__2p     0.01042422    -0.01941627     0.48235415    -0.36903306

          mcscf orbitals of the final iteration,   au block   5

               MO   25        MO   26
  25C__3d     0.39487806    -0.86474769
  26H__2p     0.31191752     0.52800850

          mcscf orbitals of the final iteration,  b1u block   6

               MO   27        MO   28        MO   29        MO   30        MO   31        MO   32        MO   33        MO   34
  27C__1s     0.70843696    -0.01312866     0.05158361    -0.07130382    -0.00943419     0.14941126    -0.62726756     0.19772289
  28C__1s     0.00661980     0.47012416    -0.16838257    -0.03022607    -0.45972377     0.56885773    -2.71866387     0.96054752
  29C__1s    -0.00810223    -0.10706936    -1.40029214     3.83347156    -0.20656907     0.08926146     7.74363597     0.28164194
  30C__2p     0.00002364     0.24050509    -0.12103918    -0.20955056     0.19836155     0.99359198     0.14469220    -0.49194111
  31C__2p     0.00107962    -0.04101683    -0.31698918    -3.36934267     0.22518148    -2.47514693    -2.31860711    -1.06156208
  32C__3d     0.00013702    -0.00284929     0.00008189     0.01346832     0.02917262     0.08087720     0.07646373     0.02893117
  33C__3d     0.00032282     0.00299633    -0.01203553     0.01718366     0.09008553    -0.04815087    -0.04823957    -0.17236431
  34H__1s    -0.00059580     0.33344647     0.03218090     0.18023229     0.92378523    -0.08724249    -0.42007703     0.47731882
  35H__1s     0.00096704    -0.12994518     1.05313894     0.63183684    -0.92297913     0.94111123    -0.00684294    -0.02830517
  36H__2p     0.00026136    -0.00935873    -0.00462326     0.00331178     0.05903264     0.00042852     0.08261173     0.42721577
  37H__2p     0.00046570    -0.02032265    -0.01770311     0.01774804     0.09382845    -0.13375958     0.08619194    -0.07038014

               MO   35        MO   36        MO   37
  27C__1s    -0.15140334     0.43847161    -0.09556906
  28C__1s    -0.40982956     2.08684751    -0.05926929
  29C__1s     4.48244954    -0.56799500     3.04042879
  30C__2p    -1.32609219    -0.50546301     1.15678265
  31C__2p    -1.69264711     0.57683911    -1.18426456
  32C__3d     0.21533767     0.28087834    -0.15051761
  33C__3d     0.23924399     0.03943252     0.54744561
  34H__1s    -0.41240470    -0.55299684    -1.59025829
  35H__1s     0.52925698    -0.02862373     0.55369915
  36H__2p     0.08001134    -0.11471587     0.69590892
  37H__2p    -0.29221132     0.63175896     0.37991386

          mcscf orbitals of the final iteration,  b2u block   7

               MO   38        MO   39        MO   40        MO   41
  38C__2p     0.59583286    -0.99310762     0.30652967    -0.37891209
  39C__2p     0.00037098     1.08518191    -0.28939879     0.00577200
  40C__3d    -0.02786225     0.11277330     0.47895789    -0.42960260
  41H__2p     0.01295076    -0.01826273     0.19901456     0.54941610

          mcscf orbitals of the final iteration,  b3u block   8

               MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  42C__2p     0.46301396    -0.33824588    -0.08260167     0.89554676     0.04660385    -0.12007100     1.56522478
  43C__2p    -0.09730861    -0.60046853     0.40016288    -1.76485471    -0.57898333     0.26074267    -0.09872125
  44C__3d    -0.00010443     0.00335328    -0.14608105    -0.23364519    -0.34370023    -0.43985052     0.80989859
  45H__1s     0.35551504     0.05642739    -0.88592058    -0.02764919     0.66479923     0.36533206    -1.43411053
  46H__1s    -0.11409453     1.13006206     0.71584096     1.08029222    -0.22134997    -0.46372373     0.28544608
  47H__2p    -0.01078888    -0.01099094    -0.01423050    -0.13837074     0.35974157    -0.08085480     0.67373080
  48H__2p    -0.01105081    -0.00673173    -0.06565853    -0.07078667    -0.20303662     0.44540816     0.44783596

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1C__1s     0.70774353    -0.02319326    -0.00260079     0.04861357     0.09139118     0.16887081     0.49092975    -0.13493439
   2C__1s     0.00520230     0.60203183    -0.05557336    -0.10186483     0.39081394     0.58364726     2.27092659    -0.47758544
   3C__1s    -0.00481820    -0.15565466     0.04633409    -1.33262977     0.17054312    -0.84461660    -3.57827379     1.26531591
   4C__2p     0.00034358    -0.18199185     0.59927776    -0.20691594    -0.64942644    -0.36081730     0.34747316    -0.02529779
   5C__2p    -0.00059326     0.05973591    -0.13730389    -0.45179085     1.35345021     0.16879033    -0.83718416     0.64495197
   6C__3d     0.00008278     0.00563273    -0.00528585    -0.00142550     0.01490233     0.00800779     0.03488180    -0.00491215
   7C__3d     0.00008269    -0.00101134     0.00609328    -0.00733929    -0.01170957     0.10332162    -0.03005545     0.31801562
   8H__1s    -0.00028563     0.18461223     0.28733004     0.01512840    -0.35562824     0.85923556    -0.05803676    -0.89898467
   9H__1s     0.00116998    -0.08473964    -0.07854651     0.92684614     0.10996749    -0.58325231     0.84828574     0.28127666
  10H__2p     0.00028979    -0.01398921    -0.01343023    -0.00999338    -0.00114081     0.08645168    -0.14559496    -0.00038987
  11H__2p     0.00005415    -0.01105710     0.00211075    -0.00975160     0.03816040     0.02317853    -0.05545851    -0.15854822

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

   1C__1s    -0.34452779    -0.25786192     0.31964664
   2C__1s    -1.38876497    -0.93825992     1.57192206
   3C__1s     1.03520790     1.19309736    -0.07289652
   4C__2p     0.34913706     0.45671543     0.88811757
   5C__2p    -0.87358067    -0.04732602    -0.13766943
   6C__3d     0.14329851     0.22975049    -0.03313239
   7C__3d     0.11080519    -0.12202024     0.26847264
   8H__1s     0.04175734     0.01517943    -1.15972632
   9H__1s    -0.00342859    -0.21654702     0.29796565
  10H__2p    -0.18740928     0.35523782     0.65276805
  11H__2p     0.28804812    -0.45183058     0.48710802

          natural orbitals of the final iteration, block  2

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  12C__3d     0.48344542    -0.57730466
  13H__2p     0.19852271     0.54626449

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  14C__2p     0.43517854    -0.29608203    -1.03114672     0.29663802    -0.13052180     1.16009438     0.77818665
  15C__2p    -0.07517028    -1.49563671     2.41902052    -3.12453478     2.15010523     0.06755935     0.32396541
  16C__3d     0.04032471    -0.03626758     0.13451825     0.24453434     0.05226747    -0.26159451     1.98385401
  17H__1s     0.42988517    -0.03969493     0.26544074     1.09667946    -0.88875927    -0.36797880    -1.99587924
  18H__1s    -0.08324484     1.66771075    -1.33209617     0.33873992    -0.17280295    -0.29925330     0.79084206
  19H__2p    -0.01182921    -0.01848807     0.14107073     0.13258141     0.34049674     0.49186350     0.14080191
  20H__2p    -0.00663400    -0.01600116     0.12039238    -0.03735780    -0.35963659     0.38874144     0.76018452

          natural orbitals of the final iteration, block  4

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     0.08476739     0.00000000     0.00000000     0.00000000

  21C__2p     0.87951658    -0.92234381    -0.28800053    -0.24640202
  22C__2p    -0.02633089     1.74145759    -0.50399751     0.03108296
  23C__3d     0.00589578    -0.07277958     0.40496295     1.09598641
  24H__2p     0.01042422    -0.01941183     0.48235433    -0.36903306

          natural orbitals of the final iteration, block  5

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  25C__3d     0.39487754    -0.86474793
  26H__2p     0.31191784     0.52800831

          natural orbitals of the final iteration, block  6

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  27C__1s     0.70843696    -0.01312867     0.05158382    -0.07130395    -0.00943399     0.14941224    -0.62726719     0.19772281
  28C__1s     0.00661981     0.47012416    -0.16838132    -0.03022713    -0.45972324     0.56886091    -2.71866249     0.96054746
  29C__1s    -0.00810223    -0.10706936    -1.40029328     3.83347461    -0.20657761     0.08924809     7.74363648     0.28163994
  30C__2p     0.00002365     0.24050509    -0.12103930    -0.20954896     0.19835915     0.99359125     0.14469275    -0.49194052
  31C__2p     0.00107962    -0.04101683    -0.31698969    -3.36934590     0.22519092    -2.47514061    -2.31861021    -1.06156066
  32C__3d     0.00013702    -0.00284929     0.00008185     0.01346844     0.02917244     0.08087732     0.07646405     0.02893080
  33C__3d     0.00032282     0.00299633    -0.01203557     0.01718366     0.09008551    -0.04815069    -0.04823949    -0.17236415
  34H__1s    -0.00059580     0.33344647     0.03218042     0.18023309     0.92378568    -0.08723982    -0.42007724     0.47731814
  35H__1s     0.00096704    -0.12994518     1.05313975     0.63183652    -0.92298126     0.94110862    -0.00684167    -0.02830503
  36H__2p     0.00026136    -0.00935873    -0.00462327     0.00331186     0.05903280     0.00042864     0.08261197     0.42721608
  37H__2p     0.00046570    -0.02032265    -0.01770315     0.01774803     0.09382872    -0.13375971     0.08619186    -0.07038021

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

  27C__1s    -0.15140365     0.43847167    -0.09556923
  28C__1s    -0.40983101     2.08684837    -0.05927016
  29C__1s     4.48244711    -0.56799003     3.04042762
  30C__2p    -1.32609211    -0.50546477     1.15678347
  31C__2p    -1.69264625     0.57683668    -1.18426429
  32C__3d     0.21533721     0.28087855    -0.15051775
  33C__3d     0.23924402     0.03943298     0.54744564
  34H__1s    -0.41240372    -0.55299772    -1.59025819
  35H__1s     0.52925668    -0.02862280     0.55369921
  36H__2p     0.08001168    -0.11471530     0.69590874
  37H__2p    -0.29221210     0.63175866     0.37991366

          natural orbitals of the final iteration, block  7

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     1.91523261     0.00000000     0.00000000     0.00000000

  38C__2p     0.59583286    -0.99310762     0.30652933    -0.37891237
  39C__2p     0.00037098     1.08518192    -0.28939876     0.00577227
  40C__3d    -0.02786225     0.11277330     0.47895754    -0.42960300
  41H__2p     0.01295076    -0.01826275     0.19901501     0.54941593

          natural orbitals of the final iteration, block  8

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  42C__2p     0.46301396    -0.33824507    -0.08260142     0.89554667     0.04660497    -0.12006982     1.56522507
  43C__2p    -0.09730861    -0.60046980     0.40016393    -1.76485394    -0.57898445     0.26074069    -0.09872165
  44C__3d    -0.00010443     0.00335285    -0.14608067    -0.23364525    -0.34369872    -0.43985127     0.80989888
  45H__1s     0.35551504     0.05642658    -0.88592104    -0.02764937     0.66479746     0.36533294    -1.43411087
  46H__1s    -0.11409453     1.13006367     0.71583984     1.08029146    -0.22134818    -0.46372387     0.28544656
  47H__2p    -0.01078888    -0.01099104    -0.01423044    -0.13837097     0.35974200    -0.08085341     0.67373069
  48H__2p    -0.01105081    -0.00673178    -0.06565862    -0.07078686    -0.20303785     0.44540773     0.44783579
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
         80 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                         ag partial gross atomic populations
   ao class       1 ag       2 ag       3 ag       4 ag       5 ag       6 ag
    C__ s       1.999292   1.495374   0.009158   0.000000   0.000000   0.000000
    C__ p       0.000134   0.169026   1.379068   0.000000   0.000000   0.000000
    C__ d       0.000099   0.013086   0.013241   0.000000   0.000000   0.000000
    H__ s       0.000631   0.296181   0.582503   0.000000   0.000000   0.000000
    H__ p      -0.000157   0.026333   0.016030   0.000000   0.000000   0.000000

   ao class       7 ag       8 ag       9 ag      10 ag      11 ag

                        b1g partial gross atomic populations
   ao class       1b1g       2b1g

                        b2g partial gross atomic populations
   ao class       1b2g       2b2g       3b2g       4b2g       5b2g       6b2g
    C__ p       0.785074   0.000000   0.000000   0.000000   0.000000   0.000000
    C__ d       0.057285   0.000000   0.000000   0.000000   0.000000   0.000000
    H__ s       1.147250   0.000000   0.000000   0.000000   0.000000   0.000000
    H__ p       0.010391   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7b2g

                        b3g partial gross atomic populations
   ao class       1b3g       2b3g       3b3g       4b3g
    C__ p       0.083707   0.000000   0.000000   0.000000
    C__ d       0.000270   0.000000   0.000000   0.000000
    H__ p       0.000790   0.000000   0.000000   0.000000

                         au partial gross atomic populations
   ao class       1 au       2 au

                        b1u partial gross atomic populations
   ao class       1b1u       2b1u       3b1u       4b1u       5b1u       6b1u
    C__ s       1.999881   0.728571   0.000000   0.000000   0.000000   0.000000
    C__ p       0.000424   0.413601   0.000000   0.000000   0.000000   0.000000
    C__ d      -0.000162   0.006174   0.000000   0.000000   0.000000   0.000000
    H__ s       0.000161   0.815488   0.000000   0.000000   0.000000   0.000000
    H__ p      -0.000305   0.036166   0.000000   0.000000   0.000000   0.000000

   ao class       7b1u       8b1u       9b1u      10b1u      11b1u

                        b2u partial gross atomic populations
   ao class       1b2u       2b2u       3b2u       4b2u
    C__ p       1.872939   0.000000   0.000000   0.000000
    C__ d       0.022999   0.000000   0.000000   0.000000
    H__ p       0.019294   0.000000   0.000000   0.000000

                        b3u partial gross atomic populations
   ao class       1b3u       2b3u       3b3u       4b3u       5b3u       6b3u
    C__ p       1.143882   0.000000   0.000000   0.000000   0.000000   0.000000
    C__ d       0.000001   0.000000   0.000000   0.000000   0.000000   0.000000
    H__ s       0.839610   0.000000   0.000000   0.000000   0.000000   0.000000
    H__ p       0.016507   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7b3u


                        gross atomic populations
     ao           C__        H__
      s         6.232276   3.681825
      p         5.847855   0.125050
      d         0.112995   0.000000
    total      12.193125   3.806875


 Total number of electrons:   16.00000000

 !timer: mcscf                           user+sys=     0.720 walltime=     1.000
