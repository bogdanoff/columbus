
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=   8000000 mem1=1108557832 ifirst=-264082542

 drt header information:
  h2o pvdz                                                                       
 nmot  =    24 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            1311
 map(*)=    20 21 23  1  2  3  4  5  6  7  8 22  9 10 11 12 13 14 24 15
            16 17 18 19
 mu(*)=      2  2  2  2  2
 syml(*) =   1  1  2  1  3
 rmo(*)=     1  2  1  3  1

 indx01:    31 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:09 2003
  h2o pvdz                                                                       
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:10 2003
 energy computed by program ciudg.       hochtor2        Thu Jun  5 14:27:11 2003

 lenrec =    4096 lenci =      1311 ninfo =  6 nenrgy =  6 ntitle =  5

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       98% overlap
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -8.521397112908E+01, ietype=    5,   fcore energy of type: Vref(*) 
 energy( 3)= -7.623198236580E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  7.771988098838E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  2.986386840598E-08, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  1.925476925770E-09, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   3997862 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:     113 coefficients were selected.
 workspace: ncsfmx=    1311
 ncsfmx= 1311

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1311 ncsft =    1311 ncsf =     113
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       6 ***
 1.5625E-02 <= |c| < 3.1250E-02      36 ******************
 7.8125E-03 <= |c| < 1.5625E-02      70 ***********************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =      113 total stored =     113

 from the selected csfs,
 min(|csfvec(:)|) = 1.0036E-02    max(|csfvec(:)|) = 9.7502E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1311 ncsft =    1311 ncsf =     113

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 

 internal level =    1    2    3    4    5
 syml(*)        =    1    1    2    1    3
 label          =  a1   a1   b1   a1   b2 
 rmo(*)         =    1    2    1    3    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1  0.97502 0.95067 z*                    33333
      537 -0.04751 0.00226 w           b2 :  2  333330
      691 -0.03495 0.00122 w  a1 :  5  b1 :  3 1233123
      548  0.03451 0.00119 w  a1 :  6  b2 :  2 1233312
      774 -0.03355 0.00113 w           b1 :  3  333033
      587 -0.03288 0.00108 w           a1 :  6  333303
       37 -0.03204 0.00103 x  a1 :  6  b2 :  2 1133322
      773 -0.02698 0.00073 w  b1 :  2  b1 :  3 1233033
      650  0.02697 0.00073 w  b1 :  4  b2 :  2 1233132
       73 -0.02680 0.00072 x  b1 :  4  b2 :  2 1133232
      738 -0.02660 0.00071 w           a1 :  5  333033
      772 -0.02630 0.00069 w           b1 :  2  333033
       71  0.02608 0.00068 x  b1 :  2  b2 :  2 1133232
      802  0.02495 0.00062 w  a1 :  4  b2 :  2 1231332
      775  0.02479 0.00061 w  b1 :  2  b1 :  4 1233033
      107  0.02458 0.00060 x  a1 :  6  b1 :  2 1133223
      648 -0.02455 0.00060 w  b1 :  2  b2 :  2 1233132
      620 -0.02376 0.00056 w           b1 :  3  333303
      115  0.02257 0.00051 x  a1 :  6  b1 :  3 1133223
      683 -0.02175 0.00047 w  a1 :  5  b1 :  2 1233123
      920 -0.02083 0.00043 w  a1 :  4  b1 :  4 1231233
      777 -0.02074 0.00043 w           b1 :  4  333033
      806 -0.02070 0.00043 w  a1 :  8  b2 :  2 1231332
      839  0.02033 0.00041 w  a1 :  4  a1 :  5 1231323
       90 -0.02020 0.00041 x  a1 :  5  a2 :  1 1133232
      913 -0.02015 0.00041 w  a1 :  5  b1 :  3 1231233
      116  0.01998 0.00040 x  a1 :  7  b1 :  3 1133223
      904  0.01969 0.00039 w  a1 :  4  b1 :  2 1231233
      700 -0.01966 0.00039 w  a1 :  6  b1 :  4 1233123
      850  0.01929 0.00037 w  a1 :  6  a1 :  8 1231323
      682  0.01919 0.00037 w  a1 :  4  b1 :  2 1233123
      684  0.01868 0.00035 w  a1 :  6  b1 :  2 1233123
      737  0.01834 0.00034 w  a1 :  4  a1 :  5 1233033
      123 -0.01827 0.00033 x  a1 :  6  b1 :  4 1133223
      840 -0.01812 0.00033 w           a1 :  5  331323
       36  0.01796 0.00032 x  a1 :  5  b2 :  2 1133322
      690  0.01795 0.00032 w  a1 :  4  b1 :  3 1233123
      803 -0.01755 0.00031 w  a1 :  5  b2 :  2 1231332
      924  0.01719 0.00030 w  a1 :  8  b1 :  4 1231233
      619 -0.01698 0.00029 w  b1 :  2  b1 :  3 1233303
      586  0.01688 0.00028 w  a1 :  5  a1 :  6 1233303
      807  0.01647 0.00027 w  a1 :  9  b2 :  2 1231332
      594 -0.01604 0.00026 w  a1 :  6  a1 :  8 1233303
      841 -0.01543 0.00024 w  a1 :  4  a1 :  6 1231323
      583  0.01534 0.00024 w  a1 :  4  a1 :  5 1233303
      163  0.01510 0.00023 x  a1 :  8  b2 :  2 1132332
      159 -0.01502 0.00023 x  a1 :  4  b2 :  2 1132332
      838 -0.01500 0.00023 w           a1 :  4  331323
      649 -0.01495 0.00022 w  b1 :  3  b2 :  2 1233132
       72  0.01484 0.00022 x  b1 :  3  b2 :  2 1133232
       60 -0.01469 0.00022 x  b1 :  3  a2 :  1 1133322
      591 -0.01454 0.00021 w           a1 :  7  333303
      584 -0.01453 0.00021 w           a1 :  5  333303
      783  0.01447 0.00021 w  b1 :  3  b1 :  6 1233033
      799 -0.01439 0.00021 w           a2 :  1  333033
      582 -0.01433 0.00021 w           a1 :  4  333303
       35 -0.01423 0.00020 x  a1 :  4  b2 :  2 1133322
      959  0.01421 0.00020 w  a1 :  4  a1 :  5 1230333
      921  0.01408 0.00020 w  a1 :  5  b1 :  4 1231233
       57 -0.01389 0.00019 x  a1 : 10  b2 :  4 1133322
       78  0.01381 0.00019 x  b1 :  3  b2 :  3 1133232
      805  0.01373 0.00019 w  a1 :  7  b2 :  2 1231332
      915  0.01365 0.00019 w  a1 :  7  b1 :  3 1231233
      731  0.01328 0.00018 w  b2 :  3  a2 :  1 1233123
      547 -0.01327 0.00018 w  a1 :  5  b2 :  2 1233312
      736 -0.01315 0.00017 w           a1 :  4  333033
      960 -0.01309 0.00017 w           a1 :  5  330333
      122  0.01294 0.00017 x  a1 :  5  b1 :  4 1133223
      494 -0.01289 0.00017 w           a1 :  8  333330
      629  0.01268 0.00016 w  b1 :  3  b1 :  6 1233303
      542 -0.01251 0.00016 w           b2 :  4  333330
      907  0.01232 0.00015 w  a1 :  7  b1 :  2 1231233
      695  0.01228 0.00015 w  a1 :  9  b1 :  3 1233123
      139 -0.01225 0.00015 x  a1 :  6  b1 :  6 1133223
      545 -0.01220 0.00015 w           a2 :  2  333330
      846 -0.01216 0.00015 w  a1 :  6  a1 :  7 1231323
      104  0.01212 0.00015 x  a1 : 11  a2 :  2 1133232
      571  0.01208 0.00015 w  b1 :  3  a2 :  1 1233312
      585 -0.01207 0.00015 w  a1 :  4  a1 :  6 1233303
      953  0.01193 0.00014 w  b2 :  3  a2 :  1 1231233
      776  0.01186 0.00014 w  b1 :  3  b1 :  4 1233033
      912  0.01183 0.00014 w  a1 :  4  b1 :  3 1231233
      958 -0.01177 0.00014 w           a1 :  4  330333
      908 -0.01164 0.00014 w  a1 :  8  b1 :  2 1231233
       47 -0.01164 0.00014 x  a1 :  8  b2 :  3 1133322
      119  0.01149 0.00013 x  a1 : 10  b1 :  3 1133223
      609 -0.01138 0.00013 w           a1 : 10  333303
       93  0.01135 0.00013 x  a1 :  8  a2 :  1 1133232
      905 -0.01133 0.00013 w  a1 :  5  b1 :  2 1231233
      699  0.01131 0.00013 w  a1 :  5  b1 :  4 1233123
       46 -0.01124 0.00013 x  a1 :  7  b2 :  3 1133322
      652  0.01123 0.00013 w  b1 :  6  b2 :  2 1233132
      102  0.01120 0.00013 x  a1 :  9  a2 :  2 1133232
      832 -0.01107 0.00012 w  b1 :  2  a2 :  2 1231332
       94  0.01094 0.00012 x  a1 :  9  a2 :  1 1133232
      544 -0.01085 0.00012 w  a2 :  1  a2 :  2 1233330
      196  0.01080 0.00012 x  a1 :  4  a1 :  6 1132323
      242 -0.01076 0.00012 x  a1 :  4  b1 :  2 1132233
      655 -0.01076 0.00012 w  b1 :  3  b2 :  3 1233132
      819  0.01073 0.00012 w  a1 :  5  b2 :  4 1231332
      496  0.01065 0.00011 w  a1 :  5  a1 :  9 1233330
       55 -0.01050 0.00011 x  a1 :  8  b2 :  4 1133322
      480 -0.01048 0.00011 w           a1 :  4  333330
       54 -0.01043 0.00011 x  a1 :  7  b2 :  4 1133322
      644 -0.01040 0.00011 w           b2 :  4  333303
      258  0.01031 0.00011 x  a1 :  4  b1 :  4 1132233
      692  0.01030 0.00011 w  a1 :  6  b1 :  3 1233123
       98 -0.01019 0.00010 x  a1 :  5  a2 :  2 1133232
      919 -0.01015 0.00010 w  a1 : 11  b1 :  3 1231233
      786 -0.01012 0.00010 w           b1 :  6  333033
      577  0.01011 0.00010 w  b1 :  3  a2 :  2 1233312
      513 -0.01004 0.00010 w  a1 :  9  a1 : 11 1233330
      481  0.01004 0.00010 w  a1 :  4  a1 :  5 1233330
          113 csfs were printed in this range.
