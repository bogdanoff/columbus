#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/



sub readcmdcin{

   open CMDCIN,"<cmdcin" || die "cannot find cmdcin\n";
   $/=""; 
   $flags=<CMDCIN>;
   close CMDCIN;
   $flags=~s/\n/ /g;
   $flags=~s/^.*keywords*//;
   $flags=~s/flags.*$//;
   $flags=~s/\///;
   $flags=~s/\'/ /g;
   @flags=split(/\s+/,$flags);
#  print "FLAGS: ", join(':',@flags), "\n";
   $/="\n";
#  $keystring=join(' -D_',@flags);
   $keystring=join(' -D',@flags);
   return;
}


#
# Step 1 
# read all cmdc keywords
#
#  $COLUMBUS/cmdc.pl $MACHINE_ID $MODE *.f || exit 55
#  
  $machineid=shift @ARGV;
  $mode=shift @ARGV;
  $cbus=$ENV{"COLUMBUS"};
# figure out machine configuration file name
# and read blasconversion and keystring
  $machinecfg="$cbus/machine.cfg/$machineid";
  print "machinecfg=$machinecfg\n";
  $/="\n";
  open FIN,"<$machinecfg";
  while (<FIN>)  { if (/BLASCONVERSION/) { $blasconversion=$_;}
                   if (/^KEYWORDS/) {$keywords=$_;}
                   if (/^PARALLELKEYS/) {$parallelkeys=$_;}
                   if (/^MOLCASKEYS/) {$molcaskeys=$_;}
                 }
 close FIN;
 chop($blasconversion);
 $blasconversion=~s/^.*=//;
 chop($keywords);
 chop($parallelkeys);
 chop($molcaskeys);
 $keywords=lc($keywords);
 $keywords=~s/^.*=//;
 $keywords=$keywords . ":largefiles:spinorbit";
 $parallelkeys=lc($parallelkeys);
 $parallelkeys=~s/^.*=//;
 $molcaskeys=lc($molcaskeys);
 $molcaskeys=~s/^.*=//;
#
#  depending upon mode add some flags 
#
$addflags{normal}=":";
$addflags{parallel}="$parallelkeys";
$addflags{parallelmpi}="$parallelkeys";
$addflags{direct}="big:direct";
$addflags{molcas}="$molcaskeys";

 if (! $addflags{$mode} ) {die("invalid mode<> normal|parallel|parallelmpi|direct|molcas");}
  $keywords=$keywords.':'.$addflags{$mode};
  $keywords=~s/::/:/;
  $keywords=~s/:$//;
  @flags=split(/:/,$keywords);
  $keystring=join(' -D',@flags);
  $keystring= "-D$keystring";
  print "keystring=$keystring\n";




  foreach $i (@ARGV)
   { 
     open FIN,"<$i" || die "cannot open $i\n";
     $/="\n";
     @file=<FIN>;
     close FIN;
#
#  Step 2
#  replace all /^![ \D]//  in fortran sources
#
#  (i.e. all lines with leading @)
     for ($j=0; $j<=$#file; $j++) 
       {  
# cleanup first (multiple *)
          $file[$j]=~s/^\*+/*/;
# remove leading * in all lines
          $file[$j]=~s/^\*(.)/\1/; 
# all porting keywords to lower case 
          if (grep(/^\@/,$file[$j])) {$file[$j]=lc($file[$j]);} }
     open FOUT,">$i.$$";
      print FOUT  @file;
     close FOUT;
#    system(" ls -l ${i}* ");
#
#  Step 3 
#  run cmdc_subst.pl 
# 
   print "porting file $i ( $keystring ) \n";
 #  print "$cbus/cmdc_subst.pl $keystring -b -kc @ -lc 'none' $i.$$ > $i.$$.fpp \n";
###  -lc 'none' takes none as the line continuation string, i.e. implicit none
#                                                                integer   
#                                                          becomes  implicit integer
#   print " $cbus/cmdc_subst.pl  $keystring -b -kc @ -lc 'none' $i.$$ > $i.$$.fpp \n ";
#   if ( "$i" eq "molcaswrapper.f") {system ("cat $i.$$");}
     system(" $cbus/cmdc_subst.pl  $keystring   -b -kc @   $i.$$ > $i.$$.fpp ");

#
#  Step 4
#  apply $blasconversion
#
    system("$cbus/$blasconversion $i.$$.fpp $i ");
    system("rm -f $i.$$.fpp $i.$$");
    
    } 
