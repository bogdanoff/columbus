#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

 use File::Copy; 
 use Shell;
 use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;
 use colib_perl;

$doscf=0;
$doseward=1;
$dolocal=1;
$dorasscf=1;
$coremem=100000000;
while ($#ARGV>-1) { $_=shift @ARGV;  
                     if (/-scf/) {$doscf=1;next;} 
                     if (/-nolocal/) {$dolocal=0;next;} 
                     if (/-norasscf/) {$dorasscf=0;next;} 
                     if (/-m/) {$coremem =shift @ARGV; ;next;} 
                     if (/-noseward/) {$doseward=0;next;} }

if ($doscf) { print "DOSCF ON\n";} else { print "DOSCF OFF\n";} 
if ($doseward) { print "DOSEWARD ON\n";} else { print "DOSEWARD OFF\n";}
if ($dorasscf) { print "DORASSCF ON\n";} else { print "DORASSCF OFF\n";}
if ($dolocal) { print "LOCALISATION ON\n";} else { print "LOCALISATION OFF\n";}

$mpirun="/software/mpich.pgf90/bin/mpirun";
$ENV{"MOLCASMEM"}=200;
#$molcas=$ENV{"MOLCAS"};
$COLUMBUS=$ENV{"COLUMBUS"};
$ENV{"Project"}="molcas";
$tmp=pwd(); chop $tmp;
$ENV{"WorkDir"}=$tmp;
#$ENV{"ThisDir"}=pwd();

print "Project=",$ENV{"Project"},"\n";
print "WorkDir=",$ENV{"WorkDir"},"\n";

## copy input files

### copy("../geom","geom");
copy("../infofl","infofl");
copy("../molcas.input","molcas.input");
copy("../scfin","scfin");
copy("../cidrtin","cidrtin");
copy("../ciudgin","ciudgin");



# takes geometry from geom and adds it to molcas.input.seward
# generated from molcas.input 

open(ROTMAX,">rotmax") || die "rotmax";
printf ROTMAX " 1  0  0 \n 0  1  0 \n 0  0  1\n";
close(ROTMAX);
unlink("bummer");
system( "$COLUMBUS/unik.gets.x > unikls" );

# extract seward input from molcas.input
$sewardinput=extractmolcas("molcas.input","SEWARD");
@sewardlines=split(/:/,$sewardinput);
$/="\n";
open GEOMUNIK,"<geom.unik";
@geomunik=<GEOMUNIK>;
close GEOMUNIK;
$iatom=1;
$i=0;
while ($i<$#sewardlines)
   { if (grep(/Basis set/,$sewardlines[$i]))
          { $i++; $i++;
           while (1) 
           { if (grep (/End of Basis/i,$sewardlines[$i])) {last;}
            if (grep (/charge/i,$sewardlines[$i])) { $i++;$i++;}
            $symbol=$sewardlines[$i];
            $symbol=~m/^(\S+) /;
            $symbol=$1;
            $iatom++;
            #if ($iatom >=$#geomunik) {die("inconsistency in newgeom/seward\n");}
            ($scr,$oz,$x,$y,$z)=split(/\s+/,$geomunik[$iatom]);
            $sewardlines[$i]=sprintf("%-6s  %9.5f   %9.5f   %9.5f",$symbol,$x,$y,$z); 
            $i++;
           }
          }
      $i++; 
    }

 open FOUT,">molcas.input.seward";
 $sewardinput=join(':',@sewardlines); $sewardinput=~s/:/\n/g;
 print FOUT $sewardinput,"\n";
 close FOUT;


# run  seward 
  if ($doseward) 
  { $err=system("molcas molcas.input.seward > molcas.output.seward"); 
    if ($err) { die( "seward failed  err=$err\n"); }
    if ( ! -e ONEINT ) { system("ln -s molcas.OneInt ONEINT"); }
    if ( ! -e ORDINT ) {system("ln -s molcas.OrdInt ORDINT"); }
    if ( ! -e RUNFILE ) {system("ln -s molcas.RunFile RUNFILE"); }
     copy ("molcas.output.seward","RESULTS/molcas.output.seward");
  }
  if ( $doscf ) { system("$COLUMBUS/scfpq.x "); 
                  copy("mocoef_lumorb","INPORB"); 
                  copy("scfls","RESULTS/scfls");
                  copy ("mocoef_lumorb","RESULTS/mocoef_scf_lumorb");}
  else  { copy("molcas.RasOrb","INPORB");}
  if ($dorasscf) { # extract rasscf input from molcas.input
                    $rasscfinput=extractmolcas("molcas.input","RASSCF");
                    @raslines=split(/:/,$rasscfinput);
                    open FOUT,">molcas.input.rasscf";
                    $rasscfinput=join(':',@raslines); $rasscfinput=~s/:/\n/g;
                    print FOUT $rasscfinput;
                    close FOUT;
                    $err=system("molcas molcas.input.rasscf > molcas.output.rasscf "); 
                    if ($err) { die( "rasscf failed  err=$err\n"); }
                    copy ("molcas.output.rasscf","RESULTS/molcas.output.rasscf");
                  }

copy("molcas.RasOrb","INPORB");
copy("molcas.RasOrb","RESULTS/molcas.RasOrb");

if ($dolocal) {
# first localisation   step:  freeze frozen+doc ; cp molcas.LocOrb  LOCORB1
# frozen: 5+5 orbitals
# docc  : 4+4 orbitals
# active: 6+6 orbitals
  open FOUT,">molcas.input.localisation1";
  print FOUT " &LOCALISATION &END\nBOYS\nNFROZEN\n 18 \nITERATIONS\n 500\nEnd of Input\n";
  close FOUT;
 
  system("molcas molcas.input.localisation1 > molcas.output.localisation1");
  if ($err) { die ("localisation1 failed \n"); }
  copy("molcas.LocOrb","LOCORB1");
  copy("molcas.LocOrb","RESULTS/LOCORB1");
  copy("molcas.output.localisation1","RESULTS/molcas.output.localisation1");

# second localisation  step:  freeze frozen ; localise only doc;  cp molcas.LocOrb  LOCORB2
# frozen: 5+5
# docc :4+4

  open FOUT,">molcas.input.localisation2";
  print FOUT " &LOCALISATION &END\nBOYS\nNFROZEN\n 10 \nNORBITALS\n 8 \nITERATIONS\n 500\nEnd of Input\n";
  close FOUT;
 
  $err=system("molcas molcas.input.localisation2 > molcas.output.localisation2"); 
  if ($err) { die ("localisation2 failed \n"); }
  copy("molcas.LocOrb","LOCORB2");
  copy("molcas.LocOrb","RESULTS/LOCORB2");
  copy("molcas.output.localisation2","RESULTS/molcas.output.localisation2");
# extract frozen and docc orbitals from LOCORB2


  $/="\n";
  open FIN,"<LOCORB2";
  @locorb2=<FIN>;
  close FIN;
  for ($i=0;$i<$#locorb2;$i++) { chop $locorb2[$i];}
  print "$#locorb2 lines read from LOCORB2 \n";
  $locorb2=join(':',@locorb2);
  $locorbtmp=$locorb2;
  $locorbtmp=~s/:\#ORB.*$/:#ORB/;
  $locorbtmp=~s/:/\n/g;
  $locorb2=~s/^.*\#ORB://i;
  $lococc=$locorb2;
  $lococc=~s/^.*\#OCC://;
  $lococc=~s/:\#.*$//;
  $lococc="\#OCC:" . $lococc;
  $lococc=~s/:/\n/g;
  print $lococc;
  $locorb2=~s/:\#OCC:.*$//i;
  $locorb2=~s/^\* ORBITAL//;
  @orbitals = split(/\* ORBITAL/,$locorb2);
  @outorbitals=();
  @atomorder=();
  print "found $#orbitals orbitals in LOCORB2 \n";

  open FOUT,">mocoef_localised";
  print FOUT "$locorbtmp";
  for ($i=0; $i<18; $i++) { $coef=$orbitals[$i];
                            @coef=split(/:/,$coef);
                            shift @coef;
                            $orbitals[$i]=join(':',@coef); 
                            $orbitals[$i]=~s/:/\n/g; $outorbitals[$i]=$orbitals[$i];
                           # analysiere Zuordnung Cr1 oder Cr2
                           $cr1=0; $cr2=0;
                           for ($j=1; $j<=$#coef; $j++) { $coef[$j]=~s/://; ($f1,$f2,$f3,$f4)=unpack "A18A18A18A18",$coef[$j];
                                                         # print "coef($j)=$coef[$j] ... $f1,$f2,$f3,$f4\n";
                                                         if ($j < $#orbitals/8)  { $cr1+=abs($f1)+abs($f2)+abs($f3)+abs($f4);} else {$cr2+=abs($f1)+abs($f2)+abs($f3)+abs($f4);}}
                            if ( abs($cr1-$cr2)<0.000001) {print "orbital $i unresolved  $cr1  $cr2\n"; $atomorder[$i]="U";}
                            elsif ($cr1 > $cr2)  {print "orbital $i localized on atom A $cr1 $cr2  \n"; $atomorder[$i]="A";}
                            else   {print "orbital $i localized on atom B $cr1 $cr2 \n"; $atomorder[$i]="B";}
                          }

# extract active and virtual orbitals from LOCORB1

  $/="\n";
  open FIN,"<LOCORB1";
  @locorb2=<FIN>;
  close FIN;
  for ($i=0;$i<$#locorb2;$i++) { chop $locorb2[$i];}
  print "$#locorb2 lines read from LOCORB2 \n";
  $locorb2=join(':',@locorb2);
  $locorb2=~s/^.*\#ORB://i;
  $locorb2=~s/:\#OCC:.*$//i;
  $locorb2=~s/^\* ORBITAL//;
  @orbitals = split(/\* ORBITAL/,$locorb2);
  print "found $#orbitals orbitals in LOCORB1 \n";

  for ($i=18; $i<=$#orbitals; $i++)
                          { $coef=$orbitals[$i];
                            @coef=split(/:/,$coef);
                            shift @coef;
                            $orbitals[$i]=join(':',@coef); 
                            $orbitals[$i]=~s/:/\n/g; $outorbitals[$i]=$orbitals[$i];
                           # analysiere Zuordnung Cr1 oder Cr2
                           if ($i>18+11) {$atomorder[$i]="U";next;}
                           $cr1=0; $cr2=0;
                           for ($j=0; $j<=$#coef; $j++) { $coef[$j]=~s/://; ($f1,$f2,$f3,$f4)=unpack "A18A18A18A18",$coef[$j];
                                                         # print "coef($j)=$coef[$j] ... $f1,$f2,$f3,$f4\n";
                                                         if ($j < $#orbitals/8)  { $cr1+=abs($f1)+abs($f2)+abs($f3)+abs($f4);} else {$cr2+=abs($f1)+abs($f2)+abs($f3)+abs($f4);}}
                            if ( abs($cr1-$cr2)<0.000001) {print "orbital $i unresolved $cr1 $cr2 \n"; $atomorder[$i]="U";}
                            elsif ($cr1 > $cr2)  {print "orbital $i localized on atom A $cr1 $cr2  \n";$atomorder[$i]="A";}
                            else   {print "orbital $i localized on atom B $cr1 $cr2 \n";$atomorder[$i]="B";}
                          }

  # place order  frozen : docc(A) : docc(B) : act(A):act(B): virtual

   $cntdocca=0;
   $cntdoccb=0;
   $cntacta=0;
   $cntactb=0;
  
   for ($i=0; $i<10 ; $i++) {#print "writing frozen core orbital $i \n"; 
                             print FOUT "* ORBITAL"; printf FOUT "%5d%5d\n",1,$i+1; print FOUT $outorbitals[$i],"\n"; }
   for ($i=10; $i<18 ; $i++) { if ($atomorder[$i] eq "A") { $cntdocca++; print "writing docc orbital A $i \n"; print FOUT "* ORBITAL"; printf FOUT "%5d%5d\n",1,10+$cntdocca ;
                                                            print FOUT $outorbitals[$i],"\n";  }}
   for ($i=10; $i<18 ; $i++) { if ($atomorder[$i] eq "B") { $cntdoccb++;print "writing docc orbital B $i \n"; print FOUT "* ORBITAL"; printf FOUT "%5d%5d\n",1,10+$cntdocca+$cntdoccb;
                                                            print FOUT $outorbitals[$i], "\n"; }}
   for ($i=18; $i<30 ; $i++) { if ($atomorder[$i] eq "A") { $cntacta++; print "writing active orbital A $i \n"; print FOUT "* ORBITAL"; printf FOUT "%5d%5d\n",1,18+$cntacta;
                                                            print FOUT $outorbitals[$i],"\n";  }}
   for ($i=18; $i<30 ; $i++) { if ($atomorder[$i] eq "B") { $cntactb++;print "writing active orbital B $i \n"; print FOUT "* ORBITAL"; printf FOUT "%5d%5d\n",1,18+$cntacta+$cntactb;
                                                            print FOUT $outorbitals[$i],"\n"; }}
   for ($i=30; $i<=$#orbitals; $i++) { { #print "writing virutal orbital $i \n"; 
                                         print FOUT "* ORBITAL"; printf FOUT "%5d%5d\n",1,$i+1; print FOUT $outorbitals[$i]; 
                                         if ($i<$#orbitals) {print FOUT "\n";} }}

   print "wrote 10 frozen core \n ...  $cntdocca docc (a)  + $cntdoccb docc(b) orbitals \n ...  $cntacta active (a) + $cntactb active (b) orbitals \n", $#orbitals-29," virtual orbs\n";


  print FOUT $lococc;
  close FOUT;
} # endif dolocal
  copy("mocoef_localised","mocoef_lumorb");
  copy("mocoef_localised","RESULTS/mocoef_localised");

# run tran with option lumorb=1
  unlink("bummer");
  system("$COLUMBUS/cidrt.x < cidrtin > cidrtls -m $coremem ");
  checkbummer("cidrt");
  copy("cidrtls","RESULTS/cidrtls");
  open FOUT,">tranin";
  print FOUT " &input \n ldamax=8096,\n seward=1\n lumorb=1\n &end\n";
  close FOUT;
  system("$COLUMBUS/tran.x -m $coremem");
  copy("tranls","RESULTS/tranls");
  checkbummer("tran");
# call cisrt
  open FOUT,">cisrtin";
  print FOUT " &input \n maxbl3=262000\n maxbl4=262000 \n maxbuf=150000 \n ldamax=32167 \n &end\n";
  close FOUT;
  system("$COLUMBUS/cisrt.x -m $coremem");
  checkbummer("cisrt");
  copy("cisrtls","RESULTS/cisrtls");
# call pciudg
  &changekeyword("ciudgin","ciudgin"," MOLCAS",1);
  system("$mpirun -np 2 $COLUMBUS/pciudg.x -m $coremem ");
  copy("ciudgls","RESULTS/ciudgls");
  copy("ciudgsm","RESULTS/ciudgsm");

 exit;

 sub checkbummer
 { ($what) = @_;
   if (! -s "bummer") { die (" $what failed1\n");}
   open BUMMER,"<bummer";
   $_=<BUMMER>;
   if (! /normal/){die("$what failed\n");}
   unlink("bummer");
   return;
 }
