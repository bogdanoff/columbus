#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;
use colib_perl;
#
    $scriptversion="Version 0.7, 22_May_2002 (md)";
#
#=======================================================================
#
#     PROGRAM: curve.pl, 
#     author: Michal Dallos, University Vienna, Austria
#
#     This script reads out the calculated energies and c**2 values for
#      MCSCF, CI, AQCC, AQCC-LPR calculations performed in the 
#      according to the informations in the displfl 
#
#=======================================================================
#   
   print "\n   ** curve.pl:      $scriptversion **\n\n"; 
   print "\n   ***    Potencial curve analyzes    ***\n\n";
#
   print "  usage: set up \'curvein\' in the following way:\n";
   print "       method   nstate   DRT csf comment\n";
#
#
# check the existence of displfl
   if (! -s "DISPLACEMENT/displfl") {die "DISPLACEMENT/displfl do not exists!!!\n\n"; }
#
# change to check if the units are au or Ang
   open(DISPLFL,"DISPLACEMENT/displfl") or  die "DISPLACEMENT/displfl do not exists!!!\n\n";
   $line=<DISPLFL>; $line=<DISPLFL>; $line=<DISPLFL>; #  skip the first 3 lines
   $line=<DISPLFL>;
   ($coord1,$displ1)=split(/\s+/,$line,3);
   if ( -e "DISPLACEMENT/CALC.c${coord1}.d${displ1}au") {$unit=au; }
   elsif ( -e "DISPLACEMENT/CALC.c${coord1}.d${displ1}Ang") {$unit=Ang; }
   else {die "Could not recognize the units \n"; }

#   $unit="au";
   print "\n";
   print " \*\*\*Units recognized as $unit\*\*\*\n";
   close(DISPLFL);

#  read the input file
   open (INP,"curvein") or die "curvein do not exists!!!\n\n";
   
   $i=1;
   $geomopt=0;
     print "\n\n Input echo:\n";
     while(<INP>)
      {
       chomp;
       s/^ *//;  
       ($method[$i],$nstate[$i],$drt[$i],$csf[$i],$comment[$i])=split(/\s+/,$_,5);
         if (!defined $drt[$i]){$drt[$i]="";}
         if (!defined $csf[$i]){$csf[$i]="";}
         if (!defined $comment[$i]){$comment[$i]="";}
        if ($method[$i] eq "mcscf" || $method[$i] eq "mcscfopt")
         {
	  print " --> read out $nstate[$i] MCSCF energies\n";
	  if ($method[$i] eq "mcscfopt"){$geomopt=1}
	 }
        if ($method[$i] eq "ci" || $method[$i] eq "ciopt")
         {
	  $drt[$i]=1;
	  print " --> read out $nstate[$i] CI energies\n";
	  if ($method[$i] eq "ciopt"){$geomopt=1}
	 }
        if ($method[$i] eq "msci" || $method[$i] eq "msciopt")
         {
	  print " --> read out $nstate[$i] MSCI energies\n";
	  if ($method[$i] eq "msciopt"){$geomopt=1}
	 }
        if ($method[$i] eq "mccsf" || $method[$i] eq "mccsfopt")
	 {
	  print" --> c**2 values from MCSCF\n";
          print"      DRT: $drt[$i], Nstate: $nstate[$i], CSF: $csf[$i] $comment[$i]\n";
           if ($method[$i] eq "mccsfopt"){$geomopt=1}
	 }
        if ($method[$i] eq "cicsf" || $method[$i] eq "cicsfopt")
	 {
	  print" --> c**2 values from CI\n";
          print"      DRT: $drt[$i], Nstate: $nstate[$i], CSF: $csf[$i] $comment[$i]\n";
           if ( $drt[$i] > 1){die "DRT=$drt[$i] not supported for CI\n";}
	  if ($method[$i] eq "cicsfopt"){$geomopt=1}
	 }
        if ($method[$i] eq "aqcccsf")
         {
          print" --> c**2 values from AQCC\n";
          print"      DRT: $drt[$i], Nstate: $nstate[$i], CSF: $csf[$i] $comment[$i]\n";
           if ( $drt[$i] > 1){die "DRT=$drt[$i] not supported for AQCC\n";}
         }  
        if ( $nstate[$i] < 0){die "Nstate=$nstate[$i] makes no sense!!!\n";}
       $i++;
      } # end of: while(<INP>)
   close INP;
   $nanal=$i-1;
#
##  read the dispcalcfl file
#  
   print "\n----------------------------------------------------------\n";
   print " ***  Distances:  ***\n";
   open(DISPLFL,"DISPLACEMENT/displfl") or  die "DISPLACEMENT/displfl do not exists!!!\n\n";
   $line=<DISPLFL>; $line=<DISPLFL>; $line=<DISPLFL>; #  skip the first 3 lines
     $i=1;
     while(<DISPLFL>){
     ($coord[$i],$displ[$i])=split(/\s+/,$_,3);
     print "$displ[$i]\n";
     $i++;
     } # end of: while(<DISPLFL>)
     close(DISPLFL); 
     $ndisp=$i-1;
#
#----------------------------------------------------------
#
   for ($i=1; $i<=$nanal; $i++)
    {
     $csf[$i]=" ".$csf[$i];
     if ($method[$i] eq "mcscf" || $method[$i] eq "mcscfopt"){&mcscf($nstate[$i],$geomopt);}
     if ($method[$i] eq "mccsf" || $method[$i] eq "mccsfopt"){&mccsf($nstate[$i],$csf[$i],$drt[$i],$comment[$i],$geomopt);}
     if ($method[$i] eq "ci" || $method[$i] eq "ciopt"){&ci($nstate[$i],$geomopt);}
     if ($method[$i] eq "msci"){&msci($nstate[$i],$drt[$i]);}
     if ($method[$i] eq "aqcc"){&aqcc($nstate[$i]);}
     if ($method[$i] eq "cicsf" || $method[$i] eq "cicsfopt"){&cicsf($method[$i],$nstate[$i],$csf[$i],$comment[$i],$geomopt);}
     if ($method[$i] eq "aqcccsf"){&cicsf($method[$i],$nstate[$i],$csf[$i],$comment[$i]);}
     if ($method[$i] eq "mrpt"){&mrpt($nstate[$i]);}
    } # end of: for ($i=1; $i<=$nanal; $i++)
#
###########################################################
#
#   SUBROUTINE SECTION
#
#----------------------------------------------------
#
   sub mcscf{
   my ($nstate,$geomopt)=@_;
   my ($i,$j,$energy);
#
     $end='sp';
     if ($geomopt){$end='all'}
       print "==========================================================\n";
       print " ***  MCSCF convergence informations\n";
       print "   disp.      converg.   # of iter\n";
     for ($i=1; $i<=$ndisp; $i++)
      {
       print "   $displ[$i]\t\t";
       $file="DISPLACEMENT/CALC.c$coord[$i].d$displ[$i]$unit/LISTINGS/mcscfsm.$end";
       #$file="DISPLACEMENT/CALC.c$coord[$i].d$displ[$i]/LISTINGS/mcscfsm.$end";
       ($nconv,$niter)=&mcscfconv($file);
        if ($nconv)
         {print "NOT          $niter\n"; }
        else
         {print "yes          $niter\n"; }
      }
#
     for ($i=1; $i<=$nstate; $i++)
      {
       print "----------------------------------------------------------\n";
       print " ***  MCSCF ENERGY: State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
	  {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           &getenergy("$dir/LISTINGS/mcscfls.$end","DRT #",$i,10,1.0,'10.6f');
          } # end of: for ($j=1,$j<=$ndisp,$j++)
       } # end of: for ($i=1; $i<=$nstate; $i++)
#
   } # end of: sub mcscf
#
#-----------------------------------------------------------------
#
   sub aqcc
{
   my $i;
   ($nstate)=@_;
#
       print "==========================================================\n";
       print " ***  AQCC convergence informations\n";
       print "   disp.      converg.\n";
     for ($i=1; $i<=$ndisp; $i++)
      {
       print "   $displ[$i]\t\t";
       $file="DISPLACEMENT/CALC.c$coord[$i].d$displ[$i]$unit/LISTINGS/ciudgsm.sp";
        ($nconv,$niter)=&ciconv($file);
        if ($nconv)
         {print "NOT          $niter\n"; }
        else
         {print "yes          $niter\n"; }
      } # end of: for ($i=1; $i<=$ndisp; $i++)
#
     for ($i=1; $i<=$nstate; $i++)
      {
       print "----------------------------------------------------------\n";
       print " ***  AQCC ENERGY:  State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           &getenergy("$dir/LISTINGS/ciudgls.drt1.sp ","at position",$i,7,1.0,'10.6f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
      } # end of: for ($i=1; $i<=$nstate; $i++)
#
}  # end of: sub aqcc
#
#-----------------------------------------------------------------
#
   sub msci
{
   my $i;
   ($nstate,$drt)=@_;
#
       print "==========================================================\n";
       print " ***  CI convergence informations for DRT=$drt\n";
       print "   disp.      converg.\n";
     for ($i=1; $i<=$ndisp; $i++)
      {
       print "   $displ[$i]\t\t";
       $file="DISPLACEMENT/CALC.c$coord[$i].d$displ[$i]$unit/LISTINGS/ciudgsm.drt$drt.sp";
        ($nconv,$niter)=&ciconv($file);
        if ($nconv)
         {print "NOT          $niter\n"; }
        else
         {print "yes          $niter\n"; }
      }
#
     for ($i=1; $i<=$nstate; $i++)
      {
       print "----------------------------------------------------------\n";
       print " ***  CI ENERGY:  DRT:$drt State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           &getenergy("$dir/LISTINGS/ciudgls.drt$drt.sp ","eci       =",$i,3,1.0,'10.6f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
       print "----------------------------------------------------------\n";
       print " ***  Davidson correction ENERGY: DRT:$drt State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           &getenergy("$dir/LISTINGS/ciudgls.drt$drt.sp ","dv1 = ",$i,8,27.21161,'10.6f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
       print "----------------------------------------------------------\n";
       print " ***  CI+Q ENERGY:  DRT:$drt State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           &getenergy("$dir/LISTINGS/ciudgls.drt$drt.sp","dv1   =",$i,3,1.0,'10.6f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
     print "----------------------------------------------------------\n";
       print " ***  Relaxed weight of Reference:  DRT:$drt State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           $c2=&getenergy("$dir/LISTINGS/ciudgls.drt$drt.sp","relaxed",$i,7,100,'5.2f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
   } # end of: for ($i=1; $i<=$nstate; $i++)
#
}  # end of: sub ci
#
#-----------------------------------------------------------------
#
   sub ci
{
   my $i;
   my ($nstate, $geomopt)=@_;
#  
     $end='sp';
     if ($geomopt){$end='all'}
#
       print "==========================================================\n";
       print " ***  CI convergence informations\n";
       print "   disp.      converg.\n";
     for ($i=1; $i<=$ndisp; $i++)
      {
       print "   $displ[$i]\t\t";
       $file="DISPLACEMENT/CALC.c$coord[$i].d$displ[$i]$unit/LISTINGS/ciudgsm.$end";
        ($nconv,$niter)=&ciconv($file);
        if ($nconv)
         {print "NOT          $niter\n"; }
        else
         {print "yes          $niter\n"; }
      }                    
#
     for ($i=1; $i<=$nstate; $i++)
      {
       print "----------------------------------------------------------\n";
       print " ***  CI ENERGY:  State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit"; 
           &getenergy("$dir/LISTINGS/ciudgls.drt1.$end ","eci       =",$i,3,1.0,'10.6f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
       print "----------------------------------------------------------\n";
       print " ***  Davidson correction ENERGY:  State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           &getenergy("$dir/LISTINGS/ciudgls.drt1.$end ","dv1 = ",$i,8,27.21161,'10.6f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
       print "----------------------------------------------------------\n";
       print " ***  CI+Q ENERGY:  State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit"; 
           &getenergy("$dir/LISTINGS/ciudgls.drt1.$end","dv1   =",$i,3,1.0,'10.6f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
     print "----------------------------------------------------------\n";
       print " ***  Relaxed weight of Reference:  State Nr $i  ***\n";
         for ($j=1;$j<=$ndisp;$j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           $c2=&getenergy("$dir/LISTINGS/ciudgls.drt1.$end","relaxed",$i,7,100,'5.2f');
          } # end of: for ($j=1;$j<=$ndisp;$j++)
   } # end of: for ($i=1; $i<=$nstate; $i++)  
#
}  # end of: sub ci
#
#-----------------------------------------------------------------
#
   sub cicsf
{
   my $i;
   my ($method,$nstate,$csf,$comment,$geomtop)=@_;
#
     $end='sp';
     if ($geomopt){$end='all'}
#
     for ($i=1; $i<=$nstate; $i++)
      {
       print "----------------------------------------------------------\n";
        if ($method eq "cicsf")
         {print " ***  CI c**2: State Nr $i, CSF: $csf, type: $comment  ***\n";}
        if ($method eq "aqcccsf")
         {print " ***  AQCC c**2: State Nr $i, CSF: $csf, type: $comment  ***\n";}
         for ($j=1; $j<=$ndisp; $j++)
          {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           &getc2("$dir/LISTINGS/cipcls.drt1.$end ",$csf,$i,3,'indcsf');
          } # end of: for ($j=1; $j<=$ndisp; $j++) 
       } # end of: for ($i=1; $i<=$nstate; $i++)
#
} # end of: sub cicsfc
#
#-----------------------------------------------------------------
#
   sub mccsf
{
   my $i;
   my ($nstate,$csf,$drt,$comment,$geomopt)=@_;
#
     $end='sp';
     if ($geomopt){$end='all'}
#
#   $csf=$csf." ";
     for ($i=1; $i<=$nstate; $i++)
      {
       print "----------------------------------------------------------\n";
       print " ***  MCSCF c**2: State Nr $i, DRT: $drt, csf: $csf, type: $comment  ***\n";
         for ($j=1; $j<=$ndisp; $j++)
	  {
           $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
           &getenergy("$dir/LISTINGS/mcpcls.drt$drt.state$i.$end",$csf,1,3,1.0,'4.2f');
          } # end of: for ($j=1; $j<=$ndisp; $j++)
      } # end of: for ($i=1; $i<=$nstate; $i++)
#
} # end of: sub mccsf
#       
#-------------------------------------------------------------------
#
#    MRPT
sub mrpt{
#
   ($nstate)=@_;
   my ($i,$j);
   print "\n ***  E2(MRPT)�*** \n\n";
     for ($i=1; $i<=$nstate; $i++){
     print "----------------------------------------------------------\n";
     print "E2(MRPT) State Nr $i\n";
       for ($j=1; $j<=$ndisp; $j++){ 
       $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
        &getenergy("$dir/LISTINGS/ciudgls.drt1.sp ","mrpt2",$i,3,1.0,'10.6f');
       } # end of: for ($j=1; $j<=$nstate; $j++)
     } # end of: for ($i=1; $i<=$nstate; $i++)
#
   print "\n *** E3(MRPT)�*** \n\n";
     for ($i=1; $i<=$nstate; $i++){
     print "----------------------------------------------------------\n";
     print "E3(MRPT) State Nr $i\n";
       for ($j=1; $j<=$ndisp; $j++){
       $dir="DISPLACEMENT/CALC.c$coord[$j].d$displ[$j]$unit";
        &getenergy("$dir/LISTINGS/ciudgls.drt1.sp ","mrpt3",$i,3,1.0,'10.6f');
       } # end of: for ($j=1; $j<=$nstate; $j++){
     } # end of: for ($i=1; $i<=$nstate; $i++)
#
}
####################################################################################
#
   sub getenergy {
#

     local ($energy,$found);
     ($filename, $string,$istate,$position,$factor,$fmt)=@_;
#     print "$filename,$string,$istate,$position\n";

      open (ANYFILE, $filename) or die "File: $filename not found!\n";
      $/="\n";
       $icount=0;
       while ( <ANYFILE> ){
         if ( /$string/  ){
          $icount++;
	  $found=$_;
#	  print "$found\n";
	    if ($icount == $istate){
            chomp; s/^ *//;
            (@field)=split(/\s+/,$_,$position+1); 
            $energy=$field[$position-1];
            $energy=$energy*$factor;
            printf "%$fmt\n",$energy;
	    } # end of: if ($icount == $istate)
	  }
        } # end of: while ( <ANYFILE> ) 
# 	printf "%$ "$icount,$istate\n";
	if ( $icount < $istate){ print"0.0\n";}

       close ANYFILE;
       return ($energy);
      }    
####################################################################################
#
   sub getc2 {
#

     local ($energy,$found);
     ($filename, $string,$istate,$position,$string2)=@_;
#     print "$filename,$string,$istate,$position,$string2\n";

      open (ANYFILE, $filename) or die "File: $filename not found!\n";
      $/="\n";
       $ifound=0;
       $icount=0;
       while ( <ANYFILE> ){
         if ( /$string2/  ){$icount++;}
         if ( /$string/  ){
            if ($icount == $istate){
	    $ifound=1;
            chomp; s/^ *//;
            (@field)=split(/\s+/,$_,$position+1);
            $energy=$field[$position-1];
            printf "%4.2f\n",$energy;
            last;
            } # end of: if ($icount == $istate)
          }
        } # end of: while ( <ANYFILE> )
#       print "$icount,$istate\n";
        if ( $ifound == 0){ print"0.0\n";}

       return ($energy);
      }       
#
#----------------------------------------------------------------------
#
