#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/
# LaTeX2HTML 96.1 (Feb 5, 1996)
# Associate contents original text with physical files.

$key = q/0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$toc_section_info{$key} = '0%:%rpt101.html%:%<B>O.S.U.-T.C.G. Report No. 101 <BR> Review of Self-Consistent-Field Theory <BR> with Shell-Averaged Integrals</B>' unless ($toc_section_info{$key}); 
$key = q/0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$toc_section_info{$key} = '3%:%node2.html%:%Closed-Shell Theory' unless ($toc_section_info{$key}); 
$key = q/0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$toc_section_info{$key} = '3%:%node4.html%:%Open-Shell Energy Coefficients' unless ($toc_section_info{$key}); 
$key = q/0 0 0 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$toc_section_info{$key} = '3%:%node6.html%:%  About this document ... ' unless ($toc_section_info{$key}); 
$key = q/0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$toc_section_info{$key} = '3%:%node1.html%:%Introduction' unless ($toc_section_info{$key}); 
$key = q/0 0 0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$toc_section_info{$key} = '3%:%node3.html%:%Open-Shell Theory' unless ($toc_section_info{$key}); 
$key = q/0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$toc_section_info{$key} = '3%:%node5.html%:%References' unless ($toc_section_info{$key}); 

1;

