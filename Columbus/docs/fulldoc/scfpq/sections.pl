#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/
# LaTeX2HTML 96.1 (Feb 5, 1996)
# Associate sections original text with physical files.

$key = q/0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '0%:%rpt101.html%:%<B>O.S.U.-T.C.G. Report No. 101 <BR> Review of Self-Consistent-Field Theory <BR> with Shell-Averaged Integrals</B>' unless ($section_info{$key}); 
$done{"rpt101.html"} = 1;
$key = q/0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node2.html%:%Closed-Shell Theory' unless ($section_info{$key}); 
$done{"node2.html"} = 1;
$key = q/0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node4.html%:%Open-Shell Energy Coefficients' unless ($section_info{$key}); 
$done{"node4.html"} = 1;
$key = q/0 0 0 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node6.html%:%  About this document ... ' unless ($section_info{$key}); 
$done{"node6.html"} = 1;
$key = q/0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node1.html%:%Introduction' unless ($section_info{$key}); 
$done{"node1.html"} = 1;
$key = q/0 0 0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node3.html%:%Open-Shell Theory' unless ($section_info{$key}); 
$done{"node3.html"} = 1;
$key = q/0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node5.html%:%References' unless ($section_info{$key}); 
$done{"node5.html"} = 1;

1;

