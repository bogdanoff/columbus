      -------------------------------------
                      SLOPE
      -------------------------------------

      This program orthogonalizes nonadiabatic coupling
      vectors and computes the parameters for the
      linear approximation of the adiabatic energies
      close to a conical intersection.

      Author: Mario Barbatti, 2005.

      References

      Linear approximation: D. R. Yarkony, J. Chem. Phys. 114, 2601 (2001).
      Orhogonalization:     D. R. Yarkony, J. Chem. Phys. 112, 2111 (2000).
      Implementation:       M. Barbatti, A. J. A. Aquino and H. Lischka,
                            J. Phys. Chem. A 109, 5168 (2005).


       ---------------
       Input
       ---------------

       * Geometry file:                       geom

       * State 1 grad. file (DRT A, State X): cartgrd.drtA.stateX

       * State 2 grad. file (DRT B, State Y): cartgrd.drtB.stateY

       * Nonadiabatic coupling vectors file:  cartgrd_total_times_DE              (if Nonad. coupling calc.)
                                              cartgrd.nad.drtA.stateX.drtB.stateY (if MXS search)

       * Namelist "slope":                    slope.inp

   Parameter           Description
   ---------           -----------
       crit     =        0            - Orthogonalize g and h. (a.u.)
                         1            - Do not orthogonalize g and h
                         0 < crit < 1 - Orthogonalize g and h if g*h > crit
       ort_type =        S  - Schimidt orhogonalization. g is kept fixed and h is rotated.
                         Y  - Yarkony orthogonalization [Yarkony, 2000].
       geomf    =        Input file with the geometry of the conical intersection (Columbus format)
       grad1f   =        Gradient file for the first state  (free format)
       grad2f   =        Gradient file for the second state (free format)
       coupf    =        Nonadiabatic coupling vector file  (free format)

      Example of file slope.inp:
      (Orthogonalize molecule if g*h > 0.001 au. Use Yarkony orthog.)

 &slope
   crit     = 0.001,
   ort_type = Y,
 /end slope

       -------------------------------------
       Implementation in the COLUMBUS system
       -------------------------------------

       For each iteration of MXS search, slope is called in runc
       by subroutine "orthogonalize".
       slope.inp file is automaticaly created with the following parameters:
       crit = 0
       ort_type = Y

       ---------------
       Output
       ---------------

       * slopels: results of the analysis.

       * moldenort.freq: molden format file.
         Vibration 1: g1 first-state gradient
         Vibration 2: g2 second-state gradient
         Vibration 3: s = g1 + g2 (original)
         Vibration 4: g = g1 - g2 (original)
         Vibration 5: h (original)
         Vibration 6: g (orthogonal)
         Vibration 7: h (orthogonal)

       -------------------
       Current limitations
       -------------------

       Maximum number of atoms = 100.

       The othogonalization procedure is valid only to states with the
       same multiplicity.


