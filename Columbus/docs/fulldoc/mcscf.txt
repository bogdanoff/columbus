			Documentation for Program MCSCF
			    Written by Ron Shepard
                           extended by Hans Lischka
			   Last Revision: 28-Oct-97
					




			    ***********************
			    * PROGRAM DESCRIPTION *
			    ***********************

    Program MCSCF performs CSF mixing coefficient and orbital expansion
coefficient optimization using the Graphical Unitary Group Approach Exponential
Operator MCSCF method. 

    CSF specification is performed prior to execution of MCSCF using program
MCDRT and the coupling coefficients over the active orbitals are evaluated
using program MCUFT.  The remaining input controls various options that affect
the length of the listing file, the choice of orbitals to use initially and
during the iterative process, and the iterative solutions of the HMC(*)
diagonalization and PSCI equation.  A complete listing of the current default
values of the user input may be obtained by running the program MCSCF with no
input.

			     *********************
			     * INPUT DESCRIPTION *
			     *********************

    Program MCSCF uses NAMELIST input (&input).  The following is the list of
variables and arrays(*) that may be specified:

FLAGS(*),   NPATH(*),  NITER,      TOL,        NSTATE(*),  FCIORB(*), 
NCOL(*),    NCOUPL,    NOLDV,      NUNITV,     NCIITR,     MXVADD, 
NVCIMX,     RTOLCI(*), WNORM,      NMITER,     NVRSMX,     FCORE(*), 
FVRT(*),    NMPSY(*) , NAVST(*),   WAVST(*,*)
COSMOCALC

An actual program input deck might appear as:

 &input
    npath=1,2,13,17,19
    niter=6
 /

Input Destription:

    The NPATH(*) array is used to set various logical option flags to the value
.TRUE. or .FALSE.  For example, NPATH=...,4,... results in FLAGS(4)=.TRUE.
during program execution.  The default value of all the option flags is .FALSE.
NPATH=...,-4,... results in FLAGS(4)=.FALSE.  The NPATH(*) array is scanned in
order until a zero is found.  This feature allows temporary input changes to be
effected with only minor editing changes to the input file.  The following is a
brief summary of the available NPATH(*) program options.

	(1) Perform a calculation.  This must be specified for the MCSCF
calculation to actually be performed.  If it is not specified, all the other
options are printed and the program is immediately terminated.  This allows the
user to obtain a quick reference listing of the options and the default values
of the input variables on a particular computer.

	(2) Include Orbital-State coupling.  Orbital-State coupling will be
included if this option is specified.  Otherwise, the orbital-CSF and CSF-CSF
blocks of the Hessian matrix will not be calculated and used for the
calculation of the orbital tranformation.  Second-order convergence is obtained
only if this option is specified.  However, for ground state calculations with
large CSF expansion lengths, small orbital basis sets, and if only 3 or 4
digit-convergence of the wave function is required, it may be more efficient to
neglect these terms and to perform a few additional MCSCF iterations.  The
variable NCOUPL is used to determine on which iteration to begin calculation of
the CSF blocks of the Hessian matrix.  Setting NCOUPL=2 for example, would
specify that the first iteration be performed without the coupling and the
remaining iterations, starting with iteration 2, would include the coupling
terms.  In addition to the variable NCOUPL, the WNORM tolerance, TOL(9), is
also be used to determine when to begin using the CSF blocks of the Hessian
matrix.  These parameters are useful for cases when poor starting orbitals are
to be used initially and the construction of the CSF Hessian blocks would be
inefficient.

	(3) Print intermediate timing statistics.  Intermediate timing
statistics are calculated and included in the listing.  This is useful to
determine if a timing bottleneck is occuring during the calculation that could
be eliminated with an input change.

	(4) Print the 1-E matrices each iteration.  This is intended as an
analysis and debugging tool to determine, for example, if a pair of orbitals
are being erroneously interchanged during the optimization procedure.  The
matrices printed include H1(*) and S(*) in the AO basis at the beginning of the
program and the H1(*), D1(*), and Fock matrices in the MO basis and the orbital
expansion coefficients C(*) each iteration.

	(5) Print the HMC(*) matrix in the CSF basis each iteration.  This is
useful for examining avoided crossings and is also a debugging tool for
root-flipping problems.  This option should only be used for fairly small CSF
expansions since this can produce a long listing file.

	(6) Print the Hessian matrix each iteration.  This option should also
be used sparingly since long listing files may result.

	(7) Print the iterative HMC(*) diagonalization information.  The
Davidson-Liu iterative diagonalization method is used in this program.  This
option allows the printing of the subspace representations of the HMC(*) matrix
and its eigenvectors and energies.  This information may be useful for
examining root-flipping problems or convergence problems with the HMC(*)
diagonalization step itself.

	(8) Print the iterative PSCI solution information.  A microiterative
method is used to solve for the wave function corrections.  This procedure
involves subspace representations and reduced equation solutions similar to the
Davidson diagonalization method.  This option allows printing of the
micro-iterative convergence information and matrix-vector products for the
Hessian matrix block with expasion vectors.

	(9) Suppress the DRT listing.  This option allows the printing of the
DRT and NWALK-length indexing arrays to be suppressed.  During a potential
surface calculation this information should probably be printed once since it
contains the complete CSF specification, but it could be eliminated in
subsequent runs to conserve trees.

	(10) Suppress the HMC(*) eigenvector listing.  For long CSF expansions
it may be more efficient to suppress this vector printing each iteration and to
examine the results from the last MCSCF iteration from the RESTART file.

	(11) Construct the HMC(*) matrix explicitly.  This option forces the
HMC(*) matrix to be explicitly constructed. This option is necessary if a
direct diagonalization is to be performed instead of an iterative
diagonalization.

	(12) Diagonalize the HMC(*) matrix iteratively.  This option allows a
Davidson-Liu iterative diagonalization method to be used to solve for the
appropriate eigenvectors and energies.  The program allows trial vectors from
previous MCSCF iterations to be used in later MCSCF iterations and allows the
CSF correction vector from the PSCI solution to be used in subsequent MCSCF
iterations.  The required matrix-vector products may be computed either from
the explicitly computed HMC(*) matrix or indirectly from the integral list and
coupling coefficient file.  Although the transition points are machine
dependent, small CSF expansions should be diagonalized directly from an
explicit HMC(*) matrix, medium length expansions should be diagonalized
iteratively from an explicit HMC(*) matrix, and large expansions should be
diagonalized iteratively from the integral list.

    When this option is specified, several variables are used to specify the
initial trial vector space and to control the diagonalization process:
    NCOL is the number of vectors to converge.  When the iterative
diagonalization option is specified, NCOL is reduced to be .LE. 10.  
    RTOLCI(*) is the vector of residual norm convergence tolerances.  Note that
the energy of each HMC(*) vector is converged to approximately the square of
its residual norm.  The residual norm tolerance for the state being converged
is adjusted each MCSCF iteration to assure sufficiently accurate gradient
vector evaluation.  This adjusted convergence tolerance depends on the value of
WNORM of the previous MCSCF iteration.  
    NOLDV is the number of old trial vectors on file NVFILE.  If NOLDV=0 then
the parameter NUNITV specifies how many unit vectors to use for the initial
expansion.  The diagonal HMC(*) elements are examined and the lowest energy
CSFs are used to determine these vectors.  
    NCIITR is the maximum number of CI iterations each MCSCF iteration.  
    MXVADD is the maximum number of vectors to add to the expansion space each
CI iteration (the program attempts to add NCOL vectors at a time if convergence
is not reached and if memory allows).  
    NVCIMX is the maximum number of expansion vectors to allow.  If convergence
is not obtained by the time the NVCIMX limit is reached, the expansion space is
reduced (to NCOL vectors) and the iterative process continues.  Memory
limitations may alter the values of NUNITV, MXVADD, and NVCIMX.

	(13) Get initial orbitals from the formatted MOCOEF file.  The MOCOEF
file consists of a format card followed by the symmetry blocked vectors of
expansion coefficients.  The file may either be produced from a text editor or
from another utility program such as MOFMT.  Since the MOCOEF file is
formatted, it may be transferred among various computers.

	(14) Use initial orbitals from the 1-e Hamiltonian diagonalization,
HC=SCE, where H(*) and S(*) are the AO Hamiltonian and overlap matrix.

	(15) Get initial orbitals, MORB, from the RESTART file.  The MORB
orbitals are the resolved orbitals of the last completed MCSCF iteration of the
job that created the RESTART file.  These orbitals would be appropriate if the
last iteration were to be repeated with additional print options.  These
orbitals are resolved so that the inactive orbitals are transformed to
diagonalize the FDD(*) matrix and the active orbitals are subjected to any
invariant subspace resolutions that were specified.

	(16) Get initial orbitals, MORBN, from the RESTART file.  These
orbitals are those that would have been used during the next MCSCF iteration of
the job that created the RESTART file.  These orbitals would be appropriate to
use if convergence was not reached in the previous job and additional
iterations were required.  Note that the orbitals MORBN are converged further
than the orbitals MORB by one MCSCF iteration but that the FDD(*) matrix is
only approximately diagonal and the active-orbital invariant subspaces are only
approximately resolved.  If none of the orbital options are specified (options
13-16), the default initial orbitals correspond to a symmetry blocked unit
matrix which is then Schmidt orthonormalized during the first MCSCF iteration.

	(17) Print the final natural orbitals and occupations.  These orbitals
differ from the orbitals MORB only by rotations among the active orbitals to
diagonalize the one-particle density matrix D1(*).  Depending on the CSF
expansion space, it may or may not be possible to expand the final converged
wave function in terms of its natural orbitals.  The natural orbitals and
natural occupations may be used to evaluate expectation-value properties of the
wave function of the final MCSCF iteration using the equation <P>=Tr( C(t) *
P(ao) * C * N).  Note that the ordering of the natural orbitals is determined
by their natural occupations, highest occupations occuring first within the
active orbital space.  Inactive orbitals are not affected by the natural
orbital transformation and are in the same order as in orbitals MORB.

	(18) Skip the 2-E integral transformation on the first MCSCF iteration.
This allows the reuse of a set of transformed integrals for several exploratory
wave function evaluations with minimal overhead.  Caution should be exercised
with this option since the program MCSCF performs only partial 2-e integral
transformations and produces only the transformed integrals with at most two
virtual indices.  If a subsequent calculation involves a different
inactive/active/virtual orbital partitioning, the necessary integrals may not
be available on the transformed integral file.

	(19) Transform the virtual orbitals to diagonalize the QVV(*) matrix.
This transformation is applied only to the virutal orbitals and therefore does
not affect the wave function.  This option should be used if individual virtual
orbitals should be resolved (instead of just the span of the virtual orbital
space).  Examples include freezing certain virtual orbitals for subsequent
MRSDCI calculations or including certain virtual orbitals in an active orbital
space of a subsequent MCSCF calculation.  The QVV(*) matrix is the matrix
representation of the standard Fock matrix over the virtual orbitals if the
wave function is closed-shell.

	(20) Choose the maximum overlap psci vector instead of the lowest
energy vector each iteration.
        
	(21) Write out the one- and two- electron density to files mcd1fl
and mcd2fl for further use.

        (22) Perform a direct MCSCF calculation (direct calculation
of some closed shell B*r contributions and direct integral transformation).
This option requires a suitable TURBOMOLE 'control' file (see 
README.INSTALL). It is not possible to perform a CI gradient calculation
afterwards since the Hessian matrix is not calculated completely. 
Therefore, the Hessian file MCHESS is deleted in this case at the end of
the MCSCF calculation.
 
	(23) Use the standard integral transformation from Columbus 4.1.
(default is the enhanced out-of-core version)
-> no longer available
	 
        (24) Use the direct integral transformation.This option requires a
suitable TURBOMOLE 'control' file (see README.INSTALL).
        
	(25) Print Hessian matrix size and memory requirements in different
subroutines.
       
        (26) Read AO integrals in MOLCAS format (S,T,V,VEE)

        (27) Read AO integrals in DALTON2 format (S,T,V,VEE)

        (28) Perform all-state projection in micro-iterations 
             (implemented at all??? or default operation???)

        (29) Read MO coefficients in MOLCAS lumorb format

	(30) Compute (transition) density matrices of SA-MCSCF computations
to be used with the cigrd program for computing gradients and non-adiabatic
couplings. The states are entered with the mcdenin file (description below).

        (31) Use in-core version of half-sorted AO integrals

        (32) Force printout of MO- and NO-coefficients into mcscfls. By default
these are only printed if less than 15 basis functions are present. The coefficients
may be found in MOCOEFS as well.

    In addition to the NPATH(*) options, there are also several other input
variables that may be used to control the MCSCF calculations.  These input
variables usually have acceptable default values and need not be explicitly
set.

       STATE  AVERAGING

    MCSCF program can be used for state averaging of two or more different
states in the same symetry or in diferrent symetries. 
In first case the input files 'mcdrtfl', 'mcdftfl' and 'mcoftfl' are read.
In the case of different symetries the files 'mcdrtfl.ist', 'mcdftfl.ist' and
'mcoftfl.ist' are read, where ist is used for counting the different
symetry states. The total number of states is given implicitly by the
parameter NAVST, the weights by the parameter WAVST (se bellow). 
 
        NAVST(ist) = number of states in the symmetry ist (symetry is defined
in 'mcdrtin.ist') to be mixed during the calculation,starting from the state 
NSTATE(IST). Default is NAVST(1) = 1, which actually means no mixing at all.

        WAVST(ist,i) = the weight of the i-th state of symetry ist (symetry
is defined 'mcdrtin.ist') to be mixed. The values of WAVST(ist,i) do not have 
to be normalized. Default is WAVST(1,1) = 1.0 i.e. actually no mixing at all.

	NSTATE(ist) = 0 for ground state calculations, 1 for the first excited
state, etc for the symetry ist.  This specifies which root of the HMC(*)
matrix to minimize with respect to orbital variations. Default is NSTATE(1)=0.

 
	CDIR = 1  The matrix-vector product between the state Hessian matrix C 
and the expation vectors s and r is calculated directly for all blocks (C[ad], 
C[aa], C[vd] and C[va]). This requires the reading of the formula tape in every
micro iteration. The storing of the complete state Hessian matrix C is
omitted. This option is recomended for large calculations with large number
of CSFs. (default CDIR = 0)

	NITER = the maximum number of MCSCF iterations.  Each MCSCF iteration
involves a partial 2-e transformation, an HMC(*) matrix diagonalization, the
Hessian matrix and gradient vector construction, the solution of the PSCI
equation for the orbital corrections, and the transformation of the current
orbitals to the orbitals of the next MCSCF iteration.  Note that since the
default value for this variable is 1, this parameter must usually be specified
explicitly.

	NMITER = the maximum number of micro-iterations to perform during the
iterative PSCI equation solution.

	NVRSMX = the maximum subspace dimension allowed during the PSCI
equation solution.

	TOL(1) = Delta-EMC convergence criterion.  The energy change of the
last MCSCF iteration and the previous iteration must be less than this
tolerance at convergence.

	TOL(2) = WNORM convergence criterion.  The norm of the gradient vector
W(*) must be less than this tolerance at convergence.  For response property
calculations that assume the Generalized Brillouin Theorem to be satsified,
such as MCSCF geometry gradients, this tolerance should be sufficiently small
for the gradient contributions to be negligable.

	TOL(3) = KNORM convergence criterion.  The norm of the orbital
correction vector K(*) must be less than this tolerance at convergence.  The
vector K(*) is expanded into the antisymmetric matrix K(*,*) and the matrix
exp(-K) is used to transform from the current orbital set to orbitals of the
next MCSCF iteration.  Thus, to first order, KNORM is the orbital coefficient
accuracy.

	TOL(4) = APXDE convergence criterion.  During the solution of the PSCI
equations, an approximate energy lowering for the next MCSCF iteration is
calculated.  During the initial MCSCF iterations this value may be compared to
the actual energy change of the next iteration to determine how well the
iterative process is proceeding.  During the final iterations this approximate
energy lowering becomes quite accurate and it may be used to estimate the
energy of the orbitals MORBN.  In the neighborhood of convergence, APXDE is
approximately the square of Delta-EMC, KNORM or WNORM.  All of the convergence
criteria specified by TOL(1:4) must be satisfied before the program MCSCF stops
the iterative MCSCF optimization process.

	TOL(5) = Small Diagonal Matrix element tolerance.  Both the iterative
HMC(*) diagonalization and the iterative PSCI solution used in program MCSCF
require the calculation of quantities of the form C(i)=A(i)/(B(i,i)-D) for
subspace expansion.  These denominators are compared to this tolerance to avoid
division by near-zero quantities, which could result in linear dependence of
the trial vector space, and are modified if necessary.  This denominator
modification has no effect on any of the results of program MCSCF provided
convergence is obtained for the particular iterative procedure.  This tolerance
may be adjusted for pathological cases.

	TOL(6) = Minimum CI-PSCI Residual norm.
	TOL(7) = Maximum CI-PSCI Residual norm.  The solution of the HMC(*)
matrix eigenvector or the PSCI equation need not be converged to machine
accuracy during the initial MCSCF iterations.  The convergence accuracy is
adjusted dynamically during the iterative process so that unnecessary work is
not performed during the initial iterations and also to ensure that sufficient
accuracy is achieved for second-order MCSCF convergence.  The convergence
accuracy for the HMC(*) eigenvector is initially set to approx. WNORM**4 where
WNORM is the gradient norm of the previous MCSCF iteration.  The convergence
accuracy for the PSCI solution is initially set to approx. WNORM**2 where WNORM
is the gradient norm of the current MCSCF iteration.  These initial tolerances
are then clipped by TOL(6) and TOL(7).  Since WNORM is initialized to zero, the
CI vector of the first MCSCF iteration is converged to TOL(6) accuracy,
provided enough CI iterations are allowed.  Note that the initial value of
WNORM may also be specified in the input to allow the initial CI vectors to be
only approximately calculated.  The end result of this dynamic convergence
adjustment is that TOL(6) is the approximate accuracy of the final wave
function (both HMC(*) vector coefficients and orbital expansion coefficients).
Note that the energy of the wave function is accurate to approximately the
square of this limit.  TOL(7) may be changed for particular cases where higher
accuracy is initially required (for example, to preserve some higher spatial
symmetry requirements).  Note also that TOL(6) should be small enough to ensure
that the convergence tolerances, TOL(1) - TOL(4), can be satisfied...i.e.
TOL(6) should be less than the minimum of ( TOL(1), TOL(2), TOL(3), and
SQRT(TOL(4)) ).

	TOL(8) = Maximum ABS(K(xy)) allowed.  This parameter is used to clip
the elements of the K(*) vector prior to the calculation of the orbital
transformation matrix.  This step-length control may be used in pathological
cases to damp the transformation matrix and to force convergence is cases where
the rational energy approximation of the PSCI solution does not adequately
mimic the true energy behavior.  Note that this step-length restriction is only
imposed for large K(*) vector elements and does not affect convergence within
the neighborhood of the correct solution where the K(*) vector elements are
small.  The PSCI normalization used in MCSCF results in a maximum KNORM of 1.0
so that TOL(8) must be smaller than this to have even a potential damping
effect.

	TOL(9) = WNORM orbital-state coupling tolerance.  The norm of the W(*)
vector of the previous MCSCF iteration must be below this level before the
orbital-state coupling terms of the Hessian matrix are explictly calculated.
Since WNORM is initialized to zero, this tolerance has no effect on the first
MCSCF iteration unless the initial WNORM is specified in the input.

	TOL(10) = PSCI Emergency Shift parameter.  A level shifting parameter
for the PSCI equation solution.  This is an alternative method to force
convergence for "impossible" cases (for example, small Z0 during the PSCI
solution for several MCSCF iterations).  This should only be used as a last
resort since the use of this method always slows convergence. (default
tol(10)=0.0)

        TOL(11) = Minimum of PSCI shift parameter. (default tol(11)=0.0)
	TOL(12) = Increment of PSCI shift parameter. (default tol(12)=0.0)
     If variable shifting is to be performed, the values 
           TOL(10)=0.1, TOL(11)=0.0, TOL(12)=0.02
     are recommended.    

	FCIORB(*,*,*) = Invariant orbital subspace masks.  These entries are
specified as integer triples: ISYM,IORB,IMASK.  Decimal mask IMASK is to be
used for the orbital IORB.
Note that the orbital is given by its irrep plus the orbital number within the
irrep.  Active-active rotations between orbitals within an invariant orbital
subspace are not included in the energy optimization.  These orbitals may
still be resolved however, depending on the value of the appropriate decimal
digit of IMASK according to the following scheme:

 1: no resolution.
 2: Natural orbital resolution.
 3: FAA(*) matrix resolution.
 4: QAA(*) matrix resolution.

    Orbitals sharing digits of 1 in some decimal field are not resolved.  The
orbitals within this subspace are determined only by the come-what-may
transformation that results from the energy optimization steps.  Subspaces
sharing digits of 2 are resolved to diagonalize the D1(*) matrix over these
orbitals.  Subspaces sharing digits of 3 are transformed among themselves to
diagonalize the FAA(*) matrix over these orbitals.  Subspaces sharing digits of
4 are transformed to diagonalize the QAA(*) matrix.  Note that none of these
transformations actually affect the wave function since, for a redundant
rotation variable, any of these transformations may be exactly compensated by a
conjugate transformation within the CSF expansion coefficients.

    For example, suppose that a wave function is constructed so that orbitals 1
and 2 are constrained to contain 2 singlet-coupled electrons and orbitals 3 and
4 are constrained to contain two triplet coupled electrons.  In this case, the
energy would be independent of rotations between orbitals 1 and 2 since they
belong to a Full CI subspace.  Also orbitals 3 and 4 belong to a Full CI
subspace and the energy is invariant to rotations between them.  The only
difference in these two subspaces is that rotations between 1 and 2 must be
compensated by CSF coefficient changes while rotations between 3 and 4 leave
individual CSFs unchanged.  The input line for orbitals in a given symmetry
(e.g. 1)

    FCIORB=1 1 2, 1 2 2, 1 3 30, 1 4 30

would delete rotations between orbitals 1 and 2 and orbitals between 3 and 4
from the energy optimization space.  The converged orbitals 1 and 2 would be
resolved as natural orbitals, i.e. D1(2,1) would be zero.  The converged
orbitals 3 and 4 would be such that FAA(4,3) would be zero and FAA(3,3) and
FAA(4,4) would contain a set of "orbital energies".  In this case specifying
1 3 20,1 4 20 would produce undefined results since orbitals 3 and 4 are
occupation
degenerate.  Specifying 1 3 40,1 4 40 would result in QAA(3,3) and QAA(4,4)
containing an alternative set of "orbital energies" with a slightly different
orbital resolution.  The FAA orbital energy approaches zero as its occupation
approaches zero while the QAA orbital energy approaches a virtual orbital limit
analogous to virtual orbital energies of closed-shell wave functions.

	NCOL(ist) = the number of HMC(*) eigenvectors to calculate for the
symetry ist (symetry ist is defined 'mcdrtin.ist').  It is sometimes
useful to compute the energies of the next lower and next higher states from
the required one in order to monitor avoided crossings and near degeneracies.

	NCOUPL = the iteration on which to begin considering the calculation of
the orbital-CSF coupling terms.

	FCORE(*) = the list of frozen core orbitals terminated by a zero entry.
Frozen core orbitals are Schmidt orthonormalized in the first MCSCF iteration
but are otherwise unchanged during the MCSCF optimization.  Frozen core
orbitals must belong to the Doubly-occupied orbital set as specified during the
DRT construction. (temporarily disabled -RLS)

	FVRT(*) = the list of frozen virtual orbitals terminated by a zero
entry.  Other than Schmidt orthonormalization in the first MCSCF iteration,
these orbitals are also unchanged during the MCSCF optimization. Frozen virtual
orbitals must belong to the virtual orbital set as specified during the DRT
construction. (temporarily disabled -RLS)

	NMPSY(*) = the number of MOs in each symmetry block.  The default value
is the number of basis functions in each symmetry block.  If these values are
changed, this is the number of orbitals that will be input and output by
program MCSCF.  Note that NMPSY(i) should be .LE.  NBPSY(i) for all symmetry
blocks.

       COSMOCALC [0] : Set to 1 for incorporation of solvation effects by
means of the COSMO solvens model. 

       ASPROJECTION [1] : Set to 0 to disable all-state projection during
micro iterations
   
       LVLPRT [0]  : Print level for some portions of the code

       DINTSZ [500] : maximum size of the partial half-transformed 
                      integeral file - ONLY FOR INTEGRAL-DIRECT MCSCF!
                    
              

 

				   *********
				   * FILES *
				   *********

    The following is a list of the files used by program MCSCF, along with
their usual unit number assignments.

	Name          Unit #   inunits 	  Description
	---------    -------   -------	  ---------------
	MCSCFLS		60	1 	  listing file
	MCSCFIN 	50	2	  input file
	AOINTS          10	3      	  1-e integral file
	AOINTS2         11      4         2-e integral file
	MOCOEF		14	7	  Formatted MO coefficient file
	MCVFL		63      8      	  HMC(*) vector file
	MCSCFSM 	4	9	  summary listing file
	MCOFTFL	        61      10        Off-diagonal FT file
	MCDFTFL	        62      11   	  Diagonal FT file
	RESTART		17	12	  Restart file
	MCSCR2		18	13	  MO integral file
	MCSCR1		34	14	  Temporary sort file
	MCD1FL		19	15	  Optional output D1(*) Density file.
	MCD2FL		20	16	  Optional output D2(*) Density file.
	MCDRTFL	        64      17	  DRT file
        MOS             56      19        Formatted MO coeff. file for TURBOMOLE
	HDIAGF          57      20        Diagonal H-matrix file
	INFO2		58	21	  info file in case of direct mcscf
	MRPT 		23	22	  file for mrpt calculation in ciudg.x


    Files  with unit number not stored in iunits field

	MCHESS		15		  Hessian file
	MCSDA1		41		  Temporary DA sort file
	MCSDA2		42		  Temporary DA sort file
	MCSCR3		16		  Sorted MO integral file
        TMIN            51                mcdenin file (specifies gradients and couplings)


    Of these files, only a few are of importance to the user.  MCSCFIN is the
input file and contains the Namelist user input.  MCSCFSM is a short summary
listing and contains the header information from the integral file and DRT file
and a one-line summary of each MCSCF iteration.  MCSCFLS is the normal listing
file and contains the more detailed, input-dependent, listing of each iteration
along with the final orbital coefficients.  MCVFL contains the HMC(*)
eigenvectors, one per unformatted record. MCSCR2 contains a subset of the the
MO 2-e integrals.  If several trial calculations are to be performed with the
same orbital set, MCSCR2 may be saved in order to avoid the 2-e integral
transformation at the beginning of each calculation. As discussed above, this
option should be used with caution if the number of active orbitals changes in
subsequent calculations.  The unformatted RESTART file consists of a header
record which contains the symmetry blocking information followed by a series of
2-record sets.  The first record in each set contains a CHARACTER*8 label and
integer vector length and the second record contains the vector.  The labels
and vectors currently written on the RESTART file include:

  'CONINF' a vector of convergence information,
  'MORB' the resolved orbitals of the last MCSCF iteration,
  'NORB' the natural orbitals of the last MCSCF iteration,
  'NOCC' the natural occupations of the last iteration,
  'MORBN' the orbitals of the next MCSCF iteration,
  'MORBL' resolved orbitals for MRCI gradient and
  'HVEC' the HMC(*) eigenvector of the last MCSCF iteration.

	MCDENIN: the mcdenin (analogous to transmomin) file specifies for which gradients and couplings
density matrices are computed. Example:
--
SA-MCSCF
1 2 1 2
1 2 1 3
--
computes the gradient for DRT #1, state #2 and the coupling between DRT #1, state #2 and
DRT #1, state #3.
It is only possible to compute density matrices for DRT #1. But it should always be possible
to reorder the DRT's in a way that the one of interest has index #1.
It is suggested to keep the number of unique bra states in the mcdenin file as low as possible. In the
example above the transition density to the CSF basis is only computed for DRT #1, state #2.
With the opposite input
--
SA-MCSCF
1 2 1 2
1 3 1 2
--
the results are equivalent but more time and memory are needed because the transition densities
for state #2 and #3 (in DRT #1) are computed.

			 ******************************
			 * MACHINE-DEPENDENT FEATURES *
			 ******************************

Unix: On unix computers, all filenames are translated using the process
environment variables.  Upper-case variables only are valid matches.
Workspace size may be set from the command line using "-M nnnnnn".  Both upper
and lower case "M" are valid.  For example, from the Bourne shell:

    $ MCHESS=my/directory/my_mchess  $COLUMBUS/mcscf.x -m 50000

or

    $ MCHESS=my/directory/my_mchess
    $ export MCHESS  
    $ $COLUMBUS/mcscf.x -m 50000

or from the C shell:

    % setenv MCHESS my/directory/my_mchess
    % $COLUMBUS/mcscf.x -m 50000 

causes the input integral file to be redirected and the workspace size to be
set as indicated.  On unix machines, there is no upper limit on the workspace
allocated imposed by MCSCF; invalid requests result in a nonzero return status.

				***************
				* DISCLAIMERS *
				***************

The computer programs described herein contain work performed partially or
completely by the Argonne National Laboratory Theoretical Chemistry Group under
the auspices of the Office of Basic Energy Sciences, Division of Chemical
Sciences, U.S. Department of Energy, under contract W-31-109-ENG-38.

These programs may not be (re)distributed without the written consent of the
Argonne Theoretical Chemistry Group.

Since these programs are under development, correct results are not guaranteed.

				 ************
				 * SIGN-OFF *
				 ************

Comments and suggestions concerning this documentation file and program MCSCF
are welcome.

Ron Shepard
shepard@tcg.anl.gov

