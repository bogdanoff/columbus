       program makeryd
c=============================================================
c  EXPERIMENTAL version 0.1 Februar 15th 2000
c  written by: tm
c=============================================================
c things to do
c  1. extract Rydberg information from Aoinfo header
c  2. include averaging of several set of NOs
c  3. better output
c =============================================================
c     Based on a set of n (max. 10) sets of
c     MCSCF NO coefficient files
c     a set of Rydberg contractions is generated
c
c     procedure
c    * carry out a MCSCF calculations for the Cation
c          with Rydberg primitives present
c    * select the lowest virtual orbitals corresponding to
c          the desired Rydberg states
c      ======================================================
c    * read in MO coefficient file and extract those subblocks
c      that belong the Rydberg primitives
c    * read in the overlap matrix from the aoints file and
c      extract only those parts that belong to the Rydberg
c      primitives
c    * renormalize/orthogonalize the MO-subset
c    * generate an orthonormal basis covering the complete
c      Rydberg basis
c    * calculate the AO-density (the n selected Rydberg MOs
c      have their occupation set to one)
c    * set all lm/l'm' subblocks to zero
c    * average over the lm/lm' subblocks
c    * transform back to the new orthogonal basis
c    * diagonalize the averaged density
c    * transform the eigenvectors to the AO basis

c      all off-diagonal elements of the submatrices for S,P,D vanish
c      as well as all off-diagonal submatrix blocks
c      i.e. set all these elements to zero and all of the diagonal
c      elements are set to the corresponding D(s),D(p) and D(d) value
c      averaging is only done for m-components of the same CGTO !!
c        3s : no averaging at all :== D(s)
c        3p : D(ij)= 0 if i<>j
c             D(ii)= sum_j ( D(jj)*1/3 ) :== D(p)
c        3d : D(d) = .2*D(xy)+.2*D(xz)+.2*D(yz)+.4*D(xx)+.4*D(yy)+.4*D(z
c
c    input variables:
c     fnames = filename of the MOs to be averaged
c     factor = corresponding weight factors
c     nentry = number of densities
c     lrydmo = identifies the Rydberg states in the MO basis
c              in the order of irreps as pairs : # of first MO, # of Ryd
c     lrydao = identifies the Rydberg AOs  in terms of triples
c              in the order of irreps as pairs : # of first AO, # of Ryd
c     rydtype= identifies the type of Rydberg AOs in the same order
c              as lrydao with
c              type 1(s),2(px),3(py),4(pz),5(d-2),6(d-1),7(d0),8(d1),
c                   9(d2)
c
c e.g.
c $input
c  factor(1) = 1.0
c  fnames(1) = "mocoef"
c  lrydmo = 11 2 3 1 0 0 0 0 0 0 0 0 0 0 0 0
c  lrydao =  61 16 22 8 0  0 0 0  0 0 0 0
c  rydtype = 2 3 2 3 2 3 2 3
c            2 3 2 3 2 3 2 3
c            4 4 4 4 4 4 4 4
c  nentry=1
c $end
c
c  input MOs on mocoef
c  Rydberg MOs: #11,#12 of irrep 1, #3 of irrep 2
c  Rydberg AOs: #61 to #76 of irrep 1, #22 to #29 of irrep 2
c  Rydberg type: px and py to irrep 1 in alternating order followed
c                by those of pz type in irrep 2
c
c-----------------------------------------------------------------------

