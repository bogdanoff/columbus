<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content=
  "HTML Tidy for Linux (vers 11 February 2007), see www.w3.org" />
  <meta name="generator" content="Bluefish 1.0.7" />

  <title>Methods - Overview</title>
  <link rel="STYLESHEET" type="text/css" href="style.css" />
</head>

<body bgcolor="#CCFFCC" link="blue" vlink="purple">
  <!--   <DIV class="Section1"> -->

  <h2>Methods - Overview</h2>

  <p>The COLUMBUS program system is a set of Fortran programs for
  performing general <i>ab initio</i> electronic structure
  calculations within the framework of MR-CISD and related methods
  including size-extensivity corrections. The MCSCF and CI sections
  of COLUMBUS are based on the graphical unitary group approach
  (GUGA) (see Paldus (1981) and Shavitt (1981 and 1988)). This
  allows the treatment of any pure spin state with no practical
  restriction with respect to the number of open shells in the
  individual CSF (configuration state function). The main
  conceptual aspects of the program are explained in Lischka et al.
  (1981) and Shepard et al. (1988) and references therein.
  Additionally, the GUGA spin-orbit CI (SO-CI) method developed by
  Yabushita, Zhang and Pitzer (see Yabushita et al. (1999)) is
  available. For a recent review of COLUMBUS features see Lischka
  et al. (2001). The computation of nonadiabatic coupling vectors
  employing methods based on the analytic gradient programs are
  available now also (Lischka et al. 2003).</p>

  <p>Most of the text of this documentation was written with
  respect to the nonrelativistic approach. Changes and extensions
  derived from SO-CI are collected mostly in special, additional
  paragraphs.</p>

  <p>The present COLUMBUS version contains the following major
  program sections:</p>

  <ul type="disc">
    <li>AO-integral generation</li>

    <li>SCF</li>

    <li>MCSCF</li>

    <li>Integral transformation</li>

    <li>Iterative Davidson diagonalization for nonrelativistic
    MR-CISD, MR-ACPF and MR-AQCC calculations (sequential and
    parallel code versions)</li>

    <li>GUGA spin-orbit CI</li>

    <li>First- and second-order density matrices</li>

    <li>First-order properties</li>

    <li>MCSCF, MR-CISD and MR-AQCC-LRT transition moments</li>

    <li>Finite field method for second-order properties
    (polarizabilities)</li>

    <li>MR-CISD, MR-ACPF and MR-AQCC effective density matrices in
    the MO basis for the analytic gradient</li>

    <li>Analytic cartesian gradient for MCSCF, MR-CISD, MR-ACPF and
    MR-AQCC</li>

    <li>Internal (Pulay) coordinates and GDIIS geometry
    optimization</li>

    <li>Stationary point search using the RGF (reduced gradient
    following) method</li>

    <li>Numerical harmonic force constants from analytic gradient;
    harmonic vibrational frequencies</li>

    <li>Nonadiabatic coupling vectors and optimization procedures
    for finding minima on the crossing seam</li>
  </ul>

  <p>In the following, general considerations concerning the
  structure of COLUMBUS are described and individual methods are
  briefly discussed.</p>

  <h3><a name="drt" id="drt">GUGA</a></h3>

  <p>For a general introduction into GUGA see Paldus (1981) and
  Shavitt (1981 and 1988). A discussion of the implementation of
  GUGA into COLUMBUS can be found in Lischka et al. (1981) and
  Shepard et al. (1988). The explicit treatment of the external
  space in case of a singles and doubles CI is given in Shavitt
  (1979). The concept of cumulative occupation restrictions for
  constructing classes of wave functions can be found in Shepard
  (1994) (see the <a href="app_cummocc.html">Appendix 4</a> for an
  abridged version).</p>

  <h4>Nonrelativistic case</h4>

  <p><a name="DRT" id="DRT"></a>The configuration spaces are
  defined by means of a distinct row table (DRT), which is a
  tabular representation of a distinct row graph (Shavitt 1981,
  1988). The DRT is characterized by the number of electrons for a
  given state, the multiplicity, the spatial symmetry and the
  orbitals (levels). Each CSF (Gelfand state) is described as a
  walk on the graph. Each class of states is defined by one
  individual DRT. Such a class of states is characterized by a
  specific spatial symmetry, multiplicity or number of electrons of
  the state. For each class of states several roots can be
  calculated. In the simplest case we have, for example, one DRT
  and several states (of the same symmetry and multiplicity). If we
  want to compute states of different symmetry, we need a separate
  DRT for each symmetry. The same would apply for different
  multiplicities. The concept of multiple DRTs is used in case of
  MCSCF state-averaging and in the case of computing transition
  moments between different states at the CI level. For practical
  reasons, the arrangements of internal and external MOs is
  required to be the same for all DRTs.</p>

  <h4>Spin-orbit case</h4>

  <p>The spin-orbit CI uses the relativistic approximation which is
  based on (averaged relativistic) effective core potentials and
  the corresponding spin-orbit operators, treating only the valence
  electrons fully. The DRT is now more general since the
  configuration space must span a range of multiplicity values
  rather than just a single multiplicity value.</p>

  <h3>Symmetry Considerations</h3>

  <p>Molecular symmetry is taken into account explicitly for
  D<sub>2h</sub> and its subgroups. The AO basis is constructed in
  terms of symmetry adapted orbitals (SAOs). When necessary, the
  transformation of the one- and two-electron integrals is
  performed from the SAO to the MO basis. The MO coefficient matrix
  is given with respect to SAOs and is stored symmetry blocked. The
  MOs are usually characterized by the label or number of the
  irreducible representation (irrep) and an index within the irrep.
  For spin-orbit CI, the state is characterized by the double-group
  irrep rather than the spatial-group irrep and the
  multiplicity.</p>

  <h3>Direct vs. AO-direct</h3>

  <p>The AO direct features are not available in the 7.0 version of
  COLUMBUS!</p>

  <p>The denomination "direct" is used in two different contexts
  here. The original context referred to the fact that in the
  diagonalization procedure the Hamiltonian matrix was never
  constructed explicitly (Roos (1972), Roos and Siegbahn (1977)).
  This approach is also followed in COLUMBUS. In the standard
  implementation the one- and two-electron integrals are
  transformed to the MO basis first. Quite apart from this direct
  CI technique, direct methods have been used also in the context
  of avoiding the storage of AO-integrals in SCF calculations
  (Alml&ouml;f et al. (1982)). AO-direct approaches to electron
  correlation methods have been suggested initially by Taylor
  (1987). They are now also available in COLUMBUS in the MCSCF and
  CI program sections. In order to distinguish this form of direct
  calculation from the aforementioned direct CI technique, it is
  termed AO-direct.</p>

  <h3>SCF</h3>

  <p>A standard closed-shell and restricted Roothaan open-shell
  formalism is used as described by R.M. Pitzer (O.S.U.-T.C.G.
  Report No. 101). If you want to ftp the postscript file, click
  <a href=
  "http://www.itc.univie.ac.at/~hans/Columbus/documentation/fulldoc/rpt101/rpt101.html">
  here</a>.</p>

  <h3>MCSCF</h3>

  <p>The MCSCF program is based on a quadratically convergent
  method developed by Shepard (1987). It is not restricted or
  specialized to any particular class of wave functions (such as
  CAS) but can accommodate any individual choice of CSFs.
  Single-state or state-averaged (Lischka et al. (submitted))
  energy optimizations can be performed. The standard mode of
  operation is to transform the two-electron integrals in each
  iteration into the MO basis, to construct the Hessian explicitly
  and to store it in central memory. For larger calculations
  several additional options exist which reduce memory requirements
  significantly and enhance the applicability of the program in
  several ways. These options are</p>

  <ul type="disc">
    <li>direct calculation (based on integrals in the MO basis) of
    the product C-matrix (coupling terms (CSF,ad), (CSF,aa),
    (CSF,vd) and (CSF,va) in the notation of Shepard (1987)) times
    expansion vectors.</li>

    <li>AO-direct calculation and transformation of the
    two-electron integrals into the MO basis</li>

    <li>AO-direct calculation of the doubly occupied part (vd,ad,
    vd,vd, and va,vd) of the orbital Hessian (B-matrix) times
    expansion vectors (Alml&ouml;f and Taylor (1984)).</li>
  </ul>

  <p>Here, the letter 'd' stands for doubly occupied, 'a' for
  active and 'v' for virtual orbitals.</p>

  <h3>MR-CISD, MR-ACPF and MR-AQCC</h3>

  <p>Standard MR-CISD (Shavitt (1977)) and the variationally
  oriented approaches MR-ACPF (Gdanitz and Ahlrichs (1988)) and
  MR-AQCC (Szalay and Bartlett (1995)) including size-extensivity
  corrections can be used. The implementation of the latter two
  methods follows very closely the CI code so that most of the
  features which are available for CI are also available for ACPF
  and AQCC. For a review on variationally oriented methods
  including size extensivity correction see Szalay (1997).
  Excited-state calculations using MR-ACPF/AQCC are possible via a
  state-specific root-following approach or a LRT (linear-response
  theory) method (Szalay et al. (2000))</p>

  <p>The wave function is generated from a set of reference CSFs.
  Orbitals occupied in any of the reference CSFs are denominated
  internal. The remaining orbitals are called external or virtual.
  The total set of CSFs to be used in the CI calculation is
  constructed by allowing single and double excitations from the
  reference CSFs in all possible ways into the internal and
  external (virtual) orbital space. All possible excitations from
  the internal part of a given CSF into the external orbital space
  are constructed always. No selection of individual virtual
  orbitals for a single CSF is possible. Only overall freezing of
  virtual orbitals for excitations can be performed.</p>

  <p>The orbitals can be obtained from a previous SCF or MCSCF
  calculation, or from any other convenient set of orthonormal MOs,
  like the NOs of a preceding CI calculation. The only requirement
  is that the MO coefficients refer to the same SO basis for which
  the whole input procedure has been set up, including the ordering
  of the basis functions.</p>

  <p>The Davidson diagonalization technique (Davidson (1975)) is
  used to determine the lowest eigenvalues and eigenvectors of the
  Hamiltonian matrix <b>H</b>. In this scheme, the computationally
  most important step is the calculation of the sparse
  matrix-vector product <b>w</b><sub>i</sub> =
  <b>Hv</b><sub>i</sub> where the <b>v</b><sub>i</sub>'s are the
  expansion vectors of the CI wave function and the
  <b>w</b><sub>i</sub>'s are the resulting product vectors. For
  more information on this iterative eigenvector method as used in
  COLUMBUS can be found in the CI documentation. A direct CI
  procedure (Roos (1972), Roos and Siegbahn (1977)) is used to
  compute this matrix-vector product. It is driven by the four
  indices of the two-electron integrals. The contributions of the
  integrals to <b>H</b> are called CI coupling coefficients or GUGA
  loop values. Similar to the work of Siegbahn (1981), these
  coefficients are factorized into internal and external orbital
  contributions. In the original implementation the internal parts
  of the coupling coefficients are computed beforehand and stored
  on a file ("formula file"). The external contributions are
  computed on the fly. Now, the storage of the formula file can be
  avoided by recalculating these elements directly as needed.</p>

  <p>In the standard approach all one- and two-electron integrals
  are transformed into the MO basis first. This transformation step
  creates a major bottleneck in terms of disk space and I/O for
  larger basis sets. Therefore, following the work of Ahlrichs
  (1981), Meyer (1977) and Werner and Reinsch (1982), the
  contribution of the 3- and 4-external two-electron integrals can
  be computed in such cases alternatively on the basis of the
  AO-integrals. An AO-direct version avoiding the storage of the
  AO-integrals is available also. This AO-driven approach
  facilitates the integral transformation step considerably and
  reduces the amount of required disk space drastically, since only
  up to two-external integrals plus the diagonal four-external MO
  integrals need to be calculated.</p>

  <h3>Spin-orbit CI</h3>

  <p>In this case the standard approach in terms of MO integrals
  must be used, and the integral program must be capable of
  computing the spin-orbit integrals. These integrals couple CSFs
  with different multiplicities. Some of the CI coefficients are
  pure imaginary. Such coefficients are not labeled, but are
  recognized as pure imaginary by programs which compute property
  values.<br />
  <b>Note:</b> at present, the spin-orbit CI works with the
  <tt>Argos</tt> and <tt>Molcas</tt> integral programs.</p>

  <h3>Cartesian Gradient</h3>

  <p>The calculation of the analytical MCSCF, MR-CISD, MR-ACPF and
  MR-AQCC gradients is described in the references Pulay (1977),
  Shepard et al. (1992), Shepard (1995) and Lischka et al. (2002).
  For computing the MCSCF gradient two steps have to be
  performed:</p>

  <ul type="disc">
    <li>the one- and two-particle MCSCF density matrices and the
    MCSCF Fock matrix are transformed back to the AO basis and</li>

    <li>a trace operation with the one-electron, two-electron and
    overlap derivative integrals is carried out.</li>
  </ul>

  <p>In the CI, ACPF and AQCC cases effective density and Fock
  matrices have to be constructed first (detailed formulas for that
  step have been given by Shepard et al. (1995) and Lischka et al.
  (submitted)). Next, the effective density and Fock matrices are
  back-transformed to the AO basis and then used in the trace
  operation step as has been described just above for the MCSCF
  gradient.</p>

  <p>The resolution of invariant orbital spaces is of great
  practical importance and is taken care of in a very general way.
  This resolution has to be considered when the partitioning of
  invariant orbital subspaces in the MCSCF and MR-CI wave functions
  are not identical. Such a situation occurs when orbital rotation
  parameters are redundant in the MCSCF wave function but essential
  in the MR-CI wave function. This is the case, e.g., when a
  selected subset of the MCSCF CSFs is chosen as the reference
  space for an MR-CISD expansion, or when a subset of the MCSCF
  inactive (doubly-occupied) orbitals is frozen in the MR-CI wave
  function while the remaining MCSCF doubly-occupied orbitals are
  correlated.</p>

  <p>Two possibilities for the resolution of redundant orbital
  spaces are available:</p>

  <ul type="disc">
    <li>natural orbital (NO) and</li>

    <li>Fock operator resolution.</li>
  </ul>

  <p>For MCSCF doubly occupied orbitals the Fock operator
  resolution has to be chosen because with NO resolution occupation
  degeneracies occur which break the formalism. For a more detailed
  discussion of these questions see Shepard et al. (1992) and
  Shepard (1995).</p>

  <h3>Internal Coordinates and Geometry Optimization</h3>

  <p>The geometry optimization is performed using</p>

  <ul type="disc">
    <li>internal coordinates (Fogarasi et al. (1992) and the</li>

    <li>GDIIS formalism developed by Csaszar and Pulay (1984).</li>
  </ul>

  <p>The cartesian gradient is transformed to internal coordinates.
  Using an estimated force constant matrix an improved geometry is
  computed in internal coordinates and transformed back to
  cartesian coordinates. This new set of coordinates is then used
  in a next geometry optimization cycle. This procedure can be
  repeated until convergence is achieved.</p>

  <h3>Stationary Point search using RGF</h3>

  <p>The RGF (reduced gradient following) technique of Quapp et al.
  (1998) has been implemented into the COLUMBUS program. A curve of
  points <b>x</b> is followed which fulfills the <i>N</i>-1
  equations</p>

  <table border="0">
    <tr>
      <td><img src="formula1.jpg" /></td>

      <td width="50"></td>

      <td>(1)</td>
    </tr>
  </table>

  <p>omitting the <i>k</i>th equation. This gives the
  (<i>N</i>-1)-dimensional vector of the "reduced gradient".
  Starting from one stationary point on the PES the RGF curve is
  followed until a new stationary point is reached. A predictor
  step along the tangent <b>x</b>' to the RGF curve defined by
  equation</p>

  <table border="0">
    <tr>
      <td><img src="formula2.jpg" /></td>

      <td width="50"></td>

      <td>(2)</td>
    </tr>
  </table>

  <p>is performed. In general, the new geometry does not satisfy
  exactly the condition defined in equation (1). Therefore,
  corrector steps have to by performed. After the system fulfills
  Eq. (1) within a certain threshold a new predictor step is
  performed according to Eq. (2). This procedure is repeated until
  a stationary point is found. Calculations are carried out in
  internal coordinates. A linear combination of internal
  coordinated may be used as search direction also. For further
  details of the procedure see Quapp et al. (1998).</p>

  <h3>Nonadiabatic Coupling vector</h3>

  <p>The non-adiabatic coupling matrix element between two states I
  and J is defined by the following expression (see Lengsfield et
  al. (1984)):</p>

  <table border="0">
    <tr>
      <td><img src="NAD1.jpg" /></td>

      <td></td>

      <td>
        <p>(1)</p>
      </td>
    </tr>
  </table>

  <p>where <i>r</i> indicates integration over all electron
  coordinates.</p>

  <p>Formula (1) is split into two terms:</p>

  <table border="0">
    <tr>
      <td><img src="NAD2.jpg" /></td>

      <td></td>

      <td>
        <p>(2)</p>
      </td>
    </tr>
  </table>

  <p>Where:</p>

  <table border="0">
    <tr>
      <td><img src="DCI.jpg" /></td>

      <td></td>

      <td>
        <p>(3)</p>
      </td>
    </tr>
  </table>

  <p>and</p>

  <table border="0">
    <tr>
      <td><img src="DCSF.jpg" /></td>

      <td></td>

      <td>
        <p>(4)</p>
      </td>
    </tr>
  </table>

  <p>The calculation of the nonadiabatic coupling vector follows
  the procedures developed for the analytic gradient. Details are
  described in Lischka et al. (2003). The total coupling term
  (Eq.1) as well individual contributions D<sup>CI</sup> and
  D<sup>CSF</sup> can be calculated.</p>

  <h3>Determination of Minima on the Crossing Seam</h3>

  <p>The programs used are based on the Langrangian multiplier
  method developed by Manaa and Yarkony (1993) and on an gdiis
  implementation performed by P. G. Szalay. For more details see
  Lischka et al. (2003).</p><!--   </DIV> -->
</body>
</html>
