#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/
# LaTeX2HTML 2002-2-1 (1.70)
# Associate images original text with physical files.


$key = q/{displaymath}displacement[a.u.]=a_{up}[textrm{AA{}}]{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="192" HEIGHT="29" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="\begin{displaymath}
displacement[a.u.]=a_{up}[\textrm{\AA{}}]
\end{displaymath}">|; 

$key = q/f[cm^{-1}];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$f[cm^{-1}]$">|; 

$key = q/{displaymath}a_{lo}[a.u.]=a_{lo}[textrm{AA{}}]*1.889725988578923{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="261" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="\begin{displaymath}
a_{lo}[a.u.]=a_{lo}[\textrm{\AA{}}]*1.889725988578923
\end{displaymath}">|; 

$key = q/{displaymath}a_{up}[a.u.]=a_{up}[textrm{AA{}}]*1.889725988578923{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="269" HEIGHT="29" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="\begin{displaymath}
a_{up}[a.u.]=a_{up}[\textrm{\AA{}}]*1.889725988578923
\end{displaymath}">|; 

$key = q/hbar;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$\hbar$">|; 

$key = q/{displaymath}geometry_{final[a.u.]}=geometry_{reference}[a.u.]+displacement[a.u.]*l^{i}_{x}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="482" HEIGHT="30" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="\begin{displaymath}
geometry_{final[a.u.]}=geometry_{reference}[a.u.]+displacement[a.u.]*l^{i}_{x}
\end{displaymath}">|; 

$key = q/L_{q};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$L_{q}$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$i$">|; 

$key = q/{displaymath}l^{i}_{q}=frac{l^{i}_{q}}{parallell^{i}_{q}parallel}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="49" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="\begin{displaymath}
l^{i}_{q}=\frac{l^{i}_{q}}{\parallel l^{i}_{q} \parallel}
\end{displaymath}">|; 

$key = q/rm[amu];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$rm[amu]$">|; 

$key = q/{displaymath}w^{i}=f^{i}*2*pi{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="27" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="\begin{displaymath}
w^{i}=f^{i}*2*\pi
\end{displaymath}">|; 

$key = q/amu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$amu$">|; 

$key = q/{displaymath}l^{i}_{x}=frac{l^{i}_{q}}{sqrt{m^{j}}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="\begin{displaymath}
l^{i}_{x}=\frac{l^{i}_{q}}{\sqrt{m^{j}}}
\end{displaymath}">|; 

$key = q/3N-6;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$3N-6$">|; 

$key = q/cm^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$cm^{-1}$">|; 

$key = q/beta^{i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$\beta^{i}$">|; 

$key = q/{displaymath}displacement[a.u.]=frac{a_{up}}{beta}[a.u.]{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="210" HEIGHT="38" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="\begin{displaymath}
displacement[a.u.]=\frac{a_{up}}{\beta}[a.u.]
\end{displaymath}">|; 

$key = q/{displaymath}rm^{i}[a.u.]=rm^{i}[amu]*1822.888515{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="248" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="\begin{displaymath}
rm^{i}[a.u.]=rm^{i}[amu]*1822.888515
\end{displaymath}">|; 

$key = q/{displaymath}f^{i}[a.u.]=f^{i}[cm^{-1}]*100*c[mslashs]*2.41889*10^{-17}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="351" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="\begin{displaymath}
f^{i}[a.u.]=f^{i}[cm^{-1}]*100*c[m/s]*2.41889*10^{-17}
\end{displaymath}">|; 

$key = q/{displaymath}M^{j}[a.u.]=M^{j}[amu]*1822.888515{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="242" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="\begin{displaymath}
M^{j}[a.u.]=M^{j}[amu]*1822.888515
\end{displaymath}">|; 

$key = q/{displaymath}beta^{i}=sqrt{frac{rm^{i}*w^{i}}{hbar}}=sqrt{frac{rm^{i}*w^{i}}{1}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="216" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="\begin{displaymath}
\beta^{i}=\sqrt{\frac{rm^{i}*w^{i}}{\hbar}}=\sqrt{\frac{rm^{i}*w^{i}}{1}}
\end{displaymath}">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$j$">|; 

$key = q/{displaymath}l^{i}_{x}=frac{l^{i}_{x}}{parallell^{i}_{x}parallel}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="\begin{displaymath}
l^{i}_{x}=\frac{l^{i}_{x}}{\parallel l^{i}_{x} \parallel}
\end{displaymath}">|; 

$key = q/l_{q};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$l_{q}$">|; 

$key = q/{displaymath}frac{1}{parallell^{i}_{x}parallel}=sqrt{rm^{i}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="\begin{displaymath}
\frac{1}{\parallel l^{i}_{x} \parallel}=\sqrt{rm^{i}}
\end{displaymath}">|; 

1;

