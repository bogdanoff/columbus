/*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
/* fdate.c - uxp equivalent of fdate in Sun Fortran
 *
 * roger edberg  14-Nov-91
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int fdate_( char *date_string )
{
	char s[25];
	time_t now;

	now = time( NULL );
	strftime( s, 25, "%a %b %d %X %Y", localtime(&now) );
	strncpy( date_string, &s[0], 25 );

#ifdef DEBUG
	printf( " date_string = %s \n", date_string );
#endif

	return *date_string;
}
