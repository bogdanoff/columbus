/*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <extsymbols.h>
#ifdef EXT64 
#define FOPEN fopen64
#else
#define FOPEN fopen
#endif


/* testprogram to emulate the getvec/putvec function pair in C */

#ifdef INT64
/* several fortran argument strings are passed to C with the length attributes passed at the end */
    void EXTERNAL_GETPUTVEC(char *lname, double *data,long *dlen,long *ipos,char *mode,int lnam, int len)
#else
    void EXTERNAL_GETPUTVEC(char *lname, double *data,int *dlen,long *ipos,char *mode, int lnam, int len) 
#endif
 { 
#define MAXFILES 30
    static char *fnames[MAXFILES]; 
       fnames[0]="dummy";
       fnames[1]="coutput.dat";
     static int current_top;
   size_t dataread,dloc;
   int  itemp;
   int id; 
   char accwt[3]="wb\0";
   char accrd[3]="rb\0";
   char accwpt[4]="w+b\0";
   char accrpd[4]="r+b\0";
   char name[80];
   static FILE *fparray[MAXFILES+1] ={NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                                   NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                                   NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};
   FILE *fp;
   char temp[len];
   int i,selected ,xlen;
   static int fid[MAXFILES] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
/* mode: 'new'         create file and write data to it 
         'openrd'       open file and read from it
         'openwt'       open file and write to it
         'close'     close file  
         'delete'     close and delete file  
*/
/* find association between file pointer fp and filename fn */
/* open log file */
     if (fparray[1]==NULL) { fparray[1]=FOPEN(fnames[1],"w");}
     strncpy(temp,mode,len);
     temp[len]='\0';
     fprintf(fparray[1],">>> getputvector: len= %3d \n", len); 
     strncpy(name,lname,lnam);
     name[lnam]='\0';
     dloc = (size_t) *dlen;
    fprintf(fparray[1],">>> Entering getputvector name=%10s dlen=%15d offset=%15d  operation=%6s <<<<<<<<< \n", name,dloc,*ipos,temp );
   fp=NULL;
     itemp=fflush(fparray[1]);
/* find id associated with name */
    int icnt,ifnd;
     id=1;
     icnt=0; 
    while ( id != 0 && icnt <= MAXFILES  && fnames[icnt+1] != NULL ) 
      { icnt++; id =strcmp(fnames[icnt],name); }
    if (id != 0 ) { /* could not find a match */ 
                    if (icnt <=MAXFILES) { icnt++;}
                      else { printf(" limit MAXFILES reached \n"); exit(2);}
                  /*  icnt=0; 
                    while ( icnt <= MAXFILES && fnames[icnt] != NULL ) { icnt++;} */
                    fnames[icnt] = (char*) malloc((size_t) lnam+1);
                    strcpy(fnames[icnt],name);
                    fprintf(fparray[1], ">>> created new filename %15s (number %3d ) \n", fnames[icnt], icnt);}
    id = icnt;
    if (fid[id]==0) { fid[id]=1; selected=id; fp=NULL;}
    if (fid[id]==1) { selected=id; fp=fparray[id];}
    

  if (strncmp("new",mode,3) == 0 ) 
    { fp = FOPEN( fnames[id], accwt);
      if (fp == NULL ) { printf ("could not open %12s mode %3s \n",fnames[id],mode);exit(33);}
/* erstes seek notwendig ? */
#ifdef IBM64NOTNECESSARY
      itemp = fseek(fp,(off64_t) *ipos*8L, SEEK_SET);
#else
      itemp = fseek(fp,*ipos*8L, SEEK_SET);
#endif
/*    if (itemp != 0 ) { printf ("error occured during reading in mode openrd to %12s seek=%10d \n", fnames[id],itemp);exit(44);} */
      dataread = fwrite(data,sizeof(double), dloc, fp);
      itemp = ferror(fp);
      if (itemp != 0) {  clearerr(fp);  printf("error %5d  occured during writing in mode new to %12s\n", itemp,fnames[id]); exit(3);}
#ifdef FLUSH
      itemp = fflush(fp);
#endif
      fparray[selected]=fp;
      return;
    }

  if (strncmp("openrd",mode,6) == 0 ) 
    { if (fp == NULL) 
           {  fp = FOPEN( fnames[id], accrpd);
              if (fp == NULL ) { printf ("could not open %12s mode %3s \n",fnames[id],mode);exit(33);} 
              fparray[selected]=fp;}
#ifdef IBM64NOTNECESSARY
      itemp = fseek(fp,(off64_t) *ipos*8L, SEEK_SET);
#else
      itemp = fseek(fp,*ipos*8L, SEEK_SET);
#endif
      if (itemp != 0 ) { printf ("error occured during reading in mode openrd to %12s seek=%10d \n", fnames[id],itemp);exit(44);}
      dataread = fread(data,sizeof(double),dloc, fp);
      if (dataread != dloc) { printf ("read %8d instead of %8d words from %13s\n", dataread,dloc,fnames[id]); exit(45);}
      itemp = ferror(fp);
      if (itemp != 0) {  clearerr(fp);  printf("error occured during reading in mode openrd to %12s\n", fnames[id]); exit(3);}
      return;
    }

  if (strncmp("openwt",mode,6) == 0 )
    { if (fp == NULL) 
           {  fp = FOPEN( fnames[id], accrpd);
              if (fp == NULL ) { printf ("could not open %12s mode %3s \n",fnames[id],mode);exit(33);}        
              fparray[selected]=fp;}
#ifdef IBM64NOTNECESSARY
      itemp = fseek(fp,(off64_t) *ipos*8L, SEEK_SET);
#else
      itemp = fseek(fp,*ipos*8L, SEEK_SET);
#endif
/*    if (itemp != 0 ) { printf ("error occured during reading in mode openrd to %12s seek=%10d \n", fnames[id],itemp);exit(44);} */
      dataread = fwrite(data,sizeof(double), dloc, fp);
      if (dataread != dloc) { printf ("wrote %8d instead of %8d words to %13s\n", dataread,dloc,fnames[id]); exit(45);}
      itemp = ferror(fp);
      if (itemp != 0) {  clearerr(fp);  printf("error occured during writing in mode openwt to %12s\n", fnames[id]); exit(3);}
#ifdef FLUSH
      itemp = fflush(fp);
#endif
      return;
    }

  if (strncmp("close",mode,5) == 0 )
    { if (fp == NULL) { printf ("warning: cannot close %12s \n",fnames[id]);}  
      itemp=fclose(fp); fp=NULL; fparray[selected]=NULL; fid[selected]=0 ;
      return;
    }

  if (strncmp("delete",mode,6) == 0 )
    { if (fp == NULL) { printf ("warning associated file pointer already removed for %12s \n",fnames[id]);}
       else { itemp=fclose(fp);}
      itemp=remove(fnames[id]); fp=NULL; fparray[selected]=NULL; fid[selected]=0 ;
      return;
    }

   return;

 }
