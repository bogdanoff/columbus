/*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

/* testprogram to emulate the getvec/putvec function pair in C */

 void getputvector(char *name, int len, double *data,int dlen,long ipos,char *mode)
 { 
   size_t dataread;
   int  itemp;
   char accwt[3]="wb\0";
   char accrd[3]="rb\0";
   char accwpt[4]="w+b\0";
   char accrpd[4]="r+b\0";
   char ctmp[20];
   static FILE *fparray[10]= {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL} ;
   FILE *fp;
   int i,selected ;
   static char *fnarray[10]= {"XXXXXXXXXX","XXXXXXXXXX","XXXXXXXXXX","XXXXXXXXXX","XXXXXXXXXX","XXXXXXXXXX","XXXXXXXXXX","XXXXXXXXXX","XXXXXXXXXX","XXXXXXXXXX"} ;
/* mode: 'new'         create file and write data to it 
         'openrd'       open file and read from it
         'openwt'       open file and write to it
         'close'     close file  
*/
/* find association between file pointer fp and filename fn */
   fp=NULL;
    printf("entering getputvector name=%13s\n",name);
   for (i=0; i<10; i++ ) 
     { if (strncmp(fnarray[i],name,len)==0) { fp=fparray[i];selected=i;break;} 
       if (strncmp(fnarray[i],"XXXXXXXXXX",10) ==0 ) { strncpy(fnarray[i],name,len); selected=i; fp=NULL;  break;}
       if (i==9) { printf ("overflow of namearray ...\n"); exit(400);} 
     }
     
    
    printf ("selected %3d name %12s fnarray %12s \n", selected, name, fnarray[selected]);
    if (fp == NULL ) { printf (" fp = NULL\n");}
           else      { printf (" fp = vALID\n");}
       
       

  if (strncmp("new",mode,3) == 0 ) 
    { fp = fopen64( name, accwt);
      if (fp == NULL ) { printf ("could not open %12s mode %3s \n",name,mode);exit(33);}
      itemp = fseek(fp,ipos*8L, SEEK_SET);
      dataread = fwrite(data,sizeof(double),(size_t) dlen, fp);
      itemp = ferror(fp);
      if (itemp != 0) {  clearerr(fp);  printf("error occured during writing in mode new to %12s\n", name); exit(3);}
      itemp = fflush(fp);
      fparray[selected]=fp;
      return;
    }

  if (strncmp("openrd",mode,6) == 0 ) 
    { if (fp == NULL) 
           {  fp = fopen64( name, accrpd);
              if (fp == NULL ) { printf ("could not open %12s mode %3s \n",name,mode);exit(33);} 
              strncpy(fnarray[i],name,len); fparray[selected]=fp;}
      itemp = fseek(fp,ipos*8L, SEEK_SET);
      if (itemp != 0 ) { printf ("error occured during reading in mode openrd to %12s seek=%10d \n", name,itemp);exit(44);}
      dataread = fread(data,sizeof(double),(size_t) dlen, fp);
      itemp = ferror(fp);
      if (itemp != 0) {  clearerr(fp);  printf("error occured during reading in mode openrd to %12s\n", name); exit(3);}
      return;
    }

  if (strncmp("openwt",mode,6) == 0 )
    { if (fp == NULL) 
           {  fp = fopen64( name, accrpd);
              if (fp == NULL ) { printf ("could not open %12s mode %3s \n",name,mode);exit(33);} }
      itemp = fseek(fp,ipos*8L, SEEK_SET);
      dataread = fwrite(data,sizeof(double),(size_t) dlen, fp);
      itemp = ferror(fp);
      if (itemp != 0) {  clearerr(fp);  printf("error occured during writing in mode openwt to %12s\n", name); exit(3);}
      return;
    }

  if (strncmp("close",mode,5) == 0 )
    { if (fp == NULL) { printf ("cann not close %12s \n",name);exit(13);}  
      itemp=fclose(fp); fp=NULL; fparray[selected]=NULL; fnarray[selected]="XXXXXXXXXX";
      return;
    }
   return;

 }

void main()
{ double vec[100];
   char accwt[3]="wb\0";
   char accrd[3]="rb\0";
   char accwpt[4]="w+b\0";
   char accrpd[4]="r+b\0";
  char  name[9]="testfile\0";
  int i,j,ii[5];
  long off;
  ii[0]=4;
  ii[1]=3;
  ii[2]=2;
  ii[3]=0;
  ii[4]=1;
  for (i=0; i<5;i++)
   { for (j=0; j<100; j++) { vec[j]=j*0.5+ii[i]*1000.0 ; }
     off = ii[i]*100;
     printf ("putvector\n"); 
     if (i == 0 ) { getputvector(name, 8, vec,100,off,"new");}
           else   { getputvector(name, 8, vec,100,off,"openwt");}
   }
   getputvector(name,8,vec,100,off,"close");
  ii[0]=2;
  ii[1]=1;
  ii[2]=0;
  ii[3]=4;
  ii[4]=3;
  for (i=0; i<5;i++)
   { off = ii[i]*100;
     printf ("getvector\n"); 
     getputvector(name, 8, vec,100,off,"openrd");
     printf ("Elements found in range %4d to %4d \n", off,off+99);
/*    for (j=0; j<100; j++) { printf (" %4d : %8.3f \n", j+off, vec[j]) ; }
*/
   }
   getputvector(name,8,vec,100,off,"close");
   exit;
}

