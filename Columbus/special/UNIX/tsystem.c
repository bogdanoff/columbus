/*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
#if defined(_CRAY)
/* fortran character strings are passed by descriptor */
#include <fortran.h>
#include <stdlib.h>
int EXTERNAL_TSYSTEM( fdesc )
_fcd fdesc ; /* fortran character descriptor */
{
  char *name ;  /* pointer to the fortran string */
  int lenname ; /* fortran-generated length value */
  int i, ierr ;

  /* localize the string address and argument length */

  /* _fcdtocp() and _fcdlen() are macros that extract the string address
     bits and string length respectively from fdesc */
  name    = _fcdtocp( fdesc ) ;
  lenname = _fcdlen( fdesc ) ;
  ierr = system(name);


  return( ierr ) ;
}
#endif /* _CRAY */
