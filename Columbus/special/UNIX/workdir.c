/*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
#include "extsymbols.h"
#include "unistd.h"
#ifdef INT64
long EXTERNAL_WORKDIR( fstring, flength )
char *fstring ; /* string address */
long  flength  ; /* string length  */
#else
int EXTERNAL_WORKDIR( fstring, flength )
char *fstring ; /* string address */
int  flength  ; /* string length  */
#endif
{
int  i; 
#ifdef INT64
long ierr;
#else
int ierr;
#endif

/* initialize the return string */
for (i = 0; i < flength; i++ )
    *(fstring + i) = ' ' ;

 ierr=0;
 if (getcwd(fstring, flength) == NULL) {ierr=1;}
 return ierr;
}
