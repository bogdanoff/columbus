#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


$moldenfile=shift @ARGV;

%oz=( 
 'H',1.0,'He',2.0,'Li',3.0,'Be',4.0,'B',5.0,'C',6.0,'N',7.0,'O',8.0,'F',9.0,'Ne',10.0,
 'Na',11,'Mg',12,'Al',13,'Si',14,'P',15,'S',16,'Cl',17,'Ar',18);
%mass=( 
 'H',1.0,'He',2.0,'Li',3.0,'Be',4.0,'B',5.0,'C',6.0,'N',7.0,'O',8.0,'F',9.0,'Ne',10.0,
 'Na',11,'Mg',12,'Al',13,'Si',14,'P',15,'S',16,'Cl',17,'Ar',18);


 open MOLDEN,"<$moldenfile" || die ('could not open molden file $moldenfile\n');

 $fac=1.0/0.529177;
 $natoms=<MOLDEN>;
 chop $natoms;
 $temp=<MOLDEN>;
 for ($i=1;$i<=$natoms;$i++)
 { $temp=<MOLDEN>;
   chop $temp;
   ($symbol,$x,$y,$z)=split(/\s+/,$temp);
#  print '$temp \n';
   printf "%2s%8.1f%14.8f%14.8f%14.8f%14.8f \n", uc($symbol),$oz{uc($symbol)}, $fac*$x, $fac*$y, $fac*$z,$mass{uc($symbol)};
 }


