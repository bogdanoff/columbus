package  model;
use Exporter();
@ISA       =qw(Exporter);
@EXPORT    =qw( encode_zerotask createciudgin partition_data refine_tasks comp_buckets comp_boundaries print_tasks search_emptypatches rd_seganalyse_hdr rd_seganalyse_datablock print_2D 
                getfsize printstring  partition_4x);   # exported per default
#       @EXPORT_OK =qw();   # exported on request 
#
#==========================================================
#
   sub getfsize
   { my ($fname)=@_;
     if ( ! -e $fname ) {print "missing file $fname \n"; return -1;}
     system ("ls -l $fname > xxx ");
     open TMP,"<xxx";
     $xxx = <TMP> ; @xxx = split(/\s+/,$xxx); return $xxx[4];
    }


   sub rd_seganalyse_hdr {
   my ($nsym,$nval_zyxw,$nextern, $nsize_zyxw, $csfbatch_z,$csfbatch_y,$csfbatch_x,$csfbatch_w,$statseg);
   my ($i,$tmpz,@tmpz);
   ($nsym,$nval_zyxw,$nextern, $nsize_zyxw, $csfbatch_z,$csfbatch_y,$csfbatch_x,$csfbatch_w,$statseg,$totcsf) = @_;
   
   open FIN, "<SEGANALYSE";
        $_=<FIN>; if ( ! /NVALZYXW/) {die ("inconsistent SEGANALYSE file \n");}
        chop; s/^.*= *//; @$nval_zyxw=split(/\s+/,$_);
        $_=<FIN>; if ( ! /NSYM/) {die ("inconsistent SEGANALYSE file \n");}
        chop; s/^.*= *//; @tmpz=split(/\s+/,$_); $nsym=shift @tmpz;

         $_=<FIN>; if ( ! /NEXTERN/) {die ("inconsistent SEGANALYSE file \n");}
         chop; s/^.*= *//; @$nextern=split(/\s+/,$_);
         $_=<FIN>;

         $_=<FIN>; if ( ! /BATCHSIZE_ZYXW/) {die ("inconsistent SEGANALYSE file \n");}
         chop; s/^.*= *//; @$nsize_zyxw=split(/\s+/,$_);
 
         $_=<FIN>; if ( ! /CSFBATCH_Z/) {die ("inconsistent SEGANALYSE file Z: $_\n");}
            $tmpz=" ";
            while (<FIN>)  { if ( /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
            $tmpz =~ s/^ *//;
            $tmpz =~ s/ *$//;
            @$csfbatch_z = split(/\s+/,$tmpz);
            unshift @$csfbatch_z, -1;

        if ( ! /CSFBATCH_Y/) {die ("inconsistent SEGANALYSE file Y:$_ \n");}
            $tmpz=" ";
            while (<FIN>)  { if (  /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
            $tmpz =~ s/^ *//;
            $tmpz =~ s/ *$//;
            @$csfbatch_y = split(/\s+/,$tmpz);
            unshift @$csfbatch_y, -1;

        if ( ! /CSFBATCH_X/) {die ("inconsistent SEGANALYSE file X:$_ \n");}
            $tmpz=" ";
            while (<FIN>)  { if (  /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
            $tmpz =~ s/^ *//;
            $tmpz =~ s/ *$//;
            @$csfbatch_x = split(/\s+/,$tmpz);
            unshift @$csfbatch_x, -1;

        if ( ! /CSFBATCH_W/) {die ("inconsistent SEGANALYSE file W:$_ \n");}
           $tmpz=" ";
           while (<FIN>)  { if ( /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
           $tmpz =~ s/^ *//;
           $tmpz =~ s/ *$//;
           @$csfbatch_w = split(/\s+/,$tmpz);
           unshift @$csfbatch_w, -1;
       
       close FIN;

       @$totcsf=();
       for ($i=1; $i<=$#$csfbatch_z; $i++) { $$totcsf[0]+=$$csfbatch_z[$i];}
       for ($i=1; $i<=$#$csfbatch_y; $i++) { $$totcsf[1]+=$$csfbatch_y[$i];}
       for ($i=1; $i<=$#$csfbatch_x; $i++) { $$totcsf[2]+=$$csfbatch_x[$i];}
       for ($i=1; $i<=$#$csfbatch_w; $i++) { $$totcsf[3]+=$$csfbatch_w[$i];}

       for ($i=0; $i<4; $i++) { $$statseg[$i]=int(($$nval_zyxw[$i]-1)/$$nsize_zyxw[$i])+1;}
       start_tracker("rd_seganalyse_hdr");
       printf "number of irred. repres.      : %10d \n", $nsym; 
       printf "nmb. external orb./irrep     : ";
       for ($i=0;$i<$nsym;$i++) {printf " %10d",$$nextern[$i];} print "\n"; 
       printf "                                %10s  %10s %10s %10s \n","----z----","----y----","----x----","----w----";
       printf "number of valid internal walks: %10d %10d %10d %10d \n",@$nval_zyxw;
       printf "number of configurations      : %10d %10d %10d %10d \n",@$totcsf; 
       printf "batch sizes                   : %10d %10d %10d %10d \n",@$nsize_zyxw;
       printf "typical CSF batch size        : %10d %10d %10d %10d \n",$$csfbatch_z[1],$$csfbatch_y[1],$$csfbatch_x[1],$$csfbatch_w[1];
       printf "number of statistic segments  : %10d %10d %10d %10d \n",@$statseg;
       end_tracker("rd_seganalyse_hdr");

#
#       Der erste Wert des csfbatch arrays [0] = -1
#

       return 0 ;
  }

########################################################################################################################################

sub rd_seganalyse_datablock { 
 my($segtype,$pairtype,$array,$ibatch,$jbatch, $iseg, $jseg,$scilab)=@_;
 my ($tmp,$s,$p,$ilo,$ihi,$jlo,$jhi,$val,$i,$j,$foundentry);

#
#  IN:  $segtype = 0x,1x,2x,3x,4x,dg
#       $pairtype = zz,yy,xx,ww,...
#       $ibatch,$jbatch = batchsize i,j
#       $iseg,$jseg = maximum number of segments i ,j
#  OUT: $array[i][j] :  #loops for batch (i,j)


#print "rd_seganalyse_datablock: starting ... \n";
open FIN,"<SEGANALYSE";
$foundentry=0;
$summed=0;
while (<FIN>)
  {  if (/BEGIN ANALYSIS FOR/) { chop;s/^.*FOR//; $tmp=$_;
                              s/^ *//; s/ .*$//; ($s,$p)=split(/_/,$_);
                           if ($s eq $segtype && $p eq $pairtype)
                               { $foundentry=1; 
                                while (<FIN>)
                                { if (/SUM OF/) {last;}
                                  chop; s/[\[\.,\]=]/ /g; s/^ *//;
                                  ($ilo,$ihi,$jlo,$jhi,$val)=split(/\s+/,$_);
                                  $i=int($ihi/$ibatch+0.5); $j=int($jhi/$jbatch+0.5);
                                  if ($i > $iseg) {die("rd_seganalyse_dataablock: invalid i stat segment index $i vs. $iseg\n");}
                                  if ($j > $jseg) {die("rd_seganalyse_dataablock: invalid j stat segment index $j vs. $jseg\n");}
                                  $$array[$i][$j]=$val;
                                  $summed+=$val;
                               #print "P( $i,$j ) = $val\n";
                                }
                              }
                           next;}
  }
 close FIN;
#print "+++++++++++ Summe der Kosten $segtype.$pairtype = $summed\n";

 if (! $foundentry ) {die("rd_seganalyse_dataablock: cannot find entry $segtype $pairtype\n");}

 if ( uptrg($pairtype) ) {#$summed+=$summed; 
                         # print "pairtype $pairtype upper triangular: $seg1 $seg2\n";
                         #  print "symmetrizing dataarray ...\n";
                        if ($jseg != $iseg) {die("error in data symmetrization (uptrg)\n");}
                        for ($j=1; $j <= $jseg; $j++) 
                         # { for ($i=1; $i<= $iseg; $i++) 
                           { for ($i=1; $i<= $j; $i++) 
                             { $temp=$$array[$i][$j]+$$array[$j][$i];
                              $$array[$i][$j]=$temp;
                              $$array[$j][$i]=$temp;
                             }
                            $$array[$j][$j]*=0.5;
                          #   $summed-=$$array[$j][$j];
                           }
#                        print "+++++++++++ Summe der Kosten up $segtype.$pairtype = $summed\n";
                        }

 $zero=0;
 if ($scilab) {
 open TEMP,">$segtype.$pairtype.scilab";
  for ($j=1; $j <= $jseg; $j++) { for ($i=1; $i<= $iseg; $i++) { if (! $$array[$i][$j]) {$zero++;}
                                                                 printf TEMP "%10d  ",$$array[$i][$j]  ;} print TEMP "\n"; }
 print TEMP "\n";
 close TEMP; 
 print "SCILAB written (symmetrized): $segtype.$pairtype  #segments=$jseg,$iseg   zeroes=";
 printf "%10d ( %6.2f \% ) \n", $zero,$zero*100/($jseg*$iseg);
  }
 else { 
  for ($j=1; $j <= $jseg; $j++) { for ($i=1; $i<= $iseg; $i++) { if (! $$array[$i][$j]) {$zero++;}}}

 printf "Vanishing matrix elements for $segtype.$pairtype %6.2f %.\n", $zero*100/($jseg*$iseg); }
 return ;
}

#############################################################################################################################################

sub search_emptypatches {

  my ($iseg,$jseg,$datablock,$skipsize,$skipx,$skipy,$assoc,$patch,$xpatch,$skipthresh)=@_;

# find empty patches
# input  $iseg,$jseg: number of segments
#        @datablock [i][j]  number of contributions for seg i j combination 
#        $skipsize = all empty patches below $skipsize segment pairs are marked by -1 contributions
#        $skipy    = all empty patches with y dimension below $skipy are marked by -1 contributions
#        $skipx    = all empty patches with x dimension below $skipx are marked by -1 contributions
#       -----------------------------------------batches below skipthres are considered empty --------------------
#        $skipthresh = if $skipthresh>= 0 all batches below $skipthres are set to zero
#                     if $skipthresh< 0   all batches above -$skipthresh are set to zero and the
#                                         remainder to one  
#
# output @assoc[i][j] = $clusterid
#        @patch[cnt][0..3] = coordinates of patch cnt with xup,yup,xlo,ylo (0..3)
#        $npatch    0 number of patches found
#        @datablock  empty patches to be ignored (due to their size) are set to -1 (instead of 0) 
#    
#
@$assoc=();
@$patch=();
$npatch=0;
my $ntmp=0;
 print "search: $skipx,$skipy,$skipsize,$skipthresh (x,y,size,thresh)\n";
my @savecopy=();
my ($icnt,$jcnt); 
#if ($skipthresh != 0 ) { 
if ($skipthresh >=0 ) 
      { for ($i=1; $i<=$iseg; $i++) 
         { for ($j=1; $j<=$jseg; $j++) 
            { $savecopy[$i][$j]=$$datablock[$i][$j];
              if ($$datablock[$i][$j]<$skipthresh) { $$datablock[$i][$j]=0; $ntmp++;}
            }
         }
      }
else    {
         $icnt=0; $jcnt=0;
         for ($i=1; $i<=$iseg; $i++) 
         { for ($j=1; $j<=$jseg; $j++) 
            { $savecopy[$i][$j]=$$datablock[$i][$j];
  #           print "D:$$datablock[$i][$j]\n";
              if ($$datablock[$i][$j]<abs($skipthresh)) { $$datablock[$i][$j]=100; $icnt++;}
                      else                         { $$datablock[$i][$j]=0;$jcnt++;}
            }
         }
      }
#}
# print "set $icnt elements to 100 and $jcnt elements to 0  , ",-$skipthresh,"\n"; 
# print " $ntmp empty datablocks\n";

 my ($izero,$jzero,$val);
 my ($ok,$notok,$i,$j,$xup,$yup,$xlo,$ylo,$npatch);

  while (1)
  {
  ($izero,$jzero)=getzerotask($datablock,$assoc,$iseg,$jseg);
  if (!( $izero + $jzero)) {last;}  # no more empty patches 
  ($xup,$yup,$xlo,$ylo)=makecluster($datablock,$assoc,$izero,$jzero,$iseg,$jseg);

  if (  ($xlo-$xup+1)*($ylo-$yup+1) <= $skipsize  || ($xlo-$xup+1) <= $skipx || ($ylo-$yup+1) <= $skipy )
   # skipping task
   { for ($i=$xup; $i<=$xlo; $i++) { for ($j=$yup; $j<=$ylo; $j++) { $$datablock[$i][$j]=-1;}}
   }
   else
     # updating $assoc ,$patch array
    { $npatch++;
      for ($i=$xup; $i<=$xlo; $i++) { for ($j=$yup; $j<=$ylo; $j++) { $$assoc[$i][$j]=$npatch;}}
      $$patch[$npatch][0]=$xup;
      $$patch[$npatch][1]=$yup;
      $$patch[$npatch][2]=$xlo;
      $$patch[$npatch][3]=$ylo;
#     printf "patch %5d = %5d %5d : %5d %5d \n", $npatch,$xup,$yup,$xlo,$ylo;
    }
  }


#  check assoc vs. datablock

   $ok=0; $notok=0;
   for ($i=1; $i <=$iseg; $i++) { for ($j=1; $j<=$jseg; $j++) {
      if ($$datablock[$i][$j] > 0 ) 
            {  if (! $$assoc[$i][$j]) { $ok++;} else {$notok++;}}
   }}
   if ($notok) { die ( "search_emptypatches: assoc-datablock test failed: ok/notok $ok / $notok\n");}
   print "search_emptypatches: $npatch generated\n";
   $$xpatch=$npatch;
  print "copying back original datablock\n";
# for ($i=1; $i<=$iseg; $i++) { for ($j=1; $j<=$jseg; $j++) { $$datablock[$i][$j] = $savecopy[$i][$j];}}
 @$datablock=@savecopy;


 return;
}
###############################################################################################################################################



  sub getzerotask
{ my ($datablock,$assoc,$iseg,$jseg)= @_;
  my ($i,$j,$ii,$jj,$iii,$jjj,$found,$sum);
  $found=0;
  for ($j=1; $j <= $jseg; $j++)
       { for ($i=1; $i<= $iseg; $i++)
            { if (( ! $$datablock[$i][$j]) && (! $$assoc[$i][$j])) { $found="$i:$j"; last; }}
        if ($found) {last;} 
       }
 if (! $found) { return (0,0);}
 ($ii,$jj)=split(/:/,$found);
 # try to shift the zero into the center of some zeropatch
 # find last zero in this line
  $j=$ii;
  while ($j <=$iseg) { if ($$datablock[$j][$jj]!= 0  || $$assoc[$j][$jj] !=0 ) {last;} $j++;}
  $iii=int(($j-$ii)/2)+$ii;

  $j=$jj;
  while ($j <=$jseg) { if ($$datablock[$iii][$j]!= 0 || $$assoc[$iii][$j]!= 0)  {last;} $j++;}
  $jjj=int(($j-$jj)/2)+$jj;
  return($iii,$jjj);
  }



################################################################################################################################################



  sub makecluster
 { my($datablock,$assoc,$izero,$jzero,$iseg,$jseg)=@_;
   my ($i,$j,$found,$xup,$yup,$xlo,$ylo,$cycle);
   # start from i,j and extend to -x +x -y +y
   $xup=$izero; $xlo=$izero; $yup=$jzero; $ylo=$jzero;
#  print "makecluster: $izero,$jzero .... $xup:$xlo $yup:$ylo\n";
   $found=1;
   $cycle=0;
   while ($found)
   { $found=0;
   if ($xup-1 > 0 ) {  # check xup-1,yup:ylo
                       $sum=0;
                       for ($j=$yup; $j<=$ylo; $j++) { $sum=$sum+$$datablock[$xup-1][$j]+$$assoc[$xup-1][$j];}
                       if ( ! $sum) { $found=1;$xup=$xup-1; }
                    }
   if ($xlo+1 <= $iseg  ) {  # check xlo+1,yup:ylo
                       $sum=0;
                       for ($j=$yup; $j<=$ylo; $j++) { $sum=$sum+$$datablock[$xlo+1][$j]+$$assoc[$xlo+1][$j];}
                       if ( ! $sum) { $found=1;$xlo=$xlo+1; }
                    }
   if ($yup-1 > 0 ) {  # check yup-1,xup:xlo
                       $sum=0;
                       for ($i=$xup; $i<=$xlo; $i++) { $sum=$sum+$$datablock[$i][$yup-1]+$$assoc[$i][$yup-1];}
                       if ( ! $sum) { $found=1;$yup=$yup-1; }
                    }
   if ($ylo+1 <= $jseg   ) {  # check ylo+1,yup:ylo
                       $sum=0;
                       for ($i=$xup; $i<=$xlo; $i++) { $sum=$sum+$$datablock[$i][$ylo+1]+$$assoc[$i][$ylo+1];}
                       if ( ! $sum) {$found=1; $ylo=$ylo+1; }
                    }
#  $cycle++; print "cycle number $cycle : range (xy)    $xup,$yup  $xlo,$ylo \n";
   }
   return ($xup,$yup,$xlo,$ylo);
  }

####################################################################################################################################################

  sub comp_boundaries {
#
#  input $patch    patch array
#        $npatch   number of patches
#        $iseg,$jseg : max segments
#        $pairtype 
#  output
#        @xb  : initial x boundaries
#        @yb  : initial y boundaries
#

  my ($npatch,$patch,$iseg,$jseg,$xb,$yb,$pairtype)=@_;
  my ($i,@xbound,@ybound);

# print "comp_boundaries: $npatch,$iseg,$jseg\n";
 for ($i=1; $i<=$npatch; $i++)
   { $xbound[$$patch[$i][0]]=1;
     $xbound[$$patch[$i][2]]=1;
     $ybound[$$patch[$i][1]]=1;
     $ybound[$$patch[$i][3]]=1;
      }

 @$xb=();
 for ($i=1; $i<=$iseg; $i++) { if ($xbound[$i]) {push @$xb,$i;}}
 if (uptrg($pairtype)) {@$yb=@$xb;}
  else {  @$yb=();
          for ($i=1; $i<=$jseg; $i++) { if ($ybound[$i]) {push @$yb,$i;}}
       }
 if ($xb[0]>1) { unshift @$xb,1;}
 push @$xb, $iseg+1;
 if ($$yb[0]>1) { unshift @$yb,1;}
 push @$yb, $jseg+1;

if ($debug) { print "comp_boundaries: generated $#$xb x-boundaries and $#$yb y boundaries\n"; }
 return;
}

###################################################################################################################################################

sub comp_buckets{
 my ($name,$datablock,$xb,$yb,$iseg,$jseg,$csfbatch_i,$csfbatch_j,$timeitr,$zeroarray,$upper,
     $print, $ifilesize, $costnmx,$memfac,$TASKLIST )=@_;
#
#  input $datablock : to evaluate total costs , also corrects the -1
#        @xb,@yb    : x and y boundaries
#        $iseg,$jseg : max i, jsegs
#        @csfbatch_i,_j : number of csfs for segment i,j
#        $wantedtasks:  number of desired tasks - for avtask computation
#        $memory factor
#
 my ($bsum,%buckets,$checksum,$VOLUME,$totcost,$i,$j);
%buckets=();
$zerotasks=0;
$checksum=0;
$VOLUME=0;
$totcost=0;
if ($memfac == 1) {$cfac=0.5;} else {$cfac=1;}
if ($upper) { for ($i=1; $i<=$iseg; $i++) 
                         { for ($j=1; $j<=$i; $j++) 
                           {  $totcost+=$$datablock[$i][$j]; }}
                        $avtask=$totcost/$costnmx* $timeitr;
                        $cost=$costnmx/$totcost;
                        print "comp_buckets - upper:  costnmx=$costnmx, totcost=$totcost,avtask=$avtask, cost=$cost \n";
                        $checksum+= mk_buckets_lt($xb,$yb,$avtask,
                                    \%buckets,$datablock,$name,$csfbatch_i,$csfbatch_j,
                                    \$VOLUME,\$zerotasks,$zeroarray,$cost,$ifilesize*.00000000093132257461,$memfac,$TASKLIST);
                      }
              else    { for ($i=1; $i<=$iseg; $i++) 
                         { for ($j=1; $j<=$jseg; $j++) 
                           {  $totcost+=$$datablock[$i][$j]; }}
                       #print "totcost=$totcost\n";
                       #print "xb=",join(':',@$xb),"\n";
                       #print "yb=",join(':',@$yb),"\n";
                        $avtask=$totcost/$costnmx* $timeitr;
                        $cost=$costnmx/$totcost;
                        print "comp_buckets - rect:  costnmx=$costnmx, totcost=$totcost,avtask=$avtask, cost=$cost \n";
                      # print "comp_buckets: avtask=$avtask, cost=$cost \n";
                        $checksum+= mk_buckets_q($xb,$yb,$avtask,
                                    \%buckets,$datablock,$name,$csfbatch_i,$csfbatch_j,
                                    \$VOLUME,\$zerotasks,$zeroarray,$cost,$ifilesize*.00000000093132257461,$memfac,$TASKLIST);
                      }
$bsum=0;
my $maxbucket=0;
foreach $i  (sort msort keys(%buckets)) 
 { $bsum+= $buckets{$i}; 
  if ($print) {printf "time range:  %8.5f - %8.5f seconds : %5d tasks \n", 
       $cost*$avtask*($i)*0.1, $avtask*$cost*($i+1)*0.1,$buckets{$i};}
   $maxbucket=($i*10);}
if ($print)
{  $ints=.00000000093132257461*$bsum*$ifilesize;
  printf "total number of non-zero %8s tasks= %6d zerotasks= %6d VOLUME=%8.3f GB  plus integrals %8.3f GB \n",$name,$bsum,$zerotasks,$VOLUME,$ints;
  print "checksum=$checksum\n";
 }

 return ($VOLUME,$maxbucket);
}


#####################################################################################################################################################

sub mk_buckets_q
{ my($borderi, $borderj,$avtask,$buckets,$array,$txt,$icsf,$jcsf,$XVOLUME,$zerotasks,$zeroarray,$cost,$intfile,$memfac,$TASKLIST)=@_;
  my ($i,$j,$jj,$checksum,$b,$VOLUME,$zero_task);
  my (@imap,@jmap);
  @imap=();
  @jmap=();
  $tmp=1;
  $imap[1]=$tmp;
  for ($i=1; $i<=$#$icsf; $i++)  {$tmp+=$$icsf[$i]; $imap[$i+1]=$tmp;
  }
  $tmp=1;
  $jmap[1]=$tmp;
  for ($i=1; $i<=$#$jcsf; $i++)  {$tmp+=$$jcsf[$i]; $jmap[$i+1]=$tmp;
}



 
  $checksum=0;
  $zero_task=0;
  $addcheck=0;
  if ($memfac == 1) {$cfac=0.5;} else {$cfac=1;}
 for ($i=0; $i<$#$borderi; $i++)
  {
    for ($j=0; $j<$#$borderj; $j++)
    {
       $val=patchij($$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$array);
      $checksum+=$val;
      $b=$val/$avtask;
      if (! $val) {$zero_task++; next;}
      $taskvolume=volij($$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$icsf,$jcsf);
      $VOLUME+=2*$taskvolume;
      $LARGEST_TASK= $b > $LARGEST_TASK ? $b : $LARGEST_TASK;
     if ($cost)  { printf "$txt: [  %3d:%3d,%3d:%3d ] => [ %12d:%12d,%12d:%12d]  rel.cost %8.3f  %10.2f secnds.  volume %6.2f GB   local memory %6.2f MB (%1d tasks) \n", 
                                     $$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1 , 
                                     $imap[$$borderi[$i]],$imap[$$borderi[$i+1]]-1,
                                     $jmap[$$borderj[$j]],$jmap[$$borderj[$j+1]]-1,
                                     $b*$cfac,$val*$cost*$cfac, $memfac*$taskvolume+$intfile, 1024*$memfac*$taskvolume ,1/$cfac;
                                     $addcheck+=$val*$cost;
                                     $r=sprintf "%-10s | %3d:%3d",$txt,$i+1,$j+1;
                                     push @{$$TASKLIST{'index'}},$r; 
                                     $r=sprintf " %3d:%3d,%3d,%3d ",$$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1;
                                     push @{$$TASKLIST{'range'}}, $r;
                                     $r=$val*$cost*$cfac;
                                     push @{$$TASKLIST{'time'}}, $r;
                                     $r=$memfac*$taskvolume+$intfile;
                                     push @{$$TASKLIST{'volume'}}, $r;
                                     $r=1024*$memfac*$taskvolume;
                                     push @{$$TASKLIST{'memory'}}, $r;
                                     $r = sprintf "%1d",1/$cfac;
                                     push @{$$TASKLIST{'tasks'}}, $r;
                                     }
        $b=int(10*$b*$cfac);$$buckets{"$b"}=$$buckets{"$b"}+3-$memfac;
  }}
 $$XVOLUME=$VOLUME;
 $$zerotasks+=$zero_task;
 if ($memfac ==1 ) {$$zerotasks+=$zero_task;}
  print "mk_buckets_q: addcostcheck=", $addcheck*$cfac," seconds  checksum=$checksum \n";
   return $checksum;
}

########################################################################################################################################################



 sub patchij
 { my ($ilo,$ihi,$jlo,$jhi,$array)=@_;
   my ($b,$i,$j,$val,$jj);
   $val=0;
   for ($i=$ilo;$i<=$ihi;$i++)
    {
      for ($j=$jlo; $j<=$jhi; $j++)
     { $val+=$$array[$i][$j];}}

   return $val;
}




 sub volij
 { my ($ilo,$ihi,$jlo,$jhi,$icsf,$jcsf)=@_;
   my ($factor,$b,$i,$j,$vol,$jj);
#
#  a task is associated  v1 (ilo:ihi)  v2(jlo:jhi)
#
   $vol=0;
   $factor=.00000000745058059692 ; # 8/1024/1024/1024
   for ($i=$ilo;$i<=$ihi;$i++) { $vol+= $$icsf[$i];}
    { for ($i=$jlo;$i<=$jhi;$i++) { $vol+= $$jcsf[$i];}}
   $vol*=$factor;
    return $vol;
}


####################################################################################################################################################
sub refine_tasks {

my ($xb,$yb,$datablock,$iseg,$jseg,$timeitr,$wantxseg,$wantyseg,$redx,$redy,$uptrg,$maxmem,$xbatch,$ybatch,$memfac,$costnmx,$bw,$intfile)=@_;
my ($i,$j,$totcost,$avcost);
my ($linesy,$linesx,@lastlinex,@lastliney);

my $debug=0;
$totcost=0;
$totcostlow=0;
if ($uptrg) {
for ($i=1; $i<=$iseg; $i++) { for ($j=1; $j<=$i; $j++) {
   if ($$datablock[$i][$j] < 0) {$$datablock[$i][$j]=0; $$datablock[$j][$i]=0;} 
      $totcost+=$$datablock[$i][$j];
 }}
}
else
{
for ($i=1; $i<=$iseg; $i++) { for ($j=1; $j<=$jseg; $j++) {
   if ($$datablock[$i][$j] < 0) {$$datablock[$i][$j]=0;} $totcost+=$$datablock[$i][$j]; }}
}
#$avtask=$totcost/($wantedtasks);  # avtask is now the maximum cost in internal units
$avtask = $totcost/$costnmx*$timeitr;
$bww = $totcost/($costnmx*$bw*65536);
$intcost=$totcost/$costnmx *$intfile/(1024*1024*$bw);

if ($debug) { print "+++++++++++ refine_tasks (tot, maxtask): $totcost, $avtask \n";
              print "+++++++++++ refine_tasks: constnmx timeitr =$costnmx, $timeitr, uptrg=$uptrg \n"; }
$lastlinex[1]=1001; $lastliney[1]=1002;
$lastlinex[3]=1001; $lastliney[3]=1002;

$dir=1;

if ($uptrg) 
   { if ($redy > 1) { $linesy=combine_uptrg($yb,$datablock,$wantyseg,$redy,$totcost,$avtask,$dir,$ybatch,$memfac,$maxmem,$bww,$intcost);}
     @$xb=@$yb; $linesx=$linesy;
     return;
      }
#
#  ensure that segmentation @xb @yb corresponds to the correct order $daten[$x][$y]
#
# avoid non-terminating loop by restricting counter 
$my_cnt=0;
while (1)
{
  {if ($redy > 1) { $linesy=combine($yb,$xb,$datablock,$wantyseg,$redy,0,$totcost,$avtask,$dir,$maxmem,$ybatch,$xbatch,$memfac,$bww,$intcost);}
  if ($redx > 1) { $linesx=combine($xb,$yb,$datablock,$wantxseg,$redx,1,$totcost,$avtask,$dir,$maxmem,$xbatch,$ybatch,$memfac,$bww,$intcost);}}
  if ($debug) { print "refine+++: linesxy $linesx $linesy  want  $wantxseg $wantyseg  lastline $lastlinex $lastliney \n";} 
   $my_cnt++;
  if ($linesy <= $wantyseg && $linesx <=$wantxseg) {last;}
  if ($lastlinex[$dir+2] == $linesx && $lastliney[$dir+2] == $linesy) {last;}
  $lastlinex[$dir+2]=$linesx; $lastliney[$dir+2]=$linesy;
  $dir=-$dir;
#  if ($my_cnt > 100) {die("potentially non-terminating loop in refine\n");}
  if ($my_cnt > 100) {warn("*** Refine did not converge! ***\n"); print "Parallelization degree may be low.\n"; last;}
}

 return
}

###################################################################################################################################################


 sub combine
#
#  input:  $border border to be optimized
#          $other  border to remain constant
#          $array  datablock with cost estimates
#          $maxlines  maximum number of segments in $border
#          $red      $reduction factor per cycle
#          $xdir   border to be optimized xdir (=1) or ydir (=0) 
#          $totcost     : total cost estimate         
#          $avcost      : cost per task limit
#          $direction   : from left to right or vice versa
#          $maxmem      : maximum memory
#          $borderbatch : cisegment length corresponding to some elementary block
#          $otherbatch  : cisegment length corresponding to some elementary block
#          $memory factor: 1 or 2 

#

{ my ($border,$other,$array,$maxlines,$red,$xdir,$totcost,$avcost,$dir,$maxmem,$borderbatch,$otherbatch,$memfac,$bw,$intcost)=@_;
  my (@size,$l,$ll,@vval,@val,$sum,$i,$j,$lcost,@bordertmp,$curr);
  my ($mmax,$iutmp,$iltmp,@bmap);
  @val=();
  @vval=();
  @vmemo=();
  @vmemb=();
  $ll=0;
  my $debug=0;

  if ($debug) {print " ---- starting   combine ----- \n";}
  for ($i=0; $i<$#$border; $i++)
    { for ($j=$$border[$i];$j<=$$border[$i+1]-1;$j++) { $vmemb[$i]+=$$borderbatch[$j];}}
     #  print "vmemb[",$i,"]=",$vmemb[$i],"\n";}
  for ($i=0; $i<$#$other; $i++)
    { for ($j=$$other[$i];$j<=$$other[$i+1]-1;$j++) { $vmemo[$i]+=$$otherbatch[$j];}}
     #  print "vmemo[",$i,"]=",$vmemo[$i],"\n";}
# vmemo/vmemb contain the memory demand for the existing segmentation in $other/$border

  if ( $xdir)
   {
  for ($i=0; $i<$#$border; $i++)
    { $val[$i] = patchij($$border[$i],$$border[$i+1]-1,1,150,$array);
      for ($j=0; $j<$#$other; $j++)
         {$vval[$i][$j]=patchij($$border[$i],$$border[$i+1]-1,$$other[$j],$$other[$j+1]-1,$array);
            }
      $ll=$ll+1;
      }
  }
  else
   {
  for ($i=0; $i<$#$border; $i++) 
    { $val[$i] = patchij(1,150,$$border[$i],$$border[$i+1]-1,$array);
      for ($j=0; $j<$#$other; $j++)
         {$vval[$i][$j]=patchij($$other[$j],$$other[$j+1]-1,$$border[$i],$$border[$i+1]-1,$array);
            }
      $ll=$ll+1;
      }
   }

 $mxline=0;
 $avline=0;
 $mnline=1000000;
 for ($i=0; $i<$#$border; $i++) {$avline+=$val[$i]; if ($val[$i] > $mxline) {$mxline=$val[$i];}
                                                    if ($val[$i] < $mnline) {$mnline=$val[$i];}}
 $avline=$avline/($ll ); 

 # $val[$i]  contains the cost estimate for the entire row $$border
 # $vval[$i][$j] contains the cost estimate for block[$$border][$$other] 
 # $ll is the current number of lines

  if ($ll <= $maxlines) {return $ll;}
  $l=int($ll/$red);
  if ($l < $maxlines) { $l=$maxlines;}
# $l is the new desired number of lines

 if ($debug) { print "try reducing number of tasks from $ll to ",$l," maxlines=$maxlines   xdir=$xdir  direction=$dir \n"; }
# in case of many zerotasks this produces more segments
# potentially buggy construction with transfer times included!

# forcing more stable partitioning in case of inhomogenous, low-time tasks
# it is still not sufficiently robust
# $lcost=$totcost/$l;
# Um bei gleichverteilten Kosten ueberhaupt eine Reduktion zu erzielen, muss mindestens
# um den Faktor 2 reduziert werden.
  if ($minline > 100) { if ($avline/$minline < 2 ) { $lcost = $mxline*2.2;}
                                       else        {$lcost=$mxline*$red;}}
             else     { $lcost=$mxline*$red;} 
       
# $linefactor=1.5; 
# if ($lcost < $linefactor*$mxline)  {$lcost=$linefactor*$mxline; print "resetting lcost ... ";}
  if ($debug) { print "totcost=$totcost, l=$l, lcost=$lcost mxline=$mxline avline=$avline  minline=$mnline red=$red \n"; }
  @bordertmp=();
  $curr=0; 
  $currmem=0;
  $costfac=1;
  if ($memfac==1) {$costfac=0.5;}
  $maxblockmem=0;
  for ($j=0; $j<$#$other; $j++) {if ($vmemo[$j]>$maxblockmem) {$maxblockmem=$vmemo[$j];}}

# Prozess des Zusammenfuegens sollte nicht unbedingt vom Rand starten
# get smallest non assoc block, add neigbouring blocks until exceeding
# mark assoc blocks  and continue
    if ($dir==1)
    {
     $mmax=0;
     $mmaxmem=0;
     for ($i=0; $i<$#$border; $i++)   # optimized  border
        {  $curr+=$val[$i];           #current computational cost
           $currmem+=$vmemb[$i];      # current memory demand
           $maxblock=0;
           for ($j=0; $j<$#$other; $j++) { if ($vval[$i][$j] > $maxblock) { $maxblock=$vval[$i][$j];} }  # constant border
           $mmax+=$maxblock;
#          print "combineA: i (border) ,mmax,maxblock,avcost=$i (",$$border[$i];
#          print "), $mmax, $maxblock,$avcost\n";
#          if ($curr>$lcost  || $costfac*$mmax > $avcost || $maxmem < $memfac*($maxblockmem+$currmem) ) 
#          print "block $i  curr=$curr val(i)=",$val[$i] ,"\n";
           if ($curr>$lcost  || ($costfac*$mmax+$memfac*($maxblockmem+$currmem)*$bw +$intcost) > $avcost || $maxmem < $memfac*($maxblockmem+$currmem) ) 
                                                 { push @bordertmp, $$border[$i];
           if ($debug) { print "DIR=1_",$curr>$lcost,"( $curr , $lcost ):",($costfac*$mmax+$memfac*($maxblockmem+$currmem)*$bw +$intcost)>$avcost ,":",  $maxmem < $memfac*($maxblockmem+$currmem),"\n" ; } 
                                                   $c=$mmax-$maxblock;
            if ($debug) {print "adding new border at $i( ",$$border[$i], ") cost= $c  mem=",$memfac*($maxblockmem+$currmem-$vmemb[$i]),"\n"; }
                                                   $mmax=$maxblock; $curr=$val[$i]; $currmem=$vmemb[$i];}
        }
     }
    else
     {
         $mmax=0;
     for ($i=$#$border-1; $i>=0; $i--)
        {  $curr+=$val[$i];
           $currmem+=$vmemb[$i]; 
           $maxblock=0;
           for ($j=0; $j<$#$other; $j++) { if ($vval[$i][$j] > $maxblock) { $maxblock=$vval[$i][$j];}}
           $mmax+=$maxblock;
#          print "combineB: i (border) ,mmax,maxblock,avcost=$i(",$$border[$i];
#          print "), $mmax, $maxblock,$avcost\n";

# check case that even with one segment only 
           if ($curr>$lcost  || ($costfac*$mmax+$memfac*($maxblockmem+$currmem)*$bw +$intcost) > $avcost || $maxmem < $memfac*($maxblockmem+$currmem) ) 
                                                                     { unshift @bordertmp, $$border[$i+1]; 
           if ($debug) { print "DIR=0_",$curr>$lcost,"( $curr , $lcost ):",($costfac*$mmax+$memfac*($maxblockmem+$currmem)*$bw +$intcost)>$avcost ,":",  $maxmem < $memfac*($maxblockmem+$currmem),"\n" ;} 
                                                                       $c=$mmax-$maxblock+$memfac*($maxblockmem+$currmem-$vmemb[$i])/($bw*65536);
           if ($debug) { print "adding new border at $i( ",$$border[$i+1], ") cost= $c mem =", ") cost= $c  mem=",$memfac*($maxblockmem+$currmem-$vmemb[$i]),"\n"; }
                                                   $mmax=$maxblock; $curr=$val[$i];$currmem=$vmemb[$i];}
        }
     }
  if ($bordertmp[0]!=1) { unshift @bordertmp,1;}
   push @bordertmp, 151 ;
   if ($debug) { print "bordertmp=",join(':',@bordertmp), "Direction $dir \n";
                 print "retaining $#bordertmp  X=$xdir segments\n"; }
   $l=$#bordertmp;
    @$border= @bordertmp ;
  return $l;

}  
   

####################################################################################################################################################
sub partition_data {

my ( $inttype,$pairtype,$isegtype,$jsegtype,$nsize_zyxw,$statseg,$name,
     $skipsize,$skipx,$skipy,$csfbatches,$timeitr,$wantxseg,$wantyseg,$redx,$redy,
     $fixbd,$fixx,$zeroarray,$skipthresh, $ifilesize, $costnmx,$prtlevel, $maxmem, $TASKLIST,$bw, $preseg,$scilab)=@_;

#
#  input   inttype = integral type  (0x,1x,2x,3x,4x,dg)
#          pairtype= yx,yw,...
#          $isegtype,jsegtype: 0,1,2,3 for z,y,x,w segments
#          nsize_zyxw: number of internal walks per statistic segment 
#          statseg   : number of statistic segments
#          fixx      :  0 none
#                       1 yseg
#                       2 xseg
#                       3 all
#          fixbd     : fixed boundary values  fixx 0  @fixbd=()
#                                                  1,2 @fixbd=(...)
#                                                  3  @(fixbd[1,2]
#          zeroarray : list of zerotasks
#          csfbatches: @$csfbatches[i] i=0-3  zyxw
#          skipthresh: enforce sparse dataarray for loadbalancing purposes by
#                      if >=0 setting all elements  < $skipthresh temporarily to zero
#                      if <0 setting all elements  > -$skipthresh temporarily to zero and all others to one
#                      given in % relative to maxvalue
#          ifilesize : integral file size in bytes 
#          maxmem    : maximum local memory
#           preseg   : skip presegmentation
#          scilab    : 1 if scilab files should be written
#          
# output  returns boundaries as  (\@xb,\@yb);

  my (@help);
 @help=('z','y','x','w');
 my (@datablock,$square,@assoc,@patch,$npatch,@xb,@yb);
 $square = ! uptrg($pairtype);

  print "---------------------------------- partition $inttype $pairtype ($isegtype,$jsegtype) ------------------------------------------\n";

 @datablock=();
 
 rd_seganalyse_datablock($inttype,$pairtype,\@datablock,
                         $$nsize_zyxw[$isegtype],$$nsize_zyxw[$jsegtype],
                         $$statseg[$isegtype],$$statseg[$jsegtype],$scilab);
 
 if ($prtlevel > 1) { print_2D($name . "data_array",\@datablock,$$statseg[$isegtype],$statseg[$jsegtype],$square);}

#search_emptypatches ( $iseg,$jseg,$datablock,$skipsize,$skipx,$skipy,$assoc,$patch,$npatch)
# input  $iseg,$jseg: number of segments
#        @datablock [i][j]  number of contributions for seg i j combination
#        $skipsize = all empty patches below $skipsize segment pairs are marked by -1 contributions
#        $skipy    = all empty patches with y dimension below $skipy are marked by -1 contributions
#        $skipx    = all empty patches with x dimension below $skipx are marked by -1 contributions
# 
# output @assoc[i][j] = $clusterid
#        @patch[cnt][0..3] = coordinates of patch cnt with xup,yup,xlo,ylo (0..3)
#        $npatch    0 number of patches found
#        @datablock  empty patches to be ignored (due to their size) are set to -1 (instead of 0)
#
if ($fixx !=3 )
  {  if ($preseg) {  # 1.. nseg onto xb and yb 
                     for ($i=1; $i<=$$statseg[$isegtype]; $i++) {push @xb,$i;}
                     for ($i=1; $i<=$$statseg[$jsegtype]; $i++) {push @yb,$i;}
                  }
         else     { search_emptypatches ($$statseg[$isegtype],$$statseg[$jsegtype],\@datablock,$skipsize,$skipx,$skipy,
                       \@assoc,\@patch,\$npatch,0);
                    if ($npatch<2) {print "cannot work with just $npatch patches ... skipping\n"; return (\@xb,\@yb);}

                     if ($prtlevel > 2 ) {print_2D($name . "assoc array",\@assoc,
                                     $$statseg[$isegtype],$$statseg[$jsegtype],$square);}

                     if ($prtlevel > 1) { print_tasks($name . "EMPTY_PATCHES",$$statseg[$isegtype],$$statseg[$isegtype],\@assoc);}

                      comp_boundaries($npatch,\@patch,$$statseg[$isegtype],$$statseg[$jsegtype],\@xb,\@yb,$pairtype);
                   }

#    print "comp_boundaries xb:",join(':',@xb),"\n";
#    print "comp_boundaries yb:",join(':',@yb),"\n";

       if ($fixx == 2 ) { @yb = @$fixbd;}
       if ($fixx == 1 ) { @xb = @$fixbd;}
 }

 $memfac=2;
 if ($pairtype eq "xx" || $pairtype eq "ww" || $pairtype eq "wx" || $pairtype eq "xw") {$memfac=1;} 

  $maxmemloc=$maxmem*131072;                     

 if ($fixx !=3 )
{
    refine_tasks(\@xb,\@yb,\@datablock,$$statseg[$isegtype],$$statseg[$jsegtype],
                  $timeitr ,$wantxseg,$wantyseg,$redx,$redy,uptrg($pairtype), $maxmemloc, 
                  $$csfbatches[$isegtype],$$csfbatches[$jsegtype],$memfac,$costnmx,$bw,$ifilesize);

    print "desired number of segments: $wantxseg $help[$isegtype] $wantyseg $help[$jsegtype]",
          " (reduction factor $redx:$redy) --> generated number of segments $#xb $help[$isegtype] $#yb $help[$jsegtype] \n";
if ($debug) { print "--> $help[$isegtype] ",join(',',@xb),"\n";
              print "--> $help[$jsegtype] ",join(',',@yb),"\n"; }
comp_buckets($name,\@datablock,\@xb,\@yb,$$statseg[$isegtype],
             $$statseg[$jsegtype],$$csfbatches[$isegtype],$$csfbatches[$jsegtype],$timeitr,$zeroarray,
             uptrg($pairtype),1, $ifilesize,$costnmx,$memfac,$TASKLIST);
}
else
{
comp_buckets($name,\@datablock,$$fixbd[1],$$fixbd[2],$$statseg[$isegtype],
             $$statseg[$jsegtype],$$csfbatches[$isegtype],$$csfbatches[$jsegtype],$timeitr,$zeroarray,
             uptrg($pairtype),1, $ifilesize,$costnmx,$memfac,$TASKLIST);
}

return (\@xb,\@yb);


}




####################################################################################################################################################



sub print_2D
{ my($header,$array,$ncols,$nrows,$full)=@_;
 my ($ncol,$irow,$icol,$ic,$ir,$outstring,$upc,$upr,$full);
 $ncol=10;

  print "                       ",uc($header)," ( $nrows * $ncols )\n";

  for ($irow=1;  $irow < $nrows; $irow+=$ncol )
   { for ($icol=1; $icol < $ncols; $icol+=$ncol )
      { # print patch [($irow-1)*$ncol+1:min($irow*$ncol,$nrows), ($icol-1)*$ncol:min($icol*$ncol,$ncols)]
        if (( ! $full ) && $icol>$irow ) {next;}
        $outstring= "  ";
        $upc=$icol+$ncol-1 > $ncols ? $ncols : $icol+$ncol-1;
        $upr=$irow+$ncol-1 > $nrows ? $nrows : $irow+$ncol-1;
        for ($ic=$icol; $ic<=$upc; $ic++) { $outstring .= sprintf "%8d ",$ic;}
        print "\n $outstring\n    -------------------------------------------------------------------------------------------\n";
        for ($ir=$irow; $ir<=$upr; $ir++)
         { $outstring= sprintf "%3d |", $ir;
           for ($ic=$icol;$ic<=$upc ; $ic++)
            { $outstring .= sprintf "%8d ",$$array[$ir][$ic];}
            print "$outstring \n";
         }
      }
   }
  return;
  }


 sub print_tasks
 {my ($header,$iseg,$jseg,$assoc)=@_;
  my ($zero,$i,$j,$line,@achr);
  @achr=('A' .. 'Z','a' .. 'z');
  $zero=0;
  for($j=1; $j <=$jseg; $j++)
  { $line=' ';
    for ($i=1;$i<=$iseg; $i++) { if ( $$assoc[$i][$j] ) {$zero++; $x=$achr[(($$assoc[$i][$j]-1) % $#achr)]; } else {$x=' ';}
                                                                $line=$line . $x;}
    print "$line\n";
  }
  print "Zero tasks  retained: ",$zero*100/($iseg*$jseg),"%\n";
  return;
 }






##############################################################################################################################
      
       sub start_tracker {
       my ($name); $name=shift @_;
       print "...................................", " $name .........................................\n";
       return; }

       sub end_tracker {
       my ($name); $name=shift @_;
       print "-----------------------------------", " $name -----------------------------------------\n";
       return; }


 sub msort
 {
   if ($a<$b) { return -1; }
   else {return  1;}
 }


###############################################################################################################################


sub createciudgin
{
 my ($borders_zz,$borders_yy,$borders_xx,$borders_ww,$nsize_zyxw,$nval_zyxw)=@_;
 my  (@nseg,@nnseg,$noffset,$tmp);

 @nnseg=();
  shift @$borders_zz;
  push @nnseg,1;
  $nseg[0]=0;
  $nseg[0]++;
  pop @$borders_zz;
  while ($tmp = shift @$borders_zz ) {if (($tmp-1)*$$nsize_zyxw[0]<$$nval_zyxw[0]) { $nseg[0]++; push @nnseg,($tmp-1)*$$nsize_zyxw[0];}}

  shift @$borders_yy;
  $noffset=$$nval_zyxw[0];
  push @nnseg, $noffset+1;
  $nseg[1]=0;
  $nseg[1]++;
  pop @$borders_yy;
  while ($tmp = shift @$borders_yy ) {if (($tmp-1)*$$nsize_zyxw[1]<$$nval_zyxw[1])
                                      {$nseg[1]++; push @nnseg,($tmp-1)*$$nsize_zyxw[1]+$noffset;}}

  shift @$borders_xx;
  $noffset+=$$nval_zyxw[1];
  push @nnseg, $noffset+1;
  $nseg[2]=0;
  $nseg[2]++;
  pop @$borders_xx;
  while ($tmp = shift @$borders_xx ) {if (($tmp-1)*$$nsize_zyxw[2]<$$nval_zyxw[2])
                                      {$nseg[2]++;push @nnseg,($tmp-1)*$$nsize_zyxw[2]+$noffset;}}

  shift @$borders_ww;
  $noffset+=$$nval_zyxw[2];
  push @nnseg, $noffset+1;
  $nseg[3]=0;
  $nseg[3]++;
# print "join borders=",join(':',@$borders_ww),"\n";
   pop @$borders_ww;
  while ($tmp = shift @$borders_ww ) { if (($tmp-1)*$$nsize_zyxw[3]<$$nval_zyxw[3])
                                       {#print "tmp=$tmp\n"; 
                                      $nseg[3]++; push @nnseg,($tmp-1)*$$nsize_zyxw[3]+$noffset;}}

  $noffset+=$$nval_zyxw[3];
# push @nnseg,$noffset+1;
  return (\@nseg,\@nnseg);
 return;
}


sub encode_zerotask{
 my ($zero_task)=@_;


print " -----------------------------------  ZERO Tasks --------------------------------------------------------------\n";

{ my ($i,$tmpc,$tasktype,$tmp1,$tmp2,$tmp3,$tmp4,$bra,$ket,$tmp5,@tmpci);
 $tmpc=""; @tmpci=();
for ($i=0; $i<=$#$zero_task; $i++) {#printf " %4d  %-60s \n", $i, $$zero_task[$i];
                                  push @tmpci,$tmpc;
                                   $tmpc=$$zero_task[$i]; $tmpc=~s/^ *//; $tmpc=~s/=>//; $tmpc=~s/://;
                                   ($tasktype,$tmp1,$tmp2,$tmp3,$tmp4,$bra,$ket,$tmp5)=split(/\s+/,$tmpc);
                                   if ($tasktype eq "YY_2x") {$tmpc = ($ket+($bra+21000)*1000);next;}
                                   if ($tasktype eq "XX_2x") {$tmpc = ($ket+($bra+22000)*1000);next;}
                                   if ($tasktype eq "WW_2x") {$tmpc = ($ket+($bra+23000)*1000);next;}
                                   if ($tasktype eq "XZ_2x") {$tmpc = ($ket+($bra+24000)*1000);next;}
                                   if ($tasktype eq "WZ_2x") {$tmpc = ($ket+($bra+25000)*1000);next;}
                                   if ($tasktype eq "WX_2x") {$tmpc = ($ket+($bra+26000)*1000);next;}
                                   if ($tasktype eq "YZ_1x") {$tmpc = ($ket+($bra+11000)*1000);next;}
                                   if ($tasktype eq "YX_1x") {$tmpc = ($bra+($ket+12000)*1000);next;}
                                   if ($tasktype eq "YW_1x") {$tmpc = ($bra+($ket+13000)*1000);next;}
                                   if ($tasktype eq "YX_3x") {$tmpc = ($bra+($ket+31000)*1000);next;}
                                   if ($tasktype eq "YW_3x") {$tmpc = ($bra+($ket+32000)*1000);next;}
                                   if ($tasktype eq "ZZ_0x") {$tmpc = ($ket+($bra+1000)*1000);next;}
                                   if ($tasktype eq "YY_0x") {$tmpc = ($ket+($bra+2000)*1000);next;}
                                   if ($tasktype eq "XX_0x") {$tmpc = ($ket+($bra+3000)*1000);next;}
                                   if ($tasktype eq "WW_0x") {$tmpc = ($ket+($bra+4000)*1000);next;}
                                   die ("invalid tasktype $tasktype\n");
                                   }
 push @tmpci,$tmpc;
 shift @tmpci;
 sort msort @tmpci;
  $tmpc=" zerotasks=" . join(',',@tmpci) . "\n";
#push @ciudgin,$tmpc;
 print $tmpc;
}
print " --------------------------------------------------------------------------------------------------------------\n";

 return;
}




############################################################################################################################################

 sub uptrg{
  my ($pt)=@_;
  my ($seg1,$seg2,$res);
  ($seg1,$seg2)=split('',$pt);
  $res= ($seg1 eq $seg2);
  return $res;
}
#######################################################################################################

sub mk_buckets_lt
{ my($borderi, $borderj,$avtask,$buckets,$array,$txt,$icsf,$jcsf,$XVOLUME,$zerotasks,$zeroarray,$cost,$intfile,$memfac,$TASKLIST)=@_;
  my ($i,$j,$jj,$checksum,$b,$VOLUME,$zero_task);
  $checksum=0;
  $addcost=0;
  $zero_task=0;
  $prtlevel=0;
  my (@imap);
  @imap=();
  $tmp=1;
  $imap[1]=$tmp;
  for ($i=1; $i<=$#$icsf; $i++)  {$tmp+=$$icsf[$i]; $imap[$i+1]=$tmp;}
  $addcheck=0;


 for ($i=0; $i<$#$borderi; $i++)
  {
    for ($j=0; $j<=$i; $j++)
    {
      $val=patchij_lt($$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$array);
#     if ($i == $j || $memfac == 2) {$cfac=1.0; $icnt=1; $mfac=2;} 
#     else {$cfac=0.5; $icnt=2; $mfac=1;}
#     Die Statistik Felder wurden nur upper-oder lower-triangular akkumuliert, daher 0.5
#     Es werden im Falle von memfac=1 auch die Diagonalfaelle gesplittet (ciudg!)
      if ($memfac == 2) 
        {if ($i == $j) 
          {$cfac=1.0; $icnt=1; $mfac=1;} # v + w       = taskvolume    
         else
          {$cfac=1.0; $icnt=1; $mfac=2;} # v+v'+w+w'   = 2*taskvolume 
        }
      else 
      {$cfac=0.5; $icnt=2; $mfac=1;}  # offdg  v+w', v'+w  = taskvolume, taskvolume, 
                                      # offdg v+w , v+ w  = taskvolume 
      $checksum+=$val;
      $b=$val/$avtask;
      if (! $val) {$zero_task++; next;}
       $taskvolume=volij_lt($$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$jcsf);
#  $taskvolume = 1seg for xx 2seg xx'
#  generally xx one task xx' two tasks 
       $VOLUME+=2*$taskvolume;
      $LARGEST_TASK= $b > $LARGEST_TASK ? $b : $LARGEST_TASK;
     if ($cost)  { printf "$txt: [  %3d:%3d,%3d:%3d ] => [ %12d:%12d,%12d:%12d]  rel.cost %8.3f  %10.2f secnds.  volume %6.2f GB   local memory %6.2f MB (%1d tasks) \n",
                                     $$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1 ,
                                     $imap[$$borderi[$i]],$imap[$$borderi[$i+1]]-1,
                                     $imap[$$borderj[$j]],$imap[$$borderj[$j+1]]-1,
                                     $b*$cfac,$val*$cost*$cfac, $mfac*$taskvolume+$intfile, $mfac*1024*$taskvolume ,$icnt;
                                     $r=sprintf "%-10s | %3d:%3d",$txt,$i+1,$j+1;
                                     push @{$$TASKLIST{'index'}},$r;
                                     $r=sprintf " %3d:%3d,%3d,%3d ",$$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1;
                                     push @{$$TASKLIST{'range'}}, $r;
                                     $r=$val*$cost*$cfac;
                                     push @{$$TASKLIST{'time'}}, $r;
                                     $r=$mfac*$taskvolume+$intfile;
                                     push @{$$TASKLIST{'volume'}}, $r;
                                     $r=1024*$mfac*$taskvolume;
                                     push @{$$TASKLIST{'memory'}}, $r;
                                     $r = sprintf "%1d",$icnt;
                                     push @{$$TASKLIST{'tasks'}}, $r;
                                     $addcheck+=$val*$cost;}
        $b=int(10*$b*$cfac);$$buckets{"$b"}=$$buckets{"$b"}+$icnt;
  }}
 $$XVOLUME=$VOLUME;
 $$zerotasks+=$zero_task*$icnt;
#  print "mk_buckets_lt: addcheck : $addcheck seconds  checksum=$checksum \n";

   return $checksum;
}







#  Ordnunb von ilo vs jlo muss beruecksichtigt werden.

 sub patchij_lt
 { my ($ilo,$ihi,$jlo,$jhi,$array)=@_;
   my ($b,$i,$j,$val,$jj);
   $val=0;
   if ($ilo == $jlo && $ihi == $jhi ) 
   { for ($i=$ilo;$i<=$ihi;$i++)
      { for ($j=$jlo; $j<=$i; $j++) { $val+=$$array[$i][$j];}}
   }
   else
  { for ($i=$ilo;$i<=$ihi;$i++)
    { $jmax=$jhi; if ($jmax>$ihi) {$jmax=$ihi;}
      for ($j=$jlo; $j<=$jmax; $j++)
     { $val+=$$array[$i][$j];}}
   }
#   print "patchij_lt: $ilo:$ihi,$jlo:$jhi = $val\n";
    { return $val;}

   return $val;
}




 sub volij_lt
 { my ($ilo,$ihi,$jlo,$jhi,$icsf)=@_;
   my ($factor,$b,$i,$j,$vol,$jj);
#
#  a task is associated  v1 (ilo:ihi)  v2(jlo:jhi)
#
   $vol=0;
   $factor=.00000000745058059692 ; # 8/1024/1024/1024
   if ($ilo != $jlo) 
   { for ($i=$ilo;$i<=$ihi;$i++) { $vol+= $$icsf[$i]*$factor;} 
     for ($i=$jlo;$i<=$jhi;$i++) { $vol+= $$icsf[$i]*$factor;}}
   else
   { for ($i=$ilo;$i<=$ihi;$i++) { $vol+= $$icsf[$i]*$factor;}
     $vol+=$vol;}
    return $vol;
}


############################################################################################

   
 sub equal_size_segs{

 my ($iseg,$jseg, $icsf,$jcsf,$upper,$datablock,$tasks,$nsegx,$nsegy,$range,
     $isegtype,$jsegtype, $ifilesize,$cost)=@_;
#
#  computes volume etc. for segment range   $nsegx-$range ... $nsegx+$range,
#                                           $nsegy-$range ... $nsegy+$range
#
 
my @help = ( 'z', 'y', 'x', 'w');
my ($startx,$starty,$xrange,$yrange,$rrange,$i);
my @results=();
print "equal_size: $iseg,$jseg,type=$isegtype,$jsegtype\n";
$rrange=$range;
$startx=$nsegx-$range;
if ($nsegx-$range <=0) {$rrange=$nsegx-1;}
if ($nsegy-$range <=0) {$rrange=$nsegy-1;}
my $minbucket=1000;
print "********************#################*******************************\n";
print "testing range ",$nsegx-$rrange,":",$nsegx+$rrange,',',$nsegy-$rrange,':',$nsegy+$rrange,"\n";
for ($xrange=$nsegx-$rrange; $xrange <=$nsegx+$rrange; $xrange++)
 { $maxy=$nsegy+$rrange;
   $miny=$nsegy-$rrange;
   if ($upper) {$maxy=$xrange;$miny=$xrange;} 
   for ($yrange=$miny; $yrange <=$maxy; $yrange++)
  { my @scratch=();
    my @xb_equal=();
    $increment=$iseg/$xrange;
    $i=1;
    while ($i<$iseg){ push @xb_equal, int($i); $i+=$increment;}
    push @xb_equal, $iseg+1;
    my @yb_equal=();
    $increment=$jseg/$yrange;
    $i=1;
    while ($i<$jseg){ push @yb_equal, int($i); $i+=$increment;}
    push @yb_equal, $jseg+1;
#   print "EQUAL SIZES: i.e. $#xb_equal $help[$isegtype] ",
#       " and $#yb_equal $help[$jsegtype] segments \n";
# print " $help[$isegtype]: ", join(':',@xb_equal),"\n";
# print " $help[$jsegtype]: ", join(':',@yb_equal),"\n";
 ($vol,$maxbucket)= 
    comp_buckets($help[$isegtype] . $help[$jsegtype],$datablock,\@xb_equal,\@yb_equal,$iseg,
                 $jseg,$icsf,$jcsf,$tasks,\@scratch,$upper,0, $ifilesize);
  push @results, "$xrange $help[$isegtype] $yrange $help[$jsegtype] = $vol : $maxbucket"; 
  if ($maxbucket < $minbucket) {$minbucket=$maxbucket;}
#encode_zerotask(\@scratch);
 }
}
  $minvol=1000000000.00;
  foreach $i (@results)
  { ($tmp,$vol,$maxbucket)=split/[=:]/,$i;
    if ($maxbucket <= $minbucket) { if ($minvol > $vol) { $minvol=$vol; $save=$i;} }
  }
  $save=~s/^ *//;
  ($xrange,$x,$yrange,$y)=split(/\s+/,$save);
  @scratch=();
  @xb_equal=();
  $increment=$iseg/$xrange;
  $i=1;
  while ($i<$iseg){ push @xb_equal, int($i); $i+=$increment;}
  push @xb_equal, $iseg+1;
  @yb_equal=();
  $increment=$jseg/$yrange;
  $i=1;
  while ($i<$jseg){ push @yb_equal, int($i); $i+=$increment;}
  push @yb_equal, $jseg+1;
    print "EQUAL SIZES: i.e. $#xb_equal $help[$isegtype] ",
        " and $#yb_equal $help[$jsegtype] segments \n";
  print " $help[$isegtype]: ", join(':',@xb_equal),"\n";
  print " $help[$jsegtype]: ", join(':',@yb_equal),"\n";
    comp_buckets($help[$isegtype] . $help[$jsegtype],$datablock,\@xb_equal,\@yb_equal,$iseg,
                 $jseg,$icsf,$jcsf,$tasks,\@scratch,$upper,1, $ifilesize,$cost);

print "********************#################*******************************\n";
 return;
}

  
 sub combine_uptrg
#   xx, yy, ww,zz combinations 
#  input:  $border border to be optimized
#          $array  datablock with cost estimates
#          $maxlines  maximum number of segments in $border
#          $red      $reduction factor per cycle
#          $totcost     : total cost estimate
#          $avcost      : cost per task limit
#          $direction   : from left to right or vice versa
#          $borderbatch : csf sizes per elementary batch
#          $memfac      : memory factor
#          $maxmem      : maximum available memory
#  attention!
#  yy,zz diagonal & offdiag 1 task
#  xx,ww diagonal 1 task , offdiag 2 tasks
#
#

{ my ($border,$array,$maxlines,$red,$totcost,$avcost,$dir,$borderbatch,$memfac,$maxmem,$bw,$intcost)=@_;
  my (@size,$l,$ll,@vval,@val,$sum,$i,$j,$lcost,@bordertmp,$curr);
  my ($mmax,$iutmp,$iltmp,@bmap);


# since now optimization of borders are not independent,
# the lines array $val is not necessary any longer
# $array has been symmetrized before!
# add constraint: minimum number of segments

  @val=();
  @vval=();
  $ll=0;
  @vmemb=();
  $maxmemblock=0;

  for ($i=0; $i<$#$border; $i++)
    { for ($j=$$border[$i];$j<=$$border[$i+1]-1;$j++) { $vmemb[$i]+=$$borderbatch[$j];}}
 for ($i=0; $i<$#$border; $i++)
   { if ($maxmemblock < $vmemb[$i]) {$maxmemblock=$vmemb[$i];}} 

# if ($maxmemblock < $memfac*$maxmemblock*2) {die ("insufficient memory at statistics resolution\n");}

# or take uptrg only + patchij_uptrg

  $sum=0;
  for ($i=0; $i<$#$border; $i++)
    { for ($j=0; $j<=$i; $j++)
        {$vval[$i][$j]=patchij_lt($$border[$i],$$border[$i+1]-1,$$border[$j],$$border[$j+1]-1,$array);
        $vval[$j][$i]=$vval[$i][$j];}
      $ll=$ll+1;
      }

   
  $tmp=0;
 
    for ($i=0; $i<$#$border; $i++) { for ($j=0; $j<=$i; $j++) {$tmp+=$vval[$i][$j]; 
         }}
   $costfac=0.5;
   if ($memfac == 2) {$costfac=1;}
#  print "check total cost=$tmp  costfac=$costfac\n";
#     Die Statistik Felder wurden nur upper-oder lower-triangular akkumuliert, daher 0.5
#     Es werden im Falle von memfac=2 auch die Diagonalfaelle gesplittet (ciudg!)

  while ($#bordertmp < $maxlines && $avcost > 1 ) 
  {
  @bordertmp=();
  @admborder=();
  $curr=0;
  my $temp;
# get smallest non assoc block, add neigbouring blocks until exceeding
# cost crsterion must run over accumulated square blocks!
# memory just check diagonal blocks
#   if ($dir==1)
    {
     $maxblock=0;
     $first_i=0;
     $mtemp=0;
     $maxmemblock=0;
     for ($i=0; $i<$#$border; $i++)
        {  $mmax=$maxblock; 
           $maxblock=0;
           $maxmemblock=$mtemp; 
      # current combined lines plus old higher borders
           for ($j=$i+1; $j<$#$border; $j++) {  $temp=0;
               for ($k=$first_i; $k<=$i; $k++) { $temp+= $vval[$k][$j]}
#ok            if (0.5*$temp>$maxblock)  { $maxblock=$temp*0.5;}} # offdiagonal ==> 2 tasks
               if ($temp>$maxblock)  { $maxblock=$temp;}} # offdiagonal ==> 2 tasks
# since the (largest) diagonal block needs only a pair this
# estimate is not valid!
       $mdiag=0;
       for ($j=$$border[$first_i];$j<$$border[$i];$j++){  $mdiag+= $$borderbatch[$j];}
#
# check also maximum size of previous combined blocks
#
       $moffdg=0;
       for ($j=1; $j<$$border[$admborder[0]]; $j++) { $moffdg+=$$borderbatch[$j];}
       for ($j=1; $j<=$#admborder; $j++)
         { $mtemp=0; for ($jj=$$border[$admborder[$j-1]]; $jj<$$border[$admborder[$j]]; $jj++){ $mtemp+=$$borderbatch[$jj];}
           if ($moffdg < $mtemp) {$moffdg=$mtemp;}}
           if ($memfac == 2) {  # two pairs of vectors
                               $mtemp=($moffdg+$mdiag)*$memfac;
                               if ($mtemp < $mdiag*2) {$mtemp=2*$mdiag;}}
            else             { $mtemp=($moffdg+$mdiag);
                               if ($mtemp < $mdiag*2) {$mtemp=2*$mdiag;}} 
          $temp=0;
      # current combined lines times current combined lines
           for ($j=$first_i; $j<=$i; $j++) {  
               for ($k=$first_i; $k<=$j; $k++) { $temp+= $vval[$k][$j]}}  # 1 task aber nur halbe groesse 
               if ($temp>$maxblock)  { $maxblock=$temp;}
  
      # current c mbined lines times previous combined blocks
           $temp=0;
           for ($j=0; $j<$admborder[0]; $j++) {  
               for ($k=$first_i; $k<=$i; $k++) { $temp+= $vval[$k][$j]}} 
#ok??          if (0.5*$temp>$maxblock)  { $maxblock=0.5*$temp;}
               if ($temp>$maxblock)  { $maxblock=$temp;}
         #  print "checking range 0 : $admborder[0] , $first_i : $i  $temp\n"; 
           for ($j=1; $j<=$#admborder; $j++){
               $temp=0;
               for ($jj=$admborder[$j-1];$jj<$admborder[$j]; $jj++){
               for ($k=$first_i; $k<=$i; $k++) { $temp+= $vval[$k][$jj]}}
         #     print "checking range ", $admborder[$j-1] , ":", $admborder[$j], " $first_i : $i  $temp\n"; 
#ok??          if (0.5*$temp>$maxblock)  { $maxblock=0.5*$temp;}
               if ($temp>$maxblock)  { $maxblock=$temp;}
              }
#          print "combining $first_i : $i, $first_i : $i = $temp \n";
#          print "combine_uptrg  mdg= $mdiag, moff=$moffdg, mtemp= $mtemp bw=$bw cost=",  $maxblock*$costfac, "\n";
#  fuer xx,ww 2  fuer yy zz 4
           if ( ($maxblock*$costfac+$mtemp*$bw+$intcost) > $avcost || $maxmem < $mtemp ) { 
                            push @bordertmp, $$border[$i]; push @admborder, $i;
#                           print "adding new border at $i( ",$$border[$i], ") cost= $mmax dim_bordertmp= $#admborder ",
#                                                        "maxmemory = ",$maxmemblock,"\n"; 
                                                   $mtemp=0;
                                                   $first_i=$i; }
        }
     }

  if ($bordertmp[0]!=1) { unshift @bordertmp,1;}
   push @bordertmp, 151 ;
#   print "bordertmp=",join(':',@bordertmp), "Direction $dir \n";
#   print "retaining $#bordertmp  segments\n";
    if ($#bordertmp < $maxlines) 
      { $avcost=$avcost*($#bordertmp*($#bordertmp+1))/($maxlines*($maxlines+1)); 
       print "too few segments resegmenting with avcost=$avcost\n"; }
  }
    $l=$#bordertmp;
    @$border= @bordertmp ;
  return $l;

}



 sub printstring
 {my ($name,$array)=@_;
  @tmparray=@$array;
  $retstring=" $name=";
  while ($#tmparray > -1 )
    { @tmp=splice @tmparray,0,8 ;
      $retstring.= sprintf "   %s,\n",join(',',@tmp); }
   chop $retstring; chop $retstring; $retstring.="\n";
  print $retstring;
  }


    sub partition_4x 
     { my ($type,$cost4x,$timeitr,$wantseg,$totcsf,$csfbatch,$TASKLIST,$intfile,$maxmem,$bw)=@_;
       my ($tasks,$i,$j);
       my $debug=0;
       $totcost4x=$cost4x; 
       if ($timeitr < 0.1) { $tasks = int(($totcost4x)/(0.1))+1;}
                     else  { $tasks = int(($totcost4x)/($timeitr))+1;} 
       if ($tasks < $wantseg) {$tasks=$wantseg;}
       if ($debug) {print "timeitr=$timeitr  totcost4x=$totcost4x , tasks=$tasks  wantseg=$wantseg \n";}
      $ltimeitr=(($totcost4x+1)/($tasks));
      if ($ltimeitr < 0.1) {$ltimeitr=0.1;}
      if ($ltimeitr > $timeitr) {$ltimeitr=$timeitr;}
      

#  first pass determine upper bound for total cost
      $vol=0;
      $totcost4x=0;
      $istart=1; $ilast=1;
      for ($i=1; $i<=$#$csfbatch; $i++) 
           {$vol+=$$csfbatch[$i];
            if ( ($vol/$totcsf*$cost4x + $vol/($bw*65536) + $intfile/($bw*1048576)) > $ltimeitr || $vol > $maxmem*65536) 
             { $ilast=$i-1;
               if ($ilast < $istart) {$ilast=$istart;}
               else {$vol-=$$csfbatch[$i];}
               $totcost4x=$totcost4x+$vol/$totcsf*$cost4x+$vol/($bw*65536)+ $intfile/($bw*1048576);
               if ($ilast<$i )
                 {$vol=$$csfbatch[$i];
                  $istart=$i;}
               else
                 {$vol=0; $istart=$i+1;}
             }
           }
        if ($istart < $#$csfbatch+1 ) { $totcost4x=$totcost4x+$vol/$totcsf*$cost4x+$vol/($bw*65536)+ $intfile/($bw*1048576);}

     if ($debug) {print "computed upper bound to totalcost= $totcost4x \n";}

#  second pass 

       if ($timeitr < 0.1) { $tasks = int(($totcost4x)/(0.1))+1;}
                     else  { $tasks = int(($totcost4x)/($timeitr))+1;}
       if ($tasks < $wantseg) {$tasks=$wantseg;}
      if ($debug) { print "timeitr=$timeitr  totcost4x=$totcost4x , tasks=$tasks  wantseg=$wantseg \n";}
      $ltimeitr=(($totcost4x+1)/($tasks));
      if ($ltimeitr < 0.1) {$ltimeitr=0.1;}
      if ($ltimeitr > $timeitr) {$ltimeitr=$timeitr;}

      my @zbd=(); push @zbd,1 ;
      if( $debug){ print "generate $tasks tasks with at most $ltimeitr seconds $#$csfbatch statblocks \n";}

      $istart=1;
      $ilast=1;
      $vol=0;
      for ($i=1; $i<=$#$csfbatch; $i++) 
           {
            $vol+=$$csfbatch[$i];
#           print $vol/$totcsf*$cost4x, $vol/($bw*65536) , $intfile/($bw*1048576), "\n";
            if ( ($vol/$totcsf*$cost4x + $vol/($bw*65536) + $intfile/($bw*1048576)) > $ltimeitr || $vol > $maxmem*65536) 
             { $ilast=$i-1;
               if ($ilast < $istart) {$ilast=$istart;}
               else {$vol-=$$csfbatch[$i];}
               $r=sprintf "%-10s | %3d:%3d",$type,$#zbd+1,$#zbd+1;
               push @{$$TASKLIST{'index'}},$r;
               $r=sprintf " %3d:%3d,%3d,%3d ",$istart,$ilast,$istart,$ilast;
               push @{$$TASKLIST{'range'}}, $r;
               $r=$vol/$totcsf*$cost4x;
               push @{$$TASKLIST{'time'}}, $r;
               $r=$vol*.00000001490116119384+$intfile*.00000000093132257461;
               push @{$$TASKLIST{'volume'}}, $r;
               $r=1024*$vol*.00000001490116119384;
               push @{$$TASKLIST{'memory'}}, $r;
               push @{$$TASKLIST{'tasks'}}, 1;
               push @zbd, $ilast+1;
               if ($ilast<$i )
                 {$vol=$$csfbatch[$i];
                  $istart=$i;}
               else
                 {$vol=0; $istart=$i+1;}
             }
           }
        if ($istart < $#$csfbatch+1 ) { $ilast =$#$csfbatch;
                                     $r=sprintf "%-10s | %3d:%3d",$type,$#zbd+1,$#zbd+1;
                                     push @{$$TASKLIST{'index'}},$r;
                                     $r=sprintf " %3d:%3d,%3d,%3d ",$istart,$ilast,$istart,$ilast;
                                     push @{$$TASKLIST{'range'}}, $r;
                                     $r=$vol/$totcsf*$cost4x;
                                     push @{$$TASKLIST{'time'}}, $r;
                                     $r=$vol*.00000001490116119384+$intfile*.00000000093132257461;
                                     push @{$$TASKLIST{'volume'}}, $r;
                                     $r=1024*$vol*.00000001490116119384;
                                     push @{$$TASKLIST{'memory'}}, $r;
                                     push @{$$TASKLIST{'tasks'}}, 1;
                                     push @zbd, $ilast;
                                      }

       if ($debug) {  print join(':',@zbd ), "csfbatch=",$#$csfbatch,"\n";}
        return \@zbd;
        }




