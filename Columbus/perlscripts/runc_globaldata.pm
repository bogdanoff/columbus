package  runc_globaldata;
use Exporter();
@ISA       =qw(Exporter);
@EXPORT    =qw(%kwords %molecule %control @ERRCODE %debug  %direct %clock %nad %efile %grad @MCSCF_wf %MCSCF_wf @MRCI_wf %MRCI_wf
               %translation %mcscfgrad %mcscftrans %cigrad %citrans %trans1e );


# keywords as found in control.run
$kwords{ignore} =1;
# kwords{GEOMOPT}=(0|NAD|GEOM|CROSS)
# kwords{SINGLEGRAD} 
# kwords{SINGLENAD} 
# kwords{SINGLEPOINT} 
# wall clock times 
$clock{ignore}=1;
# executioncontrol
$control{ignore}=1;
$control{USENEWCIGRD}=1;
$control{USENEWCIDENSITY}=1;
$control{CART2INT}="cart2int.x";
$control{GDIIS}="gdiis.x";
$control{INTC}="intc.x";
$control{pcoremem}=100;
$control{coremem}=0;
$control{chains}=0;
$control{chaini}=0;
$control{chainp}=0;
$control{counter_rgf}=0;
$control{cienergy_last}=0;
$control{iter}=0;
$control{thisdrt}=0;
$control{start_iter}=0;

# error codes
$ERRCODE[2]  ='general error';
$ERRCODE[10] ='input file missing';
$ERRCODE[11] ='abnormal program termination';
$ERRCODE[101]='calculation not converged';
$ERRCODE[201]='cigrd error: inconsistent energies';
$ERRCODE[301]='rgf error';

# debug levels
#%debug=();
$debug{debug_runc}=0;
$debug{debug_geo}=0 ;
$debug{debug_integ}=0;
$debug{debug_scf}=0;
$debug{debug_mcscf}=0;
$debug{debug_ci}=0;
$debug{debug_dens}=0;
$debug{debug_deriv}=0;
$debug{debug_par}=0;

# directories
#%direct=(l);
$direct{JDIR}=`pwd`;
chop $direct{JDIR};
$direct{WORK}="WORK";
$direct{MODIR}="MOCOEFS";
$direct{LIST} = "LISTINGS";        # listing files
$direct{REST} = "RESTART";         # MCSCF restart files and soinfo.dat from hermit
$direct{GEOMDIR} = "GEOMS";
$direct{GRADDIR} = "GRADIENTS";
$direct{MOLDEN} = "MOLDEN";
$direct{COSMO}  = "COSMO";
$direct{HESSIAN}  = "HESSIAN";
$direct{list} ="$direct{JDIR}/$direct{LIST}";
$direct{modir} ="$direct{JDIR}/$direct{MODIR}";
$direct{rest} ="$direct{JDIR}/$direct{REST}";
$direct{geom} ="$direct{JDIR}/$direct{GEOMDIR}";
$direct{grad} = "$direct{JDIR}/$direct{GRADDIR}";
$direct{work} ="$direct{JDIR}/$direct{WORK}";
$direct{molden} ="$direct{JDIR}/$direct{MOLDEN}";

#nadcoupling
# Nullsetzen funktioniert nicht ?????
# fehler  runc_globaldata does not return in true value in the use statement????

#%nad=();
$nad{nadcalc}=1;
$nad{nnadcoupl}=1;
$nad{nadstate1}=1;
$nad{nadstate2}=1;
$nad{nadcalc}=1;  # saves nadcalc value in cigrdin 
$nad{ngrad}=1;    
$nad{energy1}=0;
$nad{energy2}=0;
$nad{deltae}=0;
@{$nad{nad_drt1}}=(1);
@{$nad{nad_drt2}}=(1);
@{$nad{nad_state1}}=(1);
@{$nad{nad_state2}}=(1);
@{$nad{grad_drt}}=(1);
@{$nad{grad_state}}=(1);
%{$nad{dens_calc}}={1};

#%efile=();
$efile{ntransitions}=0;
@{$efile{transition}}=();
@{$efile{roots}}=();
$efile{nroots}=0;


#Single State GeomOptimization
$grad{drt1}=0;
$grad{state1}=0;
$grad{energy}=0;


#Brief summary of computed data
#per geom optimization cycle

$summary{ignore}=1;
#${summary}{energies}{scf}   - single number  term symbols for identification! 
#${summary}{energies}{mcscf} - multiple states
#${summary}{energies}{mrci}  - multiple states, multiple DRTs

#if necessary the individual 
#summaries can be combined into
#an array for post-analysis

@complete_summary=();

# characteristics of the molecule 
$molecule{ignore}=1;
#$molecule{pointgroup}                   ok
#$molecule{mass}                         ok
#$molecule{formula}                      ok
#$molecule{irreps}{nsym}                 ok
#$molecule{irreps}[symbol]               #translation number to symbol 
#$molecule{irreps}[nbfao]                ok

#%translation=();  # mapping between DRT encoded state (^mult irrep) and DRT index
# ${translation}{cidrt}{statesymbol}=DRTindex
# ${translation}{cidrt}[DRTindex]= statesymbol

# MCSCF wavefunction
@MCSCF_wf=();
$MCSCF_wf{ignore}=1;
#$MCSCF_wf[DRTx]{symmetry}
#$MCSCF_wf[DRTx]{multiplicity}
#$MCSCF_wf[DRTx]{ncsf}
#$MCSCF_wf[DRTx]{DOCC}
#$MCSCF_wf[DRTx]{ACT}
#$MCSCF_wf{ndrt}                

# MRCI wavefunction
@MRCI_wf=();
$MRCI_wf{ignore}=1;
#$MRCI_wf[DRTx]{symmetry}
#$MRCI_wf[DRTx]{multiplicity}
#$MRCI_wf[DRTx]{ncsf}
#$MRCI_wf[DRTx]{FROZEN}
#$MRCI_wf[DRTx]{DOCC}
#$MRCI_wf[DRTx]{ACT}
#$MRCI_wf{ndrt}                
$geomconv{ignore}=1;

$mcscfgrad{ignore}=1;
$mcscftrans{ignore}=1;
$cigrad{ignore}=1;
$citrans{ignore}=1;
$trans1e{ignore}=1;

