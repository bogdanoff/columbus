#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


# Script to write molden files with molecular orbitals
# from a Columbus-calculation with Seward integrals.
# usage: ColSewardMO2molden.pl <geom> <sewardls> <mocoef>
#   where: <geom> is the Columbus geom-file
#          <sewardls> is the listing from Seward
#                     (needs BSSHOW in input!)
#          <mocoef> is the mocoe-file, e.g. mocoef_mc.sp

# version 1.0beta
#   author: Matthias Ruckenbauer matthias.ruckenbauer@gmail.com
#   date: November 27th 2013

# TODO:
#   *) f-functions
#   *) cartesian d-functions
#   *) more safety-checks
#   *) enance reading of "Petite list basis functions"
#   *) symmetry (other than C1)
#   *) occupation and eigenvalues of MOs (at the moment
#       output is in transmo-style :-( )

use strict;
use warnings;

my ( $geomfile, $sewardls, $mofile );
my ( %BasInfo, %GeomInfo, %MOInfo, $moldenfile );
my ( @buffer );

# %Basinfo - Hash with basis set information
#   { 'spherical' => +1 ... spherical basis (d/f)
#                   -1 ... cartesian basis
#     'allmaxl' => maximum l value in all basis sets
#     %'$atyp' - Hashes with information per atom type
#       { 'maxl' => maximum l value (1-s, 2-p, 3-d, ...)
#         'nprim' => nprim[l]-array of the number of primitives
#         'ncon' => ncon[l]-array of the number of contractions
#         'exp' => exp[l][prim]-array of exponents
#         'coef' => coef[l][prim][]-array of coefficients to the exponents
#       }
#    }

# %GeomInfo - Hash with geometry information
#   { 'atsym' => atsym[]-array with the atomic symbols (order as in geom-file)
#     'nucch' => nucch[]-array with the nuclear charges (--"--)
#     'cartx' => cartx[]-array with the cartesian x coords  (--"--)
#     'carty'                                     y
#     'cartz'                                     z
#     'amass' => amass[]-array with the atomic masses  (--"--)
#     'natom' => number of atoms in the geom-file
#   }

# %MOInfo - Hash with MO information
#   { 'nmos' => number of MOs
#     'naos' => number of AOs
#     'coef' => coef[mo][ao]-array of the mo-coefficients
#   }

$geomfile = shift(@ARGV);    # geom
if ( not -f "$geomfile" ) { die "cannot find geometry file >$geomfile<\n"; }
$sewardls = shift(@ARGV);    # seward listing
if ( not -f "$sewardls" ) { die "cannot find seward listing >$sewardls<\n"; }
$mofile = shift(@ARGV);      # MOCOEFS/mocoef_mc.sp
if ( not -f "$mofile" ) { die "cannot find MO-coefficient file >$mofile<\n"; }

open(SEWL,$sewardls) or die "cannot read $sewardls\n$!\n";
@buffer=<SEWL>;
close(SEWL);
if(grep(/bsshow/i,@buffer)<1) { die "You need to use BSSHOW in the seward input\n"; }

$moldenfile = ${mofile};
$moldenfile =~ s/\.sp//;     # remove eventual .sp
while ( index( "$moldenfile", "/" ) > -1 )
{
  $moldenfile =~ s/^.*\///;    # remove any directory before the filename
}
$moldenfile = "${moldenfile}.molden";

#print "reading from:\n\t$geomfile\n\t$sewardls\n\t$mofile\n";
print "writing to:\n\t$moldenfile\n";

&readGeom( $geomfile, \%GeomInfo );
&readBasis( $sewardls, \%BasInfo );
if($BasInfo{'spherical'}<0 and $BasInfo{'allmaxl'}>2)
{
  die 'I have not learned yet how to handle cartesian d- or f-functions\n';
}
&readMOs( $mofile, \%MOInfo );
&transMOs( $sewardls, \%MOInfo );
&writeMolden( $moldenfile, \%GeomInfo, \%BasInfo, \%MOInfo );

#==============================================================================

sub readGeom {
  my ( $file, $gi, $count );
  my (@line);

  $file = shift(@_);
  $gi   = shift(@_);
  open( GEOM, $file ) or die "cannot read geometry from $file\n$!\n";
  $count=0;
  while (<GEOM>)
  {
    $count++;
    chomp;
    s/^\s+//;
    @line = split /\s+/;
    push( @{ $$gi{'atsym'} }, $line[0] );
    push( @{ $$gi{'nucch'} }, $line[1] );
    push( @{ $$gi{'cartx'} }, $line[2] );
    push( @{ $$gi{'carty'} }, $line[3] );
    push( @{ $$gi{'cartz'} }, $line[4] );
    push( @{ $$gi{'amass'} }, $line[5] );
  }
  $$gi{'natom'} = $count;
#  foreach ( keys(%$gi) )
#  {
#    if("$_" eq "natom")
#    {
#      print "DEBUGMR: $_ $$gi{$_}\n";
#    }
#    else
#    {
#      print "DEBUGMR: $_ @{$$gi{$_}}\n";
#    }
#  }
} ## end sub readGeom

#------------------------------------------------------------------------------

sub readBasis {

  # BEWARE! THIS IS WITHOUT SYMMETRY!

  # Basic assumptions!
  #
  ## C1 Symmetry!
  ## Atoms in the same order as in the geom-file
  ## Only one kind of Basis set per atom-type (H,C,N,O,...)
  ### (this is the normal case, because in colinp one can give only one basis set per atom-type)
  ## Either *all* (d-) basis sets are spherical or *none* - no mixing
  ## Certain formats of the seward output are assumed
  ### Each basic basis set definition (nprim, ncon, ...) start with /Basis set label:/
  ### The actual listing of these values is titled with a line containing /Cartesian/ and /Spherical/ and /Contaminant/
  ### The primitive basis function listings starts with /Primitive Basis info/
  ### Each basis set therein starts with /Basis set:/

  # go through the file, search for /Basis set label:/
  # count the number of function sets
  ## s/^\s*Basis set label://;
  ## get the index of the first "."
  ## The atomic symbol for this basis set is [0:index-1] -> $atyp
  ## The title of the basis set is [index+1:] (we don't need it if there is only one basis per type)

# the second next line has to contain /Valence basis set/ or else die -> unknown format

# from there on go through the file, search for /Cartesian/ and /Spherical/ and /Contaminant/
# get the indices for "Cartesian" and "Spherical"
# from there go on till blank line
  ## for each line get the first letter (s,p,d,...)
  ## set $maxl accordingly to the "highest" letter (s->1, p->2, d->3,...)
  ## set $l according to the current letter (s->1, p->2, d->3, ...)
  ## for each line get the $nPrim and $nCon
  ## ${$anPrim{$atsym}}[$l]=$nPrim;
  ## ${$anCon{$atsym}}[$l]=$nCon;
  ## for each line inbetween get the index of " X "
  ## if any index of " X " is greater than the index of "Spherical" -> $isph = 1;

  # go on through file, search for /Primitive Basis info/
  # for each set of basis functions
  # from there on search for /Basis set:/
  ## s/^\s+Basis set://
  # go on through file, search for /Type/
  # next line gives type of basis function (s,p,d,f,...) -> use lc()
  # omit one line
  # read the exponents and coefficients for each l (1->s, 2->p, 3->d, ...)
  ## format is: num(d) exponent(e;s/D/E/) contr.coeff.(f f f ...)

  # all information is stored in a hash %BasInfo
  ## first level indices are the atom types as found in the basis set labels
  ## second level indices are 'maxl', 'ncon', 'nprim', 'exp' and 'coef'
  ## $Basinfo{$atyp}{'exp'} and $BasInfo{$atyp}{'coef'} are multidim-arrays

  my ( $file, $bi );
  my ( $atyp, $baslabel, $l, $curmaxl, @anPrim, @anCon, @aexp, @acoef, @ssym );
  my ( $i,    $j, $k, @line, $cindex, $cartindex, $sphindex, $nbsets, $bset );

  $ssym[1] = 's';
  $ssym[2] = 'p';
  $ssym[3] = 'd';
  $ssym[4] = 'f';
  $ssym[5] = 'g';

  $file = shift(@_);    # the name of the seward listing
  $bi   = shift(@_);    # the hash where to store the information in

  $nbsets = 0;
  $$bi{'spherical'} = -1;
  $$bi{'allmaxl'} = 0;

  open( SEWL, $file ) or die "cannot read basis from $file\n$!\n";
  while (<SEWL>)
  {
    if (/\s+Basis set label:/)
    {
      $nbsets++;
      $atyp     = '';
      $baslabel = '';
      $curmaxl  = 0;
      undef(@anPrim);
      undef(@anCon);

      chomp;
      s/\s+//;
      s/Basis set label://;
      $cindex = index( $_, '.' );
      if ( $cindex < 0 )
      {
        die "Basis set label not in known format - expecting \'.\'\n$_\n";
      }
      $atyp = substr $_, 0, $cindex;
      $baslabel = substr $_, $cindex + 1;
      $baslabel =~ s/\s+//g;
      $baslabel = lc($baslabel);
      
      $$bi{$atyp}{'maxl'} = 0;

      while ( not( /Cartesian/ and /Spherical/ and /Contaminant/ ) )
      {
        $_ = <SEWL>;
      }
      $cartindex = index( $_, 'Cartesian' );
      $sphindex  = index( $_, 'Spherical' );
      $_         = <SEWL>;
      while ( not /^\s*$/ )
      {
        $l = 0;
        $cindex = index( $_, ' X ' ) + 1;
        if ( $cindex > $sphindex ) { $$bi{'spherical'} = 1; }

        # add here a check against the cartesian later
        # add here a check for 'Contaminant' later

        chomp;
        s/^\s+//;
        @line = split /\s+/;

        if    ( lc( $line[0] ) eq "s" ) { $l = 1; }
        elsif ( lc( $line[0] ) eq "p" ) { $l = 2; }
        elsif ( lc( $line[0] ) eq "d" ) { $l = 3; }
        elsif ( lc( $line[0] ) eq "f" ) { $l = 4; }
        if ( $l > $$bi{$atyp}{'maxl'} ) { $$bi{$atyp}{'maxl'} = $l; }
        if ( $l > $$bi{'allmaxl'} ) { $$bi{'allmaxl'} = $l; }

        if ( defined( $anPrim[$l] ) )
        {
          die "found two sets for shell=$l\n$_\n";
        }
        $anPrim[$l] = $line[1];
        $anCon[$l]  = $line[2];

        $_ = <SEWL>;
      }    # while($_ ne /^\s*$/)
      $$bi{$atyp}{'nprim'} = [@anPrim];
      $$bi{$atyp}{'ncon'}  = [@anCon];
      if ( ( $curmaxl > 3 ) and ( $$bi{'spherical'} > 0 ) )
      {
        die "spherical f-functions not yet implemented!\n";
      }
    }    # if(/Basis set label:/)

    if (/\+\+       Primitive basis info:/)
    {

      for ( $bset = 1; $bset <= $nbsets; $bset++ )
      {
        while ( not /Basis set:/ )
        {
          $_ = <SEWL>;
        }

        #print $_;
        chomp;
        s/\s+//;
        s/Basis set://;
        $cindex = index( $_, '.' );
        if ( $cindex < 0 )
        {
          die "Basis set label not in known format - expecting \'.\'\n$_\n";
        }
        $atyp = substr $_, 0, $cindex;
        $baslabel = substr $_, $cindex + 1;
        $baslabel =~ s/\s+//g;
        $baslabel = lc($baslabel);

        #print "found Basis set $baslabel for atom type $atyp\n";
        $_ = <SEWL>;    # skip blank line

        for ( $i = 1; $i <= $$bi{$atyp}{'maxl'}; $i++ )
        {
          while ( not /^\s+Type\s*$/ )
          {
            $_ = <SEWL>;
          }
          $_ = <SEWL>;
          chomp;
          s/^\s+//;
          @line = split /\s+/;

          # for checking purposes only
          if    ( lc( $line[0] ) eq 's' ) { $l = 1; }
          elsif ( lc( $line[0] ) eq 'p' ) { $l = 2; }
          elsif ( lc( $line[0] ) eq 'd' ) { $l = 3; }
          elsif ( lc( $line[0] ) eq 'f' ) { $l = 4; }

          if ( $l != $i )
          {
            die "unexpected format for Primitive Basis functions\n";
          }

          <SEWL>;
          for ( $j = 1; $j <= $$bi{$atyp}{'nprim'}[$l]; $j++ )
          {
            $_ = <SEWL>;
            chomp;
            s/^\s+//;
            s/D/E/g;
            @line = split /\s+/;
            $$bi{$atyp}{'exp'}[$i][$j] = $line[1];
            for ( $k = 1; $k <= $$bi{$atyp}{'ncon'}[$l]; $k++ )
            {
              $$bi{$atyp}{'coef'}[$i][$j][$k] = $line[ $k + 1 ];
            }
          }    # for($j=1;$j<=$BasInfo{$atyp}{'nprim'}[$l];$j++)
        }    # for($i=1;$i<=$BasInfo{$atyp}{'maxl'};$i++)
      }    # for($bset=1;$bset<=$nbsets;$nbsets)
    }    # if(/\+\+       Primitive basis info:/)
  }    # while(<SEWL>)
  close(SEWL);

#  foreach ( keys %$bi )
#  {
#    if ( "$_" eq "spherical" or   "$_" eq "allmaxl" )
#    {
#      printf "DEBUGMR: %9s %2d\n", $_, $$bi{$_};
#    }
#    else
#    {
#      printf "DEBUGMR:   -- %2s --\n", $_;
#      printf "DEBUGMR: maxl   %3d\n",  $$bi{$_}{'maxl'};
#      printf "DEBUGMR: nprim ";
#      for ( $i = 1; $i <= $$bi{$_}{'maxl'}; $i++ )
#      {
#        printf " %3d", $$bi{$_}{'nprim'}[$i];
#      }
#      print "\n";
#      printf "DEBUGMR: ncon  ";
#      for ( $i = 1; $i <= $$bi{$_}{'maxl'}; $i++ )
#      {
#        printf " %3d", $$bi{$_}{'ncon'}[$i];
#      }
#      print "\n";
#      for ( $i = 1; $i <= $$bi{$_}{'maxl'}; $i++ )
#      {
#        for ( $j = 1; $j <= $$bi{$_}{'nprim'}[$i]; $j++ )
#        {
#          printf "DEBUGMR: %1s%3d: %12.5e |", $ssym[$i], $j, $$bi{$_}{'exp'}[$i][$j];
#          for ( $k = 1; $k <= $$bi{$_}{'ncon'}[$i]; $k++ )
#          {
#            printf " %12.5e", $$bi{$_}{'coef'}[$i][$j][$k];
#          }
#          print "\n";
#        }
#      }
#      print "\n";
#    } ## end else [ if ( "$_" eq "spherical"...)]
#    print "------------------------------\n";
#
#  } ## end foreach ( keys %$bi )
} ## end sub readBasis

#------------------------------------------------------------------------------

sub readMOs {
  my ( $file, $mi, $i, $j, $k, @rawmos );
  my (@line);
  $file = shift(@_);
  $mi   = shift(@_);

  open( MOFILE, $file ) or die "Cannot read MOs from $file\n$!\n";

  while (<MOFILE>)
  {
    if (/header/)
    {
#      print STDOUT "DEBUGMR: found MO header\n";
      # next line contains the number of header lines to skip
      $_ = <MOFILE>;
      chomp;
      s/\s+//g;
#      print STDOUT "DEBUGMR: skipping $_ lines\n";
      for ( $i = 1; $i <= $_; $i++ )
      {
        <MOFILE>;
      }
      last;
    }
  }
  $_ = <MOFILE>;
  chomp;
  s/^\s+//;
  if ( $_ > 1 ) { die "This is probably a symmetry run. Will not work. I give up\n"; }
  $_ = <MOFILE>;
  chomp;
  s/^\s+//;
  @line        = split /\s+/;
  $$mi{'nmos'} = $line[0];
  $$mi{'naos'} = $line[1];
  
  # skip three lines
  for ( $i = 0; $i < 3; $i++ )
  {
    <MOFILE>;
  }

  #read in all the orbitals
  while (<MOFILE>)
  {
    chomp;
    s/^\s*//;
    s/\s*$//;
    s/D/E/g;
    if (/^[a-zA-Z]/) { last; }
    @line = split /\s+/;
    push( @rawmos, @line );
  }
  close(MOFILE);

  for ( $i = 0; $i < $$mi{'nmos'}; $i++ )
  {
    for ( $j = 0; $j < $$mi{'naos'}; $j++ )
    {
      $$mi{'coef'}[$i+1][$j+1] = $rawmos[ $i * $$mi{'naos'} + $j ];
    }
  }

#  print "DEBUGMR: nmos $$mi{'nmos'}\n";
#  print "DEBUGMR: naos $$mi{'naos'}\n";
#
#  for ( $i = 0; $i < $$mi{'nmos'}; $i++ )
#  {
#    printf "DEBUGMR: mo %3d |", $i + 1;
#    $k=0;
#    for ( $j = 0; $j < $$mi{'naos'}; $j++ )
#    {
#      printf " %12.5e", $$mi{'coef'}[$i+1][$j+1];
#      $k++;
#      if($k >= 5)
#      {
#        $k=0;
#        print "\n                |";
#      }
#    }
#    print "\n";
#  }
} ## end sub readMOs

#------------------------------------------------------------------------------

sub transMOs {
  my ($sls,$mi,$index,$npsets,$ndsets,$nfsets,@ssets,@psets,@dsets,@fsets,@types,@newmos, $i, $j, $k, $rdind1, $rdind2);
  
  $sls = shift(@_); # seward listing
  $mi  = shift(@_); # \%MOInfo
  
  
  open( SEWLS, "<$sls" ) or die "Cannot open $sls to read\n$!\n";
  while (<SEWLS>)
  {
    unless (/Petite list Basis Functions/) { next; }
    for ( $i = 1; $i <= 6; $i++ ) { <SEWLS>; }    # skip 6 lines
    $_ = <SEWLS>;
    chomp;
    s/^\s+//;
    $index  = 0;
    $npsets = 0;
    $ndsets = 0;
    $nfsets = 0;
    @ssets  = ();
    @psets  = ();
    @dsets  = ();
    @fsets  = ();

    push( @types, -99 );

    while ( $_ ne '' )    # go on till blank line
    {
      $index++;

      if (/s/)
      {

        # s-functions remain as they are
        # push( @ssets, $index );
        push( @types, 0 );    # flag for s-functions
      }
      elsif (/px/)
      {

        # open a new pset, mark the fields for py and pz as unset (-1)
        $npsets++;
        $psets[$npsets][0] = $index;
        $psets[$npsets][1] = -1;
        $psets[$npsets][2] = -1;
        push( @types, 1 );    # flag for start of a p-set
      }
      elsif (/py/)
      {
        for ( $i = 1; $i <= $npsets; $i++ )
        {

          # search the next pset with unset py
          if ( $psets[$i][1] < 0 )
          {
            $psets[$i][1] = $index;
            push( @types, -1 );    # this is a flag for "ignore, has been treated elsewhere"
            last;                  # only use the first of course
          }
        }
      }
      elsif (/pz/)
      {
        for ( $i = 1; $i <= $npsets; $i++ )
        {

          # search the next pset with unset pz
          if ( $psets[$i][2] < 0 )
          {
            $psets[$i][2] = $index;
            push( @types, -1 );    # this is a flag for "ignore, has been treated elsewhere"
            last;                  # only use the first of course
          }
        }
      }
      elsif (/d2-/)
      {

        # open a new dset, mark the fields for d1- d0 d1+ and d2+ as unset (-1)
        $ndsets++;
        $dsets[$ndsets][0] = $index;    # d2-
        $dsets[$ndsets][1] = -1;        # d1-
        $dsets[$ndsets][2] = -1;        # d0
        $dsets[$ndsets][3] = -1;        # d1+
        $dsets[$ndsets][4] = -1;        # d2+
        push( @types, 2 );              # flag for start of a d-set
      }
      elsif (/d1-/)
      {
        for ( $i = 1; $i <= $ndsets; $i++ )
        {
          if ( $dsets[$i][1] < 0 )
          {
            $dsets[$i][1] = $index;
            push( @types, -1 );         # this is a flag for "ignore, has been treated elsewhere"
            last;                       # only use the first of course
          }
        }
      }
      elsif (/d0/)
      {
        for ( $i = 1; $i <= $ndsets; $i++ )
        {
          if ( $dsets[$i][2] < 0 )
          {
            $dsets[$i][2] = $index;
            push( @types, -1 );         # this is a flag for "ignore, has been treated elsewhere"
            last;                       # only use the first of course
          }
        }
      }
      elsif (/d1\+/)
      {
        for ( $i = 1; $i <= $ndsets; $i++ )
        {
          if ( $dsets[$i][3] < 0 )
          {
            $dsets[$i][3] = $index;
            push( @types, -1 );         # this is a flag for "ignore, has been treated elsewhere"
            last;                       # only use the first of course
          }
        }
      }
      elsif (/d2\+/)
      {
        for ( $i = 1; $i <= $ndsets; $i++ )
        {
          if ( $dsets[$i][4] < 0 )
          {
            $dsets[$i][4] = $index;
            push( @types, -1 );         # this is a flag for "ignore, has been treated elsewhere"
            last;                       # only use the first of course
          }
        }
      }

      $_ = <SEWLS>;
      chomp;
      s/^\s+//;
    } ## end while ( $_ ne '' )
  } ## end while (<SEWLS>)
  close(SEWLS);

  # now reorder the original mos
  @newmos = ();
  for ( $i = 1; $i <= $$mi{'nmos'}; $i++ )
  {
    $npsets = 0;
    $ndsets = 0;

    for ( $j = 1; $j <= $$mi{'naos'}; $j++ )
    {

      if ( $types[$j] < 0 )
      {

        # skip this one
        next;
      }
      elsif ( ( -1 < $types[$j] ) and ( $types[$j] < 1 ) )    # == 0
      {

        # s-function is pushed
        push( @newmos, $$mi{'coef'}[$i][$j] );
      }
      elsif ( ( 0 < $types[$j] ) and ( $types[$j] < 2 ) )     # == 1
      {
        $npsets++;

        # begin of a p-set
        # print STDOUT "DEBUGMR: p-set \$j=$j type=$types[$j] pset=$npsets \n";
        if ( $psets[$npsets][0] != $j ) { print STDOUT "DEBUGMR WARNING psets[npsets] !=j\n\t$psets[$npsets][0] != $j\n"; }
        $rdind1 = $psets[$npsets][0];
        push( @newmos, $$mi{'coef'}[$i][$rdind1] );     # px directly used
        $rdind1 = $psets[$npsets][1];
        push( @newmos, $$mi{'coef'}[$i][$rdind1] );     # py
        $rdind1 = $psets[$npsets][2];
        push( @newmos, $$mi{'coef'}[$i][$rdind1] );     # pz
      }
      elsif ( ( 1 < $types[$j] ) and ( $types[$j] < 3 ) )     # == 2
      {
        $ndsets++;
        # begin of a d-set
        # ordering in $dsets is: d-2, d-1,  d0,  d1,  d2
        # ordering in molden is:  d0,  d1, d-1,  d2, d-2
        # no scaling needed
        if ( $dsets[$ndsets][0] != $j ) { print STDOUT "DEBUGMR WARNING dsets[ndsets] !=$j\n\t$dsets[$ndsets][0] != $j\n"; }
        $rdind1 = $dsets[$ndsets][2];
        push( @newmos, $$mi{'coef'}[$i][$rdind1] );
        $rdind1 = $dsets[$ndsets][3];
        push( @newmos, $$mi{'coef'}[$i][$rdind1] );
        $rdind1 = $dsets[$ndsets][1];
        push( @newmos, $$mi{'coef'}[$i][$rdind1] );
        $rdind1 = $dsets[$ndsets][4];
        push( @newmos, $$mi{'coef'}[$i][$rdind1] );
        $rdind1 = $dsets[$ndsets][0];
        push( @newmos, $$mi{'coef'}[$i][$rdind1] );
      }
    }
  }
  
  $$mi{'coef'} = ();
  # write information back to %MOInfo
  for ( $i = 0; $i < $$mi{'nmos'}; $i++ )
  {
    for ( $j = 0; $j < $$mi{'naos'}; $j++ )
    {
      $$mi{'coef'}[$i+1][$j+1] = $newmos[ $i * $$mi{'naos'} + $j ];
    }
  }

#  for ( $i = 0; $i < $$mi{'nmos'}; $i++ )
#  {
#    printf "DEBUGMR: mo %3d |", $i + 1;
#    $k=0;
#    for ( $j = 0; $j < $$mi{'naos'}; $j++ )
#    {
#      printf " %12.5e", $$mi{'coef'}[$i+1][$j+1];
#      $k++;
#      if($k >= 5)
#      {
#        $k=0;
#        print "\n                |";
#      }
#    }
#    print "\n";
#  }
}
#------------------------------------------------------------------------------

sub writeMolden {
  my ($mf,$gi,$bi,$mi,$fh);
# called as:
# &writeMolden( $moldenfile, \%GeomInfo, \%BasInfo, \%MOInfo );
  $mf = shift(@_);
  $gi = shift(@_);
  $bi = shift(@_);
  $mi = shift(@_);
  
  open($fh,">$moldenfile") or die "cannot write to $moldenfile\n$!\n";
  
  print $fh "[MOLDEN FORMAT]\n";
  if($$bi{'spherical'}>0)
  {
    print $fh "[5D]\n[7F]\n";
  }
  &writeMoldenATOMS($fh,$gi);
  &writeMoldenGTO($fh,$bi,$gi);
  &writeMoldenMO($fh,$mi);
}

#------------------------------------------------------------------------------

sub writeMoldenATOMS {
  my($fh,$gi,$i);
  $fh = shift(@_);
  $gi = shift(@_);
  print $fh "[ATOMS] AU\n";
  for($i=0;$i<$$gi{'natom'};$i++)
  {
    printf $fh " %2s %3d %3d % 15.10f % 15.10f % 15.10f\n", $$gi{'atsym'}[$i], $i+1, $$gi{'nucch'}[$i], $$gi{'cartx'}[$i], $$gi{'carty'}[$i], $$gi{'cartz'}[$i];
  }  
}

#------------------------------------------------------------------------------

sub writeMoldenGTO {
  my($fh,$bi,$gi,$i,$l,$prim,$con,@ssym,$as);
  
# %Basinfo - Hash with basis set information
#   { 'spherical' => +1 ... spherical basis (d/f)
#                   -1 ... cartesian basis
#     'allmaxl' => maximum l value in all basis sets
#     %'$atyp' - Hashes with information per atom type
#       { 'maxl' => maximum l value (1-s, 2-p, 3-d, ...)
#         'nprim' => nprim[l]-array of the number of primitives
#         'ncon' => ncon[l]-array of the number of contractions
#         'exp' => exp[l][prim]-array of exponents
#         'coef' => coef[l][prim][]-array of coefficients to the exponents
#       }
#    }


  $fh = shift(@_);
  $bi = shift(@_);
  $gi = shift(@_);
  
  $ssym[1] = 's';
  $ssym[2] = 'p';
  $ssym[3] = 'd';
  $ssym[4] = 'f';
  $ssym[5] = 'g';
  
  print $fh "[GTO]\n";
  for($i=0;$i<$$gi{'natom'};$i++)
  {
    $as = $$gi{'atsym'}[$i];
    printf $fh " %3d 0\n",$i+1;
    for($l=1;$l<=$$bi{$as}{'maxl'};$l++)
    {
      for($con=1;$con<=$$bi{$as}{'ncon'}[$l];$con++)
      {
        printf $fh " %2s %3d 1.00\n",$ssym[$l],$$bi{$as}{'nprim'}[$l];
        for($prim=1;$prim<=$$bi{$as}{'nprim'}[$l];$prim++)
        {
          printf $fh " %15.8e %15.8e\n",$$bi{$as}{'exp'}[$l][$prim], $$bi{$as}{'coef'}[$l][$prim][$con];
        }
      }
    }
    print $fh "\n";
  }
}

#------------------------------------------------------------------------------

sub writeMoldenMO {
  my($fh,$mi,$mo,$ao);
  $fh=shift(@_);
  $mi=shift(@_);
# %MOInfo - Hash with MO information
#   { 'nmos' => number of MOs
#     'naos' => number of AOs
#     'coef' => coef[mo][ao]-array of the mo-coefficients
#   }
  print $fh "[MO]\n";
  for($mo=1;$mo<=$$mi{'nmos'};$mo++)
  {
    printf $fh "Sym = A\n Ene = %20.12e\n Spin = ALPHA\n Occup = %20.12e\n",$mo,1.0;
    for($ao=1;$ao<=$$mi{'naos'};$ao++)
    {
      printf $fh "  %4d % 24.16e\n",$ao,$$mi{'coef'}[$mo][$ao];
    }
  }
}

#------------------------------------------------------------------------------
