#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


#%labtomass=( H => 1 , HE => 4 , O => 8, C => 12, N=> 14); 
 %labtomass=( H => 1 , HE => 1 , O => 1, C => 1, N=> 1); 

 $fin=shift @ARGV;

 open FIN,"<$fin" || die("could not open $fin\n");

 $natom=<FIN>; chop $natom;
 <FIN>;
 for ($iatom=1; $iatom<=$natom; $iatom++) { $l=<FIN>; chop $l; 
                                            $l=~s/^ *//; 
                                           ($label[$iatom],$x[$iatom],$y[$iatom],$z[$iatom]) = split(/\s+/,$l); 
                                            $mass[$iatom]=$labtomass{$label[$iatom]}}

 print "input data \n";

 $totmass=0.0; $cmx=0.0; $cmy=0.0;$cmz=0.0;

 for ($iatom=1; $iatom<=$natom; $iatom++) { 
    $cmx=$cmx+$x[$iatom]*$mass[$iatom];
    $cmy=$cmy+$y[$iatom]*$mass[$iatom];
    $cmz=$cmz+$z[$iatom]*$mass[$iatom];
    $totmass=$totmass+$mass[$iatom];
    
    printf "  %8s  %10.5f  %10.5f  %10.5f  %10.5f \n", $label[$iatom],$x[$iatom],$y[$iatom],$z[$iatom],$mass[$iatom];}

    printf " Center of Mass =   %12.8f  %12.8f  %12.8f \n", $cmx/$totmass, $cmy/$totmass, $cmz/$totmass; 
 

    printf " Shifting orign to center of mass, new coordinates \n";

 for ($iatom=1; $iatom<=$natom; $iatom++) {
    $xnew[$iatom]=$x[$iatom] - $cmx/$totmass;
    $ynew[$iatom]=$y[$iatom] - $cmy/$totmass;
    $znew[$iatom]=$z[$iatom] - $cmz/$totmass;
    printf "  %8s  %10.5f  %10.5f  %10.5f  %10.5f \n", $label[$iatom],$xnew[$iatom],$ynew[$iatom],$znew[$iatom],$mass[$iatom];
    }

 printf " Computing the tensor of moment of inertia \n";

  @I=();

 for ($iatom=1; $iatom<=$natom; $iatom++) {
   $I[1][1]+= $mass[$iatom]*($ynew[$iatom]*$ynew[$iatom]+$znew[$iatom]*$znew[$iatom]);
   $I[1][2]+= -$mass[$iatom]*$xnew[$iatom]*$ynew[$iatom];
   $I[1][3]+= -$mass[$iatom]*$xnew[$iatom]*$znew[$iatom];
   $I[2][1]+= -$mass[$iatom]*$xnew[$iatom]*$ynew[$iatom];
   $I[2][2]+= $mass[$iatom]*($xnew[$iatom]*$xnew[$iatom]+$znew[$iatom]*$znew[$iatom]);
   $I[2][3]+= -$mass[$iatom]*$ynew[$iatom]*$znew[$iatom];
   $I[3][1]+= -$mass[$iatom]*$xnew[$iatom]*$znew[$iatom];
   $I[3][2]+= -$mass[$iatom]*$ynew[$iatom]*$znew[$iatom];
   $I[3][3]+= $mass[$iatom]*($xnew[$iatom]*$xnew[$iatom]+$ynew[$iatom]*$ynew[$iatom]);
 }

  print "Tensor of inertia \n [";
  for ($i=1; $i<=3; $i++) { for ($j=1; $j<=3; $j++) { print "   ", $I[$i][$j] ;} print " ;";}
  print "] \n  Coordinates  \n [";
 for ($iatom=1; $iatom<=$natom; $iatom++) {
    printf "  %10.5f  %10.5f  %10.5f   ;", $xnew[$iatom],$ynew[$iatom],$znew[$iatom];
    }
 print "] \n";

