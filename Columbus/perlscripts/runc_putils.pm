package  runc_putils;
use Exporter();
@ISA       =qw(Exporter);
@EXPORT    =qw(createlocdir removelocdir pcheck pcallprog );
use File::Copy 'cp';  # use perl cp
use Cwd;
use Shell qw(pwd cd date mv cat);
use runc_globaldata;


 sub pcheck
  { my ($proprefix);
    $progprefix = shift (@_);

    if (!-s "bummer") {$errno = $! + 256 ;}
#
    open (BUMMER, "bummer");
     while ( <BUMMER> )
      {if ( ( ! /normal/ ) && ( ! /warning/) ) {$errno = $! + 256 ;}}
    close (BUMMER);
#
   &analyse_output("$progprefix.x");
   if ( $progprefix eq "pciudg" ) { cp("ciudgls","$direct{list}/ciudgls.sp");
                                   if ( $kwords{"nproc"} > 1){ system("$ENV{COLUMBUS}/perlscripts/tmodel.pl ciudg.timings > ciudg.perf"); }}
   if ( $progprefix eq "pciden" ) { cp("cidenls","$direct{list}/cidenls.sp");}
   if ($errno)
    {
     `tail $direct{JDIR}/runc.error `;
     $!=11; die "Error occurred in $progprefix errno=$errno \n";
    }
   unlink ("bummer");
   $finished=" finished $x at %s " . date();
   return;
  }

########################################################################################################
  sub createlocdir
         { my ($ahost, $currdir,$mfile,$done);
           $mfile = shift @_; # data are already available on process0
           $done=":";
           # make copies of the content of the current directory
           # create local directories with the same path
           $currdir=pwd();
           foreach $ahost (@mfile) {
                if (grep(/:$ahost:/,$done)) { print "already local directory generated on $ahost\n";}
                               else        { system("$rsh_localdir $ahost -p mkdir $currdir");
                                             system("$rsh_localdir $ahost cp * $currdir/.");
                                             $done=$done."$ahost:";}
                                   }
           return;
         }

  sub removelocdir
         { my ($ahost,$currdir,$mfile);
           $mfile=shift @_;
           # copy results back and delete remote directories
           $currdir=pwd();
           system("$rsh_localdir $$mfile[0] cp $currdir/* .");
           # remove (all) remote data
           $done=":";
           foreach $ahost (@mfile) {
                if (grep(/:$ahost:/,$done)) { print "already local directory generated on $ahost\n";}
                               else        { system("$rsh_localdir $ahost rm -rf  $currdir/.");
                                             $done=$done."$ahost:";}
                                   }
          return;
         }


############################################################################################################

   sub pcallprog {
   local ($cbus,$errno,$progprefix,$moption,$started,@hosts,@nproc,$prog,$mfile);
   local (@timing,$startt,$endt,$i,$wallt,$wallh,$wallm,$walls);
   # get user time for process and all child processes
#    $i=1;
#    undef(@timing);
#    @timing=times();
#    until(not defined($timing[$i])){
#      $startt += $timing[$i];
#      $i+=2;
#    }
   $clock{startt}=time();

   $cbus = $ENV{"COLUMBUS"};
   $prog = $_[0] ; # print "DEBUGMR: pcallprog called with $prog\n";
   $progprefix = $prog;
   $progprefix =~ s/[\<\>-].*$//;
   $progprefix =~ s/\.x *//;
   $moption = $prog;
   $moption =~ s/^.*\.x *//;
   $started="starting $progprefix at %s " . date();

      if ($nproc_nl) {  # use the command line options
                        print "nproc: $nproc_nl machinefile: $machinefile_nl \n";
                        print "assuming machinefile syntax: one host name per line optionally repeated\n";
                        my $save=$/;
                        $/="\n";
                        $mfile=$machinefile_nl;
                        $login=`whoami`;
                        open MFILE,"<$machinefile_nl";
                        @hosts=<MFILE>;
                        chop @hosts;
                        $allproc=$nproc_nl;
                        for ($i=1; $i<=$nproc_nl; $i++) { push @nproc, 1; }
                        if ($#hosts < $#nproc  && (grep(/(_HOSTS_|_NPROCPERHOST_|_NHOSTS_)/,$mpi_startup)) ) {die("insufficient number of host names in $machinefile_nl\n");}
                        $/=$save;
                     }
       else
                    {  $login=`whoami`;
                       @hosts=split(/:/,$kwords{hosts});
                       @nproc=split(/:/,$kwords{nproc});
                       if ( $kwords{hosts} ) {print "... @hosts .#. $#hosts \n";}
                       if ( $kwords{nproc}) {print "... @nproc .#. $#nproc \n";}
                       if ( $kwords{machinefile} ){ print "... machinefile=$kwords{machinefile}\n"; system("cp $direct{JDIR}/$kwords{machinefile} .");}
                       $mfile=$kwords{machinefile};
                       $allproc=0;
                       for ( $i=0; $i<=$#nproc; $i++ ) { $allproc=$allproc+$nproc[$i];}
                        if ( ($#hosts != $#nproc) && (! $kwords{machinefile}) &&  (grep(/(_HOSTS_|_NPROCPERHOST_|_NHOSTS_)/,$mpi_startup)) )
                                  { die "hosts line and nproc line do not match $#hosts versus $#nproc ";}
                    }


     if (grep(/pdalton/,$proprefix)) { print " found pdalton ($progprefix) ... assuming SMP node \n";
                                      print " setting nproc temporarily to 2 \n";
                                      $allproc=2;}
     if (grep(/pcigrd/,$proprefix)) { print " found pcigrd ($progprefix) ... assuming SMP node \n";
                                      print " setting nproc temporarily to 2 \n";
                                      $allproc=2;}
     if (grep(/pmcscf/,$proprefix)) { print " found pmcscf ($progprefix) ... assuming SMP node \n";
                                      print " setting nproc temporarily to 2 \n";
                                      $allproc=2;}


#
#
#  determine how to start up the parallel program

     $gacom=$$installconfig{"GACOMMUNICATION"};
     $pscript=$$installconfig{"PSCRIPT"};
     $mpi_maindir=$$installconfig{"MPI_MAINDIR"};
     $mpi_startup=$$installconfig{"MPI_STARTUP"};
#
#
      if ($debug{debug_par}) {print "pcallprog startup1:   gacom:$gacom   pscript:$pscript   mpi_startup:$mpi_startup\n";
                   print " hosts=",join(':',@hosts),"\n";}

#
#   if GACOMMUNICATION is set to TCGMSG use this standard procedure
#   this assumes a system of loosely coupled work stations with no
#   batch system present, i.e. hostlist and number of processors is
#   explicitly stated in the control.run file
#

    if ( grep /tcgmsg/i, $gacom )
     {
#
#    TCGMSG procgroup file
#
     if ( ! $mfile )
       {open PROCGRP,">$progprefix.p";
       for ( $i=0; $i<=$#nproc ; $i++ )
       { print PROCGRP "$login $hosts[$i] $nproc[$i] $cbus/$progprefix.x $direct{JDIR}/WORK \n";}
       close PROCGRP ; $mfile="$progprefix";
      }
     else
      {$mfile=~s/\.p$//;}
     if ( ! -x $cbus/parallel )
       {die "apparently you forgot to copy the startup code g/tcgmsg/ipcv4.0/parallel (GA) into $COLUMBUS! exiting\n";}
     print "$cbus/parallel $mfile $moption -m $control{pcoremem} >> $direct{JDIR}/runc.error 2>&1 \n";
     system( "$cbus/parallel $mfile $moption -m $control{pcoremem} >> $direct{JDIR}/runc.error 2>&1" );
     pcheck($progprefix);
     return;
    }
#
#  requirements
#
     $hosts_=grep(/_HOSTS_/,$mpi_startup);
     $nproc_=grep(/_NPROC_/,$mpi_startup);
     $nprocperhost_=grep(/_NPROCPERHOST_/,$mpi_startup);
     $nhosts_=grep(/_NHOSTS_/,$mpi_startup);
     $create_localdir=grep(/LOCDIR_CREATE/,$pscript);
     $remove_localdir=grep(/LOCDIR_REMOVE/,$pscript);
     $rsh_localdir=grep(/LOCDIR_RSH/,$pscript);
     if (! $rsh_localdir && ($create_localdir || $remove_localdir) )
     { die("in order to modify local directories on the workers , specify the appropriate command in install.config\n");}
     if ( $rsh_localdir ) { print "found rsh_localdir = $rsh_localdir\n";
                            $rsh_localdir=$pscript; chop($rsh_localdir); $rsh_localdir=~s/^.*LOCDIR_RSH=//; $rsh_localdir=~s/^ *$//;
                            if ($create_localdir) { $currdir=cwd();
                                                    $comstr="$rsh_localdir ";
                                                    print "currdir=$currdir\n nhosts=$#hosts \n";
                                                    for ( $i=1; $i<=$#hosts ; $i++ )
                                                       {  $thisdir=$currdir;
                                                          $thisdir_exists = system("$comstr $hosts[$i] cd $thisdir ");
                                                          if ( ! $thisdir_exists) { print " directory $hosts[$i] $thisdir already existent \n"; }
                                                          else  { print "creating $comstr  $hosts[$i] $thisdir\n";
                                                                  if ( system("$comstr  $hosts[$i] mkdir -p $thisdir"))
                                                                  { system("$comstr  $hosts[$i] mkdir  $thisdir") || die("could not create $comstr $hosts[$i] mkdir -p  $thisdir\n"); }}
                                                       }
                                                   }
                          }

     if ($debug{debug_par})
    { printf "pcallprog startup: hosts_= %1d nproc_=%1d,  nprocperhost_=%1d nhosts_=%1d \n %s\n, create_localdir=%1d remove_localdir=%1d rsh_localdir=%1d \n ",
           $hosts_,$nproc_,$nprocperhost_,$nhosts_, $mpi_startup, $create_localdir,$remove_localdir,$rsh_localdir; }

#
# now we assume standard MPI startup procedures
# The startup sequence is determined from the MPI_STARTUP line in install.config
#

      if ( ! ($hosts_ + $nproc_ + $nprocperhost_ + $nhosts_) )
       {
#
#   nothing to specify
#   i.e. batch system supplies hostlist and passes it
#   directly to mpirun without the need to explicitly specify it
#   on the commandline
#   SYSTEMS:
#   CRAY-T3E                MPI_STARTUP=mpprun -np _NPROC_ _EXE_ _EXEOPTS_
#   IBM REGATTA (JUELICH)   MPI_STARTUP=poe -n _NPROC_  _EXE_ _EXEOPTS_
#
       # just substitute _EXE_ and _EXEOPTS_

       $mpi_startup=~s/_EXE_/$cbus\/$progprefix.x/;
       $mpi_startup=~s/_EXEOPTS_/-m $control{pcoremem} /;

       if ($debug{debug_par}) {print "section1: $mpi_startup >> $direct{JDIR}/runc.error 2>&1 \n";}
       system("$mpi_startup  \n");
       pcheck($progprefix);
       goto normal
       }

      if ( ! ($hosts_ + $nprocperhost_ + $nhosts_)  && $nproc_  )
      {
#
#    only  nproc must be  specified explicitly
#   i.e. batch system supplies hostlist and passes it
#   directly to mpirun without the need to explicitly specify it
#   on the commandline
#
#   if there is a machinefile entry in control.run the number
#   of processors equals the number entries in there
#   otherwise take the nproc entry to be correctly set
#   SYSTEM: NONE

       $mpi_startup=~s/_NPROC_/$allproc/;
       if ($mfile) { $mpi_startup=~s/_EXE_/-machinefile $mfile _EXE_/;}
       $mpi_startup=~s/_EXE_/$cbus\/$progprefix.x/;
       $mpi_startup=~s/_EXEOPTS_/-m $control{pcoremem} /;


       if ($debug{debug_par}) {
            print " section2: $mpi_startup >> $direct{JDIR}/runc.error 2>&1 \n";
            system("$mpi_startup 2>> $direct{JDIR}/runc.error\n");
        }
       else {system("$mpi_startup >> $direct{JDIR}/runc.error 2>&1 \n");}

      pcheck($progprefix);
       goto normal;
    }

      if ( ! ($hosts_ + $nprocperhost_ + $nproc_)  && $nhosts_  )
      {
#
#    only  nhosts must be  specified explicitly
#   i.e. batch system supplies hostlist and passes it
#   directly to mpirun without the need to explicitly specify it
#   on the commandline
#
#   if there is a machinefile entry in control.run the number
#   of processors equals the number entries in there
#   otherwise take the nproc entry to be correctly set
#   SYSTEM: LINUX CLUSTER SCHROEDINGER (VIENNA)
#   MPI_STARTUP=mpirun n0-_NHOSTS_ _EXE_ _EXEOPTS_

      $nhosts=$#hosts+1;
      if (! $nhosts ) { die("runc: number of hosts equals 0, exiting\n");}


       $mpi_startup=~s/_NHOSTS_/$nhosts/;
       $mpi_startup=~s/_EXE_/$cbus\/$progprefix.x/;
       $mpi_startup=~s/_EXEOPTS_/-m $control{pcoremem} /;

       if ($debug{debug_par}) {print "$mpi_startup >> $direct{JDIR}/runc.error 2>&1 \n";}
       system("$mpi_startup >> $direct{JDIR}/runc.error 2>&1 \n");
       pcheck($progprefix);
        goto normal;

       return;
    }

      if ( ! ($nprocperhost_ + $nhosts_)  && $nproc_ && $hosts_ )
      {
#
#    hosts and nproc must be specified  explicitly
#
#   if there is a machinefile entry in control.run the number
#   of processors equals the number entries in there
#   otherwise take the nproc entry to be correctly set
#   SYSTEM: LINUX CLUSTER JAZZ (ANL) INTERACTIVE & BATCH USAGE
#   MPI_STARTUP mpirun -np _NPROC_ -machinefile _HOSTS_ _EXE_ _EXEOPTS_  (ethernet)
#   MPI_STARTUP=mpirun.ch_gm -np _NPROC_ --gm-f _HOSTS_ _EXE_ _EXEOPTS_  (myrinet)
#   SYSTEM: IBM WORKSTATION CLUSTER
#   MPI_STARTUP=poe _EXE_ -EXEOPTS_ -hostfile _HOSTS_ -procs _NPROC_ -pmmodel spmd
#
#    hostlist and nproc is required
#    case 1)  it is automatically supplied in a machinefile
#             as specified in control.run
#             AND we assume that machinefile has already
#             the correct format for use with MPI
#             i.e., one line per processor
#             fine for single-processor systems, usually
#             not compliant with PBS for multi-processor system
#             The PBS_NODEFILE contains the numbers of hosts, only.
#             This usually requires rewriting the machinefile.
#
#   PSCRIPT PBS_PROCPERNODE=2
#   SYSTEM: LINUX CLUSTER CHIBA (ANL) INTERACTIVE & BATCH USAGE
#   MPI_STARTUP=mpirun.ch_gm -np _NPROC_ --gm-f _HOSTS_ _EXE_ _EXEOPTS_
#
#

         print "entering secion hosts && nproc \n";
       if ($mfile)
            {if (grep(/PBS_PROCPERNODE/,$pscript))
                { $pscript=~s/^ *//; @tmp=split(/[\s=]+/,$pscript);   # $tmp[2]=numbers of processors per node
                  die("strange jazz stuff \n");
                  open NEWMACH,"newmachinefile";
                  $allproc=0;
                   while ($#hosts >0 )
                   { for ($i=1; $i<=$tmp[2]; $i++) { print NEWMACH $_;}
                     $allproc=$allproc+$tmp[2]; }
                   close NEWMACH;
                  $mpi_startup=~s/_HOSTS_/newmachinefile/;
                 }
             else
            { $mpi_startup=~s/_HOSTS_/$mfile/; }

            $mpi_startup=~s/_NPROC_/$allproc/;
            $mpi_startup=~s/_EXE_/$cbus\/$progprefix.x/;
            $mpi_startup=~s/_EXEOPTS_/-m $control{pcoremem} /;
            print "$mpi_startup A >> $direct{JDIR}/runc.error 2>&1 \n";
            system("$mpi_startup >> $direct{JDIR}/runc.error 2>&1 \n");
            pcheck($progprefix);
            goto normal;
           }
#
#   case 2)   a machinefile must be generated on the fly
#             based on nhosts and nproc line in control.run
#             unfortunately, the syntax is system dependent
#             (mpi version & configuration)
#             hence, this is a good place for system specific
#             modifications through pscript
#             Ideally pscript contains a sequence of one or more
#             keywords describing the properties of the system
#             and runc must be explicitly modified to take care
#             of it!!
#
#             default  one host per line, for SMP system with
#             n procs per host, the hostname is replicated n times.

       else
              {open PROCGRP, ">machines";
               for ( $i=0; $i<=$#nproc ; $i++ ) {
                 for ( $j=1; $j<=$nproc[$i]; $j++) { print PROCGRP "$hosts[$i] \n" ;}}
               close PROCGRP ;
               $mpi_startup=~s/_NPROC_/$allproc/;
               $mpi_startup=~s/_HOSTS_/machines/;
               $mpi_startup=~s/_EXE_/$cbus\/$progprefix.x/;
               $mpi_startup=~s/_EXEOPTS_/-m $control{pcoremem} /;
               print "$mpi_startup >> $direct{JDIR}/runc.error 2>&1 \n";
               system("$mpi_startup >> $direct{JDIR}/runc.error 2>&1 \n");
               pcheck($progprefix);
               goto normal;
              }
         }
    die (" pcallprog: should never arrive here!\n");

 normal:

        if ( $rsh_localdir ) { print "found rsh_localdir = $rsh_localdir\n";
                            $rsh_localdir=$pscript; chop($rsh_localdir); $rsh_localdir=~s/^.*LOCDIR_RSH=//; $rsh_localdir=~s/^ *$//;
                            if ($remove_localdir) { $currdir=cwd();
                                                    $comstr="$rsh_localdir ";
                                                    print "currdir=$currdir\n nhosts=$#hosts \n";
                                                    for ( $i=1; $i<=$#hosts ; $i++ )
                                                       {  $thisdir=$currdir;
                                                          $thisdir_exists = system("$comstr $hosts[$i] cd $thisdir ");
                                                          if ( $thisdir_exists) { print " directory $hosts[$i] $thisdir does not exist  ... ignoring \n"; }
                                                          else  { print "deleting $comstr  $hosts[$i] $thisdir\n";
                                                                  system("$comstr  $hosts[$i] rm -rf $thisdir");}
                                                       }
                                                   }
                          }
#    $i=1;
#    undef(@timing);
#    @timing=times();
#    until(not defined($timing[$i])){
#      $endt += $timing[$i];
#      $i+=2;
#    }
    $clock{endt}=time();
    $wallt = $clock{endt}-$clock{startt};
#   if ($wallt < 5)
#   {
#      $wallreport .=sprintf " * %-15s  walltime    -\n", $prog;
#   }
#   else
    {
        $walls = $wallt%60;
        $wallt = ($wallt-$walls)/60;
        $wallm = $wallt%60;
        $wallt = ($wallt-$wallm)/60;
    #    $wallh = ($wallt/60);
        $wallh = $wallt;
        $clock{wallreport} .= sprintf " * %-15s  walltime %4d:%02d:%02d\n", $prog, $wallh, $wallm, $walls;
    }
   return;
   }

#   else
#   {
#       $walls = $wallt%60;
#       $wallt = ($wallt-$walls)/60;
#       $wallm = $wallt%60;
#       $wallt = ($wallt-$wallm)/60;
#       $wallh = $wallt;
#       $wallreport .= sprintf " * %-15s  walltime %4d:%02d:%02d\n", $x, $wallh, $wallm, $walls;
#   }
#   analyse_output($x,$started,$finished);
#}



