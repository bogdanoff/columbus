#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


  foreach $infile  (@ARGV) 
  { $/="\n";  
    open FIN,"<$infile";
     print "opening $infile\n";
    $mopen=0;
     while (<FIN>)
      { if (/====>Begin Module /) { $modname=$_; chop $modname; $modname=~s/^.*Module//; $modname=~s/ *File .*$//; $modname=~s/ //g; 
                                    if ($mopen) {close FOUT;}
                                    print ".... $modname\n";
                                    open FOUT,">$modname.putin"; $mopen=1; }
        print FOUT;
      }
   if ($mopen) {close FOUT;}
  }
 exit; 
