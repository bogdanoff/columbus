#!/bin/csh  
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#
#  $1 = machine_id 
#
#  produces xxxx.machine_id.x 
#
 
 if ( -z "$COLUMBUS" ) then
    echo COLUMBUS variable not set
    exit
 endif
 echo "machine_id=$1" 
 if ( -z "$1" ) then
   echo machine_id not set
   exit
 endif 

 echo info copy everything from `pwd`

set grad=( cmdc )
set libs=( clib scflib1 scflib2 coloplib )
set tc=( scf_d tran_d ciudg_d mcscf_d )

 echo "preparing grad part "
 foreach i ( 1  )
set j=source/$grad[$i]/binaries 
  if ( ! -d $COLUMBUS/$j ) then
   mkdir $COLUMBUS/$j
  endif 
 cp  $grad[$i].x $COLUMBUS/$j/$grad[$i].$1.x
 echo "cp  $grad[$i].x $COLUMBUS/$j/$grad[$i].$1.x "
 end
  
 echo "preparing libs part "

 foreach i ( 1 2 3 4 )
  if ( ! -d $COLUMBUS/turbocol.5.7/source/libs/binaries )  then
    mkdir $COLUMBUS/turbocol.5.7/source/libs/binaries
  endif
  cp -f $libs[$i].a $COLUMBUS/turbocol.5.7/source/libs/binaries/$libs[$i].$1.a
  echo " cp -f $libs[$i].a $COLUMBUS/turbocol.5.7/source/libs/binaries/$libs[$i].$1.a "
 end
  
 echo "preparing tc part "

 foreach i ( 1 2 3 4 )
  if ( ! -d $COLUMBUS/turbocol.5.7/source/$tc[$i]/binaries )  then
    mkdir $COLUMBUS/turbocol.5.7/source/$tc[$i]/binaries
  endif
  cp -f $tc[$i].x $COLUMBUS/turbocol.5.7/source/$tc[$i]/binaries/$tc[$i].$1.x
  echo "cp -f $tc[$i].x $COLUMBUS/turbocol.5.7/source/$tc[$i]/binaries/$tc[$i].$1.x "
 end
  

