#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


#################################################################
####                                                         ####
####  Program displace_NC.pl, written by Bernhard Sellner    ####
####                       10.12.2007                        ####
####                                                         ####
####                      version 2.0 xx.xx.2008             ####
#################################################################

print ("\n");
print ("Program disp_nc.pl version 2.0\n");
print ("\n");

# prepare input #################################################

open(IN,">inp") or die "cannot write file inp\n";

print ("COLUMBUS [1] or TURBOMOLE [2] ?\n");

$pr=<STDIN>;
#$pr="2\n";
chop($pr);
if ($pr=="1") {
  print IN ("col\n");
}
if ($pr=="2") {
  print IN ("tur\n");
}
if ($pr >= "3") {
  die "invalid answer";
}

print ("How many modes (integer) ?\n");
$nmod=<STDIN>;
chop($nmod);
#$nmod=3;
print IN ("$nmod\n");
print ("Which modes (integer, blank separated) ?\n");
$lofm=<STDIN>;
#$lofm='7 8 9';
print IN ("$lofm");
@lofm=split(/\s+/,$lofm);



print ("Lower and upper bound in dimensionless units [1] or in Angstrom [2] ?\n");

$un=<STDIN>;
#$un="1\n";
chop($un);
if ($un >= "3") {
  die "invalid answer";
}
print IN ("$un\n");



print ("Lower bound for displacement (floating point) ?\n");
$l=<STDIN>;
#$l=-1.0;
print IN ("$l");
print ("Upper bound for displacement (floating point) ?\n");
$u=<STDIN>;
#$u=1.0;
print IN ("$u");
print ("Displacement steps (integer) ?\n");
$steps=<STDIN>;
#$steps=2;
print IN ("$steps");

if ($pr==1) {
  open(GE,"geom") or die "cannot open file geom\n";
  $nat=0;
  while ($_=<GE>){
    $nat=$nat+1;
  }
  close(GE);
}

if ($pr==2) {
  open(CO,"coord") or die "cannot open file coord\n";
  $lines=0;
  while ($_=<CO>){
    $lines=$lines+1;
  }
  close(CO);
  open(CO,"coord") or die "cannot open file coord\n";
  $trash=<CO>;
  $_="asfd";
  $nat=-1;
  until ($_ =~ /^\$/){
    $_=<CO>;
    $nat=$nat+1;
  }
  close(CO);
}

print IN ("$nat\n");

close (IN);

$degfred=3*$nat;

if($pr==2) {
  $mode=("");
  $freq=("");
  @coord=("") x ($degfred+1);
  $redm=("");
  open(FO,"freq.out") or die "cannot open file freq.out\n";
  while (<FO>){
    if (/the normal mode derivative of the dipole moment/){
      $blocks=int $degfred/6;
      for ($k=1;$k<=($blocks+1);$k++){
        chop($_=<FO>);
        chop($_=<FO>);
        chop($_=<FO>);
        $trash=("");
        ($trash,$_)=split(/\w{4,4}\s{6,6}/,$_);
        $mode=($mode.$_);
        chop($_=<FO>);
        chop($_=<FO>);
        ($trash,$_)=split(/\w{9,9}\s{5,5}/,$_);
        $freq=($freq.$_);
        for ($j=1;$j<=10;$j++){
        chop($_=<FO>);
        }
        for ($j=0;$j<=($nat-1);$j++){
          $i=$j*3;
          $i++;
          chop($_=<FO>);
          ($trash,$_)=split(/\d{1,1}\s{3,3}\w{1,1}\s{11,11}\w{1,1}/,$_);
          $coord[$i]=($coord[$i].$_);
          $i++;
          chop($_=<FO>);
          ($trash,$_)=split(/\s{18,18}\w{1,1}/,$_);
          $coord[$i]=($coord[$i].$_);
          $i++;
          chop($_=<FO>);
          ($trash,$_)=split(/\s{18,18}\w{1,1}/,$_);
          $coord[$i]=($coord[$i].$_);
        }
        chop($_=<FO>);
        chop($_=<FO>);
        ($trash,$_)=split(/mol\)/,$_);
        $redm=($redm.$_); 
      }
    }
  }

open(FRO,">frequencies") or die "cannot write to file frequencies\n";
  print FRO ("$mode\n");
  print FRO ("$freq\n");
  print FRO ("$redm\n");
  for ($i=1;$i<=$degfred;$i++){
    print FRO ("$coord[$i]\n");
  }
close(FRO);

}

# calculate the displaced geometries ############################

if ($pr=="1") {
  if (! defined($ENV{"COLUMBUS"})) {die "You have to define \$COLUMBUS\n";}
  system('$COLUMBUS/disp_normcoord.x >log');
} else {
  system('disp_normcoord.x >log');
}

open(LO,"log") or die "Cannot open log";


while(<LO>){
  chop($_=<LO>);
  chop($_=<LO>);
  if ("$_" ne " Job done"){
    die "Problem with disp_normcoord.x program";
  }
  else{
  print ("Job done in disp_normcoord.x\n");
  last;
  }
}

close(LO);

system("rm -f log");

system("rm -f inp");

# make the directory structure  ###############

open(WF,"displacels") or die "cannot open file displacels";

for ($i=0;$i<=($nmod-1);$i++){
  system("mkdir -p normcoord$lofm[$i]");
  system("cp * normcoord$lofm[$i]/ 2>trash");
  system("mkdir -p normcoord$lofm[$i]/DISPLACEMENT");
  system("rm -rf normcoord$lofm[$i]/DISPLACEMENT/*");
}

while(<WF>){
  if (/displaced geometries/){
    for ($j=0;$j<=($nmod-1);$j++){
      open(DS,">normcoord$lofm[$j]/DISPLACEMENT/displfl") or die "Cannot write displfl in normcoord$lofm[$j]/DISPLACEMENT/displfl";
      print DS ("$degfred   /number of degrees of freedom\n");
      print DS ("no  /calculate dip.mom.derivatives\n");
      print DS ("no  /calculate reference point\n");
      for ($k=1;$k<=$steps;$k++){
        chop($df=<WF>);
        ($trash1,$df,$trash2)=split(/\s+/,$df);
        chop($disp=<WF>);
        ($trash1,$disp,$trash2)=split(/\s+/,$disp);
        print DS ("$df   $disp\n");
        system("mkdir -p normcoord$df/DISPLACEMENT/CALC.c$df.d$disp");
        if ($pr=="1") {
        open(NG,">normcoord$df/DISPLACEMENT/CALC.c$df.d$disp/geom") or die "Cannot write geom file";
          for ($i=1;$i<=$nat;$i++){ 
            chop($_=<WF>); 
            print NG ("$_\n");
          }
        }
        if ($pr=="2") {
        system("cp * normcoord$df/DISPLACEMENT/CALC.c$df.d$disp/ 2>trash");
        open(NG,">normcoord$df/DISPLACEMENT/CALC.c$df.d$disp/coord") or die "Cannot write coord file";
          open(CO,"coord");
          chop($_=<CO>);
          print NG ("$_\n");
          for ($i=1;$i<=$nat;$i++){ 
            chop($_=<WF>); 
            print NG ("$_\n");
            $trash=<CO>;
          }
          for ($l=1;$l<($lines-$nat);$l++){
            chop($_=<CO>); 
            print NG ("$_\n");
          }
          close(CO); 
        }
      }
      close(DS)
    }
  }
}

close(WF);

print ("Job done in disp_nc.pl\n");
