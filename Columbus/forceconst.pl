#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

	use Shell qw(pwd cd date tail grep | ); 
	use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;
	use colib_perl;
        use File::Copy 'cp';  # use perl cp
#
#==========================================================
#
#   perl program: freqcalc.pl
#   aim: program calculates the force constants, frequencies
#        and dipole moment derivatives based on 
#        on the informations in displfl
#   author: Michal Dallos, 
#           Institute for Theoretical Chemistry and Molecular Biology
#           University of Vienna
#           Waehringerstrasse 17, A-1090 Vienna, Austria
#
#Log:
#HL: April 1, 2005: displacements are computed directly from displ file
#   program description:
#  Program is driven by the file DISPLACEMENT/displfl, which has the following
#  structure:
#
#  N /number of internal coordinates
#  yes/no /calculate dip.mom.derivatives
#  yes/no /calculate reference point
#  coordinate1 displacement1+
#  coordinate1 displacement1-
#        :
#        :
#  coordinateN displacementN+
#  coordinateN displacementN-
#
#  at the end of the lines defining the coordinates and displacements
#  the following additional key words may be added:
#  skip - used for all '+' displacement in case numerical differentiation
#         is done relatively to the reference point only
#  fixc - used for all displacements of one coordinate, in order to skip the
#         calculation for this coordinate. If set, forceconst.pl program assumes
#         1 at the diagonal for the force constant matrix. This option 
#         may be useful if user wants to calculate the some frequency 
#         components in higher symmetry, and skip contributions from
#         displacements which are not present in the given point group.
#==========================================================
#
#   GLOBAL VARIABLES
#
   $debug=0;         #  if =1, write out som debuf informations
   $scriptversion="Version 1.1, 13_Nov_2002 (md)"; 
   $nintc = 0;       #  number of internal coordinates
   $dipmom = "no";   #  calculation of dipol moment derivative flag
   $ncalc = 0;       #  number of performed calculations
   @coor = ();       #  int. coordinate number
   @displ =();       #  displacement corresponding to a int. coordinate
   @flag =();        #  calculate derivative relatively to reference point
   $JDIR = pwd();             # job directory
   $JDIR =~ s/\n//;                          
# 
   if (! -s "LISTINGS"){ mkdir("LISTINGS",0755);}
   open(OUT,">LISTINGS/forceconstls");

#  print some header
   print{OUT} "\n   ** forceconst.pl:      $scriptversion **\n\n";
   print{OUT} "   ####################################################### \n";
   print{OUT} "   ##                                                   ## \n";
   print{OUT} "   ##    Program for force constants, frequencies and   ## \n";
   print{OUT} "   ##         dip.mom. derivative calculations          ## \n";
   print{OUT} "   ##                                                   ## \n";
   print{OUT} "   ##  written by: Michal Dallos,                       ## \n";
   print{OUT} "   ##              Institute for Theoretical Chemistry  ## \n";
   print{OUT} "   ##              University of Vienna                 ## \n";
   print{OUT} "   ##              Waehringerstr. 17                    ## \n";
   print{OUT} "   ##              A-1090 Vienna, Austria               ## \n";
   print{OUT} "   ##                                                   ## \n";
   print{OUT} "   ####################################################### \n\n\n";
#
#  check the necessary input flag combinations
#
#  check the current gradient type
    $mcgrad=0;$cigrad=0;$ciprop=0;$mcprop=0;$nadcoupl=0;
    open(CONTROL,"control.run");
      while (<CONTROL>){
      chop();
      if ($_ eq "mcscfgrad"){$mcgrad=1;}
      if ($_ eq "samcgrad"){$samcgrad=1;}
      if ($_ eq "cigrad"){$cigrad=1;}
      if ($_ eq "mcscfprop"){$mcprop=1;}
      if ($_ eq "ciprop"){$ciprop=1;}
      if ($_ eq "nadcoupl"){$nadcoupl=1;}
      }
    close(CONTROL); 
#
   if ($nadcoupl == 1) {&nadtype}
#
   if (! -e "DISPLACEMENT/displfl") {$!=10; die "File: DISPLACEMENT/displfl missing!!!";}
   open(INPUT, "DISPLACEMENT/displfl") or die "could not open DISPLACEMENT/displfl !!!";
   $nintc=<INPUT>; chomp ($nintc); $nintc=~s/\W.*//;
   print{OUT} "  Number of internal coordinates: $nintc \n" ;
   $dipmom=<INPUT>; chomp ($dipmom); $dipmom=~s/\W.*//;
   print{OUT} "  Dipolmoment derivative calculation: $dipmom \n";
   $refcalc=<INPUT>; chomp ($refcalc); $refcalc=~s/\W.*//;
   print{OUT} "  Reference calculation performed: $refcalc \n\n";

   print{OUT}"  Coordinate   Displacement     Flag\n";
   print{OUT}" -------------------------------------\n";
   $i=1; @flag=();%fixc=();
      while (<INPUT>) 
       {
        chop($_); 
        ($coor[$i],$displ[$i],$flag[$i])=split(/\s+/,$_,3);
         if (!defined $flag[$i]){$flag[$i]="";}
         if ($flag[$i] eq "fixc") {$fixc{$coor[$i]}=$flag[$i]}
        printf{OUT}( "%7s   %15.6f    %7s\n", $coor[$i],$displ[$i],$flag[$i])  ;
        $i++;
       }
   $ncalc=$i-1;
#
#   perform some error checking
#
       if ($dipmom eq "yes" && $refcalc eq "no") 
        { die " For the calculation of the dip. mom. derivates you have to calculate the reference point !!!\n";}
       for ($i=1; $i<=$ncalc; $i++)
        {
	 if ($dipmom eq "yes" && $flag[$i] eq "skip")
	  {
           print " For the coordinate: $coor[$i] the displacement: $displ[$i] has not been calculated !!!\n";
           die " For the calculation of the dip. mom. derivates you have to calculate all displacements !!!\n";
          }
         if ($refcalc eq "no" && $flag[$i] eq "skip")
          {       
	   die" if you calculate negative displacements only reference point is required !!!\n";
	  }
        } # end of: if ($dipmom eq "yes" && $refcalc eq "no")
#
#   read the reference point informations
    if ($refcalc eq "yes"){ &refpoint();}
#
#
#   read the dipol moment
    if ($dipmom eq "yes"){ &dipmomread();}
#
#   read the energies
    &energy();
#
#   read the gradients
    if ($cigrad ==1 || $mcgrad ==1 || $samcgrad ==1) 
     {&disppointread(\@grad,\@gradref,"",0);}
    elsif ($nadcoupl==1)
     {
      print {OUT} "\n*********************************************************************\n";
      print {OUT} "\n    Energy gradient values for state: DRT$naddrt1, STATE$nadstate1\n";
      &disppointread(\@grad_st1,\@gradref_st1,".drt$naddrt1.state$nadstate1",1);
      print {OUT} "\n*********************************************************************\n";
      print {OUT} "\n    Energy gradient values for state: DRT$naddrt2, STATE$nadstate2\n";
      &disppointread(\@grad_st2,\@gradref_st2,".drt$naddrt2.state$nadstate2",2);
      print {OUT} "\n*********************************************************************\n";
      print {OUT} "\n    Non-adiabatic coupling values between state: DRT$naddrt1 STATE$nadstate1 and DRT$naddrt2, STATE$nadstate2\n";
      &disppointread(\@grad_nad,\@gradref_nad,".nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2",3);
     }
#
#   calculate the for constants
    if ($cigrad ==1 || $mcgrad ==1 || $samcgrad==1)
     {&forceconscalc(\@grad,\@fgrad,"",1.0);}
    elsif ($nadcoupl==1)
     {
      $title="Energy gradient STATE: DRT$naddrt1 STATE$nadstate1";
      &forceconscalc(\@grad_st1,\@fgrad_st1,$title,1.0);
      $title="Energy gradient STATE: DRT$naddrt2 STATE$nadstate2";
      &forceconscalc(\@grad_st2,\@fgrad_st2,$title,1.0);
      $title="Non-adiabatic coupling between: DRT$naddrt1 STATE$nadstate1 and  DRT$naddrt2 STATE$nadstate2";
      &forceconscalc(\@grad_nad,\@fgrad_nad,$title,0.0);
     }
#
#   make the input for suscal.x and perform the suscal.x calculation
    if ($cigrad ==1 || $mcgrad ==1 || $samcgrad ==1)
     {
      print{OUT} "\n Creating SUSCAL input.\n\n";
      &suscal(\@fgrad);
     }
    elsif ($nadcoupl==1)
     {
      print{OUT} "\n Creating SUSCAL input for: DRT$naddrt1 STATE:$nadstate1\n\n";
      &suscal(\@fgrad_st1);
      cp ("LISTINGS/suscalls","LISTINGS/suscalls.drt$naddrt1.state$nadstate1");
      print{OUT} "\n Creating SUSCAL input for: DRT$naddrt2 STATE:$nadstate2\n\n";
      &suscal(\@fgrad_st2);
      cp ("LISTINGS/suscalls","LISTINGS/suscalls.drt$naddrt2.state$nadstate2");
     }
#
#   copy the frequency informations form suscalls to MOLDEN/molden.freq
    if ($cigrad ==1 || $mcgrad ==1 || $samcgrad ==1){&inp4molden();}
#
#   write out the hessian matrix (set zero diagonal elements 0)
#    NOTE: force constant matrix is returned changed
    if ($cigrad ==1 || $mcgrad ==1 || $samcgrad ==1)
     {&hessianout(\@fgrad,"hessian",1.0);}
    elsif ($nadcoupl==1)
     {
      &hessianout(\@fgrad_st1,"hessian_st1",1.0);
      &hessianout(\@fgrad_st2,"hessian_st2",1.0);
      &hessianout(\@fgrad_nad,"hessian_nad",1.0);
     }
 
#
    close(OUT);
#   all done... bye!
#
############################################################
##                                                        ## 
##   subroutine section                                   ##  
##                                                        ##   
############################################################
#
#  read in REFPOINT calculation data
#==========================================================
  sub refpoint
  {
   my $dir;
#
   print{OUT} "\n\n  ***   Reference point informations:   ***\n";
   print{OUT} "  =========================================\n\n";
#
#  read energy
#
   $dir="DISPLACEMENT/REFPOINT/LISTINGS";
   $/="\n";
   open(FILE,"$dir/energy") or die "Cannot open file $dir/energy \n";
#
   if ($nadcoupl ==1)
    {
     $_=<FILE>;
     chomp; s/^ *//;
     ($eref[1],$eref[2])=split(/\s+/,$_,3);
    }
   else
    {
     while (<FILE>) { chomp; s/^ *//; $eref[1]=$_; } # end of: while (<FILE>)
    }
#
#  read internal coordinates
#
   if ($mcgrad eq 1 || $samcgrad eq 1) { print{OUT}  "    Eref(MCSCF): $eref[1]\n";}
   if ($cigrad eq 1) {print{OUT}  "    Eref(CI): $eref[1]\n";}
   if ($nadcoupl eq 1) 
    {
     print{OUT}  "    Eref(CI):  E[DRT$naddrt1,STATE$nadstate1]: $eref[1]\n";
     print{OUT}  "               E[DRT$naddrt2,STATE$nadstate2]: $eref[2]\n\n";
    }
   $dir="DISPLACEMENT/REFPOINT";
   &convtest($dir);
#
   if ($mcgrad==1 || $cigrad==1 || $samcgrad==1)
    {
     $dir="$JDIR/DISPLACEMENT/REFPOINT";
     &readintgrad("$dir","");
     @coordref=@coordtmp; @gradref=@gradtmp;
    }
   elsif ($nadcoupl == 1)
    {
     $dir="$JDIR/DISPLACEMENT/REFPOINT";
     print {OUT} "\n     Energy gradient for state: DRT$naddrt1, STATE$nadstate1\n";
     &readintgrad("$dir",".drt$naddrt1.state$nadstate1");
     @coordref=@coordtmp; @gradref_st1=@gradtmp;
     print {OUT} "\n     Energy gradient for state: DRT$naddrt2, STATE$nadstate2\n";
     &readintgrad("$dir",".drt$naddrt2.state$nadstate2");
     @gradref_st2=@gradtmp;
     print {OUT}"\n     Non-adiabatic coupling  between states: DRT$naddrt1, STATE$nadstate1 and DRT$naddrt2, STATE$nadstate2\n";
     &readintgrad("$dir",".nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2");
     @gradref_nad=@gradtmp;
    }
#
  } # end of: sub refpoint
#==========================================================
#
#  read the internal gradient 
#==========================================================
  sub readintgrad
{
 my ($j,$filegrd,$filegeom,$dir,$name);
 ($dir,$name)=@_;
   $/="";
   @coordtmp=();@gradtmp=(); 
   
   $filegrd="$dir/GRADIENTS/intgrd$name.sp";
   $filegeom="$dir/GEOMS/intgeom.sp";
   
   #fp: cart2int has to be called here now since it is no longer called in runc
   if ( !-e $filegrd || !-e $filegeom)
   {
     $work = "$dir/WORK";
     $list = "../LISTINGS";
     print{OUT}"Calling cart2int ...\n";
     chdir $work;
     
     unlink("intcfl");
     cp ("../intcfl","intcfl") or die "Failed to copy file: ../intcfl\n";
     unlink("geom");
     cp ("../GEOMS/geom.1","geom") or die "Failed to copy file: ../GEOMS/geom.1\n";
     #fp: geom.1 is the starting geometry. If gdiis was called, then the geometry in WORK is wrong!
     
    open (NIN,">cart2intin") or die "Cannot open file: cart2intin\n";
    print NIN " &input\n calctype='cart2int',\n intgradout=1,\n";
    print NIN " gradfl=\'cartgrd$name\',\n forceinau=0,\n";
    print NIN " intgradfl=\'intgrd$name\',\n\/&end\n";
    close NIN;
    
    
  &callprog2("cart2int.x");
  
  cp ("$work/intgrd$name","$filegrd");
  cp ("$work/intgeom","$filegeom");
  
  chdir $JDIR;
  }
   open(FILE,"$filegrd") or die "Failed to open file: $filegrd !!!";
   $line=<FILE>; @gradtmp=split(/\n/,$line); @gradtmp=(0,@gradtmp);
   close(FILE);
#
   open(FILE,"$filegeom") or die "Failed to open file: $filegeom !!!";
   $line=<FILE>; @coordtmp=split(/\n/,$line);@coordtmp=(0,@coordtmp);
   close(FILE);
#
   print{OUT}  "\n      int.coor.      gradient   source: $filegrd\n";
   for ($j=1;$j<=$#coordtmp;$j++)
    { printf{OUT} ("%13.6f  % 13.6f\n",$coordtmp[$j],$gradtmp[$j]); }
#
 $/="\n";
  my ($name,$type,$forceinau)=@_;
  
  #fp: cart2int.x was causing problems. It is now only called if specifically requested.
  #  for frequency calculatioins cart2int is called by forceconst.pl
  
}
#
#==========================================================
#
#   read the energies
#==========================================================
  sub energy{
  my ($icount,$i);
#
#  read displacement point energies
#
     for ($i=1; $i<=$ncalc; $i++) # $i: loop over int.coordinates to which the displ. is applied
       {
          if (($flag[$i] ne "skip")&&($flag[$i] ne "fixc"))
	   {
            $dir="DISPLACEMENT/CALC.c$coor[$i].d$displ[$i]/LISTINGS";
              if (! -e "$dir/energy")
               {$energy[$i]=0;}
              else
               {
	        if ($mcgrad==1 || $cigrad==1 || $samcgrad==1)
		 {
                  open(FILE,"$dir/energy") or die "Cannot open file $dir/energy \n";
                    while (<FILE>)
                     {
                      s/^ *//; 
                      $energy[$i]=$_;
                      if ($refcalc eq "yes"){ $del_e[$i]=$energy[$i]-$eref[1];}
                     } # end of: while (<FILE>)
		  close FILE;
		 }
                if ($nadcoupl==1)
		 {
		  open(FILE,"$dir/energy") or die "Failed to open $dir/energy\n";
                  $_=<FILE>;
                  chomp; s/^ *//;
                  ($energy1[$i],$energy2[$i])=split(/\s+/,$_,3);
		  $del_e1[$i]=$energy1[$i]-$eref[1]; 
		  $del_e2[$i]=$energy2[$i]-$eref[2]; 
		 }
               } # end of: if (! -e "$dir/energy") 
           } # end of: if ($flag[$i] ne "skip") 
       } # end of: for ($i=1; $i<=$ncalc; $i++)
#

  } # end of: sub energy
#
#==========================================================
#
#   dipole moment input
#==========================================================
#
   sub dipmomread{
   my ($root,$text,$f1,$f2,$f3,$i1,$i2,$icount,$mcgrad,$cigrad,$ciprop,$mcprop);
#
   $nerr=0;
#
#
   print{OUT} "\n\n";
   print{OUT} "  ***   Dipol moment derivative input:   ***\n";
   print{OUT} "  ==========================================\n\n";
# 
#  check the necessary input flag combinations
#
#  check the current gradient type
    $mcgrad=0;$samcgrad=0;$cigrad=0;$ciprop=0;$mcprop=0;
    open(CONTROL,"control.run");
      while (<CONTROL>){
      chop();
      if ($_ eq "mcscfgrad"){$mcgrad=1;}
      if ($_ eq "samcgrad"){$samcgrad=1;}
      if ($_ eq "cigrad"){$cigrad=1;}
      if ($_ eq "mcscfprop"){$mcprop=1;}
      if ($_ eq "ciprop"){$ciprop=1;}
      }
    close(CONTROL);
#
#  only one gradient type calculation is allowed
    if ($mcgrad==1 && $cigrad==1){$nerr++; print{OUT} " Cannot use mcgrad and cigrad together!!!\n";}
    if ($mcgrad==0 && $cigrad==0){$nerr++; print{OUT} " No gradient type specified!!!\n";}
#
#  check for dipol moment derivation
    if (($mcgrad==1 || $samcgrad==1) && $mcprop==0){
    $nerr++; print{OUT} " In case dip.mom. calc. specify for mcgrad the property calculation for MCSCF\n";}
    if ($cigrad==1 && $ciprop==0){
    $nerr++; print{OUT} " In case dip.mom. calc. specify for cigrad the property calculation for MCSCF\n";}
#
     if ($nerr > 0 ) {die "\n\n   INPUT ERROR! Number of errors: $nerr\n\n";}       
#
#  mcscf gradient calculation  
       if ($mcgrad==1){
#
#  NOTE: mcscf gradient type calculations are allowed for no state mixing cases
        $calctype="mcscf";
	$drt=1,$root=1;
        $en=&getvalue("DISPLACEMENT/REFPOINT/LISTINGS/mcscfls.all","DRT #",10);
        print{OUT} "\n The dipole moment will be read for:\n";
        print{OUT} " * calculation type: MCSCF\n";
        print{OUT} " * root:             $root\n";
        print{OUT} "  Getting dip.mom for the reference point\n";   
       } # end of: if ($mcgrad==1)
#  state averaged mcscf gradient calculation  
       elsif ($samcgrad==1){
# fp: this has to be rechecked
        print "Dipole moment derivatives not properly implemented at the SA-MCSCF level";
        print "contact the Columbus support team";
        die "Dipole moment derivatives not implemented";
        $calctype="mcscf";
        $drt=1;
        open (INPFL,"transmomin") or die "Could not open file: transmomin which contains the gradient information!\n";
        $/="\n";
        <INPFL>;
        ($tmp_drt1,$tmp_state1,$tmp_drt2,$tmp_state2) = split(/\s+/,$_,4);
        if ($tmp_state1 ne $tmp_state2) {die "Incorrect gradient specification in transmomin";}
        else {$root = $tmp_state1;}
        close INPFL;
    
        $en=&getvalue("DISPLACEMENT/REFPOINT/LISTINGS/mcscfls.all","DRT #",10);
        print{OUT} "\n The dipole moment will be read for:\n";
        print{OUT} " * calculation type: SA-MCSCF\n";
        print{OUT} " * root:             $root\n";
        print{OUT} "  Getting dip.mom for the reference point\n";   
       } # end of: if ($mcgrad==1)
#
#  ci gradient calculation  
       elsif ($cigrad==1){
        $calctype="ci";$drt=1;
        ($root,$en) = &frootci("DISPLACEMENT/REFPOINT/LISTINGS/ciudgls.drt1.all");
	$root=&keyinfile("ciudgin","froot"); 
	if ($root eq ""){ $root=&keyinfile("cidenin","lroot"); }
        print{OUT} "\n The dipole moment will be read for:\n";
        print{OUT} " * calculation type: CI\n";
        print{OUT} " * root:             $root\n\n";
        print{OUT} "  Getting dip.mom for the reference point\n";
       } # end of: if ($cigrad==1) 
#
#  get the dipol moment values
#
#   getting the dipole moment values for the reference point
#
	$file="DISPLACEMENT/REFPOINT/LISTINGS/propls.$calctype.drt$drt.state$root.all";
        print{OUT} "  Getting dip.mom. from file: $file \n" ;
        open (PROPERTY,"$file") or die "Can not open file: $file, error: $!\n";
        while (<PROPERTY>)
          {
           if (/Dipole moments:/)
            {
             <PROPERTY>;<PROPERTY>;<PROPERTY>;<PROPERTY>;
             $_=<PROPERTY>; s/^ *//; chomp;
             ($text,$dipx[0],$dipy[0],$dipz[0])=split(/\s+/,$_,4);
             last
            }
          } 
           printf{OUT} ("   x:%10.6f  y:%10.6f  z:%10.6f\n\n",$dipx[0],$dipy[0],$dipz[0]);
#
#   getting the dipole moment values for the displaced points
#
        $icount=1;
   for ($i1=1; $i1<=$nintc; $i1++)
   {
      for ($i2=1; $i2<=2; $i2++)
      {
         if (! $displ[$icount]==0 )
         {
	    $dir="DISPLACEMENT/CALC.c$coor[$icount].d$displ[$icount]/LISTINGS";
              if ($cigrad==1)
              {  
               ($root,$en) = &frootci("$dir/ciudgls.drt1.all");
	        if ($root eq ""){ $root=&keyinfile("cidenin","lroot"); }
              } # end of: if ($cigrad==1)
	    $file="$dir/propls.$calctype.drt$drt.state$root.all";
            print{OUT} "  Getting dip.mom. from file: $file \n" ;
            open (PROPERTY,"$file") or die "Can not open file: $file, error: $!\n";
              while (<PROPERTY>)
              {
                if (/Dipole moments:/)
                 {
                  <PROPERTY>;<PROPERTY>;<PROPERTY>;<PROPERTY>;
                  $_=<PROPERTY>; s/^ *//; chomp;
                  ($text,$dipx[$icount],$dipy[$icount],$dipz[$icount])=split(/\s+/,$_,4);
                  last
                 }
              }
             printf{OUT} ("   x:%10.6f  y:%10.6f  z:%10.6f\n\n",$dipx[$icount],$dipy[$icount],$dipz[$icount]);

         } # end of: if (! $displ[$icount]==0 )
       $icount++;	
      } # end of: for ($i2=1; $i2<=2; $i2++)
    } # end of: for ($i1=1; $i1<=$nintc; $i1++)
#
  } # end of: sub dipmomread
#
#==========================================================
#==========================================================
#
#   read displacement point informations
#==========================================================
  sub disppointread{
  my ($grad,$gradref,$name,$type)=@_;
  my ($icount,$i1,$i2,$title,$j,$i,$factor);
#
   print{OUT} "\n\n";
   print{OUT} "  ***   Displacement point informations:   ***\n";
   print{OUT} "  ============================================\n\n";
#
   $icount=1;
     for ($i1=1; $i1<=$nintc; $i1++) # $i1: loop over int.coordinates to which the displ. is applied 
      { 
         for ($i2=1; $i2<=2; $i2++) # $i2: loop over +/-displacement
          { 
	     if (( $flag[$icount] ne "skip")&&( $flag[$icount] ne "fixc"))
	      {
	       print{OUT}  "    Int.coord: $coor[$icount]  Displacement: $displ[$icount]\n";
	        if ($mcgrad eq 1 && $refcalc eq "yes")
		 { print{OUT}  "    E(MCSCF): $energy[$icount]     E-Eref(MCSCF) = $del_e[$icount]\n";}
	        if ($cigrad eq 1 && $refcalc eq "yes")
	         {print{OUT}  "     E(CI): $energy[$icount]     E-Eref(CI) = $del_e[$icount]\n";}
	        if ($nadcoupl eq 1 && $refcalc eq "yes")
	         {
		  if ($type ==1 || $type ==3)
		   {print{OUT}  " DRT$naddrt1 STATE$nadstate1    E(CI): $energy1[$icount]     E-Eref(CI) = $del_e1[$icount]\n";}
		  if ($type ==2 || $type ==3)
		{print{OUT}  " DRT$naddrt2 STATE$nadstate2    E(CI): $energy2[$icount]     E-Eref(CI) = $del_e2[$icount]\n";}
		 }
	       $dir="$JDIR/DISPLACEMENT/CALC.c$coor[$icount].d$displ[$icount]";
	       &convtest($dir); 
               &readintgrad("$dir","$name");
#
               $factor=1.0;
               $_="$dir/GRADIENTS/intgrd$name.sp";
               if (/nad/)
                {
                 $order=1;$phase=1;
                 open (NIN,"$dir/GRADIENTS/ciphase.sp") or die"Failed to open file:$dir/GRADIENTS/ciphase.sp\n";
                 $_=<NIN>;chomp;s/^ *//;($order)=split(/\s+/,$_,2);
                 $_=<NIN>;chomp;s/^ *//;($phase)=split(/\s+/,$_,2);
                 close NIN;
                 $factor=$phase; 
                  if ($factor == -1) 
		   {print{OUT}"   Changing phase for the non-adiabatic coupling term, new gradient values:\n";}
                }
#   $i1 - number of the point
#   $j  - counter over components of grad cased by the $i displacement 
#   $i2 - +/- component
                  for($j=1; $j<=$nintc; $j++)
                   {
                    $coord[$i1][$j][$i2]=$coordtmp[$j];
                    $$grad[$i1][$j][$i2] =$factor*$gradtmp[$j];
		    if ($flag[2*$j] eq "fixc") {$$grad[$i1][$j][$i2]=0.0;}
                    if ($factor == -1) {printf{OUT} ("%13.6f  %13.6f\n",$coord[$i1][$j][$i2],$$grad[$i1][$j][$i2]);}
                   }# end of: for($j=1; $j<=$nintc; $j++)
                 print{OUT}  "\n  ------------------------------------------------\n";
              }
#
             if ($flag[$icount] eq "skip")
              {
	       print{OUT}  " NOTE:  Using reference point data for this point !\n\n";
               print{OUT}  "    Int.coord: $coor[$icount]  Displacement: 0\n";
               print{OUT}  "    Energy: $eref[1]\n";
               print{OUT}  "      int.coor.      gradient    source: reference point data see above\n";
                for($j=1; $j<=$nintc; $j++)
                 {
                  $coord[$i1][$j][$i2]=$coordref[$j];
                  $$grad[$i1][$j][$i2] =$$gradref[$j];
		  if ($fixc{$coordref[$j]} eq "fixc") {$$grad[$i1][$j][$i2]=0.0}
                   printf{OUT} ("%13.6f  % 13.6f\n",$coord[$i1][$j][$i2],$$grad[$i1][$j][$i2]);
                 }# end of: if ($displ[$icount]==0 )
               print{OUT}  "\n  ------------------------------------------------\n"; 
              } # end of: if ($flags[$icount] eq "skip")
#
           $icount++;
          } # end of: for ($i2=1; $i2<=2; $i2++)
      } # end of: for ($i1=1; $i1<=$nintc; $i1++)
   
#
#   in calse of symmetry coordinate get the gradient value from the symmetry related component

  } # end of: gradread
#
#==========================================================
#
#   force constant calculation
#==========================================================
   sub forceconscalc{
   my ($i,$j,$delta);
   my ($grad,$fgrad,$title,$factor)=@_;
#
#   $coord[$i1][$j][$i2],$grad[$i1][$j][$i2])
#   $i1 - displacement in coordinate $i
#   $j  - counter over components of grad cased by the $i displacement
#   $i2 - +/- component
# 
     for ($i=1; $i<=$nintc; $i++) #  displacement in coordinate $i
       {
#hl        if (! defined ($fixc{$i} )){$delta=$coord[$i][$i][2]-$coord[$i][$i][1];}
#hl change in computation of displacement because of inconsistencies in torsional
#hl coordinates
#hl displacements delta are computed directly from displ array
        if (! defined ($fixc{$i} ))
          {if ($flag[2*$i] eq "skip"){$delta=-$displ[2*$i-1];}
           else {$delta=$displ[2*$i]-$displ[2*$i-1];}
          }
        for ($j=1; $j<=$nintc; $j++)
          {  
           if (! defined ($fixc{$i} ))
            {
             $$fgrad[$i][$j]=($$grad[$i][$j][2]-$$grad[$i][$j][1])/$delta;
               if ($debug > 0)
                {
                 print{OUT} "----------------\n";
                 print{OUT}"i,coord[$i][$i][1],coord[$i][$i][2]\n";
                 print{OUT}"$i,$coord[$i][$i][1],$coord[$i][$i][2]\n";
                 printf{OUT}"i,j,delta,grad[$i][$j][1],grad[$i][$j][2],fgrad[$i][$j]\n";
                 printf{OUT}("%2.0f %2.0f %10.6f  %10.6f %10.6f %10.6f\n",$i,$j,$delta,$$grad[$i][$j][1],$$grad[$i][$j][2],$$fgrad[$i][$j]);
                } # end of: if ($debug > 0)
            }
           elsif ($fixc{$i} eq "fixc")
            {
             $$fgrad[$i][$j]=0;
            } # end of: for ($j=1; $j<=$nintc; $j++)
       } # end of: for ($j=1; $j<=$nintc; $j++)
     } # end of: for ($i=1; $i<=$nintc; $i++)
#
   print{OUT} "\n\n ===========================================================\n\n";
   print{OUT} "   ***   OUTPUT   ***\n\n";
   print{OUT} "  $title\n";
   print{OUT} "\n Force constant matrix:\n";
     for ( $i=1; $i<= $nintc; $i++){
       for ( $j=1; $j<= $nintc; $j++){
       printf{OUT}( "%10.6f ", $$fgrad[$i][$j]);
       } # end of: for ( $j=1; $j<= $nintc; $j++)
     print{OUT} "\n";
     } # end of: for ( $i=1; $i<= $nintc; $i++)
#
#  #  check if force constant matrix is symmetry and symmetrize it
#
     if ($factor == 0) {return};
     $symtol=0.001;
     for ( $i=1; $i<= $nintc; $i++)
      {
         for ( $j=1; $j<= $i; $j++)
          {
           $delta=$$fgrad[$i][$j]-$factor*$$fgrad[$j][$i];
            if (abs($delta) > $symtol)
             {
              printf{OUT}( "  warning: i=%2.0f j=%2.0f:  f'(ij)-f'(ji)=%11.7f \n",$i,$j,$delta);
             }
           $$fgrad[$i][$j] = ($$fgrad[$i][$j]+$factor*$$fgrad[$j][$i])/2;
           $$fgrad[$j][$i] = $factor*$$fgrad[$i][$j];
          } # end of: for ( $j=1; $j<= $nintc; $j++)
     } # end of: for ( $i=1; $i<= $nintc; $i++) 
#
#  print out symmetrized force constant matrix:
   if ($factor==1) { print{OUT} "\n Symmetrized force constant matrix:\n";}
   if ($factor==-1) { print{OUT} "\n Anti-symmetrized force constant matrix:\n";}
     for ( $i=1; $i<= $nintc; $i++)
      {
         for ( $j=1; $j<= $i; $j++)
          {
           printf{OUT}( "%10.6f", $$fgrad[$i][$j]);
          } # end of: for ( $j=1; $j<= $nintc; $j++)
       print{OUT} "\n";
      } # end of: for ( $i=1; $i<= $nintc; $i++)
# 
#
   } # end of: sub forceconscalc
#
#==========================================================
#
#   write out the hessian matrix and set zero diag.el. to 1
#==========================================================
#
   sub hessianout
{
   my ($fgrad,$file,$factor)=@_;
#
   print{OUT} "\n   Writing out hessian file:$file\n";
#  set diagonal element equal 0 to 1
     for ( $i=1; $i<= $nintc; $i++)
      {  
         if ($$fgrad[$i][$i] == 0 && $factor==1)
          {
           print" WARNING: diagonal element fconst[$i][$i] is zero, setting it to 1\n";
           print{OUT}" WARNING: diagonal element fconst[$i][$i] is zero, setting it to 1\n";
           $$fgrad[$i][$i] = 1;
          } # end of: if ($$fgrad[$i][$i] eq 0)
      } # end of: for ( $i=1; $i<= $nintc; $i++) 
#
#
   open(HESS,">$file");
    $ncol=8; # defined the number of columns for the force constat matrix
       for ( $i=1; $i<= $nintc; $i++){
          $icount =0;
          for ( $j=1; $j<= $nintc; $j=$j+$ncol)
           {
            $jstart = $j;
            $jstop = $j+$ncol-1;
            if ($jstop > $nintc) {$jstop = $nintc}
              for ($j1=$jstart; $j1<=$jstop; $j1++)
               {
                printf{HESS}( "%13.6f", $$fgrad[$i][$j1]);
               } # end of: for ($j1=$j; $j1<=$j+$nact-1; $j1++)
           $icount++;
           print{HESS} "\n";
           } # end of: for ( $j=1; $j<= $nintc; $j=$j+$ncol) 
       } # end of: for ( $i=1; $i<= $nintc; $i++)
#
   close HESS;
#
} # end of: sub hessianout
#
#==========================================================
#
#   write out the hessian matrix and set zero diag.el. to 1
#==========================================================
#
   sub inp4molden
{
#
   print{OUT} "\n   Writtig out file: MOLDEN/molden.freq\n\n";
#
   open(FILE1,"LISTINGS/suscalls") or die "cannot open LISTINGS/suscalls !!!\n";
   if (! -s "MOLDEN"){mkdir("MOLDEN",0755); }
   open(FILE2,">MOLDEN/molden.freq") or die "cannot open MOLDEN/molden.freq !!!\n";
   
     $write="no";
     while (<FILE1>)
      {
       if (/start of molden input/){$write="yes"}
       if ($write eq "yes"){print {FILE2} "$_"}
       } # end of: for ( $i=1; $i<= $nintc; $i++)
#
   close FILE1; close FILE2;
#
} # end of: sub hessianout 
#
#==========================================================
#
#   creation of suscin and suscal.x calculations
#==========================================================
# 
  sub suscal{
     local ($icount,$ncol);
     ($fgrad)=@_;
#
     open(SUSCALIN, ">suscin"); 
     print {SUSCALIN} "gf        KRAFTFELD TEST RECHNUNG \n";
     printf {SUSCALIN} ("%-10s%10.5f  \n","dim",$nintc);
  
     open(GEOM, "$JDIR/geom") or die "\n\n  File $JDIR/geom not found!!!\n\n";
     print{OUT} "  Taking reference geometry from file $JDIR/geom: \n";
     $i=1;
       while (<GEOM>) {
       s/^ *//;chomp;
       ($label[$i],$charge[$i],$x[$i],$y[$i],$z[$i],$mass[$i])=split(/\s+/,$_,6);
       printf{OUT}("%3s%10.5f%10.5f%10.5f%5.2f\n",$label[$i],$x[$i],$y[$i],$z[$i], $charge[$i]);
       $i++;
       }   
     close(GEOM);
     $natom=@x-1;
#
       if ($dipmom eq "yes"){ 
        printf{SUSCALIN}("%-10s%-10s%10.6f%10.6f%10.6f%10.6f%10.6f\n",'dipm','calc',$ncalc,0,0,0,$natom);
        printf{SUSCALIN}("%40s%10.6f%10.6f%10.6f\n",' ',$dipx[0],$dipy[0],$dipz[0]);    
        $icount=1;
         for ($i=1; $i<=2*$nintc; $i++){
           if (! $displ[$icount]==0 ){  
            printf{SUSCALIN}("%4.0f%6.3f%30s%10.6f%10.6f%10.6f\n",$coor[$i],$displ[$i],' ',$dipx[$i],$dipy[$i],$dipz[$i]);
           } # end of: if (! $displ[$icount]==0 )
         $icount++;
         } # end of: for ($i=1; $i<=2*$nintc; $i++)
       } # end of: if ($dipmom eq "yes")
#
     printf {SUSCALIN}("%-10s%10.5f%10.5f%10.5f\n","bmat",$natom,0,1); 
       for ($i=1; $i<=$natom; $i++){
       printf {SUSCALIN} ("%4s%10.6f%10.6f%10.6f%10.6f\n",$label[$i],$x[$i],$y[$i],$z[$i],$charge[$i]);
       } # end of: for ($i=1; $i<=$natom; $i++)
#
     @coortype=qw(stre bend out tors lin1 lin2 tor2);
     open(INTC,"intcfl") or die " File: intcfl missing";
     <INTC>;    
     $i=1;
     $icount=0;
       while (<INTC>) {
       @field=split('');
       $string=join'',@field[20..23];
       $string=~tr/A-Z/a-z/;
       $string=~s/\W.*//;
       $print="no";
         for ($i=0; $i<=6;$i++){
           if ($string eq $coortype[$i]){$print="yes";}
         } # end of: for ($i=0; $i<=6;$i++)
       $line=$_;
       $line=~tr/A-Z/a-z/;
         if ($print eq "yes"){print {SUSCALIN} "$line";}
       } # end of: while (<INTC>)    
     print {SUSCALIN} "gmat      calc\n";
#
#  print out force constant matrix
#
     print{SUSCALIN} "fmat\n";

     $ncol=8; # defined the number of columns for the force constat matrix
       for ( $i=1; $i<= $nintc; $i++){
	  $icount =0;
          for ( $j=1; $j<= $i; $j=$j+$ncol)
	   {
	    $nact=$ncol;
	    if ($nact > $i-$icount*$ncol) {$nact=$i-$icount*$ncol;}
	      for ($j1=$j; $j1<=$j+$nact-1; $j1++)
	       {
                printf{SUSCALIN}( "%10.5f", $$fgrad[$i][$j1]);
	       } # end of: for ($j1=$j; $j1<=$j+$nact-1; $j1++)
	   $icount++;
           print{SUSCALIN} "\n";
           } # end of: for ( $j=1; $j<= $i; $j=$j+$ncol)
       } # end of: for ( $i=1; $i<= $nintc; $i++)
     print{SUSCALIN} "stop\n";
     close(SUSCALIN);
#
#   perform suscal calculation
#
     &callprog2("suscal.x");
     cp ("suscalls","LISTINGS/suscalls");
     print{OUT} "\n suscal.x executed \n\n";
  } # end of: sub suscal  
#
#======================================================================
# 
#   convergence test
#======================================================================
#
  sub convtest
   {
    local ($niter, $nconv);
 
    ($dir)=@_;
    $mcscfinfo=0;
    $ciinfo=0;
     open(FILE, "$dir/control.run");
       while (<FILE>)
        {
         $string =$_;
	 chop($string);
	 if ( $string eq "mcscf") {$mcscfinfo=1}
# if ( $string eq "ciudg") {$ciinfo=1}
	 if ( /ciudg/) {$ciinfo=1}
        } # end of: while (<FILE>)
    close FILE;
#
#  MCSCF
#   $nconv=0;
    if ($mcscfinfo == 1)
     {
      ($nconv,$niter)=&mcscfconv("$dir/LISTINGS/mcscfsm.all");
      if ($nconv) 
       {print {OUT} "          --> MCSCF calculation NOT converged\n"; }
      else 
       { print {OUT} "          --> MCSCF calculation converged\n"; }  
      print {OUT} "              --> Number of MCSCF iterations:  $niter\n";
     }  
#
#  CI
    $nconv=0;
    if ($ciinfo == 1)
     {
      if ($nadcoupl==1) { ($nconv,$niter)=&ciconv("$dir/LISTINGS/ciudgsm.drt$naddrt1.all");}
      else {($nconv,$niter)=&ciconv("$dir/LISTINGS/ciudgsm.all");}
      if ($nconv) 
       { print {OUT} "          --> CI calculation NOT converged\n"; }
      else 
       {print {OUT} "          --> CI calculation converged\n"; }  
      print {OUT} "              --> Number of CI iterations:  $niter\n";
     }  

    

   } # end of: sub convtest
#
#======================================================================
# 
#   determine the NAD calculation type
#======================================================================
#
  sub nadtype
{
  open (INPFL,"$JDIR/transmomin") or die "Could not open file: $JDIR/transmomin!\n";
  $/="\n";
  <INPFL>; $_=<INPFL>; chomp;s/^ *//;
  ($naddrt1,$nadstate1,$naddrt2,$nadstate2) = split(/\s+/,$_,4);
  close INPFL;
  $nadcalc=0;
  $nadcalc=keyinfile("$JDIR/cigrdin","nadcalc");
  $nadtype[1]="DCI";$nadtype[2]="DCSF";$nadtype[3]="DCI+DCSF";
    if ($nadcalc==0)
     {
      print "\n\n   ERROR:\n";
      print " According to control.run file you want to perform:\n";
      print " Nonadiabatic couling term calculation!!!\n";
      print " input error in file: cigrdin, flag nadcalc not set!\n";
      print "        ... reconsider your input...\n\n\n";
      die   "nadtype: input error!\n";
     }
#
  print {OUT} "\n  Nonadiabatic couling term calculation type: $nadtype[$nadcalc]\n";
  print {OUT} "  States involved in calcualtion: DRT$naddrt1,state$nadstate1; DRT$naddrt2,state$nadstate2\n\n";
#
}
  
