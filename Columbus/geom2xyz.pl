#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#
#   program to converts the COLUMBUS geom file (given as input argument)
#    to molden format and start the MOLDEN program on the new geom file
#
    use Shell qw(pwd cd date tail grep | ); 
#
  $factor=0.529177;
  $file=$ARGV[0];
   if ($file eq "") {die "\n ***> Please specify geommetry file as an argument \n\n";}
   open (FILE, $file) or die "Failed to open file: $file\n";
   $o=0;
   while (<FILE>)
    {
    $i++;
     chomp; s/^ *//; 
     ($symbol[$i],$charge[$i],$x[$i],$y[$i],$z[$i])=split(/\s+/,$_,5); 
    $natom=$i; 
    }

  open (OUT,">$file.xyz")or die" Failed to open file: $file.xyz\n";  
   print {OUT}" $natom\n\n";
  for ($i=1; $i<=$natom; $i++)
   {
    $x[$i]=$x[$i]*$factor;
    $y[$i]=$y[$i]*$factor;
    $z[$i]=$z[$i]*$factor;
    printf {OUT}("%7s  %15.6f  %15.6f   %15.6f\n", $symbol[$i],$x[$i],$y[$i],$z[$i]);
   }
  close OUT;
