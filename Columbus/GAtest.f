!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program GAtest

      implicit none
      integer*4 ierr
      logical status,ga_uses_ma, ma_init
      integer ma_sizeof
      integer mxarray,nbuffer
      parameter (mxarray=9,nbuffer=2048)
      character*8 ganame(mxarray)
      logical gainit(mxarray)
      integer  gasize(mxarray,2),gahdle(mxarray)
      real*8 gainitv(mxarray)
      integer i,j,global,heap,stack
      integer MT_BYTE,MT_INT,MT_LOG,MT_REAL,MT_DBL,MT_SCPL,MT_DCPL
      parameter (MT_BYTE = 1009 )
      parameter (MT_INT = 1010 )
      parameter (MT_LOG = 1011 )
      parameter (MT_REAL = 1012)
      parameter (MT_DBL =  1013 )
      parameter (MT_SCPL = 1014 )
      parameter (MT_DCPL = 1015 )
      character*10 outfile
      integer me,ga_nodeid,ilo,ihi,jlo,jhi
      real*8 buffer(nbuffer)

      call mpi_init(ierr)
      if (ierr.ne.0) stop 'could not initialize mpi'
      call ga_initialize()

      me=ga_nodeid()
c     if (me.gt.0) then
         write(outfile,'(a9,i1)') 'OUTPUT_PC',me
         open(unit=6,file=outfile,form='formatted')
c     endif
      
      heap=1000
      stack=1000

       ganame(1)='DGINT    '
       ganame(2)='OFDGINT  '
       ganame(3)='FIL4X    '
       ganame(4)='FIL4W    '
       ganame(5)='FIL3X    '
       ganame(6)='FIL3W    '
       ganame(7)='CI       '
       ganame(8)='SIGMA    '
       ganame(9)='SRTSCR   '
       gainit(1)= .false.
       gainit(2)= .false.
       gainit(3)= .true. 
       gainit(4)= .true. 
       gainit(5)= .true. 
       gainit(6)= .true. 
       gainit(7)= .true. 
       gainit(8)= .true.
       gainit(9)= .true.
       gasize(1,1)= 177712/8
       gasize(1,2)= 1
       gasize(2,1)= 5190416/8
       gasize(2,2)= 1
       gasize(3,1)= 85490240/8
       gasize(3,2)= 1
       gasize(4,1)= 86801440/8
       gasize(4,2)= 1
       gasize(5,1)= 35402400/8
       gasize(5,2)= 1
       gasize(6,1)= 3540240/8
       gasize(6,2)= 1
       gasize(7,1)= 7099689
       gasize(7,2)= 6
       gasize(8,1)= 7099689
       gasize(8,2)= 6
       gasize(9,1)= 20872579
       gasize(9,2)= 1
       gainitv(1)=1.0d0
       gainitv(2)=1.0d0
       gainitv(3)=1.0d0
       gainitv(4)=1.0d0
       gainitv(5)=1.0d0
       gainitv(6)=1.0d0
       gainitv(7)=0.0d0
       gainitv(8)=0.0d0
       gainitv(9)=1.0d0

       global=1000
       do i=1,9 
        if (gainit(i)) then
           global=global+gasize(i,1)*gasize(i,2)
        endif
      enddo


      if (ga_uses_ma()) then
            status = ma_init(MT_DBL, stack, heap+global)
        else
            status = ma_init(MT_DBL,stack,heap)
            call ga_set_memory_limit(ma_sizeof(MT_DBL,global,MT_BYTE))
        endif
      if(.not. status) stop 'setting mmory limit'

       write(6,100) heap,stack,global
 100   format(' Memory limits:'/' heap ',t15,i10,/,' stack ',t15,i10,/,
     .   ' global ', t15,i10)



       do i=1,9 
         if (gainit(i)) 
     .   call createga(ganame(i),gasize(i,1),gasize(i,2),
     .                 gainitv(i),gahdle(i))
       enddo

      do i=1,9
        if (gainit(i)) call ga_print_distribution(gahdle(i))
      enddo

      if (me.eq.0) then 
c     call ga_distribution(gahdle(8),1,ilo,ihi,jlo,jhi)
c     do i=1,1 
        buffer(1:nbuffer)=10.0d0
c       do j=ilo,ihi,nbuffer
c      call ga_acc(gahdle(8),j,min(j+nbuffer,ihi),i,i,buffer,nbuffer,1.0d0)
c        write(6,*) 'accumulating 15.0d0 to ',j,min(j+nbuffer,ihi)
c       enddo         
       do j=1,gasize(8,1),nbuffer
        call ga_acc(gahdle(8),j,min(j+nbuffer,gasize(8,1)),
     .              1,1,buffer,nbuffer,1.0d0)
         write(6,*) 'accumulating 15.0d0 to ',
     .                 j,min(j+nbuffer,gasize(8,1))
       enddo
c     enddo
      else
        buffer(1:nbuffer)=10.0d0
       do j=gasize(8,1),1,-nbuffer
        call ga_acc(gahdle(8),max(1,j-nbuffer+1),j,
     .              1,1,buffer,nbuffer,1.0d0)
         write(6,*) 'accumulating 15.0d0 to ',
     .                 max(1,j-nbuffer+1),j
       enddo
      endif 
      call ga_sync()
c     call ga_print(gahdle(8))
      call ga_sync()
      do i=1,gasize(8,2)
        call checkgadbl(gahdle(8),nbuffer,i)
      enddo 
      do i=1,gasize(8,2)
        call checkga(gahdle(8),buffer,nbuffer,i)
      enddo 

      if (me.gt.0) close(6)
      call ga_terminate()
      call mpi_finalize(ierr)
      stop 9999
      end


      subroutine createga(name,dim1,dim2,initval,hdle)
      implicit none
      character*(*) name
      integer dim1,dim2,hdle
      real*8  initval
      logical ga_create,status
      integer mt_dbl
      parameter (mt_dbl=1013)

      external ga_create


      status=ga_create(MT_DBL,dim1,dim2,name,1,dim2,hdle)

      if (.not. status) stop 'could not create ga'

      call ga_fill(hdle,initval)
      write(6,'(a,a,2i10,a,f8.4)') 
     .     'created ',name,dim1,dim2,' initval=',initval
      return
      end
 
      subroutine checkgadbl(hdle,nblock,nline)
      implicit none
      real*8 dbl_mb,dasum,p1,ptot
      integer hdle,nblock,nline
      integer ilo,ihi,jlo,jhi,adr,ld
      integer i,j, ga_nodeid,me
      integer*8 ii
      COMMON /mbc_dbl/dbl_mb(2)
       

      me=ga_nodeid()
      call ga_distribution(hdle,me,ilo,ihi,jlo,jhi)
      call ga_access(hdle,ilo,ihi,jlo,jhi,adr,ld)
      write(6,'(a,i6,a,i6,a,i10)') 
     .'CHECKING GADBL ',hdle, 'jrange=',nline, ' resolution',nblock
      ptot=0
      do i=ilo,ihi,nblock
       ii = min(nblock,ihi-i+1)
       p1= dasum(ii,dbl_mb(adr+i-ilo+ld*(nline-1)),1)
       write(6,'(a,2i10,2x,e24.12)') 
     .    'partial contribution:',i,min(i+nblock-1,ihi),p1
       ptot=ptot+p1
      enddo

       write(6,'(a,2x,e24.12)') 'total contribution=',ptot
       return
       end
      
      subroutine checkga(hdle,buffer,nblock,nline)
      implicit none
      integer nblock,nline,hdle,ii
      real*8 dbl_mb,dasum,p1,ptot,buffer(nblock)
      integer ilo,ihi,jlo,jhi,adr,ld
      integer i,j, ga_nodeid,me
      me=ga_nodeid()
      call ga_distribution(hdle,me,ilo,ihi,jlo,jhi)
      write(6,'(a,i6,a,i6,a,i10)') 
     .'CHECKING GA ',hdle, 'jrange=',nline, ' resolution',nblock
      ptot=0.0d0
      do i=ilo,ihi,nblock
       call ga_get(hdle,i,min(i+nblock-1,ihi),
     .            nline,nline,buffer,nblock)
       ii = min(nblock,ihi-i+1)
       p1= dasum(ii,buffer,1)
       write(6,'(a,2i10,2x,e24.12)') 
     .    'partial contribution:',i,min(i+nblock-1,ihi),p1
       ptot=ptot+p1
      enddo

       write(6,'(a,2x,e24.12)') 'total contribution=',ptot
       return
       end
