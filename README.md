COLUMBUS
========

The COLUMBUS Program System is a collection of programs for high-level ab initio molecular electronic structure calculations.
The programs are designed primarily for extended multi-reference (MR) calculations on electronic ground and excited states of atoms and molecules.
A variety of methods, including

* MCSCF (multiconfiguration self-consistent field)

* MR-CISD (multireference configuration interaction with all single and double excitations),

* MR-ACPF (multireference averaged coupled-pair-functional) and

* MR-AQCC (multi-reference average quadratic coupled-cluster)

are available.
An important feature of COLUMBUS is its flexibility.
In addition to standard classes of reference wave functions, such as CAS or RAS, calculations can be performed with selected reference configurations.
Though the multi-reference aspect of COLUMBUS is emphasized, single-reference calculations can also be carried out very efficiently.

Literature
----------

Scientific publications arising from the use of the COLUMBUS program must be referenced in appropriate form, e.g.:

* H. Lischka, T. Müller, P. G. Szalay, I. Shavitt, R. M. Pitzer, R. Shepard, [WIREs Comput. Mol. Sci. 2011, 1, 191–199](https://dx.doi.org/10.1002/wcms.25).

* H. Lischka, R. Shepard, T. Müller, P. G. Szalay, R. M. Pitzer, A. J. A. Aquino, M. M. Araújo do Nascimento, M. Barbatti, L. T. Belcher, J.-P. Blaudeau, et al., [J. Chem. Phys. 2020, 152, 134110](https://dx.doi.org/10.1063/1.5144267).

* H. Lischka,
R. Shepard,
I. Shavitt,
R. M. Pitzer,
Th. Müller,
P. G. Szalay,
S. R. Brozell,
G. Kedziora,
E. A. Stahlberg,
R. J. Harrison,
J. Nieplocha,
M. Minkoff,
M. Barbatti,
M. Schuurmann, Y. Guan, D. R. Yarkony,
S. Matsika,
F. Plasser,
E. V. Beck, J.-P. Blaudeau,
M. Ruckenbauer, B. Sellner, J.J. Szymczak,
R. F. K. Spada,
A. Das,
L. T. Belcher,
R. Nieman.
COLUMBUS, an ab initio electronic structure program, release 7.2 (2022)

Documentation and User Support
------------------------------

The [COLUMBUS web page](https://columbus-program-system.gitlab.io/columbus) and detailed [documentation](https://columbus-program-system.gitlab.io/columbus/doc.html) are available online.

In case of questions or problems please contact the [COLUMBUS mailing list](https://lists.osu.edu/mailman/listinfo/columbus) or use gitlab Issues.
However, there is no official support to COLUMBUS as it is not a commercial product.

Code distribution
-----------------

COLUMBUS is distributed open-source under the LGPL.
See `LICENSE` for licensing details and `CONTRIBUTORS.md` for a full list of authors.

You can download COLUMBUS as binaries or source code.

### Binary distribution

This is the suggested mode for most COLUMBUS users.
Download pre-compiled binaries from [github releases](https://gitlab.com/columbus-program-system/columbus/-/releases/).
These can be run directly on most Linux systems.

### Source code

If you wish to obtain the source code for compiling the code yourself,
you can download release versions from [github releases](https://gitlab.com/columbus-program-system/columbus/-/releases/).

To obtain the newest development version and/or develop code yourself, please use GIT

```
git clone https://gitlab.com/columbus-program-system/columbus.git
```

If you want to get OpenMolcas as well, run

```
git clone --recursive https://gitlab.com/columbus-program-system/columbus.git
```

Installation
------------

First, install the interface tools

```
./install.automatic cpan
```

Per default this activates a precompiled perl executable `colperl` containing all required packages.

In case of the binary distribution this is all that needs to be done.
If you are compiling COLUMBUS from source, please see the detailed [installation instructions](https://columbus-program-system.gitlab.io/columbus/installation.html).

Running Columbus
----------------

Before running COLUMBUS, set the COLUMBUS variable

```
export COLUMBUS=<path>/Columbus
```

Input generation proceeds via the `colinp` program

```
$COLUMBUS/colinp
```

Then, call the main driver script

```
$COLUMBUS/runc -m 2000 > runls
```

Contributing
------------

Columbus is free open-source software and you can download, modify and distribute it
according to the terms of the LGPL.
Contributions to the main repository are very welcome. To do so, please follow the instructions
in `CONTRIBUTING.md` and/or
contact one of the developers.
